<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/sba_process.php';

// Change Data Section

if(isset($_POST['go_activeyear']))
{
    if(isset($_POST['year']))
    {$yrch = $_POST['year'];} else {$yrch = '';}
    $yr = "SELECT year from year WHERE isactive='1'";
    $yrq=mysqli_fetch_array(mysqli_query($con,$yr));
    $year=$yrq['year'];
    if($yrch != '') 
    {
        $query = "UPDATE year SET isactive = '1' WHERE year = '$yrch'";
        mysqli_query($con,$query);
        $query = "UPDATE year SET isactive = '0' WHERE year = '$year'";
        mysqli_query($con,$query);
    }
    header("location:/admin.php?alert=success");
}
else if(isset($_POST['go_predictions']))
{
    if(isset($_POST['maintenance']))
    {$maint = '1';} else {$maint = '0';}
    $query = "UPDATE forms SET isactive = '$maint' WHERE form = 'maintenance'";
    mysqli_query($con,$query);

    if(isset($_POST['rollover']))
    {$roll = '1';} else {$roll = '0';}
    $query = "UPDATE forms SET isactive = '$roll' WHERE form = 'rollover'";
    mysqli_query($con,$query);

    if(isset($_POST['sbapred']))
    {$sbap = '1';} else {$sbap = '0';}
    $query = "UPDATE forms SET isactive = '$sbap' WHERE form = 'predictions'";
    mysqli_query($con,$query);

    if(isset($_POST['sbdlpred']))
    {$ncaap = '1';} else {$ncaap = '0';}
    $query = "UPDATE forms SET isactive = '$ncaap' WHERE form = 'sbdl_predictions'";
    mysqli_query($con,$query);

    if(isset($_POST['bigthree']))
    {$bigthreep = '1';} else {$bigthreep = '0';}
    $query = "UPDATE forms SET isactive = '$bigthreep' WHERE form = 'big_3'";
    mysqli_query($con,$query);
    header("location:/admin.php?alert=success");

    if(isset($_POST['fivebyfive']))
    {$fives = '1';} else {$fives = '0';}
    $query = "UPDATE forms SET isactive = '$fives' WHERE form = '5x5'";
    mysqli_query($con,$query);
    header("location:/admin.php?alert=success");
}
else if(isset($_POST['go_giveaway']))
{
    $gtask = sanitize($con,$_POST['giveaway_task']);
    $gleague = sanitize($con,$_POST['giveaway_league']);
    $gpe = sanitize($con,$_POST['giveaway_pe']);
    $gbegdate = sanitize($con,$_POST['giveaway_beg_date']);
    $genddate = sanitize($con,$_POST['giveaway_end_date']);
    $sql = "INSERT INTO giveaways (task,league,beg_date,end_date,pe) VALUES ('$gtask','$gleague','$gbegdate','$genddate','$gpe')";
    mysqli_query($con,$sql);
    header("location:/admin.php?alert=success");
}
else if(isset($_POST['go_updatescale']))
{
    global $hostlg;
    $count = count($_POST['row']);
    $arr = isset($_POST['row']) ? $_POST['row'] : '';
    for($x=0;$x<$count;$x++)
    {
        $y = $x+1;
        $range = sanitize($con,$arr[$x]['range']);
        $pe = sanitize($con,$arr[$x]['pe']);
        $sql = "SELECT id FROM update_scale WHERE `range` = \"$range\" AND pe = \"$pe\" AND league = \"$hostlg\"";
        $check = mysqli_num_rows(mysqli_query($con,$sql));
        if($check == 0)
        {
            $sql = "INSERT INTO update_scale (`range`,pe,league) VALUES (\"$range\",\"$pe\",\"$hostlg\")";
            mysqli_query($con,$sql);
        }
    } 
    header("location:/admin.php?alert=success");
}
else if(isset($_POST['go_fibapayout']))
{
    $country = sanitize($con,$_POST['fiba_country']);
    $payout = sanitize($con,$_POST['fiba_payout']);
    $system = new System();
    $year = $system->get_year();
    $sql = "SELECT id FROM players WHERE country='$country' AND (active = '1' || active = '3') AND league='SBA'";
    $result = mysqli_query($con,$sql);
    while($r = mysqli_fetch_array($result))
    {
        $pid = $r['id'];
        $player = new Player($pid);
        $player->set_cash($payout);
        $player->add_earn_history($payout,'S'.$year.' FIBA Payout');
    }
    header("location:/admin.php?alert=success");
}
else if(isset($_POST['go_deleterange']))
{
    $rid = sanitize($con,$_POST['rid']);
    $sql = "DELETE FROM update_scale WHERE id=\"$rid\"";
    mysqli_query($con,$sql);
    header("location:/admin.php?alert=success");
}
else
{
    header("location:/admin.php?alert=success");   
}
?>