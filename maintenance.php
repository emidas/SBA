<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/index_head.php';
include $path.'/includes/index_process.php';
if(isset($_GET['alert'])) 
{
    if ($_GET['alert'] == 'wrongpass') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Wrong Password.</strong></div>";
    }
    else if ($_GET['alert'] == 'wronguser') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Wrong Username.</strong></div>";
    }
}
else
{$alert = '';}
?>

<!-- Body Start -->
<body class="bg-info">
    <div class="container">
        <?=$alert?>
        <div class="row" style="margin-top:20px;">
            <div class="col-xs-3"></div>
            <form class="form-horizontal col-xs-6" action="/checklogin.php" method="POST">
                <div class="panel panel-primary">
                    <div class="panel-heading"><?=$hostlg?>O Maintenance</div>
                    <div class="panel-body">
                        <div class="form-group">
                            <?=LogoImagePrint($hostlg,'light')?>
                        </div>
                        <p><?=$hostlg?> is currently down for maintenance. Please check the forums for any relevant announcement, or contact an <?=$hostlg?> Admin on Discord (links to both below) if you think this is displaying by mistake. Thank you for your patience.
                    </div>
                    <div class="panel-footer">
                        <?=HotlinePrint($hostlg)?>
                    </div>
                </div>
            </form>
            <div class="col-xs-3"></div>
        </div>
    </div>
</body>
<!-- Body End -->

<?php
include $path.'/footer.php';
?>