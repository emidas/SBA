<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/index_head.php';
include $path.'/includes/index_process.php';
if(isset($_GET['alert'])) 
{
    if ($_GET['alert'] == 'wrongpass') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Wrong Password.</strong></div>";
    }
    else if ($_GET['alert'] == 'wronguser') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Wrong Username.</strong></div>";
    }
}
else
{$alert = '';}
?>

<!-- Body Start -->
<body class="bg-info">
    <div class="container">
        <?=$alert?>
        <div class="row" style="margin-top:20px;">
            <form class="form-horizontal col-xs-8 col-xs-offset-2" action="/checklogin.php" method="POST">
                <div class="panel panel-primary">
                    <div class="panel-heading">Login</div>
                    <div class="panel-body">
                        <div class="form-group">
                            <?=LogoImagePrint($hostlg,'light')?>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="user"><b>Username:</b></label>
                            <div class="col-sm-4">
                                <input type="text" placeholder="Enter Username" name="user" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="password"><b>Password:</b></label>
                            <div class="col-sm-4">
                                <input type="password" placeholder="Enter Password" name="password" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-4">
                                <div class="checkbox">
                                    <label><input type="checkbox" name="remember"><b>Remember Me</b></label>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-login btn-block btn-primary" id="login"><span class="glyphicon glyphicon-off"></span>Login</button>
                    </div>
                    <div class="panel-footer">
                        <?=HotlinePrint($hostlg)?>
                    </div>
                </div>
            </form>
        </div>
    </div>
</body>
<!-- Body End -->

<?php
include $path.'/footer.php';
?>