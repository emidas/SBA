<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
$user=$_SESSION['user'];

if(isset($_POST['year']))
{$year = $_POST['year'];} else {$year = '1';}
if(isset($_POST['league']))
{$league = $_POST['league'];} else {$league = 'sba';}

$yquery = "SELECT season FROM SBA_Years";
$yresult = mysqli_query($con,$yquery);
$yecho = '';
$echo = '';
while ($r = mysqli_fetch_array($yresult)) {
    if($year == $r['season']) {$echo = "selected";} else {$echo = "";}
    $yecho .= "<option value = '".$r['season']."' $echo>".$r['season']."</option>";
}
?>

<!-- Body Start -->
<body>
<div id="wrapper" style="position:relative;top:0;left:0;">
    <?php include $path.'/includes/nav.php'; ?>
    <div class="container">
        <?=HeaderImagePrint($hostlg,$_SESSION['style'])?>
        <form class="form-horizontal" method='POST'>
            <div class="panel-group">
                <div class="panel panel-primary">
                    <div class="panel-heading">League Standings Filters</div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="stat">Set Season:</label>
                            <div class="col-xs-2">
                                <select name="year" class="form-control"><?=$yecho?></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="league">Set League:</label>
                            <div class="col-xs-2">
                                <select name="league" class="form-control">
                                    <option value='sba' <?php if ($league == 'sba') echo "selected";?>>SBA</option>
                                    <option value='ncaa' <?php if ($league == 'ncaa') echo "selected";?>>NCAA</option>
                                    <option value='fiba' <?php if ($league == 'fiba') echo "selected";?>>FIBA</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-default">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <div class="row">
            <div class="col-xs-12">
                <?=printleaguestanding($con,$league,$year);?>
            </div>
        </div>
    </div>
</div>
<!-- Body End -->

<?php
include $path.'/footer.php';
?>
