<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
$user=$_SESSION['user'];

$system = new System();
$year = $system->get_year();

if(isset($_POST['year']))
{$year = $_POST['year'];} else {}
if(isset($_POST['league']))
{$league = $_POST['league'];} else {$league = 'sba';}

$yquery = "SELECT year FROM year";
$yresult = mysqli_query($con,$yquery);
$yecho = '';
$echo = '';
while ($r = mysqli_fetch_array($yresult)) {
    if($year == $r['year']) {$echo = "selected";} else {$echo = "";}
    $yecho .= "<option value = '".$r['year']."' $echo>".$r['year']."</option>";
}
?>

<!-- Body Start -->
<body>
<div id="wrapper" style="position:relative;top:0;left:0;">
    <?php include $path.'/includes/nav.php'; ?>
    <div class="container">
        <?=HeaderImagePrint($hostlg,$_SESSION['style'])?>
        <form class="form-horizontal" action='prediction_data.php' method='POST'>
            <div class="panel-group">
                <div class="panel panel-primary">
                    <div class="panel-heading">Prediction Data Filters</div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="year">Set Year:</label>
                            <div class="col-xs-2">
                                <select name="year" class="form-control"><?=$yecho?></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="league">Set League:</label>
                            <div class="col-xs-2">
                                <select name="league" class="form-control">
                                    <option value='sba' <?php if ($league == 'sba') echo "selected";?>>SBA</option>
                                    <option value='sbdl' <?php if ($league == 'sbdl') echo "selected";?>>SBDL</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-default">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <table class="table table-condensed">
            <tr>
                <td><?=printpred($con,'champion',$year,$league);?></td>
                <td><?=printpred($con,'runnerup',$year,$league);?></td>
                <td><?=printpred($con,'eoty',$year,$league);?></td>
                <td><?=printpred($con,'mvp',$year,$league);?></td>
                <td><?=printpred($con,'opoty',$year,$league);?></td>
            </tr>
            <tr>
                <td><?=printpred($con,'dpoty',$year,$league);?></td>
                <td><?=printpred($con,'smoty',$year,$league);?></td>
                <td><?=printpred($con,'pgoty',$year,$league);?></td>
                <td><?=printpred($con,'sgoty',$year,$league);?></td>
                <td><?=printpred($con,'sfoty',$year,$league);?></td>
            </tr>
            <tr>
                <td><?=printpred($con,'pfoty',$year,$league);?></td>
                <td><?=printpred($con,'coty',$year,$league);?></td>
                <td><?=printpred($con,'roty',$year,$league);?></td>
                <td><?=printpred($con,'mip',$year,$league);?></td>
            </tr>
        </table>
    </div>
</div>
<!-- Body End -->

<?php
include $path.'/footer.php';
?>
