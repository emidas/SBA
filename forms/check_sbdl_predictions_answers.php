<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
$user = new User($_SESSION['user']);
$username = $user->username;
$userid = $user->id;

$sql = "SELECT isactive FROM forms WHERE form = 'sbdl_predictions'";
$result = mysqli_fetch_array(mysqli_query($con,$sql));
$active = $result['isactive'];

if($active == 1)
{
    // Check if user already created an entry this year
    $yr = "SELECT year from year WHERE isactive='1'";
    $yrq=mysqli_fetch_array(mysqli_query($con,$yr));
    $year=$yrq['year'];
    $sql = "SELECT username FROM sbdl_predictions WHERE a_fk='$userid' AND year='$year'";
    $result=mysqli_query($con,$sql);
    $row = mysqli_fetch_array($result);
    $count=mysqli_num_rows($result);
    if(isset($_POST['champion']))
    {
        $champ = sanitize($con,$_POST['champion']);
        $runner = sanitize($con,$_POST['runnerup']);
        $exec = sanitize($con,$_POST['exec']);
        $mvp = sanitize($con,$_POST['mvp']);
        $op = sanitize($con,$_POST['opoty']);
        $dp = sanitize($con,$_POST['dpoty']);
        $sm = sanitize($con,$_POST['smoty']);
        $pg = sanitize($con,$_POST['pgoty']);
        $sg = sanitize($con,$_POST['sgoty']);
        $sf = sanitize($con,$_POST['sfoty']);
        $pf = sanitize($con,$_POST['pfoty']);
        $ce = sanitize($con,$_POST['coty']);
        $ro = sanitize($con,$_POST['roty']);
        $mip = sanitize($con,$_POST['mip']);
    }
    if($count==1)
    {
        $reg="UPDATE sbdl_predictions SET champion='$champ', runnerup='$runner', eoty='$exec', mvp='$mvp', opoty='$op', dpoty='$dp', smoty='$sm', pgoty='$pg', sgoty='$sg', sfoty='$sf', pfoty='$pf', coty='$ce', roty='$ro', mip='$mip' WHERE a_fk='$userid' and year='$year'";
        $result=mysqli_query($con,$reg) or die("insert failed");

        $audit = "INSERT INTO audit_history (username,task,ipaddress,audittime) VALUES (\"$username\",'Edited SBDL Prediction','$_SERVER[REMOTE_ADDR]',now())";
        $result=mysqli_query($con,$audit) or die("insert failed");

        mysqli_close($con);
        header("location:sbdl_predictions.php?alert=success");
    }
    else
    {
        $reg="INSERT INTO sbdl_predictions (year,a_fk,champion,runnerup,eoty,mvp,opoty,dpoty,smoty,pgoty,sgoty,sfoty,pfoty,coty,roty,mip,time_stamp) VALUES ('$year','$userid','$champ','$runner','$exec','$mvp','$op','$dp','$sm','$pg','$sg','$sf','$pf','$ce','$ro','$mip',now())";
        $result=mysqli_query($con,$reg) or die("insert failed");
        prediction_submission_notification($con,$username,'SBDL');

        $audit = "INSERT INTO audit_history (username,task,ipaddress,audittime) VALUES (\"$username\",'Submitted SBDL Prediction','$_SERVER[REMOTE_ADDR]',now())";
        $result=mysqli_query($con,$audit) or die("insert failed");

        mysqli_close($con);
        header("location:sbdl_predictions.php?alert=success");
    }
}
else
{
    header("location:sbdl_predictions.php?alert=closed");
}
?>

