<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
$user = new User($_SESSION['user']);
$username = $user->username;
$userid = $user->id;

$system = new System();
$year = $system->get_year();

// Begin Import Predictions from Another User
$query = "SELECT p.a_fk,a.username FROM auth_user a INNER JOIN predictions p ON a.ID=p.a_fk where year = '$year' ORDER BY username ASC";
$result = mysqli_query($con,$query);
$users = '';
while ($q = mysqli_fetch_array($result)) 
{
    $users .= "<option value = '".$q['a_fk']."'>".$q['username']."</option>";
}
if(isset($_POST['import']))
{
    $sql = "SELECT isactive FROM forms WHERE form = 'predictions'";
    $result = mysqli_fetch_array(mysqli_query($con,$sql));
    $active = $result['isactive'];
    if($active == 1)
    {
        $import = $_POST['import'];
        $query = "SELECT * FROM predictions WHERE a_fk='$import' AND year='$year'";
        $p = mysqli_fetch_array(mysqli_query($con,$query));
        $champ = sanitize($con,$p['champion']);
        $runner = sanitize($con,$p['runnerup']);
        $exec = sanitize($con,$p['eoty']);
        $mvp = sanitize($con,$p['mvp']);
        $op = sanitize($con,$p['opoty']);
        $dp = sanitize($con,$p['dpoty']);
        $sm = sanitize($con,$p['smoty']);
        $pg = sanitize($con,$p['pgoty']);
        $sg = sanitize($con,$p['sgoty']);
        $sf = sanitize($con,$p['sfoty']);
        $pf = sanitize($con,$p['pfoty']);
        $ce = sanitize($con,$p['coty']);
        $ro = sanitize($con,$p['roty']);
        $mip = sanitize($con,$p['mip']);
        $query = "SELECT * FROM predictions WHERE a_fk='$userid' AND year='$year'";
        $importcheck = mysqli_num_rows(mysqli_query($con,$query));
        if($importcheck > 0)
        {
            $query = "UPDATE predictions SET champion='$champ', runnerup='$runner', eoty='$exec', mvp='$mvp', opoty='$op', dpoty='$dp', smoty='$sm', pgoty='$pg', sgoty='$sg', sfoty='$sf', pfoty='$pf', coty='$ce', roty='$ro', mip='$mip' WHERE a_fk='$userid' and year='$year'";
            mysqli_query($con,$query);
            $audit = "INSERT INTO audit_history (username,task,ipaddress,audittime) VALUES (\"$username\",'Imported SBA Prediction','$_SERVER[REMOTE_ADDR]',now())";
            $result=mysqli_query($con,$audit);
        }
        else
        {
            $query="INSERT INTO predictions (year,a_fk,champion,runnerup,eoty,mvp,opoty,dpoty,smoty,pgoty,sgoty,sfoty,pfoty,coty,roty,mip,time_stamp) VALUES ('$year','$userid','$champ','$runner','$exec','$mvp','$op','$dp','$sm','$pg','$sg','$sf','$pf','$ce','$ro','$mip',now())";
            mysqli_query($con,$query);
            $audit = "INSERT INTO audit_history (username,task,ipaddress,audittime) VALUES (\"$username\",'Imported SBA Prediction','$_SERVER[REMOTE_ADDR]',now())";
            $result=mysqli_query($con,$audit);
        }
    }
    else
    {
        header("location:predictions.php?alert=closed");
    }
}
// End Import Predictions from Another User

$query = "SELECT * FROM predictions WHERE a_fk='$userid' AND year='$year'";
$s = mysqli_fetch_array(mysqli_query($con,$query));

$query = "SELECT name FROM teams where league = 'SBA' ORDER BY name ASC";
$result = mysqli_query($con,$query);
$team = '';
$echo = '';
while ($r = mysqli_fetch_array($result)) {
    if ($s['champion'] == $r['name']){$echo = "selected";} else{$echo = '';}
$team .= "<option value = '".$r['name']."' $echo>".$r['name']."</option>";
}
$query = "SELECT name FROM teams where league = 'SBA' ORDER BY name ASC";
$result = mysqli_query($con,$query);
$team2 = '';
$echo = '';
while ($r = mysqli_fetch_array($result)) {
    if ($s['runnerup'] == $r['name']){$echo = "selected";} else{$echo = '';}
$team2 .= "<option value = '".$r['name']."' $echo>".$r['name']."</option>";
}
$query = "SELECT name FROM coaches where league = 'SBA' ORDER BY name ASC";
$result = mysqli_query($con,$query);
$coach = '';
$echo = '';
while ($r = mysqli_fetch_array($result)) {
    if ($s['eoty'] == $r['name']){$echo = "selected";} else{$echo = '';}
$coach .= "<option value = \"".$r['name']."\" $echo>".$r['name']."</option>";
}
$query = "SELECT CONCAT(plast,', ',pfirst) as name FROM players where league = 'SBA' AND (active='1' || active='2' || active='3') ORDER BY name ASC";
$result = mysqli_query($con,$query);
$mvp = '';
$opoty = '';
$dpoty = '';
$smoty = '';
$pgoty = '';
$sgoty = '';
$sfoty = '';
$pfoty = '';
$coty = '';
$roty = '';
$mip = '';
$echo = '';
while ($r = mysqli_fetch_array($result)) 
{
    if ($s['mvp'] == $r['name']){$echo = "selected";} else{$echo = '';}
    $mvp .= "<option value = \"".$r['name']."\" $echo>".$r['name']."</option>";
    if ($s['opoty'] == $r['name']){$echo = "selected";} else{$echo = '';}
    $opoty .= "<option value = \"".$r['name']."\" $echo>".$r['name']."</option>";
    if ($s['dpoty'] == $r['name']){$echo = "selected";} else{$echo = '';}
    $dpoty .= "<option value = \"".$r['name']."\" $echo>".$r['name']."</option>";
    if ($s['smoty'] == $r['name']){$echo = "selected";} else{$echo = '';}
    $smoty .= "<option value = \"".$r['name']."\" $echo>".$r['name']."</option>";
    if ($s['pgoty'] == $r['name']){$echo = "selected";} else{$echo = '';}
    $pgoty .= "<option value = \"".$r['name']."\" $echo>".$r['name']."</option>";
    if ($s['sgoty'] == $r['name']){$echo = "selected";} else{$echo = '';}
    $sgoty .= "<option value = \"".$r['name']."\" $echo>".$r['name']."</option>";
    if ($s['sfoty'] == $r['name']){$echo = "selected";} else{$echo = '';}
    $sfoty .= "<option value = \"".$r['name']."\" $echo>".$r['name']."</option>";
    if ($s['pfoty'] == $r['name']){$echo = "selected";} else{$echo = '';}
    $pfoty .= "<option value = \"".$r['name']."\" $echo>".$r['name']."</option>";
    if ($s['coty'] == $r['name']){$echo = "selected";} else{$echo = '';}
    $coty .= "<option value = \"".$r['name']."\" $echo>".$r['name']."</option>";
    if ($s['roty'] == $r['name']){$echo = "selected";} else{$echo = '';}
    $roty .= "<option value = \"".$r['name']."\" $echo>".$r['name']."</option>";
    if ($s['mip'] == $r['name']){$echo = "selected";} else{$echo = '';}
    $mip .= "<option value = \"".$r['name']."\" $echo>".$r['name']."</option>";
}
if(isset($_GET['alert'])) 
{
    if($_GET['alert'] == 'success') 
    {
        $echo = "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Predictions Saved.</strong></div>";
    }
    else if($_GET['alert'] == 'closed')
    {
        $echo = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Predictions Closed, Submission Not Saved.</strong></div>";
    }
    else if($_GET['alert'] == 'norecord')
    {
        $echo = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You must save your own Predictions before you can import someone else's.</strong></div>";
    }
    else 
    {
        $echo = '';
    }
}
else
{$echo = '';}
?>

<!-- Body Start -->
<body>
<div id="wrapper" style="position:relative;top:0;left:0;">
    <?php include $path.'/includes/nav.php'; ?>
    <div class="container">
        <?=HeaderImagePrint($hostlg,$_SESSION['style'])?>
        <?=$echo?>
        <div class="panel panel-primary">
            <div class="panel-heading">Import Predictions</div>
            <div class="panel-body">
                <div class="panel-group col-xs-12">
                    <form class="form-horizontal" method="POST">
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>User</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>User</span>
                                <select class='form-control' name='import'>
                                    <?=$users?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary col-xs-12">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="panel panel-primary">
            <div class="panel-heading">SBA Season Predictions</div>
            <div class="panel-body">
                <div class="panel-group col-xs-12">
                    <form class="form-horizontal" action="check_predictions_answers.php" method="post" >
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>Champion</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>Champion</span>
                                <select class='form-control' name='champion'>
                                    <?=$team?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>Runner-Up</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>Runner-Up</span>
                                <select class='form-control' name='runnerup'>
                                    <?=$team2?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>Coach of the Year</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>EotY</span>
                                <select class='form-control' name='exec'>
                                    <?=$coach?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>Most Valuable Player</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>MVP</span>
                                <select class='form-control' name='mvp'>
                                    <?=$mvp?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>Offensive Player of the Year</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>OPotY</span>
                                <select class='form-control' name='opoty'>
                                    <?=$opoty?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>Defensive Player of the Year</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>DPotY</span>
                                <select class='form-control' name='dpoty'>
                                    <?=$dpoty?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>Sixth Man of the Year</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>6MotY</span>
                                <select class='form-control' name='smoty'>
                                    <?=$smoty?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>Rookie of the Year</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>RotY</span>
                                <select class='form-control' name='roty'>
                                    <?=$roty?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>Most Improved Player</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>MIP</span>
                                <select class='form-control' name='mip'>
                                    <?=$mip?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>Point Guard of the Year</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>PGotY</span>
                                <select class='form-control' name='pgoty'>
                                    <?=$pgoty?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>Shooting Guard of the Year</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>SGotY</span>
                                <select class='form-control' name='sgoty'>
                                    <?=$sgoty?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>Small Forward of the Year</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>SMotY</span>
                                <select class='form-control' name='sfoty'>
                                    <?=$sfoty?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>Power Forward of the Year</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>pfoty</span>
                                <select class='form-control' name='pfoty'>
                                    <?=$pfoty?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>Center of the Year</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>CotY</span>
                                <select class='form-control' name='coty'>
                                    <?=$coty?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary col-xs-12">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Body End -->

<?php
include $path.'/footer.php';
?>

