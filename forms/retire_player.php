<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
$id = isset($_GET['id']) ? sanitize($con,$_GET['id']) : '';
$marked = isset($_GET['marked']) ? sanitize($con,$_GET['marked']) : '';
if($hostlg == 'SBA'){$forumid = '50';}else if($hostlg=='EFL'){$forumid='56';}
if(isset($_GET['id']) && isset($_GET['marked']))
{
    
    $sql = "UPDATE players
            SET active = '3'
            WHERE id = '$id'";
    mysqli_query($con,$sql);
    $player = new Player($id);
    $pname = $player->get_('name');
    $body = $pname." is announcing that this season will be their last in the SBA.";
    $curl_post_data = array(
            'forum' => $forumid,
            'title' => $pname." will be retiring at the end of the season!",
            'author' => '2574',
            'post' => $body
        );
    createTopic($curl_post_data);
    header("location:view_players.php?alert=retiresuccessful");
    exit();
}
else if(isset($_GET['id']) && isset($_GET['infile']))
{
    
    $sql = "UPDATE players
            SET retiredinfile = '1'
            WHERE id = '$id'";
    mysqli_query($con,$sql);
    header("location:simmer_dashboard.php");
    exit();
}
else if(isset($_GET['id']))
{
    $player = new Player($_GET['id']);
    $player->retire();
    $pid = $player->id;
    $cid = $player->get_contractID();
    complete_contract($cid,$pid);
    if($r['league'] == 'SBA')
    {
        $player->get_carryover();
    }
    header("location:view_players.php?alert=retiresuccessful");
    exit();
}
else if(isset($_GET['league']) && isset($_GET['process']))
{
    $league = isset($_GET['league']) ? sanitize($con,$_GET['league']) : '';
    $sql = "UPDATE players
            SET retiredinfile = '1'
            WHERE retiredinfile = '0' AND active = '0' AND league='$league'";
    mysqli_query($con,$sql);
    header("location:simmer_dashboard.php");
    exit();
}
else
{
    header("location:view_players.php?alert=retirefailed");
    exit();
}
?>