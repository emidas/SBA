<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
$puid = '';
$pid = '';
$deletepm = '';
$pname = '';
$user = $_SESSION['user'];
$mid = $_SESSION['memberid'];
if(isset($_POST['puid']) && isset($_POST['pid']))
{
    $pid = sanitize($con,$_POST['pid']);
    $puid = sanitize($con,$_POST['puid']);
    $deletepm = sanitize($con,$_POST['deletepm']);
    $pname = sanitize($con,$_POST['pname']);
    $pm = str_replace('\r\n' , '<br>', $denypm);
    $pm = str_replace('\r' , '<br>', $pm);
    $pm = str_replace('\n' , '<br>', $pm);
    $pm = stripslashes($pm);
    // Delete Player Updates - Start
    $sql = "DELETE FROM player_updates_tasks WHERE puid_fk = '$puid'";
    mysqli_query($con,$sql);
    $sql = "DELETE FROM player_updates_update WHERE puid_fk = '$puid'";
    mysqli_query($con,$sql);
    $sql = "DELETE FROM player_updates WHERE id = '$puid'";
    mysqli_query($con,$sql);
    // Delete Player Updates - End
    $task = "Deleted Update for ".$pname;
    audit_updates($con,$user,$task);
    $to = array();
    $tosql = "
    SELECT memberid 
    FROM auth_user a
    INNER JOIN players p ON a.ID = p.user_fk
    WHERE p.id = '$pid' ";
    $toresult = mysqli_query($con,$tosql);
    while($torow = mysqli_fetch_array($toresult))
    {
        array_push($to,$torow['memberid']);
    }
    $curl_post_data = array(
        'from' => $mid,
        'to' => $to,
        'title' => "Update Deleted",
        'body' => "<h1>SBA Online</h1><p>Your update for ".$pname." has been deleted by ".$user.". Optional message from Admin:<br><br>".$pm."</p>"
    );
    createMessage($curl_post_data);
    header("location:update_dashboard.php");
    exit();
}
else
{
    header("location:javascript://history.go(-1)");
    exit();
}

?>
