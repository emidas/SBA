<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
//Earn Points
if(isset($_POST['submitted']))
{
    $pid = $_POST['pid'];
    $earnedtpe = (isset($_POST['earned_tpe'])) ? $_POST['earned_tpe'] : '0';
    $updatenotes = sanitize($con,$_POST['notes']);
    $count = count($_POST['row']);
    $arr = isset($_POST['row']) ? $_POST['row'] : '';

    $sql = "SELECT id FROM player_updates WHERE pid_fk='$pid' AND (status < 2)";
    $numpuid = mysqli_num_rows(mysqli_query($con,$sql));
    if($numpuid == 0)
    {
        $sql = "INSERT INTO player_updates (pid_fk) VALUES ('$pid')";
        mysqli_query($con,$sql);
        $puid = mysqli_insert_id($con);
    }
    else
    {
        $puid = mysqli_fetch_array(mysqli_query($con,$sql));
        $puid = $puid['id'];
    }
    for($x=0;$x<$count;$x++)
    {
        $y = $x+1;
        $task = sanitize($con,$arr[$x]['task']);
        $week = sanitize($con,$arr[$x]['week']);
        $link = sanitize($con,$arr[$x]['link']);
        $urllink = str_replace("#","%23",$link);
        $urllink = str_replace("&","%26",$urllink);
        $pe = sanitize($con,$arr[$x]['pe']);
        $sql = "INSERT INTO player_updates_tasks (puid_fk,task,week,link,pe) VALUES ('$puid','$task','$week','$link','$pe')";
        mysqli_query($con,$sql);
    } 
    $sql = "UPDATE player_updates SET earn_tpe = earn_tpe+'$earnedtpe',status='1',notes='$updatenotes',last_submitted=now() WHERE id = '$puid'";
    mysqli_query($con,$sql);
    $sql = "SELECT id FROM player_updates_update WHERE puid_fk = '$puid'";
    $puunum = mysqli_num_rows(mysqli_query($con,$sql));
    if($puunum == 0)
    {
        $sql2 = "SELECT * FROM players WHERE id='$pid'";
        $s = mysqli_fetch_array(mysqli_query($con,$sql2));
        if($hostlg == 'SBA')
        {
            $ins=$s['ins'];
            $jps=$s['jps'];
            $ft=$s['ft'];
            $tps=$s['3ps'];
            $han=$s['han'];
            $pas=$s['pas'];
            $orb=$s['orb'];
            $drb=$s['drb'];
            $psd=$s['psd'];
            $prd=$s['prd'];
            $stl=$s['stl'];
            $blk=$s['blk'];
            $fl=$s['fl'];
            $qkn=$s['qkn'];
            $str=$s['str'];
            $jmp=$s['jmp'];
            $tpstoggle=$s['3ps_toggle'];
            $sql = "INSERT INTO player_updates_update (puid_fk,ins,jps,ft,3ps,han,pas,orb,drb,psd,prd,stl,blk,fl,qkn,str,jmp,ins_old,jps_old,ft_old,3ps_old,han_old,pas_old,orb_old,drb_old,psd_old,prd_old,stl_old,blk_old,fl_old,qkn_old,str_old,jmp_old,3ps_toggle) VALUES ('$puid','$ins','$jps','$ft','$tps','$han','$pas','$orb','$drb','$psd','$prd','$stl','$blk','$fl','$qkn','$str','$jmp','$ins','$jps','$ft','$tps','$han','$pas','$orb','$drb','$psd','$prd','$stl','$blk','$fl','$qkn','$str','$jmp','$tpstoggle')";
            mysqli_query($con,$sql);
        }
        else if($hostlg == 'EFL')
        {
            $stmt = $conn->prepare("INSERT INTO player_updates_update (puid_fk,str,agi,arm,`int`,acc,tkl,spd,han,rbk,pbk,kdi,kac,str_old,agi_old,arm_old,`int_old`,acc_old,tkl_old,spd_old,han_old,rbk_old,pbk_old,kdi_old,kac_old) VALUES (:id,:str,:agi,:arm,:inti,:acc,:tkl,:spd,:han,:rbk,:pbk,:kdi,:kac,:str,:agi,:arm,:inti,:acc,:tkl,:spd,:han,:rbk,:pbk,:kdi,:kac)");
            $stmt->execute([':id' => $puid,':str' => $s['str'],':agi' => $s['agi'],':arm' => $s['arm'],':inti' => $s['int'],':acc' => $s['acc'],':tkl' => $s['tkl'],':spd' => $s['spd'],':han' => $s['han'],':rbk' => $s['rbk'],':pbk' => $s['pbk'],':kdi' => $s['kdi'],':kac' => $s['kac']]);
        }
    }
    header("location:/forms/player_update.php?pid=".$pid."&alert=success");
    exit();
}
//Spend Points
if(isset($_POST['spend_submitted']))
{
    $pid = $_POST['pid'];
    $spendtpe = (isset($_POST['spend_tpe'])) ? $_POST['spend_tpe'] : '0';
    $costtpe = (isset($_POST['cost_tpe'])) ? $_POST['cost_tpe'] : '0';
    if($costtpe > $spendtpe)
    {
        header("location:/forms/player_update.php?pid=".$pid."&alert=cantafford");
        exit();
    }
    else
    {
        $sql = "SELECT id FROM player_updates WHERE pid_fk='$pid' AND (status < 2)";
        $numpuid = mysqli_num_rows(mysqli_query($con,$sql));
        if($numpuid == 0)
        {
            $sql = "INSERT INTO player_updates (pid_fk) VALUES ('$pid')";
            mysqli_query($con,$sql);
            $puid = mysqli_insert_id($con);
        }
        else
        {
            $puid = mysqli_fetch_array(mysqli_query($con,$sql));
            $puid = $puid['id'];
        }
        $sql = "UPDATE player_updates SET cost_tpe = '$costtpe',status='1',last_submitted=now() WHERE id = '$puid'";
        mysqli_query($con,$sql);
        $sql = "SELECT id FROM player_updates_update WHERE puid_fk = '$puid'";
        $puunum = mysqli_num_rows(mysqli_query($con,$sql));
        $sql2 = "SELECT * FROM players WHERE id='$pid'";
        $s = mysqli_fetch_array(mysqli_query($con,$sql2));
        if($hostlg == 'SBA')
        {
            $curr_ins = sanitize($con,$_POST['curr_ins']);
            $curr_jps = sanitize($con,$_POST['curr_jps']);
            $curr_ft = sanitize($con,$_POST['curr_ft']);
            $curr_3ps = sanitize($con,$_POST['curr_3ps']);
            $curr_han = sanitize($con,$_POST['curr_han']);
            $curr_pas = sanitize($con,$_POST['curr_pas']);
            $curr_orb = sanitize($con,$_POST['curr_orb']);
            $curr_drb = sanitize($con,$_POST['curr_drb']);
            $curr_psd = sanitize($con,$_POST['curr_psd']);
            $curr_prd = sanitize($con,$_POST['curr_prd']);
            $curr_stl = sanitize($con,$_POST['curr_stl']);
            $curr_blk = sanitize($con,$_POST['curr_blk']);
            $curr_fl = sanitize($con,$_POST['curr_fl']);
            $curr_qkn = sanitize($con,$_POST['curr_qkn']);
            $curr_str = sanitize($con,$_POST['curr_str']);
            $curr_jmp = sanitize($con,$_POST['curr_jmp']);
            $ins=$s['ins'];
            $jps=$s['jps'];
            $ft=$s['ft'];
            $tps=$s['3ps'];
            $han=$s['han'];
            $pas=$s['pas'];
            $orb=$s['orb'];
            $drb=$s['drb'];
            $psd=$s['psd'];
            $prd=$s['prd'];
            $stl=$s['stl'];
            $blk=$s['blk'];
            $fl=$s['fl'];
            $qkn=$s['qkn'];
            $str=$s['str'];
            $jmp=$s['jmp'];
            if(isset($_POST['tpstoggle'])){$tpstoggle = '1';} else {$tpstoggle = '0';}
            if($puunum == 0)
            {

                $sql = "INSERT INTO player_updates_update (puid_fk,ins,jps,ft,3ps,han,pas,orb,drb,psd,prd,stl,blk,fl,qkn,str,jmp,ins_old,jps_old,ft_old,3ps_old,han_old,pas_old,orb_old,drb_old,psd_old,prd_old,stl_old,blk_old,fl_old,qkn_old,str_old,jmp_old,3ps_toggle) VALUES ('$puid','$curr_ins','$curr_jps','$curr_ft','$curr_3ps','$curr_han','$curr_pas','$curr_orb','$curr_drb','$curr_psd','$curr_prd','$curr_stl','$curr_blk','$curr_fl','$curr_qkn','$curr_str','$curr_jmp','$ins','$jps','$ft','$tps','$han','$pas','$orb','$drb','$psd','$prd','$stl','$blk','$fl','$qkn','$str','$jmp','$tpstoggle')";
                mysqli_query($con,$sql);
            }
            else
            {
                $sql = "UPDATE player_updates_update
                        SET ins='$curr_ins',jps='$curr_jps',ft='$curr_ft',3ps='$curr_3ps',han='$curr_han',pas='$curr_pas',orb='$curr_orb',drb='$curr_drb',psd='$curr_psd',prd='$curr_prd',stl='$curr_stl',blk='$curr_blk',fl='$curr_fl',qkn='$curr_qkn',str='$curr_str',jmp='$curr_jmp',ins_old='$ins',jps_old='$jps',ft_old='$ft',3ps_old='$tps',han_old='$han',pas_old='$pas',orb_old='$orb',drb_old='$drb',psd_old='$psd',prd_old='$prd',stl_old='$stl',blk_old='$blk',fl_old='$fl',qkn_old='$qkn',str_old='$str',jmp_old='$jmp',3ps_toggle='$tpstoggle'
                        WHERE puid_fk='$puid'";
                mysqli_query($con,$sql);
            }
        }
        else if($hostlg == 'EFL')
        {
            $curr_str = sanitize($con,$_POST['curr_str']);
            $curr_agi = sanitize($con,$_POST['curr_agi']);
            $curr_arm = sanitize($con,$_POST['curr_arm']);
            $curr_int = sanitize($con,$_POST['curr_int']);
            $curr_acc = sanitize($con,$_POST['curr_acc']);
            $curr_tkl = sanitize($con,$_POST['curr_tkl']);
            $curr_spd = sanitize($con,$_POST['curr_spd']);
            $curr_han = sanitize($con,$_POST['curr_han']);
            $curr_rbk = sanitize($con,$_POST['curr_rbk']);
            $curr_pbk = sanitize($con,$_POST['curr_pbk']);
            $curr_kdi = sanitize($con,$_POST['curr_kdi']);
            $curr_kac = sanitize($con,$_POST['curr_kac']);
            if($puunum == 0)
            {

                $stmt = $conn->prepare("INSERT INTO player_updates_update (puid_fk,str,agi,arm,`int`,acc,tkl,spd,han,rbk,pbk,kdi,kac,str_old,agi_old,arm_old,`int_old`,acc_old,tkl_old,spd_old,han_old,rbk_old,pbk_old,kdi_old,kac_old) VALUES (:id,:str,:agi,:arm,:inti,:acc,:tkl,:spd,:han,:rbk,:pbk,:kdi,:kac,:str_old,:agi_old,:arm_old,:inti_old,:acc_old,:tkl_old,:spd_old,:han_old,:rbk_old,:pbk_old,:kdi_old,:kac_old)");
            $stmt->execute([':id' => $puid,':str' => $curr_str,':agi' => $curr_agi,':arm' => $curr_arm,':inti' => $curr_int,':acc' => $curr_acc,':tkl' => $curr_tkl,':spd' => $curr_spd,':han' => $curr_han,':rbk' => $curr_rbk,':pbk' => $curr_pbk,':kdi' => $curr_kdi,':kac' => $curr_kac,':str_old' => $s['str'],':agi_old' => $s['agi'],':arm_old' => $s['arm'],':inti_old' => $s['int'],':acc_old' => $s['acc'],':tkl_old' => $s['tkl'],':spd_old' => $s['spd'],':han_old' => $s['han'],':rbk_old' => $s['rbk'],':pbk_old' => $s['pbk'],':kdi_old' => $s['kdi'],':kac_old' => $s['kac']]);
            }
            else
            {
                $stmt = $conn->prepare("UPDATE player_updates_update SET str=:str,agi=:agi,arm=:arm,`int`=:inti,acc=:acc,tkl=:tkl,spd=:spd,han=:han,rbk=:rbk,pbk=:pbk,kdi=:kdi,kac=:kac,str_old=:str_old,agi_old=:agi_old,arm_old=:arm_old,`int_old`=:inti_old,acc_old=:acc_old,tkl_old=:tkl_old,spd_old=:spd_old,han_old=:han_old,rbk_old=:rbk_old,pbk_old=:pbk_old,kdi_old=:kdi_old,kac_old=:kac_old WHERE puid_fk=:id");
                $stmt->execute([':id' => $puid,':str' => $curr_str,':agi' => $curr_agi,':arm' => $curr_arm,':inti' => $curr_int,':acc' => $curr_acc,':tkl' => $curr_tkl,':spd' => $curr_spd,':han' => $curr_han,':rbk' => $curr_rbk,':pbk' => $curr_pbk,':kdi' => $curr_kdi,':kac' => $curr_kac,':str_old' => $s['str'],':agi_old' => $s['agi'],':arm_old' => $s['arm'],':inti_old' => $s['int'],':acc_old' => $s['acc'],':tkl_old' => $s['tkl'],':spd_old' => $s['spd'],':han_old' => $s['han'],':rbk_old' => $s['rbk'],':pbk_old' => $s['pbk'],':kdi_old' => $s['kdi'],':kac_old' => $s['kac']]);
            }
        }
        header("location:/forms/player_update.php?pid=".$pid."&alert=success");
        exit();
    }    
}
?>