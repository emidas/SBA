<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
$user=$_SESSION['user'];

if(isset($_POST['season']))
{$season = $_POST['season'];} else {$season = '1';}
if(isset($_POST['season2']))
{$season2 = $_POST['season2'];} else {$season2 = '1';}
if(isset($_POST['season3']))
{$season3 = $_POST['season3'];} else {$season3 = '1';}
if(isset($_POST['season4']))
{$season4 = $_POST['season4'];} else {$season4 = '1';}
if(isset($_POST['season5']))
{$season5 = $_POST['season5'];} else {$season5 = '1';}
if(isset($_POST['league']))
{$league = $_POST['league'];} else {$league = 'sba';}
if(isset($_POST['league2']))
{$league2 = $_POST['league2'];} else {$league2 = 'sba';}
if(isset($_POST['league3']))
{$league3 = $_POST['league3'];} else {$league3 = 'sba';}
if(isset($_POST['league4']))
{$league4 = $_POST['league4'];} else {$league4 = 'sba';}
if(isset($_POST['league5']))
{$league5 = $_POST['league5'];} else {$league5 = 'sba';}
if(isset($_POST['type']))
{$type = $_POST['type'];} else {$type = 'season';}
if(isset($_POST['type2']))
{$type2 = $_POST['type2'];} else {$type2 = 'season';}
if(isset($_POST['type3']))
{$type3 = $_POST['type3'];} else {$type3 = 'season';}
if(isset($_POST['type4']))
{$type4 = $_POST['type4'];} else {$type4 = 'season';}
if(isset($_POST['type5']))
{$type5 = $_POST['type5'];} else {$type5 = 'season';}
if(isset($_POST['name']))
{$name = $_POST['name'];} else {$name = '';}
if(isset($_POST['name2']))
{$name2 = $_POST['name2'];} else {$name2 = '';}
if(isset($_POST['name3']))
{$name3 = $_POST['name3'];} else {$name3 = '';}
if(isset($_POST['name4']))
{$name4 = $_POST['name4'];} else {$name4 = '';}
if(isset($_POST['name5']))
{$name5 = $_POST['name5'];} else {$name5 = '';}
if(isset($_POST['checkC']))
{$checkC = $_POST['checkC'];} else {$checkC = '';}
if(isset($_POST['checkD']))
{$checkD = $_POST['checkD'];} else {$checkD = '';}
if(isset($_POST['checkE']))
{$checkE = $_POST['checkE'];} else {$checkE = '';}


if($name != '')
{
    $aecho = printplayersingleseason($con,$name,$league,$type,$season,'0');
}
else
{
    $aecho = '';
}
if($name2 != '')
{
    $becho = printplayersingleseason($con,$name2,$league2,$type2,$season2,'0');
}
else
{
    $becho = '';
}
if($name3 != '' && $checkC == 'on')
{
    $cecho = printplayersingleseason($con,$name3,$league3,$type3,$season3,'0');
}
else
{
    $cecho = '';
}
if($name4 != '' && $checkD == 'on')
{
    $decho = printplayersingleseason($con,$name4,$league4,$type4,$season4,'0');
}
else
{
    $decho = '';
}
if($name5 != '' && $checkE == 'on')
{
    $eecho = printplayersingleseason($con,$name5,$league5,$type5,$season5,'0');
}
else
{
    $eecho = '';
}

// Season Dropdowns
$yquery = "SELECT year as season from year";
$yresult = mysqli_query($con,$yquery);
$yecho = '';
$echo = '';
while ($r = mysqli_fetch_array($yresult)) 
{
    if($season == $r['season']) {$echo = "selected";} else {$echo = "";}
    $yecho .= "<option value = '".$r['season']."' $echo>".$r['season']."</option>";
}
$yresult = mysqli_query($con,$yquery);
$yecho2 = '';
while ($r = mysqli_fetch_array($yresult)) 
{
    if($season2 == $r['season']) {$echo = "selected";} else {$echo = "";}
    $yecho2 .= "<option value = '".$r['season']."' $echo>".$r['season']."</option>";
}
$yresult = mysqli_query($con,$yquery);
$yecho3 = '';
while ($r = mysqli_fetch_array($yresult)) 
{
    if($season3 == $r['season']) {$echo = "selected";} else {$echo = "";}
    $yecho3 .= "<option value = '".$r['season']."' $echo>".$r['season']."</option>";
}
$yresult = mysqli_query($con,$yquery);
$yecho4 = '';
while ($r = mysqli_fetch_array($yresult)) 
{
    if($season4 == $r['season']) {$echo = "selected";} else {$echo = "";}
    $yecho4 .= "<option value = '".$r['season']."' $echo>".$r['season']."</option>";
}
$yresult = mysqli_query($con,$yquery);
$yecho5 = '';
while ($r = mysqli_fetch_array($yresult)) 
{
    if($season5 == $r['season']) {$echo = "selected";} else {$echo = "";}
    $yecho5 .= "<option value = '".$r['season']."' $echo>".$r['season']."</option>";
}
// End of Season Dropdowns

// Player Dropdowns
$query = "SELECT CONCAT(plast,', ',pfirst) as name,CONCAT(pfirst,' ',plast) as pname FROM players ORDER BY name ASC";
$result = mysqli_query($con,$query);
$plist = '';
while ($r = mysqli_fetch_array($result)) 
{
    if($name == $r['pname']) {$echo = "selected";} else {$echo = "";}
    $plist .= "<option value = '".$r['pname']."' $echo>".$r['name']."</option>";
}
$result = mysqli_query($con,$query);
$plist2 = '';
while ($r = mysqli_fetch_array($result)) 
{
    if($name2 == $r['pname']) {$echo = "selected";} else {$echo = "";}
    $plist2 .= "<option value = '".$r['pname']."' $echo>".$r['name']."</option>";
}
$result = mysqli_query($con,$query);
$plist3 = '';
while ($r = mysqli_fetch_array($result)) 
{
    if($name3 == $r['pname']) {$echo = "selected";} else {$echo = "";}
    $plist3 .= "<option value = '".$r['pname']."' $echo>".$r['name']."</option>";
}
$result = mysqli_query($con,$query);
$plist4 = '';
while ($r = mysqli_fetch_array($result)) 
{
    if($name4 == $r['pname']) {$echo = "selected";} else {$echo = "";}
    $plist4 .= "<option value = '".$r['pname']."' $echo>".$r['name']."</option>";
}
$result = mysqli_query($con,$query);
$plist5 = '';
while ($r = mysqli_fetch_array($result)) 
{
    if($name5 == $r['pname']) {$echo = "selected";} else {$echo = "";}
    $plist5 .= "<option value = '".$r['pname']."' $echo>".$r['name']."</option>";
}
// End of Player Dropdowns


if(isset($_GET['alert'])) 
{
    if($_GET['alert'] == 'success') 
    {
        $echo = "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Predictions Saved.</strong></div>";
    }
    else if($_GET['alert'] == 'closed')
    {
        $echo = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Predictions Closed, Submission Not Saved.</strong></div>";
    }
    else if($_GET['alert'] == 'norecord')
    {
        $echo = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You must save your own Predictions before you can import someone else's.</strong></div>";
    }
    else 
    {
        $echo = '';
    }
}
else
{$echo = '';}
?>

<!-- Body Start -->
<body>
<div id="wrapper" style="position:relative;top:0;left:0;">
    <?php include $path.'/includes/nav.php'; ?>
    <div class="container">
        <?=HeaderImagePrint($hostlg,$_SESSION['style'])?>
        <?=$echo?>
        <form class="form-horizontal" method='POST'>
            <div class="panel-group">
                <div class="panel panel-primary">
                    <div class="panel-heading">Player Comparison Settings</div>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="col-xs-offset-5 col-xs-2"><center><input type="checkbox" name="checkC" id="checkC" onChange="enableCompare('checkC','C');" <?php if ($checkC == 'on') echo "checked='checked'";?>></center></div>
                            <div class="col-xs-2"><center><input type="checkbox" name="checkD" id="checkD" onChange="enableCompare('checkD','D');" <?php if ($checkD == 'on') echo "checked='checked'";?>></center></div>
                            <div class="col-xs-2"><center><input type="checkbox" name="checkE" id="checkE" onChange="enableCompare('checkE','E');" <?php if ($checkE == 'on') echo "checked='checked'";?>></center></div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-offset-1 col-xs-2"><h4><center>Player A</center></h4></div>
                            <div class="col-xs-2"><h4><center>Player B</center></h4></div>
                            <div class="col-xs-2"><h4><center>Player C</center></h4></div>
                            <div class="col-xs-2"><h4><center>Player D</center></h4></div>
                            <div class="col-xs-2"><h4><center>Player E</center></h4></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-xs-1" for="stat">Season:</label>
                            <div class="col-xs-2">
                                <select name="season" class="form-control"><?=$yecho?></select>
                            </div>
                            <div class="col-xs-2">
                                <select name="season2" class="form-control"><?=$yecho2?></select>
                            </div>
                            <div class="col-xs-2">
                                <select name="season3" id="season3" class="form-control" disabled><?=$yecho3?></select>
                            </div>
                            <div class="col-xs-2">
                                <select name="season4" id="season4" class="form-control" disabled><?=$yecho4?></select>
                            </div>
                            <div class="col-xs-2">
                                <select name="season5" id="season5" class="form-control" disabled><?=$yecho5?></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-xs-1" for="league">League:</label>
                            <div class="col-xs-2">
                                <select name="league" class="form-control">
                                    <option value='sba' <?php if ($league == 'sba') echo "selected";?>>SBA</option>
                                    <option value='ncaa' <?php if ($league == 'ncaa') echo "selected";?>>NCAA</option>
                                </select>
                            </div>
                            <div class="col-xs-2">
                                <select name="league2" class="form-control">
                                    <option value='sba' <?php if ($league2 == 'sba') echo "selected";?>>SBA</option>
                                    <option value='ncaa' <?php if ($league2 == 'ncaa') echo "selected";?>>NCAA</option>
                                </select>
                            </div>
                            <div class="col-xs-2">
                                <select name="league3" id="league3" class="form-control" disabled>
                                    <option value='sba' <?php if ($league3 == 'sba') echo "selected";?>>SBA</option>
                                    <option value='ncaa' <?php if ($league3 == 'ncaa') echo "selected";?>>NCAA</option>
                                </select>
                            </div>
                            <div class="col-xs-2">
                                <select name="league4" id="league4" class="form-control" disabled>
                                    <option value='sba' <?php if ($league4 == 'sba') echo "selected";?>>SBA</option>
                                    <option value='ncaa' <?php if ($league4 == 'ncaa') echo "selected";?>>NCAA</option>
                                </select>
                            </div>
                            <div class="col-xs-2">
                                <select name="league5" id="league5" class="form-control" disabled>
                                    <option value='sba' <?php if ($league5 == 'sba') echo "selected";?>>SBA</option>
                                    <option value='ncaa' <?php if ($league5 == 'ncaa') echo "selected";?>>NCAA</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-xs-1" for="type">Type:</label>
                            <div class="col-xs-2">
                                <select name="type" class="form-control">
                                    <option value='season' <?php if ($type == 'season') echo "selected";?>>Regular Season</option>
                                    <option value='playoff' <?php if ($type == 'playoff') echo "selected";?>>Playoff</option>
                                </select>
                            </div>
                            <div class="col-xs-2">
                                <select name="type2" class="form-control">
                                    <option value='season' <?php if ($type2 == 'season') echo "selected";?>>Regular Season</option>
                                    <option value='playoff' <?php if ($type2 == 'playoff') echo "selected";?>>Playoff</option>
                                </select>
                            </div>
                            <div class="col-xs-2">
                                <select name="type3" id="type3" class="form-control" disabled>
                                    <option value='season' <?php if ($type3 == 'season') echo "selected";?>>Regular Season</option>
                                    <option value='playoff' <?php if ($type3 == 'playoff') echo "selected";?>>Playoff</option>
                                </select>
                            </div>
                            <div class="col-xs-2">
                                <select name="type4" id="type4" class="form-control" disabled>
                                    <option value='season' <?php if ($type4 == 'season') echo "selected";?>>Regular Season</option>
                                    <option value='playoff' <?php if ($type4 == 'playoff') echo "selected";?>>Playoff</option>
                                </select>
                            </div>
                            <div class="col-xs-2">
                                <select name="type5" id="type5" class="form-control" disabled>
                                    <option value='season' <?php if ($type5 == 'season') echo "selected";?>>Regular Season</option>
                                    <option value='playoff' <?php if ($type5 == 'playoff') echo "selected";?>>Playoff</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-xs-1" for="type">Player:</label>
                            <div class="col-xs-2">
                                <select name="name" class="form-control"><?=$plist?></select>
                            </div>
                            <div class="col-xs-2">
                                <select name="name2" class="form-control"><?=$plist2?></select>
                            </div>
                            <div class="col-xs-2">
                                <select name="name3" id="name3" class="form-control" disabled><?=$plist3?></select>
                            </div>
                            <div class="col-xs-2">
                                <select name="name4" id="name4" class="form-control" disabled><?=$plist4?></select>
                            </div>
                            <div class="col-xs-2">
                                <select name="name5" id="name5" class="form-control" disabled><?=$plist5?></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-offset-1 col-xs-11">
                                <button type="submit" class="btn btn-default">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="container-fluid">
        <div class="panel panel-primary">
            <div class="panel-heading">Player Comparison</div>
            <div class="panel-body">
                <?=$aecho?>
                <?=$becho?>
                <?=$cecho?>
                <?=$decho?>
                <?=$eecho?>
            </div>
        </div>
    </div>
</div>
<!-- Body End -->

<?php
include $path.'/footer.php';
?>

<script type="text/javascript">
function enableCompare(obj,id)
{
    if (document.getElementById(obj).checked)
    {
        switch(id)
        {
            case 'C':
            document.getElementById('season3').disabled = false;
            document.getElementById('league3').disabled = false;
            document.getElementById('type3').disabled = false;
            document.getElementById('name3').disabled = false;
            break;
            case 'D':
            document.getElementById('season4').disabled = false;
            document.getElementById('league4').disabled = false;
            document.getElementById('type4').disabled = false;
            document.getElementById('name4').disabled = false;
            break;
            case 'E':
            document.getElementById('season5').disabled = false;
            document.getElementById('league5').disabled = false;
            document.getElementById('type5').disabled = false;
            document.getElementById('name5').disabled = false;
            break;
        }
    }
    else
    {
        switch(id)
        {
            case 'C':
            document.getElementById('season3').disabled = true;
            document.getElementById('league3').disabled = true;
            document.getElementById('type3').disabled = true;
            document.getElementById('name3').disabled = true;
            break;
            case 'D':
            document.getElementById('season4').disabled = true;
            document.getElementById('league4').disabled = true;
            document.getElementById('type4').disabled = true;
            document.getElementById('name4').disabled = true;
            break;
            case 'E':
            document.getElementById('season5').disabled = true;
            document.getElementById('league5').disabled = true;
            document.getElementById('type5').disabled = true;
            document.getElementById('name5').disabled = true;
            break;
        }
    }
}
$(document).ready(function(){
    enableCompare('checkC','C');
    enableCompare('checkD','D');
    enableCompare('checkE','E');
});
</script>