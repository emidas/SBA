<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
$id = '';
$approvepm = '';
$pid = '';
$pname = '';
$oldlgatty = Array();
$attyquerystring = '';
$oldattyquerystring = '';
for($x=0;$x<count($lgatty);$x++)
{
    if($x == 0)
    {
        $attyquerystring .= "uu.`".$lgatty[$x]."`";
        $oldattyquerystring .= "uu.`".$lgatty[$x]."_old`";
        array_push($oldlgatty,$lgatty[$x]."_old");
    }
    else
    {
        $attyquerystring .= ",uu.`".$lgatty[$x]."`";
        $oldattyquerystring .= ",uu.`".$lgatty[$x]."_old`";
        array_push($oldlgatty,$lgatty[$x]."_old");
    }
}
if(isset($_POST['id']))
{
    $id = sanitize($con,$_POST['id']);
    $approvepm = sanitize($con,$_POST['approvepm']);
    $pid = sanitize($con,$_POST['pid']);
    $pname = sanitize($con,$_POST['pname']);
}
else
{
    header("location:update_dashboard.php");
    exit();
}
$pm = str_replace('\r\n' , '<br>', $approvepm);
$pm = str_replace('\r' , '<br>', $pm);
$pm = str_replace('\n' , '<br>', $pm);
$pm = stripslashes($pm);
$user = $_SESSION['user'];
$mid = $_SESSION['memberid'];

if($hostlg=='SBA')
{
    $sql = "SELECT $attyquerystring,$oldattyquerystring,u.earn_tpe,p.bank,uu.3ps_toggle,u.status,u.cost_tpe FROM players p INNER JOIN player_updates u ON p.id=u.pid_fk INNER JOIN player_updates_update uu ON u.id=uu.puid_fk WHERE puid_fk='$id'";
}
else if($hostlg=='EFL')
{
    $sql = "SELECT $attyquerystring,$oldattyquerystring,u.earn_tpe,p.bank,u.status,u.cost_tpe FROM players p INNER JOIN player_updates u ON p.id=u.pid_fk INNER JOIN player_updates_update uu ON u.id=uu.puid_fk WHERE puid_fk='$id'";
}
$result = mysqli_query($con,$sql);
$concat = '';
while($s = mysqli_fetch_array($result))
{
    $status = $s['status'];
    if($status > 1)
    {
        header("location:update_dashboard.php");
        exit();
    }
    for($f=0;$f<count($lgatty);$f++)
    {
        if($s[$oldlgatty[$f]] == $s[$lgatty[$f]])
        {
            $concat .= '';
        }
        else
        {
            $concat .= "1";
        }
    }
    if($concat == '')
    {
        $sql = "UPDATE player_updates SET status = '3',approved=now(),approver=\"$user\",cleared=now() WHERE id = '$id'";
        mysqli_query($con,$sql);
    }
    else
    {
        $sql = "UPDATE player_updates SET status = '2',approved=now(),approver=\"$user\" WHERE id = '$id'";
        mysqli_query($con,$sql);
    }
    if($hostlg == 'SBA')
    {
        $ins=$s['ins'];
        $jps=$s['jps'];
        $ft=$s['ft'];
        $tps=$s['3ps'];
        $han=$s['han'];
        $pas=$s['pas'];
        $orb=$s['orb'];
        $drb=$s['drb'];
        $psd=$s['psd'];
        $prd=$s['prd'];
        $stl=$s['stl'];
        $blk=$s['blk'];
        $fl=$s['fl'];
        $qkn=$s['qkn'];
        $str=$s['str'];
        $jmp=$s['jmp'];
        $tpe=$s['earn_tpe'];
        $cost=$s['cost_tpe'];
        $bank=$s['bank'];
        $banked = ($bank + $tpe - $cost);
        $tpstoggle = $s['3ps_toggle'];
    }
    if($hostlg=='SBA')
    {
        $sql = "UPDATE players
        SET ins='$ins',
            jps='$jps',
            ft='$ft',
            3ps='$tps',
            han='$han',
            pas='$pas',
            orb='$orb',
            drb='$drb',
            psd='$psd',
            prd='$prd',
            stl='$stl',
            blk='$blk',
            fl='$fl',
            qkn='$qkn',
            str='$str',
            jmp='$jmp',
            tpe=tpe+$tpe,
            ape=ape+$tpe,
            bank='$banked',
            3ps_toggle='$tpstoggle'
        WHERE id='$pid'";
    }
    else if($hostlg=='EFL')
    {
        $tpe=$s['earn_tpe'];
        $cost=$s['cost_tpe'];
        $bank=$s['bank'];
        $banked = ($bank + $tpe - $cost);
        $stmt = $conn->prepare("UPDATE players SET str=:str,agi=:agi,arm=:arm,`int`=:inti,acc=:acc,tkl=:tkl,spd=:spd,han=:han,rbk=:rbk,pbk=:pbk,kdi=:kdi,kac=:kac,tpe=tpe+:tpe,ape=ape+:tpe,bank=:banked WHERE id=:pid");
        $stmt->execute([':pid' => $pid,':str' => $s['str'],':agi' => $s['agi'],':arm' => $s['arm'],':inti' => $s['int'],':acc' => $s['acc'],':tkl' => $s['tkl'],':spd' => $s['spd'],':han' => $s['han'],':rbk' => $s['rbk'],':pbk' => $s['pbk'],':kdi' => $s['kdi'],':kac' => $s['kac'],':tpe' => $tpe,':banked' => $banked]);
    }
    mysqli_query($con,$sql);
}

$to = array();
$tosql = "
SELECT memberid 
FROM auth_user a
INNER JOIN players p ON a.ID = p.user_fk
INNER JOIN player_updates u ON p.id = u.pid_fk
WHERE u.id = '$id' ";
$toresult = mysqli_query($con,$tosql);
while($torow = mysqli_fetch_array($toresult))
{
    array_push($to,$torow['memberid']);
}
$curl_post_data = array(
    'from' => $mid,
    'to' => $to,
    'title' => "Update Approved",
    'body' => "<h1>".$hostlg." Online</h1><p>Your update for ".$pname." has been approved by ".$user.". Optional message from your updater:<br><br>".$pm."</p>"
);
createMessage($curl_post_data);
header("location:update_dashboard.php");
exit();
?>