<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
$user=$_SESSION['user'];

$league = '';
$type = '';
$stat = '';
if(isset($_POST['team']))
{$team = $_POST['team'];} else {$team = '5';}
if(isset($_POST['category']))
{$category = $_POST['category'];} else {$category = 'GameAssists';}

$statarr = array('Minutes','FGA','FGM','3PA','3PM','FTA','FTM','OffensiveRebounds','Rebounds','Assists','Steals','Blocks','Turnovers','Points','Fouls','PlusMinus','DQ','DoubleDoubles','TripleDoubles','TS%','PPS','PPS(Real)');
$secho = '';
$echo = '';
$ssize = count($statarr);
for ($p=0;$p<$ssize;$p++)
{
    if($stat == $statarr[$p]) {$echo = "selected";} else {$echo = "";}
    $secho .= "<option value = '".$statarr[$p]."' $echo>".$statarr[$p]."</option>";
}
?>

<!-- Body Start -->
<body>
<div id="wrapper" style="position:relative;top:0;left:0;">
    <?php include $path.'/includes/nav.php'; ?>
    <div class="container">
        <?=HeaderImagePrint($hostlg,$_SESSION['style'])?>
        <div class="panel panel-primary">
            <div class="panel-heading">Player Leaders</div>
            <div class="panel-body">
                <div class="panel-group">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Player Leaders Filters</div>
                        <div class="panel-body">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>League</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>League</span>
                                <select name="league" id="league" class="form-control">
                                    <option value='sba' <?php if ($league == 'sba') echo "selected";?>>SBA</option>
                                    <option value='ncaa' <?php if ($league == 'ncaa') echo "selected";?>>NCAA</option>
                                    <option value='fiba' <?php if ($league == 'fiba') echo "selected";?>>FIBA</option>
                                </select>
                            </div>
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>Category</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>Category</span>
                                <select name="category" id="category" class="form-control">
                                    <option value='game' <?php if ($category == 'game') echo "selected";?>>Game</option>
                                    <option value='season' <?php if ($category == 'season') echo "selected";?>>Season</option>
                                    <option value='career' <?php if ($category == 'career') echo "selected";?>>Career</option>
                                </select>
                            </div>
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>Stat</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>Stat</span>
                                <select name="stat" id="stat" class="form-control">
                                    <?=$secho?>
                                </select>
                            </div>
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>Type</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>Type</span>
                                <select name="type" id="type" class="form-control">
                                    <option value='season' <?php if ($type == 'season') echo "selected";?>>Regular Season</option>
                                    <option value='playoff' <?php if ($type == 'playoff') echo "selected";?>>Playoff</option>
                                </select>
                            </div>
                            <button type="button" class="btn btn-primary col-xs-12" onclick="getPlayerLeaders()">Submit</button>   
                        </div>
                    </div>
                </div>
                <div class="panel-group">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Records</div>
                        <div class="panel-body">
                            <div id="playerleaders">
                                <!--Ajax Call inserts records here -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>       
    </div>
</div>
<!-- Body End -->

<?php
include $path.'/footer.php';
?>
<script>
$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
});
function getPlayerLeaders()
{
    var category = $('#category').val();
    var league = $('#league').val();
    var type = $('#type').val();
    var stat = $('#stat').val();
    $.ajax({url: "../ajax/get_player_leaders.php?category="+category+"&league="+league+"&type="+type+"&stat="+stat, success: function(result){
      $("#playerleaders").html(result);
    }});
}
</script>