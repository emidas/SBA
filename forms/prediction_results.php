<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
$user=$_SESSION['user'];

$system = new System();
$year = $system->get_year();

if(isset($_POST['year']))
{$year = $_POST['year'];} else {}
if(isset($_POST['league']))
{$league = $_POST['league'];} else {$league = 'sba';}
if(isset($_POST['type']))
{$type = $_POST['type'];} else {$type = 'season';}

$yquery = "SELECT year FROM year";
$yresult = mysqli_query($con,$yquery);
$yecho = '';
$echo = '';
while ($r = mysqli_fetch_array($yresult)) {
    if($year == $r['year']) {$echo = "selected";} else {$echo = "";}
    $yecho .= "<option value = '".$r['year']."' $echo>".$r['year']."</option>";
}
?>

<!-- Body Start -->
<body>
<div id="wrapper" style="position:relative;top:0;left:0;">
    <?php include $path.'/includes/nav.php'; ?>
    <div class="container">
        <?=HeaderImagePrint($hostlg,$_SESSION['style'])?>
        <form class="form-horizontal" action='prediction_results.php' method='POST'>
            <div class="panel-group">
                <div class="panel panel-primary">
                    <div class="panel-heading">Prediction Results Filters</div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="year">Set Year:</label>
                            <div class="col-xs-2">
                                <select name="year" class="form-control"><?=$yecho?></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="league">Set League:</label>
                            <div class="col-xs-2">
                                <select name="league" class="form-control">
                                    <option value='sba' <?php if ($league == 'sba') echo "selected";?>>SBA</option>
                                    <option value='sbdl' <?php if ($league == 'sbdl') echo "selected";?>>SBDL</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="type">Set Type:</label>
                            <div class="col-xs-2">
                                <select name="type" class="form-control">
                                    <option value='season' <?php if ($type == 'season') echo "selected";?>>Season</option>
                                    <option value='playoff' <?php if ($type == 'playoff') echo "selected";?>>Playoff</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-default">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <?=printres($con,$year,$league,$type);?>
    </div>
</div>
<!-- Body End -->

<?php
include $path.'/footer.php';
?>
