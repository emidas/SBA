<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
$user=$_SESSION['user'];
$echo = '';

$stable = "";
$system = new System();
$year = $system->get_year();
$year2 = $year + 1;
$year3 = $year + 2;
$cap = $system->get_salary_cap();
$exp_cap = $system->get_predicted_cap();
$stmt = $conn->prepare("SELECT id FROM teams WHERE league='SBA' ORDER BY name ");
$stmt->execute();
$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
foreach ($result as $row)
{
    $team = new Team($row['id']);
    $id = $team->id;
    $tname = $team->name;
    $tabr = $team->abr;
    $roster = $team->roster_size;
    $fillers = $team->roster_size_filler;
    $sal = $team->team_salary;
    $space = $team->cap_space;
    $sal2 = $team->team_salary2;
    $sal3 = $team->team_salary3;
    $stable .= "
    <tr>
        <td><a href='team_page.php?tid=".$id."'><div class=\"hidden-xs\">".$tname."</div><div class=\"hidden-sm hidden-md hidden-lg\">$tabr</div></a></td><td>".$roster."</td><td>".$fillers."</td><td>$".number_format($sal,0)."</td><td>$".number_format($sal2,0)."</td><td>$".number_format($sal3,0)."</td>
    </tr>";
}
$sql = "SELECT t.name as tname, (SELECT count(id) FROM players WHERE team='0' AND active ='1' AND league='SBA') as roster_size, (SELECT count(id) FROM players WHERE team='0' AND active='2' AND league='SBA') as roster_size_filler,sum(min_salary) as salary from players p inner join teams t ON p.team=t.id where p.league = 'SBA' and active = '1' and t.name = 'Free Agent' group by team order by t.name";
$result = mysqli_query($con,$sql);
while($row = mysqli_fetch_array($result))
{
    $stable .= "
    <tr>
        <td><div class=\"hidden-xs\">".$row['tname']."</div><div class=\"hidden-sm hidden-md hidden-lg\">FA</div></td><td>".$row['roster_size']."</td><td>".$row['roster_size_filler']."</td><td>$".number_format($row['salary'],0)."</td><td colspan='2'></td>
    </tr>";
}
$sql = "SELECT count(id) as count,sum(exp_salary) as salary FROM players p WHERE p.league = 'SBDL' AND (exp_salary > 0)";
$result = mysqli_query($con,$sql);
while($row = mysqli_fetch_array($result))
{
    $stable .= "
    <tr>
        <td><div class=\"hidden-xs\">Draftees</div><div class=\"hidden-sm hidden-md hidden-lg\">Dft.</div></td><td>".$row['count']."</td><td></td><td>$".number_format($row['salary'],0)."</td><td colspan='2'></td>
    </tr>";
}
?>

<!-- Body Start -->
<body>
<div id="wrapper" style="position:relative;top:0;left:0;">
    <?php include $path.'/includes/nav.php'; ?>
    <div class="container">
        <?=HeaderImagePrint($hostlg,$_SESSION['style'])?>
        <?=$echo?>
        <div class="panel panel-primary">
            <div class="panel-heading">Team Salaries</div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <th>Team</th>
                            <th>TOT</th>
                            <th>FILL</th>
                            <th>S<?=$year?></th>
                            <th>S<?=$year2?></th>
                            <th>S<?=$year3?></th>
                        </thead>
                        <tbody>
                            <?=$stable?>
                        </tbody>
                        <tfoot>
                            <td colspan="3"></td>
                            <td>$<?=number_format($cap,0)?></td>
                            <td>$<?=number_format($exp_cap,0)?></td>
                            <td></td>
                            <!--<td><?=$count?></td>
                            <td><?=number_format($t_salary,0)?></td>
                            <td><?=number_format($expected_cap,0)?></td>-->
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Body End -->

<?php
include $path.'/footer.php';
?>