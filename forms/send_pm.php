<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/home.php");
}
$toid = '';
$pm = '';
$fromid = '';
$title = '';
if(isset($_POST['toid']))
{
    $toid = sanitize($con,$_POST['toid']);
    $fromid = sanitize($con,$_POST['fromid']);
    $pm = sanitize($con,$_POST['pm']);
    $title = sanitize($con,$_POST['title']);
    $to = array();
    array_push($to,$toid);
}
else if(isset($_POST['masspm']))
{
$to = array();
$tosql = "SELECT u.memberid as toid FROM auth_user u INNER JOIN players p ON u.ID = p.user_fk WHERE p.league = 'ncaa' AND (created_time > date_sub(now(), interval 1 week))";
$toresult = mysqli_query($con,$tosql);
while($torow = mysqli_fetch_array($toresult))
{
    array_push($to,$torow['toid']);
}
$fromid = sanitize($con,$_POST['fromid']);
$pm = sanitize($con,$_POST['pm']);
$title = sanitize($con,$_POST['title']);
}
else
{
    header("location:/home.php");
}
$pm = str_replace('\r\n' , '<br>', $pm);
$pm = str_replace('\r' , '<br>', $pm);
$pm = str_replace('\n' , '<br>', $pm);
$pm = stripslashes($pm);
$curl_post_data = array(
    'from' => $fromid,
    'to' => $to,
    'title' => $title,
    'body' => "<p>".$pm."</p><br><br><p><i>Sent from SBAO</i></p>"
);
createMessage($curl_post_data);
header("location:/home.php");
?>

