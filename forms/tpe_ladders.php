<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
if(isset($_SESSION['user']))
{$user = $_SESSION['user'];} else {}
$system = new System();
$year = $system->get_year();
if(isset($_GET['alert'])) 
{
    if($_GET['alert'] == 'success') 
    {
        $alert = "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Login Successful.</strong></div>";
    }
    else if ($_GET['alert'] == 'adminpermission') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You are not an Admin.</strong></div>";
    }
    else if ($_GET['alert'] == 'simmerpermission') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You are not a Simmer.</strong></div>";
    }
    else if ($_GET['alert'] == 'gmpermission') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You are not a General Manager.</strong></div>";
    }
    else if ($_GET['alert'] == 'adpermission') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You are not an Athletic Director.</strong></div>";
    }
    else if ($_GET['alert'] == 'betapermission') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>No Peeking.</strong></div>";
    }
    else if ($_GET['alert'] == 'playerexists') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You already have an NCAA player.</strong></div>";
    }
}
else
{$alert = '';}

$today = date("m/d/Y");
$days = 7 - date("N");
$deadline = date('m/d/Y', strtotime($today. ' + '.$days.' days'));
$today = date("m/d/Y h:i:s A");

// Submitted Weekly PE Leaders
$submitleader = ''; 
$submitend = date('Y-m-d', strtotime($deadline));
$submitendfor = date('m/d',strtotime($submitend));
$submitbeg = date('Y-m-d', strtotime($deadline. ' - 6 days'));
$submitbegfor = date('m/d',strtotime($submitbeg));
$sql = "SELECT CONCAT(plast,', ',pfirst) as pname, SUM(earn_tpe) as earned
FROM players p
INNER JOIN player_updates u ON p.id=u.pid_fk
WHERE status != '0' AND approved BETWEEN '$submitbeg' AND '$submitend'
GROUP BY p.id
ORDER BY earned DESC LIMIT 10";
$result = mysqli_query($con,$sql);
while($r = mysqli_fetch_array($result))
{
    $submitleader .= "<tr>";
        $submitleader .= "<td>".$r['pname']."</td>";
        $submitleader .= "<td>".$r['earned']."</td>";
    $submitleader .= "</tr>";

}
// Approved Weekly PE Leaders
$weeklyleader = ''; 
$lastend = date('Y-m-d', strtotime($deadline. ' - 7 days'));
$lastendfor = date('m/d',strtotime($lastend));
$lastbeg = date('Y-m-d', strtotime($lastend. ' - 6 days'));
$lastbegfor = date('m/d',strtotime($lastbeg));
$sql = "SELECT CONCAT(plast,', ',pfirst) as pname, SUM(earn_tpe) as earned
FROM players p
INNER JOIN player_updates u ON p.id=u.pid_fk
WHERE status != '0' AND approved BETWEEN '$lastbeg' AND '$lastend'
GROUP BY p.id
ORDER BY earned DESC LIMIT 10";
$result = mysqli_query($con,$sql);
while($r = mysqli_fetch_array($result))
{
    $weeklyleader .= "<tr>";
        $weeklyleader .= "<td>".$r['pname']."</td>";
        $weeklyleader .= "<td>".$r['earned']."</td>";
    $weeklyleader .= "</tr>";

}
// Monthly PE Leaders
$monthlyleader = ''; 
$monthend = date('Y-m-t');
$monthbeg = date('Y-m-01');
$monthendfor = date('m/d',strtotime($monthend));
$monthbegfor = date('m/d',strtotime($monthbeg));
$sql = "SELECT CONCAT(plast,', ',pfirst) as pname, SUM(earn_tpe) as earned
FROM players p
INNER JOIN player_updates u ON p.id=u.pid_fk
WHERE status != '0' AND approved BETWEEN '$monthbeg' AND '$monthend'
GROUP BY p.id
ORDER BY earned DESC LIMIT 10";
$result = mysqli_query($con,$sql);
while($r = mysqli_fetch_array($result))
{
    $monthlyleader .= "<tr>";
        $monthlyleader .= "<td>".$r['pname']."</td>";
        $monthlyleader .= "<td>".$r['earned']."</td>";
    $monthlyleader .= "</tr>";

}
?>

<!-- Body Start -->
<body>
<div id="wrapper" style="position:relative;top:0;left:0;">
    <?php include $path.'/includes/nav.php'; ?>
    <div class="container">
        <?=HeaderImagePrint($hostlg,$_SESSION['style'])?>
        <?=$alert?>
        <div class='row'>
            <div class="panel panel-primary">
                <div class="panel-heading">TPE Ladders</div>
                <div class="panel-body">
                    <div class="row no-gutter">
                        <div class='col-xs-12 col-sm-4'>
                            <div class="table-responsive">
                                <table class="table compact">
                                    <thead>
                                        <tr>
                                            <th colspan='2' class='active'>Submitted TPE <?=$submitbegfor?>-<?=$submitendfor?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?=$submitleader?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class='col-xs-12 col-sm-4'>
                            <div class="table-responsive">
                                <table class="table compact">
                                    <thead>
                                        <tr>
                                            <th colspan='2' class='active'>Approved TPE <?=$lastbegfor?>-<?=$lastendfor?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?=$weeklyleader?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class='col-xs-12 col-sm-4'>
                            <div class="table-responsive">
                                <table class="table compact">
                                    <thead>
                                        <tr>
                                            <th colspan='2' class='active'>Approved TPE <?=$monthbegfor?>-<?=$monthendfor?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?=$monthlyleader?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Body End -->

<?php
include $path.'/footer.php';
mysqli_close($con);
?>
<script>
    $('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>