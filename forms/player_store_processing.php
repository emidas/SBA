<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}

$pid = isset($_GET['pid']) ? sanitize($con,$_GET['pid']) : '';
$package = isset($_GET['package']) ? sanitize($con,$_GET['package']) : '';
if(isset($_GET['pid']) && isset($_GET['package']))
{
    switch ($package):
        case "1":
            $price=7000000; 
            $tpe=22;
            $packagename='Shane Banks Training';
            break;
        case "2":                 
            $price=5500000; 
            $tpe=14;
            $packagename='Blue Dads Training';
            break;
        case "3":                 
            $price=4000000; 
            $tpe=10;
            $packagename='Gregor Clegane Training';
            break;
        case "4":                 
            $price=2500000; 
            $tpe=6;
            $packagename='Nick Catania Training';
            break;
        case "5":                 
            $price=1000000; 
            $tpe=2;
            $packagename='Jack Salt Training';
            break;
        case "6":                 
            $price=500000; 
            $tpe=1;
            $packagename='Keenan Jefferson Training';
            break;
        case "7":                 
            $price=5000000; 
            $tpe=12;
            $packagename='Phillycheese Training';
            break;
        case "8":                 
            $price=3000000; 
            $tpe=7;
            $packagename='Shaka Training';
            break;
        case "9":                 
            $price=2000000; 
            $tpe=5;
            $packagename='Leonidas Training';
            break;
        default:
            $price=0; 
            $tpe=0;
            break;
    endswitch;
    $player = new Player($pid);
    $cash = $player->get_('p_bank');
    if($price > $cash)
    {
        header("location:player_update.php?pid=".$pid."&alert=purchasedenied");
        exit();
    }
    else
    {
        $sql = "SELECT id FROM player_updates WHERE pid_fk='$pid' AND status < '2'";
        $numsult = mysqli_query($con,$sql);
        $num = mysqli_num_rows($numsult);
        if($num > 0)
        {
            $mfa=mysqli_fetch_array($numsult);
            $puid=$mfa['id'];
        }
        else
        {
            $puid=0;
        }
        $player->make_purchase($price,$tpe,$puid);
        $player->add_purchase_history($price,$packagename);
        header("location:player_update.php?pid=".$pid."&alert=purchasesuccessful");
        exit();
    }
}
else
{
    header("location:player_update.php?pid=".$pid."&alert=somethingwentwrong");
}
?>