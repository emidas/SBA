<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
$user=$_SESSION['user'];
// year
$system = new System();
$year = $system->get_year();

$oldlgatty = Array();
$attyquerystring = '';
$oldattyquerystring = '';
for($x=0;$x<count($lgatty);$x++)
{
    if($x == 0)
    {
        $attyquerystring .= "`".$lgatty[$x]."`";
        $oldattyquerystring .= "`".$lgatty[$x]."_old`";
        array_push($oldlgatty,$lgatty[$x]."_old");
    }
    else
    {
        $attyquerystring .= ",`".$lgatty[$x]."`";
        $oldattyquerystring .= ",`".$lgatty[$x]."_old`";
        array_push($oldlgatty,$lgatty[$x]."_old");
    }
}

$query = "SELECT pu.id as puid,p.id,username,CONCAT(pfirst,' ',plast) AS pname,p.league,p.active,$attyquerystring,status,build,c90,c801,c802,c803,c804,c70,t.name as tname,p.tpe,p.bank as old_bank,pu.earn_tpe,pu.bank as new_bank,pu.last_submitted,pu.approved,pu.approver,pu.notes,pu.cost_tpe,pu.earn_tpe,pu.first_submitted
            FROM auth_user a
            INNER JOIN players p ON a.ID=p.user_fk
            INNER JOIN player_updates pu ON p.ID=pu.pid_fk
            INNER JOIN teams t ON p.team = t.id
            WHERE pu.status < 2
            AND p.active > 0
            ORDER BY CASE WHEN p.league='$mainlg' THEN 1
            WHEN p.league='$sublg' THEN 2
            ELSE 3
            END, tname,pname";
$result = mysqli_query($con,$query);
$sba = 0;
$sba3 = 0;
$ncaa = 0;
$ncaa3 = 0;
$echo = '';
$echo2 = '';
$echo3 = '';
$echo4 = '';
$echo5 = '';
$echo6 = '';
$i = 0;
$status = '';
while ($r = mysqli_fetch_array($result))
{
    $pid = $r['id'];
    if($r['status'] == '1')
    {$timestamp = $r['last_submitted'];$timestamp2 = $r['first_submitted'];}
    else
    {$timestamp = '';$timestamp2 = '';}
    $concat = "
    <div class='panel panel-primary'>
        <div class='panel-heading hidden-xs'><h4 class='panel-title'><a data-toggle='collapse' href='#collapse$i'>".$r['pname']."(".$r['username'].") - ".$r['tname']."</a> <span class='pull-right timestamp' style='background-color:transparent;'>First: (".$timestamp2.") | Last: (".$timestamp.")</span></h4></div>
        <div class='panel-heading hidden-sm hidden-md hidden-lg'><h4 class='panel-title'><a data-toggle='collapse' href='#collapse$i'>".$r['pname']."<br>".$r['tname']."<br><span style='background-color:transparent;'>First: (".$timestamp2.")</span><br><span style='background-color:transparent;'>Last: (".$timestamp.")</span></a></h4></div>
        <div class='panel-collapse collapse' id='collapse$i'>
            <div class='panel-body'>
                <ul class='nav nav-tabs' style='border-bottom:0px;'>
                    <li class='active'><a data-toggle='tab' href='#current$i'>Current</a></li>
                    <li><a data-toggle='tab' href='#history$i' onclick=\"updatehistory('".$pid."','".$i."');\">History</a></li>
                </ul>
                <div class='tab-content'>
                    <div id='current$i' class='tab-pane fade in active'>
                        <a href='player_page.php?pid=".$r['id']."' class='btn btn-primary col-xs-12 hidden-sm hidden-md hidden-lg' target='_blank'>Player Page</a>";
                        $puid = $r['puid'];
                        $pname = $r['pname'];
                        if($r['status'] != '2')
                        {
                            $concat .= "<div class='table-responsive'><table class='table table-bordered'>
                            <thead>
                                <tr >
                                    <th>Task</th>
                                    <th>Week Ending</th>
                                    <th class='hidden-xs'>Points Earned</th>
                                    <th class='text-center hidden-sm hidden-md hidden-lg'>P.E.</th>
                                </tr>
                            </thead>
                            <tbody>";
                                $query = "SELECT task,week,link,pe FROM player_updates_tasks put WHERE puid_fk = '$puid'";
                                $result2 = mysqli_query($con,$query);
                                $t = 1;
                                while($s = mysqli_fetch_array($result2))
                                {
                                    $concat .= "
                                            <tr id='addr$t'>
                                                <td><a href='".$s['link']."' style='text-decoration:underline;' target='_blank'>".$s['task']."</a></td>
                                                <td>".$s['week']."</td>
                                                <td>".$s['pe']."</td>
                                            </tr>
                                    ";
                                    $t++;
                                }
                                $concat .= "
                            </tbody>
                        </table></div>";
                        }
                        $concat .= "<table class='table'>
                            <thead>
                                <tr>
                                    <th class='hidden-xs'>Attribute Name</th>
                                    <th class='hidden-sm hidden-md hidden-lg'>Attribute</th>
                                    <th>Current</th>
                                    <th>Now</th>
                                </tr>
                            </thead>
                            <tbody>";
                        $sql = "SELECT $attyquerystring,$oldattyquerystring FROM player_updates_update WHERE puid_fk='$puid'";
                        $result3 = mysqli_query($con,$sql);
                        $attconcat = '';
                        while($w = mysqli_fetch_array($result3))
                        {
                            for($f=0;$f<count($lgatty);$f++)
                            {
                                if($r['build'] == 'Standard')
                                {
                                    if($r['c90'] == $lgatty[$f])
                                    {
                                        $max = " (MAX:90)";
                                    }
                                    else if($r['c801'] == $lgatty[$f] || $r['c802'] == $lgatty[$f] || $r['c803'] == $lgatty[$f] || $r['c804'] == $lgatty[$f])
                                    {
                                        $max = " (MAX:80)";
                                    }
                                    else if($r['c70'] == $lgatty[$f])
                                    {
                                        $max = " (MAX:70)";
                                    }
                                    else
                                    {
                                        $max = "";
                                    }
                                }
                                else
                                {
                                    $max = "";
                                }
                                if($w[$oldlgatty[$f]] == $w[$lgatty[$f]])
                                {
                                    $concat .= '';
                                }
                                else
                                {
                                    $concat .= "
                                    <tr>
                                        <td>".$lgatty[$f]."".$max."</td>
                                        <td>".$w[$oldlgatty[$f]]."</td>
                                        <td>".$w[$lgatty[$f]]."</td>
                                    </tr>";
                                }
                            }
                        }
                        if($r['status'] != '2')
                        {
                            $tpe=$r['earn_tpe'];
                            $cost=$r['cost_tpe'];
                            $bank=$r['old_bank'];
                            $banked = ($bank + $tpe - $cost);
                            $new_tpe = $r['tpe'] + $r['earn_tpe'];
                            $concat .= "
                                        <tr>
                                            <td>TPE</td>
                                            <td>".$r['tpe']."</td>
                                            <td>".$new_tpe."</td>
                                        </tr>
                                        <tr>
                                            <td>Bank</td>
                                            <td>".$r['old_bank']."</td>
                                            <td>".$banked."</td>
                                        </tr>";
                        }
                        $concat .= "
                            </tbody>
                        </table>";
                        if($hostlg == 'SBA')
                        {
                            $sql = "SELECT 3ps_toggle FROM player_updates_update WHERE puid_fk='$puid'";
                            $d = mysqli_fetch_array(mysqli_query($con,$sql));
                            $tpstoggle = $d['3ps_toggle'];
                            if($tpstoggle == '1')
                            {
                                $tpsins = '';
                            }
                            else
                            {
                                $tpsins = "Please ensure this player's 3PT is set to 0.";
                            }
                        }
                        else
                        {
                            $tpsins = '';
                        }
                        $concat .= "<div class='input-group col-xs-12'><span class='input-group-addon' style='width:120px;text-align:left;'>Notes</span><input type='text' class='form-control' readonly='readonly' value=\"".$r['notes']." ".$tpsins."\"></div><br>";
                        if($r['status'] == '1' && ($_SESSION['isupdater'] == '1' || $_SESSION['issimmer'] == '1' || $_SESSION['isadmin'] == '1'))
                        {
                        $concat .= "<div class='form-group'>
                            <div class='col-xs-12'>
                                <a href='#approve' data-toggle='modal' onclick=\"approveupdate('".$r['id']."','".$puid."','".mysqli_real_escape_string($con,$pname)."')\" class='btn btn-primary'>Approve</a>
                                <a href='#deny' data-toggle='modal' onclick=\"denyupdate('".$puid."','".mysqli_real_escape_string($con,$pname)."')\" class='btn btn-danger'>Deny</a>
                            </div>
                        </div>";
                        }
                        else if($r['status'] == '0' && ($_SESSION['isupdater'] == '1' || $_SESSION['issimmer'] == '1' || $_SESSION['isadmin'] == '1'))
                        {
                            $concat .= "<div class='form-group'>
                            <div class='col-sm-offset-1 col-sm-10'>
                                <a href='#reset' data-toggle='modal' onclick=\"resetupdate('".$puid."','".mysqli_real_escape_string($con,$pname)."')\" class='btn btn-danger'>Reset</a>
                                <a href='#delete' data-toggle='modal' onclick=\"deleteupdate('".$pid."','".$puid."','".mysqli_real_escape_string($con,$pname)."'s)\" class='btn btn-danger'>Delete</a>
                            </div>
                        </div>";
                        }
                        else
                        {

                        }
            $concat .= "
                    </div>
                    <div id='history$i' class='tab-pane fade'></div>
                </div>
            </div>
        </div>
    </div>";
    $i++;
    if($r['status'] == '1')
    {
        if ($r['league'] == $mainlg)
        {
            $echo .= $concat;
            $sba++;
        }
        else
        {
            $echo2 .= $concat;
            $ncaa++;
        }
    }
    else
    {
        if ($r['league'] == $mainlg)
        {
            $echo5 .= $concat;
            $sba3++;
        }
        else
        {
            $echo6 .= $concat;
            $ncaa3++;
        }
    }
}
$echo = '<div class="panel panel-default">
            <div class="panel-heading"><h4 class="panel-title"><a data-toggle="collapse" href="#processingsba">'.$mainlg.' ('.$sba.' New)</a></h4></div><div class="panel-collapse collapse" id="processingsba">
            <div class="panel-body">' . $echo;
$echo2 = '<div class="panel panel-default">
            <div class="panel-heading"><h4 class="panel-title"><a data-toggle="collapse" href="#processingncaa">'.$sublg.' ('.$ncaa.' New)</a></h4></div><div class="panel-collapse collapse" id="processingncaa">
            <div class="panel-body">' . $echo2;
$echo5 = '<div class="panel panel-default">
            <div class="panel-heading"><h4 class="panel-title"><a data-toggle="collapse" href="#deniedsba">'.$mainlg.' ('.$sba3.' New)</a></h4></div><div class="panel-collapse collapse" id="deniedsba">
            <div class="panel-body">' . $echo5;
$echo6 = '<div class="panel panel-default">
            <div class="panel-heading"><h4 class="panel-title"><a data-toggle="collapse" href="#deniedncaa">'.$sublg.' ('.$ncaa3.' New)</a></h4></div><div class="panel-collapse collapse" id="deniedncaa">
            <div class="panel-body">' . $echo6;
$echo .= "</div></div></div>";
$echo2 .= "</div></div></div>";
$echo5 .= "</div></div></div>";
$echo6 .= "</div></div></div>";
?>

<!-- Body Start -->
<body>
<div id="wrapper" style="position:relative;top:0;left:0;">
    <?php include $path.'/includes/nav.php'; ?>
    <div class="container">
        <?=HeaderImagePrint($hostlg,$_SESSION['style'])?>
        <div class="panel-group">
            <div class="panel panel-primary">
                <div class="panel-heading">Update Dashboard</div>
                <div class="panel-body">
                    <div class="panel-group">
                        <div class="panel panel-primary">
                            <div class="panel-heading"><h4 class="panel-title">Submitted</h4></div>
                            <div class="panel-body">
                                <?=$echo?>
                                <?=$echo2?>
                            </div>
                        </div>
                    </div>
                    <div class="panel-group">
                        <div class="panel panel-primary">
                            <div class="panel-heading"><h4 class="panel-title">Denied</h4></div>
                            <div class="panel-body">
                                <?=$echo5?>
                                <?=$echo6?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class='modal fade' id='approve' role='dialog'>
            <div class='modal-dialog modal-lg'>
                <form method='POST' action='approve_update.php'>
                    <div class='modal-content'>
                        <div class='modal-header'>
                            <button type='button' class='close' data-dismiss='modal'>&times;</button>
                            <h4 class='modal-title'>Approve Update</h4>
                        </div>
                        <div class='modal-body'>
                            <p>This will approve this players' update. Once this update is approved, this update will move to the Approved section for the Leagues' Simmer to update in the file. You should use the box below to send a PM to the player letting them know you have approved this update. Are you sure you want to do this?</p>
                            <textarea class='form-control' rows='5' name='approvepm'></textarea>
                            <input type='text' hidden='hidden' name='id' id='approveid' value="">
                            <input type='text' hidden='hidden' name='pid' id='approvepid' value="">
                            <input type='text' hidden='hidden' name='pname' id='approvepname' value="">
                        </div>
                        <div class='modal-footer'>
                            <button type='submit' class='btn btn-danger'>Approve Update</button>
                            <button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class='modal fade' id='deny' role='dialog'>
            <div class='modal-dialog modal-lg'>
                <form method='POST' action='deny_update.php'>
                    <div class='modal-content'>
                        <div class='modal-header'>
                            <button type='button' class='close' data-dismiss='modal'>&times;</button>
                            <h4 class='modal-title'>Deny Update</h4>
                        </div>
                        <div class='modal-body'>
                            <p>This will deny this players' update. You should only do this if there is an error with the update that the player will need to correct. Once this update is denied, this update will move to the Denied section until the player corrects the issue and re-submits their update. You should use the box below to send a PM to the player explaining why you are denying this update. Are you sure you want to do this?</p>
                            <textarea class='form-control' rows='5' name='denypm'></textarea>
                            <input type='text' hidden='hidden' name='id' id='denyid' value="">
                            <input type='text' hidden='hidden' name='pname' id='denypname' value="">
                        </div>
                        <div class='modal-footer'>
                            <button type='submit' class='btn btn-danger'>Deny Update</button>
                            <button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class='modal fade' id='reset' role='dialog'>
            <div class='modal-dialog modal-lg'>
                <form method='POST' action='reset_update.php'>
                    <div class='modal-content'>
                        <div class='modal-header'>
                            <button type='button' class='close' data-dismiss='modal'>&times;</button>
                            <h4 class='modal-title'>Reset Update</h4>
                        </div>
                        <div class='modal-body'>
                            <p>This will reset this players' update in the system back to Submitted, and allow the player to change it once again. You can use the box below to send a PM to the player letting them know you have reset this update. Are you sure you want to do this?</p>
                            <textarea class='form-control' rows='5' name='resetpm'></textarea>
                            <input type='text' hidden='hidden' name='id' id='resetid' value="">
                            <input type='text' hidden='hidden' name='pname' id='resetpname' value="">
                        </div>
                        <div class='modal-footer'>
                            <button type='submit' class='btn btn-danger'>Reset Update</button>
                            <button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class='modal fade' id='delete' role='dialog'>
            <div class='modal-dialog modal-lg'>
                <form method='POST' action='delete_update.php'>
                    <div class='modal-content'>
                        <div class='modal-header'>
                            <button type='button' class='close' data-dismiss='modal'>&times;</button>
                            <h4 class='modal-title'>Delete Update</h4>
                        </div>
                        <div class='modal-body'>
                            <p>This will delete this players' update in the system. You can use the box below to send a PM to the player letting them know you have deleted this update. Are you sure you want to do this?</p>
                            <textarea class='form-control' rows='5' name='deletepm'></textarea>
                            <input type='text' hidden='hidden' name='puid' id='deletepuid' value=''>
                            <input type='text' hidden='hidden' name='pid' id='deletepid' value=''>
                            <input type='text' hidden='hidden' name='pname' id='deletepname' value=''>
                        </div>
                        <div class='modal-footer'>
                            <button type='submit' class='btn btn-danger'>Delete Update</button>
                            <button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Body End -->

<?php
include $path.'/footer.php';
?>

<script>
function updatehistory(pid,i) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
     document.getElementById("history"+i).innerHTML = this.responseText;
    }
  };
  xhttp.open("GET", "../ajax/update_dashboard_history.php?pid="+pid, true);
  xhttp.send();
}
function approveupdate(pid,puid,pname)
{
    $('#approveid').val(puid);
    $('#approvepid').val(pid);
    $('#approvepname').val(pname);
}
function denyupdate(puid,pname)
{
    $('#denyid').val(puid);
    $('#denypname').val(pname);
}
function resetupdate(puid,pname)
{
    $('#resetid').val(puid);
    $('#resetpname').val(pname);
}
function deleteupdate(pid,puid,pname)
{
    $('#deletepuid').val(puid);
    $('#deletepid').val(pid);
    $('#deletepname').val(pname);
}
</script>