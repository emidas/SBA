<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
if($_SESSION['issimmer'] != '1'){
header("location:/home.php?alert=betapermission");
}
$id = '';
$clearpm = '';
$pname = '';
if(isset($_POST['id']))
{
    $id = sanitize($con,$_POST['id']);
    $clearpm = sanitize($con,$_POST['clearpm']);
    $pname = sanitize($con,$_POST['pname']);
}
else
{
    header("location:simmer_dashboard.php");
}
$pm = str_replace('\r\n' , '<br>', $clearpm);
$pm = str_replace('\r' , '<br>', $pm);
$pm = str_replace('\n' , '<br>', $pm);
$pm = stripslashes($pm);
$sql = "UPDATE player_updates SET status = '3',cleared=now() WHERE id = '$id'";
mysqli_query($con,$sql);

if($_POST['clearpm'] != '')
{
    $clearpm = sanitize($con,$_POST['clearpm']);
    $to = array();
    $tosql = "
    SELECT memberid 
    FROM auth_user a
    INNER JOIN players p ON a.ID = p.user_fk
    INNER JOIN player_updates u ON p.id = u.pid_fk
    WHERE u.id = '$id' ";
    $toresult = mysqli_query($con,$tosql);
    while($torow = mysqli_fetch_array($toresult))
    {
        array_push($to,$torow['memberid']);
    }
    $curl_post_data = array(
        'from' => '2574',
        'to' => $to,
        'title' => "Update Cleared",
        'body' => "<h1>SBA Online</h1><p>Your update for ".$pname." has been cleared by a simmer and your player is now updated in the file. Optional message from your simmer:<br><br>".$pm."</p>"
    );
    createMessage($curl_post_data);
}

header("location:simmer_dashboard.php");
?>