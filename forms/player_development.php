<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
$user = $_SESSION['user'];
$mid = $_SESSION['memberid'];
if(isset($_GET['pid']))
{
    $pid = sanitize($con,$_GET['pid']);
    $sql = "SELECT id FROM forms WHERE form='rollover' AND isactive='1'";
    $numsult = mysqli_query($con,$sql);
    $num = mysqli_num_rows($numsult);
    if($num > 0)
    {
        header("location:/forms/player_update.php?pid=".$pid."&alert=rollover");
        exit();
    }
    $week = date("m/d/Y", strtotime('Sunday'));
    $sql = "SELECT id FROM players WHERE id='$pid' AND league='SBDL'";
    $numsult = mysqli_query($con,$sql);
    $num = mysqli_num_rows($numsult);
    if($num == '1')
    {
        $sql = "SELECT id FROM player_development WHERE p_fk='$pid' AND week='$week'";
        $numsult = mysqli_query($con,$sql);
        $num = mysqli_num_rows($numsult);
        if($num == 0)
        {
            $sql = "INSERT INTO player_development (p_fk,week) VALUES ('$pid','$week')";
            mysqli_query($con,$sql);
            $sql = "UPDATE players SET tpe=tpe+2,ape=ape+2,bank=bank+2 WHERE id='$pid'";
            mysqli_query($con,$sql);
            // Player Update Record
            $sql = "INSERT INTO player_updates (pid_fk,earn_tpe,cost_tpe,status,approver,approved,cleared) VALUES ('$pid','2','0','3','Skynet',NOW(),NOW())";
            mysqli_query($con,$sql);
            $puid = mysqli_insert_id($con);
            $sql = "INSERT INTO player_updates_tasks (puid_fk,task,week,link,pe) VALUES ('$puid','SBDL Development','$week','','2')";
            mysqli_query($con,$sql);
            $sql2 = "SELECT * FROM players WHERE id='$pid'";
            $s = mysqli_fetch_array(mysqli_query($con,$sql2));
            $ins=$s['ins'];
            $jps=$s['jps'];
            $ft=$s['ft'];
            $tps=$s['3ps'];
            $han=$s['han'];
            $pas=$s['pas'];
            $orb=$s['orb'];
            $drb=$s['drb'];
            $psd=$s['psd'];
            $prd=$s['prd'];
            $stl=$s['stl'];
            $blk=$s['blk'];
            $fl=$s['fl'];
            $qkn=$s['qkn'];
            $str=$s['str'];
            $jmp=$s['jmp'];
            $tpstoggle=$s['3ps_toggle'];
            $sql = "INSERT INTO player_updates_update (puid_fk,ins,jps,ft,3ps,han,pas,orb,drb,psd,prd,stl,blk,fl,qkn,str,jmp,ins_old,jps_old,ft_old,3ps_old,han_old,pas_old,orb_old,drb_old,psd_old,prd_old,stl_old,blk_old,fl_old,qkn_old,str_old,jmp_old,3ps_toggle) VALUES ('$puid','$ins','$jps','$ft','$tps','$han','$pas','$orb','$drb','$psd','$prd','$stl','$blk','$fl','$qkn','$str','$jmp','$ins','$jps','$ft','$tps','$han','$pas','$orb','$drb','$psd','$prd','$stl','$blk','$fl','$qkn','$str','$jmp','$tpstoggle')";
            mysqli_query($con,$sql);
            header("location:/forms/player_update.php?pid=".$pid."&alert=success");
            exit();
        }
        else
        {
            $sql = "INSERT INTO cheaters (p_fk,task) VALUES ('$pid','Player Development')";
            mysqli_query($con,$sql);
            header("location:/forms/player_update.php?pid=".$pid."&alert=cheater");
            exit();
        }
    }
    else
    {
        $sql = "INSERT INTO cheaters (p_fk,task) VALUES ('$pid','Player Development')";
        mysqli_query($con,$sql);
        header("location:/forms/player_update.php?pid=".$pid."&alert=cheater");
        exit();
    }
}
else
{
    header("location:javascript://history.go(-1)");
    exit();
}
?>
