<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
$id = '';
$approvepm = '';
if(isset($_POST['id']))
{
    $id = sanitize($con,$_POST['id']);
    $approvepm = sanitize($con,$_POST['approvepm']);
}
else
{
    header("location:simmer_dashboard.php");
}
$pm = str_replace('\r\n' , '<br>', $approvepm);
$pm = str_replace('\r' , '<br>', $pm);
$pm = str_replace('\n' , '<br>', $pm);
$pm = stripslashes($pm);
$sql = "UPDATE coach_updates SET status = '2' WHERE id = '$id'";
mysqli_query($con,$sql);
$sql = "SELECT cid_fk FROM coach_updates WHERE id='$id'";
$cid = mysqli_fetch_array(mysqli_query($con,$sql));
$cid = $cid['cid_fk'];
$sql = "SELECT * FROM coach_updates cu INNER JOIN coach_updates_update cuu ON cu.id=cuu.cuid_fk WHERE cu.id = '$id'";
$result = mysqli_query($con,$sql);
while($r = mysqli_fetch_array($result))
{
    $pace = $r['pace'];
    $motion = $r['motion'];
    $tpuse = $r['tpuse'];
    $focus = $r['focus'];
    $crasho = $r['crasho'];
    $setprimary = $r['setprimary'];
    $primaryd = $r['primaryd'];
    $setsecondary = $r['setsecondary'];
    $secondd = $r['secondd'];
    $fullcourt = $r['fullcourt'];
    $crashd = $r['crashd'];
    $pnamec = isset($r['pnamec']) ? sanitize($con,$r['pnamec']) : '';
    $pnamepf = sanitize($con,$r['pnamepf']);
    $pnamesf = sanitize($con,$r['pnamesf']);
    $pnamesg = sanitize($con,$r['pnamesg']);
    $pnamepg = sanitize($con,$r['pnamepg']);
    $pname6 = sanitize($con,$r['pname6']);
    $pname7 = sanitize($con,$r['pname7']);
    $pname8 = sanitize($con,$r['pname8']);
    $pname9 = sanitize($con,$r['pname9']);
    $pname10 = sanitize($con,$r['pname10']);
    $pname11 = sanitize($con,$r['pname11']);
    $pname12 = sanitize($con,$r['pname12']);
    $posc = sanitize($con,$r['posc']);
    $pospf = sanitize($con,$r['pospf']);
    $possf = sanitize($con,$r['possf']);
    $possg = sanitize($con,$r['possg']);
    $pospg = sanitize($con,$r['pospg']);
    $pos6 = sanitize($con,$r['pos6']);
    $pos7 = sanitize($con,$r['pos7']);
    $pos8 = sanitize($con,$r['pos8']);
    $pos9 = sanitize($con,$r['pos9']);
    $pos10 = sanitize($con,$r['pos10']);
    $pos11 = sanitize($con,$r['pos11']);
    $pos12 = sanitize($con,$r['pos12']);
    $minc = sanitize($con,$r['minc']);
    $minpf = sanitize($con,$r['minpf']);
    $minsf = sanitize($con,$r['minsf']);
    $minsg = sanitize($con,$r['minsg']);
    $minpg = sanitize($con,$r['minpg']);
    $min6 = sanitize($con,$r['min6']);
    $min7 = sanitize($con,$r['min7']);
    $min8 = sanitize($con,$r['min8']);
    $min9 = sanitize($con,$r['min9']);
    $min10 = sanitize($con,$r['min10']);
    $min11 = sanitize($con,$r['min11']);
    $min12 = sanitize($con,$r['min12']);
    $key1 = sanitize($con,$r['key1']);
    $key2 = sanitize($con,$r['key2']);
    $key3 = sanitize($con,$r['key3']);
}
$query = "UPDATE coaches SET pace='$pace',motion='$motion',tpuse='$tpuse',focus='$focus',crasho='$crasho',setprimary='$setprimary',primaryd='$primaryd',setsecondary='$setsecondary',secondd='$secondd',fullcourt='$fullcourt',crashd='$crashd',pnamec='$pnamec',pnamepf='$pnamepf',pnamesf='$pnamesf',pnamesg='$pnamesg',pnamepg='$pnamepg',pname6='$pname6',pname7='$pname7',pname8='$pname8',pname9='$pname9',pname10='$pname10',pname11='$pname11',pname12='$pname12',posc='$posc',pospf='$pospf',possf='$possf',possg='$possg',pospg='$pospg',pos6='$pos6',pos7='$pos7',pos8='$pos8',pos9='$pos9',pos10='$pos10',pos11='$pos11',pos12='$pos12',minc='$minc',minpf='$minpf',minsf='$minsf',minsg='$minsg',minpg='$minpg',min6='$min6',min7='$min7',min8='$min8',min9='$min9',min10='$min10',min11='$min11',min12='$min12',key1='$key1',key2='$key2',key3='$key3' WHERE id='$cid'";
mysqli_query($con,$query);
    

$to = array();
$tosql = "
SELECT memberid 
FROM auth_user a
INNER JOIN coaches c ON a.username = c.name
INNER JOIN coach_updates cu ON c.id = cu.cid_fk
WHERE cu.id = '$id' ";
$toresult = mysqli_query($con,$tosql);
while($torow = mysqli_fetch_array($toresult))
{
    array_push($to,$torow['memberid']);
}
$curl_post_data = array(
    'from' => '2574',
    'to' => $to,
    'title' => "Strategy Approved",
    'body' => "<h1>".$hostlg." Online</h1><p>Your strategy has been approved and input into the file. Optional message from your updater:<br><br>".$pm."</p>"
);
createMessage($curl_post_data);
header("location:simmer_dashboard.php");
?>

