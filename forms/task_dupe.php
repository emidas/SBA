<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
$pid = '';
$pid2 = '';
$task = '';
$week = '';
$link = '';
$pe = '';
$notes = '';
$costpe = 0;
if(isset($_GET['pid']) && isset($_GET['puid']) && isset($_GET['type']))
{
    $pid = sanitize($con,$_GET['pid']);
    $old_puid = sanitize($con,$_GET['puid']);
    $type = sanitize($con,$_GET['type']);
    if($type == 2)
    {
        $sql = "SELECT earn_tpe,notes FROM player_updates WHERE id='$old_puid'";
        $result = mysqli_query($con,$sql);
        while($r = mysqli_fetch_array($result))
        {
            $pe = $r['earn_tpe'];
            $notes = $r['notes'];
        }
    }
    else
    {
        header("location:javascript://history.go(-1)");
        exit();
    }
}
else if(isset($_POST['pid']))
{
    $pid = sanitize($con,$_POST['pid']);
    $pid2 = sanitize($con,$_POST['pid2']);
    $task = sanitize($con,$_POST['task']);
    $week = sanitize($con,$_POST['week']);
    $link = sanitize($con,$_POST['link']);
    $pe = sanitize($con,$_POST['pe']);
}
else if(isset($_GET['pid']))
{
    $pid = sanitize($con,$_GET['pid']);
    $pid2 = sanitize($con,$_GET['pid2']);
    $task = sanitize($con,$_GET['task']);
    $week = sanitize($con,$_GET['week']);
    $link = sanitize($con,$_GET['link']);
    $pe = sanitize($con,$_GET['pe']);
}
else
{
    header("location:javascript://history.go(-1)");
    exit();
}
$sql = "SELECT id FROM player_updates WHERE pid_fk='$pid' AND (status < 2)";
$numpuid = mysqli_num_rows(mysqli_query($con,$sql));
if($numpuid == 0)
{
    $sql = "INSERT INTO player_updates (pid_fk,earn_tpe,cost_tpe,bank,notes) VALUES ('$pid','$pe','$costpe','0','$notes')";
    mysqli_query($con,$sql);
    $puid = mysqli_insert_id($con);
    if($hostlg == 'SBA')
    {
        $sql = "SELECT ins,jps,ft,3ps,han,pas,orb,drb,psd,prd,stl,blk,fl,qkn,str,jmp,3ps_toggle FROM players WHERE id='$pid'";
        $result = mysqli_query($con,$sql);
        while($s = mysqli_fetch_assoc($result))
        {
            $ins=$s['ins'];
            $jps=$s['jps'];
            $ft=$s['ft'];
            $tps=$s['3ps'];
            $han=$s['han'];
            $pas=$s['pas'];
            $orb=$s['orb'];
            $drb=$s['drb'];
            $psd=$s['psd'];
            $prd=$s['prd'];
            $stl=$s['stl'];
            $blk=$s['blk'];
            $fl=$s['fl'];
            $qkn=$s['qkn'];
            $str=$s['str'];
            $jmp=$s['jmp'];
            $tpstoggle = $s['3ps_toggle'];
        }
        $sql = "INSERT INTO player_updates_update (puid_fk,ins,jps,ft,3ps,han,pas,orb,drb,psd,prd,stl,blk,fl,qkn,str,jmp,ins_old,jps_old,ft_old,3ps_old,han_old,pas_old,orb_old,drb_old,psd_old,prd_old,stl_old,blk_old,fl_old,qkn_old,str_old,jmp_old,3ps_toggle) VALUES ('$puid','$ins','$jps','$ft','$tps','$han','$pas','$orb','$drb','$psd','$prd','$stl','$blk','$fl','$qkn','$str','$jmp','$ins','$jps','$ft','$tps','$han','$pas','$orb','$drb','$psd','$prd','$stl','$blk','$fl','$qkn','$str','$jmp','$tpstoggle')";
        mysqli_query($con,$sql);
    }
    else if($hostlg == 'EFL')
    {
        $sql2 = "SELECT * FROM players WHERE id='$pid'";
        $s = mysqli_fetch_array(mysqli_query($con,$sql2));
        $stmt = $conn->prepare("INSERT INTO player_updates_update (puid_fk,str,agi,arm,`int`,acc,tkl,spd,han,rbk,pbk,kdi,kac,str_old,agi_old,arm_old,`int_old`,acc_old,tkl_old,spd_old,han_old,rbk_old,pbk_old,kdi_old,kac_old) VALUES (:id,:str,:agi,:arm,:inti,:acc,:tkl,:spd,:han,:rbk,:pbk,:kdi,:kac,:str,:agi,:arm,:inti,:acc,:tkl,:spd,:han,:rbk,:pbk,:kdi,:kac)");
        $stmt->execute([':id' => $puid,':str' => $s['str'],':agi' => $s['agi'],':arm' => $s['arm'],':inti' => $s['int'],':acc' => $s['acc'],':tkl' => $s['tkl'],':spd' => $s['spd'],':han' => $s['han'],':rbk' => $s['rbk'],':pbk' => $s['pbk'],':kdi' => $s['kdi'],':kac' => $s['kac']]);
    }
}
else
{
    $puid = mysqli_fetch_array(mysqli_query($con,$sql));
    $puid = $puid['id'];
    $sql = "UPDATE player_updates SET earn_tpe = earn_tpe+'$pe',bank = bank+'$pe' WHERE id = '$puid'";
    mysqli_query($con,$sql);
}
if($type == 2)
{
    $sql = "SELECT task,week,link,pe FROM player_updates_tasks WHERE puid_fk='$old_puid'";
    $result = mysqli_query($con,$sql);
    while($r = mysqli_fetch_array($result))
    {
        $task = $r['task'];
        $week = $r['week'];
        $link = $r['link'];
        $pe = $r['pe'];
        $sql = "INSERT INTO player_updates_tasks (puid_fk,task,week,link,pe) VALUES ('$puid','$task','$week','$link','$pe')";
        mysqli_query($con,$sql);
    }
}
else
{
    $sql = "INSERT INTO player_updates_tasks (puid_fk,task,week,link,pe) VALUES ('$puid','$task','$week','$link','$pe')";
    mysqli_query($con,$sql);
}
header("location:player_update.php?pid=".$pid2."&alert=dupesuccess");
?>