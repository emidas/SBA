<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
$pid = '';
if(isset($_GET['pid']))
{
    $pid = sanitize($con,$_GET['pid']);
}
else
{
    header("location:/home.php");
    exit();
}
$user = $_SESSION['user'];
$mid = $_SESSION['memberid'];

$sql = "SELECT build,tpe,ape FROM players WHERE id='$pid'";
$rql=mysqli_fetch_array(mysqli_query($con,$sql));
$build=$rql['build'];
$tpe=$rql['tpe'];
$ape=$rql['ape'];
$bank=$ape;

if($build == 'Standard')
{
    $ins = $jps = $ft = $tps = $han = $pas = $orb = $drb = $psd = $prd = $stl = $blk = $qkn = $str = $jmp = '35';
    $fl = '50';
}
else
{
    $ins = $jps = $ft = $tps = $han = $pas = $orb = $drb = $psd = $prd = $stl = $blk = '35';
    $fl = $qkn = $str = $jmp = '20';
}
$sql = "UPDATE players
        SET ins='$ins',
            jps='$jps',
            ft='$ft',
            3ps='$tps',
            han='$han',
            pas='$pas',
            orb='$orb',
            drb='$drb',
            psd='$psd',
            prd='$prd',
            stl='$stl',
            blk='$blk',
            fl='$fl',
            qkn='$qkn',
            str='$str',
            jmp='$jmp',
            tpe='$tpe',
            ape='$ape',
            bank='$bank'
        WHERE id='$pid'";
mysqli_query($con,$sql);
$sql = "SELECT * FROM player_updates pu WHERE pu.pid_fk='$pid' AND (status < 2)";
$numsult = mysqli_query($con,$sql);
$num = mysqli_num_rows($numsult);
if($num > 0)
{
    $puid = mysqli_fetch_array(mysqli_query($con,$sql));
    $puid = $puid['id'];
    $earn_tpe = $puid['earn_tpe'];
    $new_bank = $bank + $earn_tpe;
}
else
{
    header("location:/home.php");
    exit();
}
$sql = "UPDATE player_updates_update
        SET ins='$ins',jps='$jps',ft='$ft',3ps='$tps',han='$han',pas='$pas',orb='$orb',drb='$drb',psd='$psd',prd='$prd',stl='$stl',blk='$blk',fl='$fl',qkn='$qkn',str='$str',jmp='$jmp',ins_old=0,jps_old=0,ft_old=0,3ps_old=0,han_old=0,pas_old=0,orb_old=0,drb_old=0,psd_old=0,prd_old=0,stl_old=0,blk_old=0,fl_old=0,qkn_old=0,str_old=0,jmp_old=0
        WHERE puid_fk='$puid'";
mysqli_query($con,$sql);
$sql = "UPDATE player_updates
        SET bank = '$new_bank'
        WHERE id='$puid'";
mysqli_query($con,$sql);
header("location:/home.php");
exit();
?>