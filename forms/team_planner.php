<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
$user=$_SESSION['user'];

$system = new System();
$cap = $system->get_salary_cap();
$capshown = number_format($cap);

$query = "SELECT id,CONCAT(plast,', ',pfirst) as name,salary FROM players where league = 'SBA' AND (active='1' || active='3') ORDER BY name ASC";
$result = mysqli_query($con,$query);
$echo = '';
$players = "<option value = \"0\">None</option>";
while ($r = mysqli_fetch_array($result)) 
{
    $players .= "<option value = \"".$r['id']."\">".$r['name']." - $".number_format($r['salary'])."</option>";
}
if(isset($_GET['alert'])) 
{
    if($_GET['alert'] == 'success') 
    {
        $echo = "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Predictions Saved.</strong></div>";
    }
    else if($_GET['alert'] == 'closed')
    {
        $echo = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Predictions Closed, Submission Not Saved.</strong></div>";
    }
    else if($_GET['alert'] == 'norecord')
    {
        $echo = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You must save your own Predictions before you can import someone else's.</strong></div>";
    }
    else 
    {
        $echo = '';
    }
}
else
{$echo = '';}
?>

<!-- Body Start -->
<body>
<div id="wrapper" style="position:relative;top:0;left:0;">
    <?php include $path.'/includes/nav.php'; ?>
    <div class="container">
        <?=HeaderImagePrint($hostlg,$_SESSION['style'])?>
        <?=$echo?>
        <div class="panel panel-primary">
            <div class="panel-heading">SBAO Team Builder</div>
            <div class="panel-body">
                <div class="panel-group col-xs-12">
                    <form class='form-horizontal'>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon' style='width:50px;'>C</span>
                                <select class='form-control' name='center' onchange="salary(this.value,'valc')">
                                    <?=$players?>
                                </select>
                                <input type='text' class='form-control' name='salaryc' id='valc' readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon' style='width:50px;'>PF</span>
                                <select class='form-control' name='pf' onchange="salary(this.value,'valpf');">
                                    <?=$players?>
                                </select>
                                <input type='text' class='form-control' name='salarypf' id='valpf' readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon' style='width:50px;'>SF</span>
                                <select class='form-control' name='sf' onchange="salary(this.value,'valsf');">
                                    <?=$players?>
                                </select>
                                <input type='text' class='form-control' name='salarysf' id='valsf' readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon' style='width:50px;'>SG</span>
                                <select class='form-control' name='sg' onchange="salary(this.value,'valsg');">
                                    <?=$players?>
                                </select>
                                <input type='text' class='form-control' name='salarysg' id='valsg' readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon' style='width:50px;'>PG</span>
                                <select class='form-control' name='pg' onchange="salary(this.value,'valpg');">
                                    <?=$players?>
                                </select>
                                <input type='text' class='form-control' name='salarypg' id='valpg' readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon' style='width:50px;'>6th</span>
                                <select class='form-control' name='six' onchange="salary(this.value,'val6');">
                                    <?=$players?>
                                </select>
                                <input type='text' class='form-control' name='salary6' id='val6' readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon' style='width:50px;'>7th</span>
                                <select class='form-control' name='seven' onchange="salary(this.value,'val7');">
                                    <?=$players?>
                                </select>
                                <input type='text' class='form-control' name='salary7' id='val7' readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon' style='width:50px;'>8th</span>
                                <select class='form-control' name='eight' onchange="salary(this.value,'val8');">
                                    <?=$players?>
                                </select>
                                <input type='text' class='form-control' name='salary8' id='val8' readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon' style='width:125px;'>Total Salary</span>
                                <input type='text' class='form-control' id='total' value='$0' readonly>
                            </div>
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon' style='width:125px;'>Salary Cap</span>
                                <input type='text' class='form-control' id='cap' value="<?='$'.$capshown?>" readonly>
                                <input type='hidden' class='form-control' id='caphidden' value='<?=$cap?>'>
                            </div>
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon' style='width:125px;'>Cap Remaining</span>
                                <input type='text' class='form-control' id='caproom' value="<?='$'.$capshown?>" readonly>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Body End -->

<?php
include $path.'/footer.php';
?>
<script>
$("body").mouseover(function(){

});
function salary(pid,pos) {
    if(pid == 0)
    {
        $("#"+pos).val(0);
        return;
    }
    $.ajax({url: "../ajax/get_salary.php?pid="+pid, success: function(result){
      $("#"+pos).val(result);
      saltotal();
    }});
}
function saltotal()
{
    var total = 0;
    $("input[name^='salary']").each(function(){
        var sal = $(this).val();
        total += Number(sal);
    });
    $('#total').val('$'+total.toLocaleString());
    var cap = $('#caphidden').val();
    var caproom = cap - total;
    $('#caproom').val('$'+caproom.toLocaleString());
}
</script>