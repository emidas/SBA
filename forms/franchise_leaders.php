<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
$user=$_SESSION['user'];

$league = '';
$type = '';
if(isset($_POST['team']))
{$team = $_POST['team'];} else {$team = '5';}
if(isset($_POST['category']))
{$category = $_POST['category'];} else {$category = 'GameAssists';}

$tquery = "SELECT ID, CONCAT(CityName,' ',Name) as team from SBA_Team t";
$tresult = mysqli_query($con,$tquery);
$techo = '';
$echo = '';
while ($r = mysqli_fetch_array($tresult)) {
    if($team == $r['ID']) {$echo = "selected";} else {$echo = "";}
    $techo .= "<option value = '".$r['ID']."' $echo>".$r['team']."</option>";
}

$cquery = "SELECT Distinct RecordTitle FROM SBA_SingleTeamPlayerRecords ORDER BY RecordTitle ASC";
$cresult = mysqli_query($con,$cquery);
$cecho = '';
$echo = '';
while ($r = mysqli_fetch_array($cresult)) {
    if($category == $r['RecordTitle']) {$echo = "selected";} else {$echo = "";}
    $cecho .= "<option value = '".$r['RecordTitle']."' $echo>".$r['RecordTitle']."</option>";
}
?>

<!-- Body Start -->
<body>
<div id="wrapper" style="position:relative;top:0;left:0;">
    <?php include $path.'/includes/nav.php'; ?>
    <div class="container">
        <?=HeaderImagePrint($hostlg,$_SESSION['style'])?>
        <div class="panel panel-primary">
            <div class="panel-heading">Franchise Records</div>
            <div class="panel-body">
                <div class="panel-group">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Franchise Records Filters</div>
                        <div class="panel-body">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>League</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>League</span>
                                <select name="league" id="league" class="form-control" onchange="getTeams()">
                                    <option value='sba' <?php if ($league == 'sba') echo "selected";?>>SBA</option>
                                    <option value='ncaa' <?php if ($league == 'ncaa') echo "selected";?>>NCAA</option>
                                    <option value='fiba' <?php if ($league == 'fiba') echo "selected";?>>FIBA</option>
                                </select>
                            </div>
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>Type</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>Type</span>
                                <select name="type" id="type" class="form-control">
                                    <option value='player' <?php if ($type == 'player') echo "selected";?>>Player</option>
                                    <option value='team' <?php if ($type == 'team') echo "selected";?>>Team</option>
                                </select>
                            </div>
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>Team</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>Team</span>
                                <select name="team" id="team" class="form-control">
                                    <?=$techo?>
                                </select>
                            </div>
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>Category</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>Category</span>
                                <select name="category" id="category" class="form-control">
                                    <?=$cecho?>
                                </select>
                            </div>
                            <button type="button" class="btn btn-primary col-xs-12" onclick="getFranchiseRecords()">Submit</button>
                        </div>
                    </div>
                </div>
                <div class="panel-group">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Records</div>
                        <div class="panel-body">
                            <div id="franchiserecords">
                                <!--Ajax Call inserts records here -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>       
    </div>
</div>
<!-- Body End -->

<?php
include $path.'/footer.php';
?>
<script>
$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
});
function getFranchiseRecords()
{
    var team = $('#team').val();
    var category = $('#category').val();
    var league = $('#league').val();
    var type = $('#type').val();
    $.ajax({url: "../ajax/get_franchise_records.php?team="+team+"&category="+category+"&league="+league+"&type="+type, success: function(result){
      $("#franchiserecords").html(result);
    }});
}
function getTeams()
{
    var league = $('#league').val();
    $.ajax({url: "../ajax/get_team_dropdown.php?league="+league, success: function(result){
        $("#team").html(result);
    }});

}
</script>