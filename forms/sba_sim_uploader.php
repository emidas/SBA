<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:../auto/main_login.php");
}
if($_SESSION['issimmer'] != '1'){
header("location:../auto/index.php?alert=simmerpermission");
}
if(isset($_POST['tid']) && isset($_POST['games']))
{
    $tid = sanitize($con,$_POST['tid']);
    $games = sanitize($con,$_POST['games']);
    if(isset($_POST['sleep'])){$sleep = $_POST['sleep'];}else{$sleep = 10;}
    $ftp_server = "ps605975.dreamhostps.com";
    $ftp_conn = ftp_connect($ftp_server) or die("Could not connect to $ftp_server");
    $login = ftp_login($ftp_conn, 'sbahistory', '+Historical12');
    $file_list = ftp_nlist($ftp_conn, "/sbahistory.com/sba/boxes/");
    array_multisort($file_list,SORT_ASC,SORT_NATURAL);
    // Connect to server and select databse.
    $count = count($file_list);
    $x = $count - $games;
    while ($x < $count)
    {
        $data = "<iframe src=https://sbahistory.com/sba/boxes/$file_list[$x] style=width:900px;height:700px;></iframe>";
        $curl_post_data = array(
        'topic' => $tid,
        'author' => '2574',
        'post' => $data
        );
        createPost($curl_post_data);
        sleep($sleep);
        $x++;
    }
    $data = "<iframe src=https://sbahistory.com/sba/standings.htm style=width:900px;height:700px;></iframe>";
    $curl_post_data = array(
        'topic' => $tid,
        'author' => '2574',
        'post' => $data
        );
    createPost($curl_post_data); 
    sleep($sleep);
    $data = "<iframe src=https://sbahistory.com/sba/leaders.htm style=width:900px;height:2500px;></iframe>";
    $curl_post_data = array(
        'topic' => $tid,
        'author' => '2574',
        'post' => $data
        );
    createPost($curl_post_data); 
    sleep($sleep);
}
?>

<!-- Body Start -->
<body>
<div id="wrapper" style="position:relative;top:0;left:0;">
    <?php include $path.'/includes/nav.php'; ?>
    <div class="container">
        <?=HeaderImagePrint($hostlg,$_SESSION['style'])?>
        <div class="panel-group">
            <div class="panel panel-primary">
                <div class="panel-heading">Upload SBA Sim</div>
                <div class="panel-body">
                    <form class="form-horizontal" action="sba_sim_uploader.php" method="post">
                        <div class="form-group">
                            <label for="tid">Topic ID:</label>
                            <input type="text" class="form-control" name="tid">
                        </div>
                        <div class="form-group">
                            <label for="games">Number of games:</label>
                            <input type="text" class="form-control" name="games">
                        </div>
                        <div class="form-group">
                            <label for="sleep">Sleep Timer(seconds):</label>
                            <input type="text" class="form-control" name="sleep">
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-default">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Body End -->

<?php
include $path.'/footer.php';
?>