<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
if(isset($_POST['template']))
{
    $league = $_POST['league'];
    $cid = $_POST['cid'];
    $template = $_POST['template'];
    $user=$_SESSION['user'];
    $yr = "SELECT year from year WHERE isactive='1'";
    $yrq=mysqli_fetch_array(mysqli_query($con,$yr));
    $year=$yrq['year'];
    $sql = "SELECT id FROM coach_updates WHERE cid_fk='$cid' AND (status = '1' || status = '0')";
    $numpuid = mysqli_num_rows(mysqli_query($con,$sql));
    if($numpuid == 0)
    {
        $sql = "INSERT INTO coach_updates (cid_fk) VALUES ('$cid')";
        mysqli_query($con,$sql);
        $cuid = mysqli_insert_id($con);
    }
    else
    {
        $cuid = mysqli_fetch_array(mysqli_query($con,$sql));
        $cuid = $cuid['id'];
    }
    $query = "SELECT * FROM coach_updates cu INNER JOIN coach_updates_update cuu ON cu.id=cuu.cuid_fk WHERE cid_fk='$cid' AND cu.id='$template'";
    $p = mysqli_fetch_array(mysqli_query($con,$query));
    $name = $p['strat_name'];
    $pace = $p['pace'];
    $motion = $p['motion'];
    $tpuse = $p['tpuse'];
    $focus = $p['focus'];
    $crasho = $p['crasho'];
    $setprimary = $p['setprimary'];
    $primaryd = $p['primaryd'];
    $setsecondary = $p['setsecondary'];
    $secondd = $p['secondd'];
    $fullcourt = $p['fullcourt'];
    $crashd = $p['crashd'];
    $pnamec = isset($p['pnamec']) ? sanitize($con,$p['pnamec']) : '';
    $pnamepf = sanitize($con,$p['pnamepf']);
    $pnamesf = sanitize($con,$p['pnamesf']);
    $pnamesg = sanitize($con,$p['pnamesg']);
    $pnamepg = sanitize($con,$p['pnamepg']);
    $pname6 = sanitize($con,$p['pname6']);
    $pname7 = sanitize($con,$p['pname7']);
    $pname8 = sanitize($con,$p['pname8']);
    $pname9 = sanitize($con,$p['pname9']);
    $pname10 = sanitize($con,$p['pname10']);
    $pname11 = sanitize($con,$p['pname11']);
    $pname12 = sanitize($con,$p['pname12']);
    $posc = sanitize($con,$p['posc']);
    $pospf = sanitize($con,$p['pospf']);
    $possf = sanitize($con,$p['possf']);
    $possg = sanitize($con,$p['possg']);
    $pospg = sanitize($con,$p['pospg']);
    $pos6 = sanitize($con,$p['pos6']);
    $pos7 = sanitize($con,$p['pos7']);
    $pos8 = sanitize($con,$p['pos8']);
    $pos9 = sanitize($con,$p['pos9']);
    $pos10 = sanitize($con,$p['pos10']);
    $pos11 = sanitize($con,$p['pos11']);
    $pos12 = sanitize($con,$p['pos12']);
    $minc = sanitize($con,$p['minc']);
    $minpf = sanitize($con,$p['minpf']);
    $minsf = sanitize($con,$p['minsf']);
    $minsg = sanitize($con,$p['minsg']);
    $minpg = sanitize($con,$p['minpg']);
    $min6 = sanitize($con,$p['min6']);
    $min7 = sanitize($con,$p['min7']);
    $min8 = sanitize($con,$p['min8']);
    $min9 = sanitize($con,$p['min9']);
    $min10 = sanitize($con,$p['min10']);
    $min11 = sanitize($con,$p['min11']);
    $min12 = sanitize($con,$p['min12']);
    $key1 = sanitize($con,$p['key1']);
    $key2 = sanitize($con,$p['key2']);
    $key3 = sanitize($con,$p['key3']);
    $sql = "UPDATE coach_updates SET status='1', strat_name = '$name' WHERE id='$cuid'";
    mysqli_query($con,$sql);
    $sql = "SELECT id FROM coach_updates_update WHERE cuid_fk = '$cuid'";
    $cuunum = mysqli_num_rows(mysqli_query($con,$sql));
    if($cuunum == 0)
    {
        $sql = "INSERT INTO coach_updates_update (cuid_fk) VALUES ('$cuid')";
        mysqli_query($con,$sql);
        $sql = "UPDATE coach_updates_update SET pace='$pace',motion='$motion',tpuse='$tpuse',focus='$focus',crasho='$crasho',setprimary='$setprimary',primaryd='$primaryd',setsecondary='$setsecondary',secondd='$secondd',fullcourt='$fullcourt',crashd='$crashd',pnamec='$pnamec',pnamepf='$pnamepf',pnamesf='$pnamesf',pnamesg='$pnamesg',pnamepg='$pnamepg',pname6='$pname6',pname7='$pname7',pname8='$pname8',pname9='$pname9',pname10='$pname10',pname11='$pname11',pname12='$pname12',posc='$posc',pospf='$pospf',possf='$possf',possg='$possg',pospg='$pospg',pos6='$pos6',pos7='$pos7',pos8='$pos8',pos9='$pos9',pos10='$pos10',pos11='$pos11',pos12='$pos12',minc='$minc',minpf='$minpf',minsf='$minsf',minsg='$minsg',minpg='$minpg',min6='$min6',min7='$min7',min8='$min8',min9='$min9',min10='$min10',min11='$min11',min12='$min12',key1='$key1',key2='$key2',key3='$key3' WHERE cuid_fk='$cuid'";
        mysqli_query($con,$sql);
    }
    else
    {
        $sql = "UPDATE coach_updates_update SET pace='$pace',motion='$motion',tpuse='$tpuse',focus='$focus',crasho='$crasho',setprimary='$setprimary',primaryd='$primaryd',setsecondary='$setsecondary',secondd='$secondd',fullcourt='$fullcourt',crashd='$crashd',pnamec='$pnamec',pnamepf='$pnamepf',pnamesf='$pnamesf',pnamesg='$pnamesg',pnamepg='$pnamepg',pname6='$pname6',pname7='$pname7',pname8='$pname8',pname9='$pname9',pname10='$pname10',pname11='$pname11',pname12='$pname12',posc='$posc',pospf='$pospf',possf='$possf',possg='$possg',pospg='$pospg',pos6='$pos6',pos7='$pos7',pos8='$pos8',pos9='$pos9',pos10='$pos10',pos11='$pos11',pos12='$pos12',minc='$minc',minpf='$minpf',minsf='$minsf',minsg='$minsg',minpg='$minpg',min6='$min6',min7='$min7',min8='$min8',min9='$min9',min10='$min10',min11='$min11',min12='$min12',key1='$key1',key2='$key2',key3='$key3' WHERE cuid_fk='$cuid'";
        mysqli_query($con,$sql);
    }
    header("location:/forms/coach_update.php?league=".$league."&alert=templateloaded");
    exit();
}
else if(isset($_POST['strat_submit'])) 
{
    $league = $_POST['league'];
    $cid = $_POST['cid'];
    $user=$_SESSION['user'];
    $name = $_POST['strat_name'];
    $pace = $_POST['pace'];
    $motion = $_POST['motion'];
    $tpuse = $_POST['tpuse'];
    $focus = $_POST['focus'];
    $crasho = $_POST['crasho'];
    $setprimary = $_POST['setprimary'];
    $primaryd = $_POST['primaryd'];
    $setsecondary = $_POST['setsecondary'];
    $secondd = $_POST['secondd'];
    $fullcourt = $_POST['fullcourt'];
    $crashd = $_POST['crashd'];
    $pnamec = isset($_POST['pnamec']) ? sanitize($con,$_POST['pnamec']) : '';
    $pnamepf = sanitize($con,$_POST['pnamepf']);
    $pnamesf = sanitize($con,$_POST['pnamesf']);
    $pnamesg = sanitize($con,$_POST['pnamesg']);
    $pnamepg = sanitize($con,$_POST['pnamepg']);
    $pname6 = sanitize($con,$_POST['pname6']);
    $pname7 = sanitize($con,$_POST['pname7']);
    $pname8 = sanitize($con,$_POST['pname8']);
    $pname9 = sanitize($con,$_POST['pname9']);
    $pname10 = sanitize($con,$_POST['pname10']);
    $pname11 = sanitize($con,$_POST['pname11']);
    $pname12 = sanitize($con,$_POST['pname12']);
    $posc = sanitize($con,$_POST['posc']);
    $pospf = sanitize($con,$_POST['pospf']);
    $possf = sanitize($con,$_POST['possf']);
    $possg = sanitize($con,$_POST['possg']);
    $pospg = sanitize($con,$_POST['pospg']);
    $pos6 = sanitize($con,$_POST['pos6']);
    $pos7 = sanitize($con,$_POST['pos7']);
    $pos8 = sanitize($con,$_POST['pos8']);
    $pos9 = sanitize($con,$_POST['pos9']);
    $pos10 = sanitize($con,$_POST['pos10']);
    $pos11 = sanitize($con,$_POST['pos11']);
    $pos12 = sanitize($con,$_POST['pos12']);
    $minc = sanitize($con,$_POST['minc']);
    $minpf = sanitize($con,$_POST['minpf']);
    $minsf = sanitize($con,$_POST['minsf']);
    $minsg = sanitize($con,$_POST['minsg']);
    $minpg = sanitize($con,$_POST['minpg']);
    $min6 = sanitize($con,$_POST['min6']);
    $min7 = sanitize($con,$_POST['min7']);
    $min8 = sanitize($con,$_POST['min8']);
    $min9 = sanitize($con,$_POST['min9']);
    $min10 = sanitize($con,$_POST['min10']);
    $min11 = sanitize($con,$_POST['min11']);
    $min12 = sanitize($con,$_POST['min12']);
    $key1 = sanitize($con,$_POST['key1']);
    $key2 = sanitize($con,$_POST['key2']);
    $key3 = sanitize($con,$_POST['key3']);
    $sql = "SELECT id FROM coach_updates WHERE cid_fk='$cid' AND (status = '1' || status = '0')";
    $numpuid = mysqli_num_rows(mysqli_query($con,$sql));
    if($numpuid == 0)
    {
        $sql = "INSERT INTO coach_updates (cid_fk) VALUES ('$cid')";
        mysqli_query($con,$sql);
        $cuid = mysqli_insert_id($con);
    }
    else
    {
        $cuid = mysqli_fetch_array(mysqli_query($con,$sql));
        $cuid = $cuid['id'];
    }
    $sql = "UPDATE coach_updates SET status = '1',strat_name='$name' WHERE id = '$cuid'";
    mysqli_query($con,$sql);
    $sql = "SELECT id FROM coach_updates_update WHERE cuid_fk = '$cuid'";
    $cuunum = mysqli_num_rows(mysqli_query($con,$sql));
    if($cuunum == 0)
    {
        $sql = "INSERT INTO coach_updates_update (cuid_fk) VALUES ('$cuid')";
        mysqli_query($con,$sql);
        $sql = "UPDATE coach_updates_update SET pace='$pace',motion='$motion',tpuse='$tpuse',focus='$focus',crasho='$crasho',setprimary='$setprimary',primaryd='$primaryd',setsecondary='$setsecondary',secondd='$secondd',fullcourt='$fullcourt',crashd='$crashd',pnamec='$pnamec',pnamepf='$pnamepf',pnamesf='$pnamesf',pnamesg='$pnamesg',pnamepg='$pnamepg',pname6='$pname6',pname7='$pname7',pname8='$pname8',pname9='$pname9',pname10='$pname10',pname11='$pname11',pname12='$pname12',posc='$posc',pospf='$pospf',possf='$possf',possg='$possg',pospg='$pospg',pos6='$pos6',pos7='$pos7',pos8='$pos8',pos9='$pos9',pos10='$pos10',pos11='$pos11',pos12='$pos12',minc='$minc',minpf='$minpf',minsf='$minsf',minsg='$minsg',minpg='$minpg',min6='$min6',min7='$min7',min8='$min8',min9='$min9',min10='$min10',min11='$min11',min12='$min12',key1='$key1',key2='$key2',key3='$key3' WHERE cuid_fk='$cuid'";
        mysqli_query($con,$sql);
    }
    else
    {
        $sql = "UPDATE coach_updates_update SET pace='$pace',motion='$motion',tpuse='$tpuse',focus='$focus',crasho='$crasho',setprimary='$setprimary',primaryd='$primaryd',setsecondary='$setsecondary',secondd='$secondd',fullcourt='$fullcourt',crashd='$crashd',pnamec='$pnamec',pnamepf='$pnamepf',pnamesf='$pnamesf',pnamesg='$pnamesg',pnamepg='$pnamepg',pname6='$pname6',pname7='$pname7',pname8='$pname8',pname9='$pname9',pname10='$pname10',pname11='$pname11',pname12='$pname12',posc='$posc',pospf='$pospf',possf='$possf',possg='$possg',pospg='$pospg',pos6='$pos6',pos7='$pos7',pos8='$pos8',pos9='$pos9',pos10='$pos10',pos11='$pos11',pos12='$pos12',minc='$minc',minpf='$minpf',minsf='$minsf',minsg='$minsg',minpg='$minpg',min6='$min6',min7='$min7',min8='$min8',min9='$min9',min10='$min10',min11='$min11',min12='$min12',key1='$key1',key2='$key2',key3='$key3' WHERE cuid_fk='$cuid'";
        mysqli_query($con,$sql);
    }
    $audit = "INSERT INTO audit_history (username,task,ipaddress,audittime) VALUES (\"$user\",'Submitted $league Strategy','$_SERVER[REMOTE_ADDR]',now())";
    mysqli_query($con,$audit);
    $sql = "SELECT * FROM coach_updates cu INNER JOIN coach_updates_update cuu ON cu.id=cuu.cuid_fk WHERE cu.cid_fk='$cid' AND (status = '1' || status = '0')";
    $numsult = mysqli_query($con,$sql);
    header("location:/forms/coach_update.php?league=".$league."&alert=stratsaved");
    exit();
}
else
{
    header("location:/home.php?alert=gmpermission");
    exit();
}
?>
