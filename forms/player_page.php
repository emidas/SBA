<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
$user=$_SESSION['user'];

if(isset($_GET['pname']))
{
    $pname = sanitize($con,$_GET['pname']);
    $sql = "SELECT id FROM players WHERE plast LIKE '%$pname%'";
    $result = mysqli_fetch_array(mysqli_query($con,$sql));
    $pid = $result['id'];
}
else
{
    // get pid
    $pid = sanitize($con,$_GET['pid']);
}

$player = new Player($pid);
$snap = $player->has_snapshot();
if($snap > 0)
{
    $snapshot = createPlayerSnapshot($con,$pid);
}
else
{
    $snapshot = "";
}
$system = new System();
$year = $system->get_year();

$sql = "SELECT CONCAT(pfirst,' ',plast) as name FROM players WHERE id='$pid'";
$nameq = mysqli_fetch_array(mysqli_query($con,$sql));
$name = $nameq['name'];

$sql = "SELECT year, award, t.name as team
FROM teams t
INNER JOIN award_winners s ON t.id=s.tid
WHERE pid = '$pid'
ORDER BY year DESC";
$result = mysqli_query($con,$sql);
$awtable = '';
while($a = mysqli_fetch_array($result))
{
    $awtable .= "<tr>";
        $awtable .= "<td>".$a['year']."</td>";
        $awtable .= "<td>".strtoupper($a['award'])."</td>";
        $awtable .= "<td>".$a['team']."</td>";
    $awtable .= "</tr>";
}

if(isset($_GET['alert'])) 
{
    if($_GET['alert'] == 'markedsuccessful')
    {
        $echo = "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Player was successfully marked as an SBA filler.</strong></div>";
    }
    else if($_GET['alert'] == 'markfailed')
    {
        $echo = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Something went wrong. Player was not marked as an SBA filler. Please try again by clicking the proper button.</strong></div>";
    }
    else if($_GET['alert'] == 'gradsuccessful')
    {
        $echo = "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Player has successfully graduated, and is now an SBA player.</strong></div>";
    }
    else if($_GET['alert'] == 'gradfailed')
    {
        $echo = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Something went wrong. Player was not graduated. Please try again by clicking the proper button.</strong></div>";
    }
    else if($_GET['alert'] == 'gradfull')
    {
        $echo = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You already have an existing active SBA player. Please retire your currently active player if you wish you graduate this player.</strong></div>";
    }
    else if($_GET['alert'] == 'retiresuccessful')
    {
        $echo = "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Player was successfully retired.</strong></div>";
    }
    else if($_GET['alert'] == 'retirefailed')
    {
        $echo = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Something went wrong. Player was not retired. Please try again by clicking the proper button.</strong></div>";
    }
    else 
    {
        $echo = '';
    }
}
else
{$echo = '';}
?>

<!-- Body Start -->
<style>
    .input-group-addon {
    // if you want width please write here //

}
</style>
<body>
<div id="wrapper" style="position:relative;top:0;left:0;">
    <?php include $path.'/includes/nav.php'; ?>
    <div class="container">
        <?=HeaderImagePrint($hostlg,$_SESSION['style'])?>
        <?=$echo?>
        <div class="panel panel-primary">
            <div class="panel-heading">Player Management</div>
            <div class="panel-body">
                <?=createPlayerPage($con,$pid,'1')?>
                <?=$snapshot?>
                <div class="panel panel-primary">
                    <div class="panel-heading"><h4 class='panel-title'><a data-toggle='collapse' href='#collapseawards'>Player Awards</a></h4></div>
                    <div class='panel-collapse collapse' id='collapseawards'>
                        <div class="panel-body">
                            <div class='table-responsive'>
                                <table class="table table-striped" id='viewsba3' style="width:100%;">
                                    <thead>
                                        <th>Season</th>
                                        <th>Award</th>
                                        <th>Team</th>
                                    </thead>
                                    <tbody>
                                        <?=$awtable?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-primary">
                    <div class="panel-heading"><h4 class='panel-title'><a data-toggle='collapse' href='#collapseuh'>Update History</a></h4></div>
                    <div class='panel-collapse collapse' id='collapseuh'>
                        <div class="panel-body">
                            <div class="panel panel-default">
                                <div class="panel-heading"><h4 class="panel-title"><a data-toggle='collapse' href='#collapsehbu'>History By Update</a></h4></div>
                                <div class='panel-collapse collapse' id='collapsehbu'>
                                    <div class="panel-body">
                                        <?=create_update_history($con,$pid,1)?>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading"><h4 class="panel-title"><a data-toggle='collapse' href='#collapsehbwe'>History By Week's End</a></h4></div>
                                <div class='panel-collapse collapse' id='collapsehbwe'>
                                    <div class="panel-body">
                                        <?=create_update_history_week($con,$pid,'')?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if($hostlg == 'SBA') { ?>
                <div class="panel panel-primary">
                    <div class="panel-heading">Player Stats</div>
                    <div class="panel-body">
                        <h4>SBA Season Stats</h4>
                        <?=printplayerseason($con,$name,'sba','season','')?>
                        <h4>SBA Playoff Stats</h4>
                        <?=printplayerseason($con,$name,'sba','playoff','')?>
                        <h4>NCAA Season Stats</h4>
                        <?=printplayerseason($con,$name,'ncaa','season','')?>
                        <h4>NCAA Playoff Stats</h4>
                        <?=printplayerseason($con,$name,'ncaa','playoff','')?>
                        <h4>FIBA Season Stats</h4>
                        <?=printplayerseason($con,$name,'fiba','season','')?>
                        <h4>FIBA Playoff Stats</h4>
                        <?=printplayerseason($con,$name,'fiba','playoff','')?>
                    </div>
                </div>
            <?php } ?>
            </div>
        </div>
    </div>
</div>
<!-- Body End -->

<?php
include $path.'/footer.php';
?>