<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
$pid = '';
$cash = '';
$reason = '';
$tid = '';
$system = new System();
$year = $system->get_year();
if(isset($_POST['pid']) && isset($_POST['tid']))
{
    $pid = sanitize($con,$_POST['pid']);
    $cash = sanitize($con,$_POST['cash']);
    $reason = sanitize($con,$_POST['reason']);
    $reason = "S".$year." ".$reason;
    $tid = sanitize($con,$_POST['tid']);
    $league = sanitize($con,$_POST['league']);
    $player = new Player($pid);
    $player->set_cash($cash);
    $player->add_earn_history($cash,$reason);
    if($tid == 0)
    {
        header("location:/forms/team_page.php?tid=".$tid."&league=".$league);
        exit();
    }
    else
    {
        header("location:/forms/team_page.php?tid=".$tid);
        exit();
    }
}
else if(isset($_POST['pid']))
{
    $pid = sanitize($con,$_POST['pid']);
    $cash = sanitize($con,$_POST['cash']);
    $reason = sanitize($con,$_POST['reason']);
    $reason = "S".$year." ".$reason;
    $league = sanitize($con,$_POST['league']);
    $active = sanitize($con,$_POST['active']);
    if($active != '')
    {
        $active = "&active=1";
    }
    $player = new Player($pid);
    $player->set_cash($cash);
    $player->add_earn_history($cash,$reason);
    header("location:/forms/manage_players.php?league=".$league."".$active);
    exit();
}
else
{
    header("location:javascript://history.go(-1)");
    exit();
}
?>