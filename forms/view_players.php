<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
if(isset($_GET['user']))
{
    $user = sanitize($con,$_GET['user']);
    $heading = 'View Players';
    if($user == $_SESSION['user'])
    {
        $self = '1';
    }
    else
    {
        $self = '0';
    }
}
else
{
    $user=$_SESSION['user'];
    $self = '1';
    $heading = 'Player Management';
}

$system = new System();
$year = $system->get_year();

$attyquerystring = '';
for($x=0;$x<count($lgatty);$x++)
{
    if($x == 0)
    {
        $attyquerystring .= "`".$lgatty[$x]."`";
    }
    else
    {
        $attyquerystring .= ",`".$lgatty[$x]."`";
    }
}

$query = "SELECT CONCAT(plast,', ',pfirst) AS pname,CONCAT(pfirst,' ',plast) AS tname,position,position2,build,num,height,weight,league,p.active,p.id,p.user_fk,$attyquerystring,c90,c801,c802,c803,c804,c70,bank,tpe,ape,experience
FROM auth_user u 
INNER JOIN players p ON u.ID = p.user_fk 
WHERE u.username = \"$user\" 
ORDER BY CASE WHEN league='$mainlg' AND p.active='1' THEN 1
WHEN league='$mainlg' AND p.active='3' THEN 2
WHEN league='$sublg' AND p.active='1' THEN 3
WHEN league='$mainlg' AND p.active='2' THEN 4
WHEN league='$sublg' AND p.active='2' THEN 5
ELSE 5
END, pname";
$result = mysqli_query($con,$query);
$ptable = '';
$g = 0;
while ($row = mysqli_fetch_array($result)) 
{
    if($row['league'] == $mainlg && ($row['active'] == '1' || $row['active'] == '3') && $self == '1')
    {
        $managebuttons = "<li><a href='/forms/player_page.php?pid=".$row['id']."' class='btn btn-primary'>Player Page</a></li><li><a href='player_update.php?pid=".$row['id']."' class='btn btn-primary'>Player Update</a></li><li><a href='/forms/view_updates.php?pid=".$row['id']."' class='btn btn-primary'>Update History</a></li><li><a href='player_image_upload.php?pid=".$row['id']."' class='btn btn-primary'>Add/Edit Picture</a></li>";
    }
    else if($row['league'] == $sublg && ($row['active'] == '1' || $row['active'] == '2') && $self == '1')
    {
        $managebuttons = "<li><a href='/forms/player_page.php?pid=".$row['id']."' class='btn btn-primary'>Player Page</a></li><li><a href='player_update.php?pid=".$row['id']."' class='btn btn-primary'>Player Update</a></li><li><a href='/forms/view_updates.php?pid=".$row['id']."' class='btn btn-primary'>Update History</a></li><li><a href='player_image_upload.php?pid=".$row['id']."' class='btn btn-primary'>Add/Edit Picture</a></li>";
    }
    else
    {
        $managebuttons = "<li><a href='/forms/player_page.php?pid=".$row['id']."' class='btn btn-primary'>Player Page</a></li>";
    }
    $ptable .= "<tr>";
        $ptable .= "<td><div class='dropdown'><a href='#' class='dropdown-toggle' data-toggle='dropdown'>".$row['pname']."<span class='caret'></span></a><ul class='dropdown-menu'>".$managebuttons."</ul></div></td>";
        $ptable .= "<td class='hidden-xs'>".$row['position'];
            if($row['position2'] != ''){$ptable .= "/".$row['position2'];}else{}
        $ptable .= "</td>";
        $ptable .= "<td class='hidden-xs'>".$row['build']."</td>";
        $ptable .= "<td class='hidden-xs'>".$row['tpe']."</td>";
        $ptable .= "<td>".$row['ape']."</td>";
        $ptable .= "<td class='hidden-xs'>".$row['bank']."</td>";
        $ptable .= "<td>".$row['experience']."</td>";
        $ptable .= "<td class='hidden-xs'>".$row['height']."</td>";
        $ptable .= "<td class='hidden-xs'>".$row['weight']."</td>";
        if($row['league'] == $mainlg && $row['active'] == '1')
        {$ptable .= "<td>".$mainlg."</td>";}
        else if($row['league'] == $sublg && $row['active'] == '1')
        {$ptable .= "<td>".$sublg."</td>";}
        else if($row['league'] == $mainlg && $row['active'] == '2')
        {$ptable .= "<td>Filler</td>";}
        else if($row['league'] == $sublg && $row['active'] == '2')
        {$ptable .= "<td>Capped</td>";}
        else if($row['league'] == $mainlg && $row['active'] == '3')
        {$ptable .= "<td>Retiring</td>";}
        else{$ptable .= "<td>Retired</td>";}
        $ptable .= "
        </tr>"; 
    $g++;
}
if(isset($_GET['alert'])) 
{
    if($_GET['alert'] == 'markedsuccessful')
    {
        $echo = "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Player was successfully marked as an SBA filler.</strong></div>";
    }
    else if($_GET['alert'] == 'markfailed')
    {
        $echo = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Something went wrong. Player was not marked as an SBA filler. Please try again by clicking the proper button.</strong></div>";
    }
    else if($_GET['alert'] == 'gradsuccessful')
    {
        $echo = "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Player has successfully graduated, and is now an SBA player.</strong></div>";
    }
    else if($_GET['alert'] == 'gradfailed')
    {
        $echo = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Something went wrong. Player was not graduated. Please try again by clicking the proper button.</strong></div>";
    }
    else if($_GET['alert'] == 'gradfull')
    {
        $echo = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You already have an existing active SBA player. Please retire your currently active player if you wish you graduate this player.</strong></div>";
    }
    else if($_GET['alert'] == 'retiresuccessful')
    {
        $echo = "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Player was successfully retired.</strong></div>";
    }
    else if($_GET['alert'] == 'retirefailed')
    {
        $echo = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Something went wrong. Player was not retired. Please try again by clicking the proper button.</strong></div>";
    }
    else 
    {
        $echo = '';
    }
}
else
{$echo = '';}
?>

<!-- Body Start -->
<body>
<div id="wrapper" style="position:relative;top:0;left:0;">
    <?php include $path.'/includes/nav.php'; ?>
    <div class="container">
        <?=HeaderImagePrint($hostlg,$_SESSION['style'])?>
        <?=$echo?>
        <div class="panel panel-primary">
            <div class="panel-heading"><?=$heading?></div>
            <div class="panel-body">
                <form class="form-horizontal" action="view_players.php" method="post" >
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <th>Name</th>
                                <th class='hidden-xs'>Position</th>
                                <th class='hidden-xs'>Build</th>
                                <th class='hidden-xs'>TPE</th>
                                <th>APE</th>
                                <th class='hidden-xs'>Banked</th>
                                <th class='hidden-xs'>Experience</th>
                                <th class='hidden-sm hidden-md hidden-lg'>Exp.</th>
                                <th class='hidden-xs'>Height</th>
                                <th class='hidden-xs'>Weight</th>
                                <th>League</th>
                            </thead>
                            <tbody>
                                <?=$ptable?>
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Body End -->

<?php
include $path.'/footer.php';
?>
<style>
.table-responsive > .table {
    margin-bottom: 0;
}
.table-responsive > .table > thead > tr > th, .table-responsive > .table > tbody > tr > th, .table-responsive > .table > tfoot > tr > th, .table-responsive > .table > thead > tr > td, .table-responsive > .table > tbody > tr > td, .table-responsive > .table > tfoot > tr > td {
    white-space: nowrap;
}
</style>
<script>
    $('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>
<script>
$(document).ready(function(){
    $('[data-toggle="popover"]').popover(); 
});
</script>
