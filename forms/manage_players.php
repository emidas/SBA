<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
$user=$_SESSION['user'];
$memberid=$_SESSION['memberid'];
$echo = '';
$clause = '';
$activehead = '';
$newhead = '';
$new = '';
$fa = '';
$order = 'pname';
$attyheaders = '';
for ($a=0;$a<count($lgatty);$a++)
{
    $attyheaders .= "<th>".strtoupper($lgatty[$a])."</th>";
}

if(isset($_GET['league']))
{
    $league = sanitize($con,$_GET['league']);
    if($league == 'all'){$league='%';}
    $clause = "AND ((tpe >= '30' AND p.active != '0') OR (p.active = '0' AND tpe > '30'))";
}
else{$league ='%';}
if(isset($_GET['active']))
{
    $active = sanitize($con,$_GET['active']);
    if($active == '1'){$active="AND (p.active != '0')";}else{$active='';}    
}
else{$active='';}
if(isset($_GET['draft']))
{
    $draft = sanitize($con,$_GET['draft']);
    if($draft == '1')
    {
        $league = $sublg;
        $clause = "AND (tpe > '199') AND (p.active != '0')";
        $order = "tpe desc";
    }
}
if(isset($_GET['freshman']))
{
    $freshman = sanitize($con,$_GET['freshman']);
    if($freshman == '1')
    {
        $league = $sublg;
        $clause = "AND (experience = 1) AND (p.active != '0')";
        $order = "experience desc";
    }
}
if(isset($_GET['regress']))
{
    $regress = sanitize($con,$_GET['regress']);
    if($regress == '1')
    {
        $league = $mainlg;
        $clause = "AND (experience >= 8) AND (p.active != '0')";
        $order = "experience desc";
    }
}
if(isset($_GET['regressed']))
{
    $regressed = sanitize($con,$_GET['regressed']);
    if($regressed == '1')
    {
        $league = $mainlg;
        $clause = "AND (experience >= 9) AND (p.active != '0')";
        $order = "experience desc";
    }
}
if(isset($_GET['rookie']))
{
    $rookie = sanitize($con,$_GET['rookie']);
    if($rookie == '1')
    {
        $league = $mainlg;
        $clause = "AND (experience = 1) AND (p.active != '0')";
        $order = "experience desc";
    }
}
if(isset($_GET['retiring']))
{
    $retiring = sanitize($con,$_GET['retiring']);
    if($retiring == '1')
    {
        $league = $mainlg;
        $clause = "AND (p.active = 3)";
        $order = "experience desc";
    }
}
if(isset($_GET['new']))
{
    $new = sanitize($con,$_GET['new']);
    if($new == '1')
    {
        $league = $sublg;
        $clause = "AND (created_time > date_sub(now(), interval 1 week)) AND (p.active != '0')";
    }
}
if(isset($_GET['fa']))
{
    $fa = sanitize($con,$_GET['fa']);
    if($fa == '1')
    {
        $league = $mainlg;
        $clause = "AND (yrs_remain = '1') AND (p.active != '0' && p.active != '2') AND username NOT IN (SELECT name FROM coaches WHERE league = '$mainlg')";
        $order = "tpe desc";
    }
}

$leagueteam = '';
//SBA Team Dropdown
$query = "SELECT id,name FROM teams where league = 'SBA' || league = 'FA' ORDER BY name ASC";
$result = mysqli_query($con,$query);
$sbateam = '';
$echo = '';
while ($r = mysqli_fetch_array($result)) 
{
    $sbateam .= "<option value = '".$r['id']."'>".$r['name']."</option>";
}

//SBDL Team Dropdown
$query = "SELECT id,name FROM teams where league = 'SBDL' || league = 'FA' ORDER BY name ASC";
$result = mysqli_query($con,$query);
$sbdlteam = '';
$echo = '';
while ($r = mysqli_fetch_array($result)) 
{
    $sbdlteam .= "<option value = '".$r['id']."'>".$r['name']."</option>";
}

// CSV
$fp = fopen('attributes.csv', 'w');
$headers = array('Name','INS','JPS','FT','3PS','HAN','PAS','ORB','DRB','PSD','PRD','STL','BLK','FL','QKN','STR','JMP');
fputcsv($fp,$headers);

if($hostlg == 'EFL')
{
    $query = "SELECT CONCAT(plast,', ',pfirst) AS pname,u.username,CONCAT(pfirst,' ',plast) AS tname,position,position2,build,height,weight,p.league,p.active,p.id,p.user_fk,tpe,t.name AS team,t.id AS tid,p.league,u.memberid as toid,created_time as created,FROM_UNIXTIME(cm.last_activity) as last_active,cm.last_activity as activity,ape,experience,str,agi,arm,`int`,acc,tkl,spd,han,rbk,pbk,kdi,kac,u.active as uactive
    FROM auth_user u 
    INNER JOIN players p ON u.ID = p.user_fk 
    INNER JOIN teams t ON p.team = t.id
    INNER JOIN efl_forums.core_members cm ON u.username = cm.name
    WHERE p.league LIKE '$league' $active $clause
    ORDER BY $order";
}
else
{
    $query = "SELECT CONCAT(plast,', ',pfirst) AS pname,u.username,CONCAT(pfirst,' ',plast) AS tname,position,position2,build,height,weight,p.league,p.active,p.id,p.user_fk,tpe,t.name AS team,t.id AS tid,p.league,u.memberid as toid,created_time as created,FROM_UNIXTIME(cm.last_activity) as last_active,cm.last_activity as activity,ape,experience,ins,jps,ft,3ps,han,pas,orb,drb,psd,prd,stl,blk,fl,qkn,str,jmp,u.active as uactive
    FROM auth_user u 
    INNER JOIN players p ON u.ID = p.user_fk 
    INNER JOIN teams t ON p.team = t.id
    INNER JOIN molholt_sba.core_members cm ON u.username = cm.name
    WHERE p.league LIKE '$league' $active $clause
    ORDER BY $order";
}
$result = mysqli_query($con,$query);
$ptable = '';
$atable = '';
$endmodal = '';
$f = '0';
while ($row = mysqli_fetch_array($result)) 
{
    if($_SESSION['isupdater'] == '1' || $_SESSION['isadmin'] == '1')
    {
        if($row['league'] == 'SBA')
        {
            $leagueteam = $sbateam;
            $managebuttons = "<li><a href='player_edit.php?pid=".$row['id']."' class='btn btn-primary'>Edit Player</a></li><li><a href='/calculator/regress-manual.php?id=".$row['id']."' class='btn btn-primary'>Regress Player</a></li>".(($_SESSION['isadmin'] == '1') ? "<li><a href='reset_tpe.php?pid=".$row['id']."' class='btn btn-primary'>Reset TPE</a></li>" : "")."";
        }
        else
        {
            $leagueteam = $sbdlteam;
            $managebuttons = "<li><a href='player_edit.php?pid=".$row['id']."' class='btn btn-primary'>Edit Player</a></li>".(($_SESSION['isadmin'] == '1') ? "<li><a href='reset_tpe.php?pid=".$row['id']."' class='btn btn-primary'>Reset TPE</a></li>" : "")."".(($_SESSION['isadmin'] == '1') ? "<li><a href='delete_player.php?pid=".$row['id']."' class='btn btn-primary'>Delete Player</a></li>" : "")."";
        }
    }
    else
    {
        $managebuttons = "";
    }
    $user = new User($row['username']);
    $carry_count = $user->has_carryover();
    $player_count = $user->player_count('sba_active_exclude',$row['id']);
    $play_count = $user->player_count('sba_retiring',$row['id']);
    if(($carry_count > 0 || $play_count == 1) && ($player_count == 0) && ($row['league'] == 'SBDL') && ($row['active'] == '1'))
    {
        $rc = "<span class='badge'>RC</span>";
    }
    else
    {
        $rc = "";
    }
    $alltable = "<tr>";
        $alltable .= "<td class='nobr'><div class='dropdown'><a href='#' class='dropdown-toggle' data-toggle='dropdown'>".$row['pname']." ".$rc.(($row['uactive'] == '0') ? "<span class='badge'>IA</span>" : "")."<span class='caret'></span></a><ul class='dropdown-menu'><li><a href='player_page.php?pid=".$row['id']."' class='btn btn-primary'>Player Page</a></li><li><a href='view_updates.php?pid=".$row['id']."' class='btn btn-primary'>Update History</a></li>".$managebuttons."</ul></div></td>";
        $ptable .= $alltable;
        $atable .= $alltable;
        $ptable .= "<td class='nobr'><div class='dropdown'><a href='#' class='dropdown-toggle' data-toggle='dropdown'>".$row['username']."<span class='caret'></span></a><ul class='dropdown-menu'><li><a href=\"view_players.php?user=".$row['username']."\" class='btn btn-primary'>View Players</a></li><li><a href='#' onclick=\"sendpm('".$row['username']."','".$row['toid']."','".$memberid."')\" class='btn btn-primary'>PM Player</a></li></ul></div></td>";
        $ptable .= "<td class='nobr'><a href='team_page.php?tid=".$row['tid']."'>".$row['team']."</a></td>";
        $ptable .= "<td>".$row['position'];
            if($row['position2'] != ''){$ptable .= "/".$row['position2'];}else{}
        $ptable .= "</td>";
        $ptable .= "<td>".$row['build']."</td>";
        if($row['active'] == '0')
        {
            $ptable .= "<td>Retired</td>";
        }
        else if($row['active'] == '1')
        {
            $ptable .= "<td>Active</td>";
        }
        else if($row['active'] == '2')
        {
            $ptable .= "<td>Filler</td>";
        }
        else if($row['active'] == '3')
        {
            $ptable .= "<td>Retiring</td>";
        }
        $ptable .= "<td>".$row['tpe']."</td>";
        if($new != '1')
        {
            $ptable .= "<td>".$row['ape']."</td>";
            $ptable .= "<td>".$row['experience']."</td>";
            $activehead = "<th>APE</th><th>Exp.</th>";
        }
        if($new == '1')
        {
            $newDate = date("m-d-y",strtotime($row['created']));
            $ptable .= "<td>".$newDate."</td>";
            if($row['activity'] == 0)
            {
                $newDate2 = '';
            }
            else
            {
                $newDate2 = date("m-d-y",strtotime($row['last_active']));
            }
            $ptable .= "<td>".$newDate2."</td>";
            $newhead = "<th>Created</th><th>Last<br>Active</th>";
        }
        for ($a=0;$a<count($lgatty);$a++)
        {
            $atable .= "<td>".$row[$lgatty[$a]]."</td>";
        }
    $ptable .= "</tr>";
    $atable .= "</tr>";
    $arow = array(
        $row['pname']
    );
    for ($a=0;$a<count($lgatty);$a++)
    {
        array_push($arow,$row[$lgatty[$a]]);
    }
    fputcsv($fp, $arow);

    $f++;
}

?>

<!-- Body Start -->
<body>
<div id="wrapper" style="position:relative;top:0;left:0;">
    <?php include $path.'/includes/nav.php'; ?>
    <div class="container">
        <?=HeaderImagePrint($hostlg,$_SESSION['style'])?>
        <?=$echo?>
        <div class="panel panel-primary">
            <div class="panel-heading">Manage Players</div>
            <div class="panel-body">
                <ul class="nav nav-tabs" style="border-bottom:0px;">
                    <li class="active"><a data-toggle="tab" href="#home">Players</a></li>
                    <li><a data-toggle="tab" href="#attributes">Attributes</a></li>
                </ul>
                <div class="tab-content">
                    <div id="home" class="tab-pane fade in active">
                        <div class="table-responsive">
                            <table class="table compact" id='viewsba' style="width:100%;">
                                <thead>
                                    <th>Name</th>
                                    <th>Username</th>
                                    <th>Team</th>
                                    <th>Pos.</th>
                                    <th>Build</th>
                                    <th>Status</th>
                                    <th>TPE</th>
                                    <?=$activehead?>
                                    <?=$newhead?>
                                </thead>
                                <tbody>
                                    <?=$ptable?>
                                </tbody>
                                <tfoot>
                                    <th>Name</th>
                                    <th>Username</th>
                                    <th>Team</th>
                                    <th>Pos.</th>
                                    <th>Build</th>
                                    <th>Status</th>
                                    <th>TPE</th>
                                    <?=$activehead?>
                                    <?=$newhead?>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <div id="attributes" class="tab-pane fade">
                        <div class='table-responsive'>
                            <table class="table compact" id='viewsba2' style="width:100%;">
                                <thead>
                                    <th>Name</th>
                                    <?=$attyheaders?>
                                </thead>
                                <tbody>
                                    <?=$atable?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class='modal fade' id='PM' role='dialog'>
            <div class='modal-dialog modal-lg'>
                <div class='modal-content'>
                    <form method='POST' action='send_pm.php'>
                        <div class='modal-content'>
                            <div class='modal-header'>
                                <button type='button' class='close' data-dismiss='modal'>&times;</button>
                                <h4 class='modal-title'>PM Player</h4>
                            </div>
                            <div class='modal-body'>
                                <p>To: <span id='toname'></span></p>
                                Title:<input type='text' name='title'><br><br>
                                <textarea class='form-control' rows='5' name='pm' id='pm'></textarea>
                                <input type='text' hidden='hidden' name='toid' id='toid' value=''>
                                <input type='text' hidden='hidden' name='fromid' id='fromid' value=''>
                            </div>
                            <div class='modal-footer'>
                                <button type='submit' class='btn btn-danger'>Send PM</button>
                                <button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Body End -->
<script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<?php
include $path.'/footer.php';
mysqli_close($con);
?>
<style>
.table-responsive > .table {
    margin-bottom: 0;
}
.table-responsive > .table > thead > tr > th, .table-responsive > .table > tbody > tr > th, .table-responsive > .table > tfoot > tr > th, .table-responsive > .table > thead > tr > td, .table-responsive > .table > tbody > tr > td, .table-responsive > .table > tfoot > tr > td {
    white-space: nowrap;
}
thead input {
    width:100% !important;
}
thead th {
 

}
.nobr {
    white-space:nowrap;
}
</style>
<script>
function sendpm(toname,toid,fromid)
{
    $('#toname').html(toname);
    $('#toid').val(toid);
    $('#fromid').val(fromid);
    $('#PM').modal();
}
$(document).ready(function() {
    // Setup - add a text input to each footer cell
    $('#viewsba thead tr').clone(true).appendTo( '#viewsba thead' );
    $('#viewsba thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input type="text" class="form-control">' );
 
        $( 'input', this ).on( 'keyup change', function () {
            if ( table.column(i).search() !== this.value ) {
                table
                    .column(i)
                    .search( this.value )
                    .draw();
            }
        } );
    } );
    var table = $('#viewsba').DataTable( {
        orderCellsTop: true,
        paging:   false,
        select: true,
        responsive: true,
        columnDefs: 
        [
            {"orderSequence": ['desc','asc'], "targets": [5]},
            {"orderSequence": ['desc','asc'], "targets": [6]},
            {"className": 'text-left', "targets": [2,3,4,5,6,7,8]}
        ]
    } );
    $('#viewsba2').DataTable( {
        paging:   false,
        select: true,
        columnDefs:
        [
            {"orderSequence": ['asc','asc'], "targets": [0]},
            {"orderSequence": ['desc','asc'], "targets": [1,2,3,4,5,6,7,8,9,10,11,12]}
        ]
    } );
    $('[data-toggle="popover"]').popover();
    $('.table-responsive').on('show.bs.dropdown', function () {
        $('.table-responsive').css( "overflow", "inherit" );
    });
    $('.table-responsive').on('hide.bs.dropdown', function () {
        $('.table-responsive').css( "overflow", "auto" );
    })
} );
</script>