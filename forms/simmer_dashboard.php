<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
if($_SESSION['issimmer'] != '1'){
header("location:/home.php?alert=simmerpermission");
}
$user=$_SESSION['user'];
// year
$system = new System();
$year = $system->get_year();

$oldlgatty = Array();
$attyquerystring = '';
$oldattyquerystring = '';
for($x=0;$x<count($lgatty);$x++)
{
    if($x == 0)
    {
        $attyquerystring .= "`".$lgatty[$x]."`";
        $oldattyquerystring .= "`".$lgatty[$x]."_old`";
        array_push($oldlgatty,$lgatty[$x]."_old");
    }
    else
    {
        $attyquerystring .= ",`".$lgatty[$x]."`";
        $oldattyquerystring .= ",`".$lgatty[$x]."_old`";
        array_push($oldlgatty,$lgatty[$x]."_old");
    }
}

$query = "SELECT pu.id as puid,p.id,CONCAT(pfirst,' ',plast) AS pname,p.league,active,$attyquerystring,status,build,c90,c801,c802,c803,c804,c70,t.name as tname,p.tpe,p.bank as old_bank,pu.earn_tpe,pu.bank as new_bank,pu.last_submitted,pu.approved,pu.approver,pu.notes FROM players p 
            INNER JOIN player_updates pu ON p.ID=pu.pid_fk
            INNER JOIN teams t ON p.team = t.id
            WHERE pu.status = '2'
            ORDER BY CASE WHEN p.league='$mainlg' THEN 1
            WHEN p.league='$sublg' THEN 2
            ELSE 3
            END, tname,pname";
$result = mysqli_query($con,$query);
$sba = 0;
$sba2 = 0;
$ncaa = 0;
$ncaa2 = 0;
$retire = 0;
$retire2 = 0;
$retired = 0;
$change = 0;
$echo = '';
$echo2 = '';
$echo3 = '';
$echo4 = '';
$echo7 = '';
$echo8 = '';
$echo9 = '';
$i = 0;
$status = '';
while ($r = mysqli_fetch_array($result))
{
    $timestamp = $r['approved']." by ".$r['approver'];
    $concat = "
    <div class='panel panel-primary'>
        <div class='panel-heading hidden-xs'><h4 class='panel-title'><a data-toggle='collapse' href='#collapse$i'>".$r['pname']." - ".$r['tname']."</a> <span class='pull-right timestamp' style='background-color:transparent;'>(".$timestamp.")</span></h4></div>
        <div class='panel-heading hidden-sm hidden-md hidden-lg'><h4 class='panel-title'><a data-toggle='collapse' href='#collapse$i'>".$r['pname']."<br>".$r['tname']."<br><span style='background-color:transparent;'>(".$timestamp.")</span></a></h4></div>
        <div class='panel-collapse collapse' id='collapse$i'>
            <div class='panel-body'>
                <ul class='nav nav-tabs' style='border-bottom:0px;'>
                    <li class='active'><a data-toggle='tab' href='#current$i'>Current</a></li>
                    <li><a data-toggle='tab' href='#history$i'>History</a></li>
                </ul>
                <div class='tab-content'>
                    <div id='current$i' class='tab-pane fade in active'>
                        <a href='player_page.php?pid=".$r['id']."' class='btn btn-primary col-xs-12 hidden-sm hidden-md hidden-lg' target='_blank'>Player Page</a>";
                        $puid = $r['puid'];
                        $pid = $r['id'];
                        $pname = $r['pname'];
                        $concat .= "<table class='table'>
                            <thead>
                                <tr>
                                    <th class='hidden-xs'>Attribute Name</th>
                                    <th class='hidden-sm hidden-md hidden-lg'>Attribute</th>
                                    <th>Current</th>
                                    <th>Now</th>
                                </tr>
                            </thead>
                            <tbody>";
                        $sql = "SELECT $attyquerystring,$oldattyquerystring FROM player_updates_update WHERE puid_fk='$puid'";
                        $result3 = mysqli_query($con,$sql);
                        $attconcat = '';
                        $listarr = Array('Inside Scoring','Jump Shot','Free Throw','Three Point','Handling','Passing','Offensive Rebounding','Defensive Rebounding','Post Defense','Perimeter Defense','Stealing','Blocking','Fouling','Quickness','Strength','Jumping');
                        $listarr_short = Array('ins','jps','ft','3ps','han','pas','orb','drb','psd','prd','stl','blk','fl','qkn','str','jmp');
                        $listarrold_short = Array('ins_old','jps_old','ft_old','3ps_old','han_old','pas_old','orb_old','drb_old','psd_old','prd_old','stl_old','blk_old','fl_old','qkn_old','str_old','jmp_old');
                        while($w = mysqli_fetch_array($result3))
                        {
                            for($f=0;$f<count($lgatty);$f++)
                            {
                                if($r['build'] == 'Standard')
                                {
                                    if($r['c90'] == $lgatty[$f])
                                    {
                                        $max = " (MAX:90)";
                                    }
                                    else if($r['c801'] == $lgatty[$f] || $r['c802'] == $lgatty[$f] || $r['c803'] == $lgatty[$f] || $r['c804'] == $lgatty[$f])
                                    {
                                        $max = " (MAX:80)";
                                    }
                                    else if($r['c70'] == $lgatty[$f])
                                    {
                                        $max = " (MAX:70)";
                                    }
                                    else
                                    {
                                        $max = "";
                                    }
                                }
                                else
                                {
                                    $max = "";
                                }
                                if($w[$oldlgatty[$f]] == $w[$lgatty[$f]])
                                {
                                    $concat .= '';
                                }
                                else
                                {
                                    $concat .= "
                                    <tr>
                                        <td>".$lgatty[$f]."".$max."</td>
                                        <td>".$w[$oldlgatty[$f]]."</td>
                                        <td>".$w[$lgatty[$f]]."</td>
                                    </tr>";
                                }
                            }
                        }
                        $concat .= "
                            </tbody>
                        </table>";
                        if($hostlg == 'SBA')
                        {
                            $sql = "SELECT 3ps_toggle FROM player_updates_update WHERE puid_fk='$puid'";
                            $d = mysqli_fetch_array(mysqli_query($con,$sql));
                            $tpstoggle = $d['3ps_toggle'];
                            if($tpstoggle == '1')
                            {
                                $tpsins = '';
                            }
                            else
                            {
                                $tpsins = "Please ensure this player's 3PT is set to 0.";
                            }
                        }
                        else
                        {
                            $tpsins = '';
                        }
                        $concat .= "<div class='input-group col-xs-12'><span class='input-group-addon' style='width:120px;text-align:left;'>Notes</span><input type='text' class='form-control' readonly='readonly' value=\"".$r['notes']." ".$tpsins."\"></div><br>";
                        if($r['status'] == '2' && ($_SESSION['issimmer'] == '1' || $_SESSION['isadmin'] == '1'))
                        {
                        $concat .= "<div class='form-group'>
                            <div class='col-sm-offset-1 col-sm-10'>
                                <a href='#clear' data-toggle='modal' onclick=\"clearupdate('".$puid."','".mysqli_real_escape_string($con,$pname)."')\" class='btn btn-primary'>Clear</a>
                            </div>
                        </div>";
                        }
                        else
                        {

                        }
            $concat .= "
                    </div>
                    <div id='history$i' class='tab-pane fade'>";
                        $concat .= "
                    </div>
                </div>
            </div>
        </div>
    </div>";
    $i++;
    if($r['status'] == '2')
    {
        if ($r['league'] == $mainlg)
        {
            $echo .= $concat;
            $sba++;
        }
        else
        {
            $echo2 .= $concat;
            $ncaa++;
        }
    }
}
// End of Update Section

// Start of Strategy Section
$query = "SELECT cu.id as cid,c.id as cidfk,c.name as cname,c.league,t.name as tname,cuu.pnamec,cuu.posc,cuu.minc,cuu.pnamepf,cuu.pospf,cuu.minpf,cuu.pnamesf,cuu.possf,cuu.minsf,cuu.pnamesg,cuu.possg,cuu.minsg,cuu.pnamepg,cuu.pospg,cuu.minpg,cuu.pname6,cuu.pos6,cuu.min6,cuu.pname7,cuu.pos7,cuu.min7,cuu.pname8,cuu.pos8,cuu.min8,cuu.pname9,cuu.pos9,cuu.min9,cuu.pname10,cuu.pos10,cuu.min10,cuu.pname11,cuu.pos11,cuu.min11,cuu.pname12,cuu.pos12,cuu.min12,cuu.key1,cuu.key2,cuu.key3,cuu.pace,cuu.motion,cuu.tpuse,cuu.focus,cuu.crasho,cuu.setprimary,cuu.primaryd,cuu.setsecondary,cuu.secondd,cuu.fullcourt,cuu.crashd FROM coaches c INNER JOIN coach_updates cu ON c.id=cu.cid_fk INNER JOIN coach_updates_update cuu ON cu.id=cuu.cuid_fk INNER JOIN teams t ON c.id=t.id WHERE cu.status = '1'";
$result = mysqli_query($con,$query);
$k = 0;
while ($r = mysqli_fetch_array($result))
{
    $cidfk = $r['cidfk'];
    $concat = '';
    if ($r['focus'] == '0'){$focus = 'Inside';}
    else if ($r['focus'] == '1'){$focus = 'Balanced';}
    else {$focus = 'Outside';}
    $posarr = Array('c','pf','sf','sg','pg','6','7','8','9','10','11','12');
    $poslabelarr = Array('Center','Power Forward','Small Forward','Shooting Guard','Point Guard','6th','7th','8th','9th','10th','11th','12th');
    $sql = "SELECT * FROM coaches WHERE id = '$cidfk'";
    $result2 = mysqli_query($con,$sql);
    $w = mysqli_fetch_array($result2);
    $concat .= "
    <div class='panel panel-primary'>
        <div class='panel-heading'><h4 class='panel-title'><a data-toggle='collapse' href='#collapsed$k'>".$r['cname']."(".$r['tname'].")</a></h4></div>
        <div class='panel-collapse collapse' id='collapsed$k'>
            <div class='panel-body'>
                <ul class='nav nav-tabs' style='border-bottom:0px;'>
                    <li class='active'><a data-toggle='tab' href='#changes$k'>Changes</a></li>
                    <li><a data-toggle='tab' href='#all$k'>All</a></li>
                </ul>
                <div class='tab-content'>
                    <div id='changes$k' class='tab-pane fade in active'>
                        <table class='table'>
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Name</th>
                                    <th>Position(s)</th>
                                    <th>Minutes</th>
                                </tr>
                            </thead>
                            <tbody>";
    for($d=0;$d<count($posarr);$d++)
    {
        $pname = "pname".$posarr[$d];
        $pos = "pos".$posarr[$d];
        $min = "min".$posarr[$d];
        if(($r[$pname] != $w[$pname]) || ($r[$pos] != $w[$pos]) || ($r[$min] != $w[$min]))
        {
            $concat .="
            <tr>
                <td>$poslabelarr[$d]:</td>";
            if($r[$pname] != $w[$pname])
            {
                $concat .= "<td><span style='color:red;'>".$r[$pname]."</span></td>";
            }
            else
            {
                $concat .= "<td>".$r[$pname]."</td>";
            }
            if($r[$pos] != $w[$pos])
            {
                $concat .= "<td><span style='color:red;'>".$r[$pos]."</span></td>";
            }
            else
            {
                $concat .= "<td>".$r[$pos]."</td>";
            }
            if($r[$min] != $w[$min])
            {
                $concat .= "<td><span style='color:red;'>".$r[$min]."</span></td>";
            }
            else
            {
                $concat .= "<td>".$r[$min]."</td>";
            }
            $concat .= "</tr>";
        }
        else
        {

        }
    }
    $concat .= "</tbody></table>";
    $concat .= "<table class='table'>
        <thead>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>";
    if($r['key1'] != $w['key1'] || $r['key2'] != $w['key2'] || $r['key3'] != $w['key3'])
    {
        $concat .="
            <tr>
                <td>Options:</td>
                <td>".$r['key1']."</td>
                <td>".$r['key2']."</td>
                <td>".$r['key3']."</td>
            </tr>";
    }
    $stratarr = Array('pace','motion','tpuse','focus','crasho','setprimary','primaryd','setsecondary','secondd','fullcourt','crashd');
    $stratlabelarr = Array('Pace','Motion','3P Usage','Focus','Crash Offensive Boards','Set Primary Defense','Primary Defense Usage','Set Secondary Defense','Secondary Defense Usage','Full Court Press','Crash Defensive Boards');
    for($d=0;$d<count($stratarr);$d++)
    {
        if($r[$stratarr[$d]] != $w[$stratarr[$d]])
        {
            if($stratarr[$d] == 'focus')
            {
                $concat .="
                <tr>
                    <td>$stratlabelarr[$d]:</td>
                    <td>".$focus."</td>
                    <td></td>
                    <td></td>
                </tr>";
            }
            else
            {
                $concat .="
                <tr>
                    <td>$stratlabelarr[$d]:</td>
                    <td>".$r[$stratarr[$d]]."</td>
                    <td></td>
                    <td></td>
                </tr>";
            }    
        }
        else
        {

        }
    }
    $concat .= "</tbody>
        </table>
        <div class='form-group'>
            <div class='col-sm-offset-2 col-sm-10'>
                <a href='#approve' data-toggle='modal' onclick=\"approvestrategy('".$r['cid']."')\" class='btn btn-primary'>Approve</a>
            </div>
        </div>
    </div>
    <div id='all$k' class='tab-pane fade'>
        <table class='table'>
            <thead>
                <tr>
                    <th></th>
                    <th>Name</th>
                    <th>Position(s)</th>
                    <th>Minutes</th>
                </tr>
            </thead>
            <tbody>";
            for($d=0;$d<count($posarr);$d++)
            {
                $pname = "pname".$posarr[$d];
                $pos = "pos".$posarr[$d];
                $min = "min".$posarr[$d];
                $concat .="
                    <tr>
                        <td>$poslabelarr[$d]:</td>";
                    if($r[$pname] != $w[$pname])
                    {
                        $concat .= "<td><span style='color:red;'>".$r[$pname]."</span></td>";
                    }
                    else
                    {
                        $concat .= "<td>".$r[$pname]."</td>";
                    }
                    if($r[$pos] != $w[$pos])
                    {
                        $concat .= "<td><span style='color:red;'>".$r[$pos]."</span></td>";
                    }
                    else
                    {
                        $concat .= "<td>".$r[$pos]."</td>";
                    }
                    if($r[$min] != $w[$min])
                    {
                        $concat .= "<td><span style='color:red;'>".$r[$min]."</span></td>";
                    }
                    else
                    {
                        $concat .= "<td>".$r[$min]."</td>";
                    }
                    $concat .= "</tr>";
            }
            $concat .= "</tbody></table>";
            $concat .= "<table class='table'>
                            <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>";
                            $concat .="
                                <tr>
                                    <td>Options:</td>
                                    <td>".$r['key1']."</td>
                                    <td>".$r['key2']."</td>
                                    <td>".$r['key3']."</td>
                                </tr>";    
                        $stratarr = Array('pace','motion','tpuse','focus','crasho','setprimary','primaryd','setsecondary','secondd','fullcourt','crashd');
                        $stratlabelarr = Array('Pace','Motion','3P Usage','Focus','Crash Offensive Boards','Set Primary Defense','Primary Defense Usage','Set Secondary Defense','Secondary Defense Usage','Full Court Press','Crash Defensive Boards');
                        for($d=0;$d<count($stratarr);$d++)
                        {
                            if($stratarr[$d] == 'focus')
                            {
                                $concat .="
                                <tr>
                                    <td>$stratlabelarr[$d]:</td>
                                    <td>".$focus."</td>
                                    <td></td>
                                    <td></td>
                                </tr>";
                            }
                            else
                            {
                                $concat .="
                                <tr>
                                    <td>$stratlabelarr[$d]:</td>
                                    <td>".$r[$stratarr[$d]]."</td>
                                    <td></td>
                                    <td></td>
                                </tr>";
                            }    
                        }
                    $concat .= "
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>";
    $k++;
    if ($r['league'] == 'SBA')
    {
        $echo3 .= $concat;
        $sba2++;
    }
    else
    {
        $echo4 .= $concat;
        $ncaa2++;
    }
}
// End of Strategy Section

//Start of Player Changes Section
$query = "SELECT c.id,CONCAT(p.pfirst,' ',p.plast) AS pname,p.id as pid,p.build,c.plast,c.pfirst,c.position,c.position2,c.num,c.height,c.weight,c.country FROM players p INNER JOIN player_changes c ON p.ID=c.pid_fk WHERE c.status = '1' ORDER BY pname";
$result = mysqli_query($con,$query);
$listarr = Array('Inside Scoring','Jump Shot','Free Throw','Three Point','Handling','Passing','Offensive Rebounding','Defensive Rebounding','Post Defense','Perimeter Defense','Stealing','Blocking','Fouling','Quickness','Strength','Jumping');
$attarr = Array('plast','pfirst','position','position2','num','height','weight','country');
$attlabarr = Array('Last Name','First Name','Position','Secondary','Number','Height','Weight','Country');
$val = '';
while ($r = mysqli_fetch_array($result))
{
    $concat = "
    <div class='panel panel-primary'>
        <div class='panel-heading hidden-xs'><h4 class='panel-title'><a data-toggle='collapse' href='#change$change'>".$r['pname']."</a></h4></div>
        <div class='panel-heading hidden-sm hidden-md hidden-lg'><h4 class='panel-title'><a data-toggle='collapse' href='#change$change'>".$r['pname']."</a></h4></div>
        <div class='panel-collapse collapse' id='change$change'>
            <div class='panel-body'>
                <table class='table'>
                    <thead>
                        <tr>
                            <th class='hidden-xs'>Attribute Name</th>
                            <th class='hidden-sm hidden-md hidden-lg'>Attribute</th>
                            <th>Current</th>
                            <th>Now</th>
                        </tr>
                    </thead>
                    <tbody>";
                    $pid = $r['pid'];
                    $sql = "SELECT plast,pfirst,position,position2,num,height,weight,country FROM players WHERE id='$pid'";
                    $player = mysqli_fetch_array(mysqli_query($con,$sql));
                    for($f=0;$f<count($attarr);$f++)
                    {
                        $concat .= "
                        <tr>
                            <td>".$attlabarr[$f]."</td>
                            <td>".$player[$attarr[$f]]."</td>
                            <td>".$r[$attarr[$f]]."</td>
                        </tr>";
                    }
                    for($f=0;$f<count($listarr);$f++)
                    {
                        if($r['build'] == 'Standard')
                        {
                            if($listarr[$f] == 'Fouling')
                            {
                                $val = '50';
                            }
                            else
                            {
                                $val = '35';
                            }
                        }
                        else
                        {
                            if(($listarr[$f] == 'Fouling') || ($listarr[$f] == 'Quickness') || ($listarr[$f] == 'Strength') || ($listarr[$f] == 'Jumping'))
                            {
                                $val = '20';
                            }
                            else
                            {
                                $val = '35';
                            }
                        }
                        $concat .= "
                        <tr>
                            <td>".$listarr[$f]."</td>
                            <td>0</td>
                            <td>".$val."</td>
                        </tr>";
                    }
                    $concat .= "
                    </tbody>
                </table>";
                $concat .="
                <div class='col-xs-12'>
                    <a href='#change' data-toggle='modal' onclick=\"changeplayer('".$r['id']."')\" class='btn btn-primary'>Process</a>
                </div>
            </div>
        </div>
    </div>";
    $echo8 .= $concat;
    $change++;
}
// End of Player Changes Section

// Start of Retired Players Section
$query = "SELECT p.id,CONCAT(pfirst,' ',plast) AS pname,league FROM players p WHERE active='0' AND retiredinfile='0' AND (league = 'SBA' || league = 'SBDL') ORDER BY pname DESC";
$result = mysqli_query($con,$query);
while ($r = mysqli_fetch_array($result))
{
    $concat = "
    <div class='panel panel-primary'>
        <div class='panel-heading hidden-xs'><h4 class='panel-title'><a data-toggle='collapse' href='#retire$retired'>".$r['pname']."</a></h4></div>
        <div class='panel-heading hidden-sm hidden-md hidden-lg'><h4 class='panel-title'><a data-toggle='collapse' href='#retire$retire'>".$r['pname']."</a></h4></div>
        <div class='panel-collapse collapse' id='retire$retired'>
            <div class='panel-body'>
                <div class='col-xs-12'>
                    <a href='#retire' data-toggle='modal' onclick=\"retireplayer('".$r['id']."')\" class='btn btn-primary'>Retire</a>
                </div>
            </div>
        </div>
    </div>";
    $retired++;
    if ($r['league'] == 'SBA')
    {
        $echo7 .= $concat;
        $retire++;
    }
    else if($r['league'] == 'SBDL')
    {
        $echo9 .= $concat;
        $retire2++;
    }
}


// End of Retired Players Section


$echo = '<div class="panel panel-default">
            <div class="panel-heading"><h4 class="panel-title"><a data-toggle="collapse" href="#approvedsba">'.$mainlg.' ('.$sba.' New)</a></h4></div><div class="panel-collapse collapse" id="approvedsba">
            <div class="panel-body">' . $echo;
$echo2 = '<div class="panel panel-default">
            <div class="panel-heading"><h4 class="panel-title"><a data-toggle="collapse" href="#approvedncaa">'.$sublg.' ('.$ncaa.' New)</a></h4></div><div class="panel-collapse collapse" id="approvedncaa">
            <div class="panel-body">' . $echo2;
$echo .= "</div></div></div>";
$echo2 .= "</div></div></div>";
$echo3 = '<div class="panel panel-default">
            <div class="panel-heading"><h4 class="panel-title"><a data-toggle="collapse" href="#strategysba">'.$mainlg.' ('.$sba2.' New)</a></h4></div><div class="panel-collapse collapse" id="strategysba">
            <div class="panel-body">' . $echo3;
$echo4 = '<div class="panel panel-default">
            <div class="panel-heading"><h4 class="panel-title"><a data-toggle="collapse" href="#strategyncaa">'.$sublg.' ('.$ncaa2.' New)</a></h4></div><div class="panel-collapse collapse" id="strategyncaa">
            <div class="panel-body">' . $echo4;
$echo3 .= "</div></div></div>";
$echo4 .= "</div></div></div>";
$echo7 = '<div class="panel panel-default">
            <div class="panel-heading"><h4 class="panel-title"><a data-toggle="collapse" href="#retired">'.$mainlg.' ('.$retire.' New)</a>   <a href="#" class="btn btn-default" onclick="processRetired(\'SBA\')">Process All</a></h4></div><div class="panel-collapse collapse" id="retired">
            <div class="panel-body">' . $echo7;
$echo8 = '<div class="panel panel-default">
            <div class="panel-heading"><h4 class="panel-title"><a data-toggle="collapse" href="#changed">'.$sublg.' ('.$change.' New)</a></h4></div><div class="panel-collapse collapse" id="changed">
            <div class="panel-body">' . $echo8;
$echo9 = '<div class="panel panel-default">
            <div class="panel-heading"><h4 class="panel-title"><a data-toggle="collapse" href="#retired2">'.$sublg.' ('.$retire2.' New)</a> <a href="#" class="btn btn-default" onclick="processRetired(\'SBDL\')">Process All</a></h4></div><div class="panel-collapse collapse" id="retired2">
            <div class="panel-body">' . $echo9;
$echo7 .= "</div></div></div>";
$echo8 .= "</div></div></div>";
$echo9 .= "</div></div></div>";


?>

<!-- Body Start -->
<body>
<div id="wrapper" style="position:relative;top:0;left:0;">
    <?php include $path.'/includes/nav.php'; ?>
    <div class="container">
        <?=HeaderImagePrint($hostlg,$_SESSION['style'])?>
        <div class="panel-group">
            <div class="panel panel-primary">
                <div class="panel-heading">Simmer Dashboard</div>
                <div class="panel-body">
                    <div class="panel-group">
                        <div class="panel panel-primary">
                            <div class="panel-heading"><h4 class="panel-title">Processed Updates</h4></div>
                            <div class="panel-body">
                                <?=$echo?>
                                <?=$echo2?>
                            </div>
                        </div>
                    </div>
                    <div class="panel-group">
                        <div class="panel panel-primary">
                            <div class="panel-heading"><h4 class="panel-title">Strategy Changes</h4></div>
                            <div class="panel-body">
                                <?=$echo3?>
                                <?=$echo4?>
                            </div>
                        </div>
                    </div>
                    <div class="panel-group">
                        <div class="panel panel-primary">
                            <div class="panel-heading"><h4 class="panel-title">Player Changes</h4></div>
                            <div class="panel-body">
                                <?=$echo8?>
                            </div>
                        </div>
                    </div>
                    <div class="panel-group">
                        <div class="panel panel-primary">
                            <div class="panel-heading"><h4 class="panel-title">Retired Players</h4></div>
                            <div class="panel-body">
                                <?=$echo7?>
                                <?=$echo9?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class='modal fade' id='clear' role='dialog'>
            <div class='modal-dialog modal-lg'>
                <form method='POST' action='clear_update.php'>
                    <div class='modal-content'>
                        <div class='modal-header'>
                            <button type='button' class='close' data-dismiss='modal'>&times;</button>
                            <h4 class='modal-title'>Clear Update</h4>
                        </div>
                        <div class='modal-body'>
                            <p>This will clear this players' update from the system and update their roster page accordingly. Only do this once you have updated the player in the file. You can use the box below to send a PM to the player letting them know you have updated them in the file, and cleared this update. Are you sure you want to do this?</p>
                            <textarea class='form-control' rows='5' name='clearpm'></textarea>
                            <input type='text' hidden='hidden' name='id' id='clearid' value="">
                            <input type='text' hidden='hidden' name='pname' id='clearpname' value="">
                        </div>
                        <div class='modal-footer'>
                            <button type='submit' class='btn btn-danger'>Clear Update</button>
                            <button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class='modal fade' id='approve' role='dialog'>
            <div class='modal-dialog modal-lg'>
                <form method='POST' action='approve_strategy.php'>
                    <div class='modal-content'>
                        <div class='modal-header'>
                            <button type='button' class='close' data-dismiss='modal'>&times;</button>
                            <h4 class='modal-title'>Approve Update</h4>
                        </div>
                        <div class='modal-body'>
                            <p>This will clear this GM's Strategy from the system and save it as their new default in the system. You can use the box below to send a PM to the GM letting them know you have updated their strategies. Are you sure you want to do this?</p>
                            <textarea class='form-control' rows='5' name='approvepm'></textarea>
                            <input type='text' hidden='hidden' name='id' id='approveid' value=''>
                        </div>
                        <div class='modal-footer'>
                            <button type='submit' class='btn btn-danger'>Approve Update</button>
                            <button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class='modal fade' id='retire' role='dialog'>
            <div class='modal-dialog modal-lg'>
                <form method='GET' action='retire_player.php'>
                    <div class='modal-content'>
                        <div class='modal-header'>
                            <button type='button' class='close' data-dismiss='modal'>&times;</button>
                            <h4 class='modal-title'>Retire Player</h4>
                        </div>
                        <div class='modal-body'>
                            <p>This will mark the player in SBAO as having been retired in the engine. Click Retire to proceed.</p>
                            <input type='text' hidden='hidden' name='id' id='retirepid' value=''>
                            <input type='text' hidden='hidden' name='infile' id='retireinfile' value='1'>
                        </div>
                        <div class='modal-footer'>
                            <button type='submit' class='btn btn-danger'>Retire Player</button>
                            <button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class='modal fade' id='change' role='dialog'>
            <div class='modal-dialog modal-lg'>
                <form method='POST' action='change_player.php'>
                    <div class='modal-content'>
                        <div class='modal-header'>
                            <button type='button' class='close' data-dismiss='modal'>&times;</button>
                            <h4 class='modal-title'>Change Player</h4>
                        </div>
                        <div class='modal-body'>
                            <p>This will change the player's demographic data within SBAO. Click Retire to proceed.</p>
                            <textarea class='form-control' rows='5' name='changepm'></textarea>
                            <input type='text' hidden='hidden' name='id' id='changeid' value=''>
                        </div>
                        <div class='modal-footer'>
                            <button type='submit' class='btn btn-danger'>Change Player</button>
                            <button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Body End -->

<?php
include $path.'/footer.php';
?>

<script>
function clearupdate(puid,pname)
{
    $('#clearid').val(puid);
    $('#clearpname').val(pname);
}
function approvestrategy(cid)
{
    $('#approveid').val(cid);
}
function retireplayer(pid)
{
    $('#retirepid').val(pid);
}
function changeplayer(id)
{
    $('#changeid').val(id);
}
function processRetired(league)
{
    theurl = "retire_player.php?league="+league+"&process=1";
    window.location.href = theurl;
}
</script>