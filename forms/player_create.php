<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
$user=$_SESSION['user'];
$mid = $_SESSION['memberid'];
// year
$system = new System();
$year = $system->get_year();

// get id
$userobj = new User($user);
$id = $userobj->get_('id');
$pfirst = isset($_POST['pfirst']) ? sanitize($con,$_POST['pfirst']) : '';
$pfirst = ucwords($pfirst);
$pfirst = trim($pfirst);
$plast = isset($_POST['plast']) ? sanitize($con,$_POST['plast']) : '';
$plast = ucwords($plast);
$plast = trim($plast);
$position = isset($_POST['position']) ? sanitize($con,$_POST['position']) : '';
$position2 = isset($_POST['position2']) ? sanitize($con,$_POST['position2']) : '';
$build = isset($_POST['build']) ? sanitize($con,$_POST['build']) : '';
$num = isset($_POST['num']) ? sanitize($con,$_POST['num']) : '';
$height = isset($_POST['height']) ? sanitize($con,$_POST['height']) : '';
$weight = isset($_POST['weight']) ? sanitize($con,$_POST['weight']) : '';
$country = isset($_POST['country']) ? sanitize($con,$_POST['country']) : '';
$country = ucwords($country);
$c90 = isset($_POST['c90']) ? sanitize($con,$_POST['c90']) : '';
$c901 = isset($_POST['c901']) ? sanitize($con,$_POST['c901']) : '';
$c801 = isset($_POST['c801']) ? sanitize($con,$_POST['c801']) : '';
$c802 = isset($_POST['c802']) ? sanitize($con,$_POST['c802']) : '';
$c803 = isset($_POST['c803']) ? sanitize($con,$_POST['c803']) : '';
$c804 = isset($_POST['c804']) ? sanitize($con,$_POST['c804']) : '';
$c70 = isset($_POST['c70']) ? sanitize($con,$_POST['c70']) : '';

// Position
if($hostlg == 'EFL')
{
    $listarr_pos = Array('QB','RB','FB','WR','TE','LB','CB','FS','SS','K');
}
else
{
    $listarr_pos = Array('PG','SG','SF','PF','C');
}
$listpos = "";
for ($a=0;$a<count($listarr_pos);$a++)
{
    if($position == $listarr_pos[$a]) {$echo = "selected";} else {$echo = "";}
    $listpos .= "<option value='$listarr_pos[$a]' $echo>$listarr_pos[$a]</option>";
}
$listpos2 = "";
for ($a=0;$a<count($listarr_pos);$a++)
{
    if($position2 == $listarr_pos[$a]) {$echo = "selected";} else {$echo = "";}
    $listpos2 .= "<option value='$listarr_pos[$a]' $echo>$listarr_pos[$a]</option>";
}

// Build
if($hostlg == 'EFL')
{
    $buildarr = Array('Pocket Passer','Field General','Gunslinger','Dual-Threat');
}
else
{
    $buildarr = Array('Standard','Freak');
}
$listbuild = "";
for ($a=0;$a<count($buildarr);$a++)
{
    if($build == $buildarr[$a]) {$echo = "selected";} else {$echo = "";}
    $listbuild .= "<option value='$buildarr[$a]' $echo>$buildarr[$a]</option>";
}

// Height
if($hostlg == 'EFL')
{
    $heightarr = Array("6'0","6'1","6'2","6'3","6'4","6'5","6'6");
}
else
{
    $heightarr = Array("5'0","5'1","5'2","5'3","5'4","5'5","5'6","5'7","5'8","5'9","5'10","5'11","6'0","6'1","6'2","6'3","6'4","6'5");
}
$listheight = "";
for ($a=0;$a<count($heightarr);$a++)
{
    if($height == $heightarr[$a]) {$echo = "selected";} else {$echo = "";}
    $listheight .= "<option value=\"$heightarr[$a]\" $echo>$heightarr[$a]</option>";
}

// generate FIBA Countries
$fiba = Array('Africa','Greece','Argentina','Canada','Mexico','China','Germany','New Zealand','United States','Australia','Japan','Brazil','United Kingdom + Ireland','Latvia','France','Russia','Croatia','Lithuania');
sort($fiba);
$fibalist = '';
for($c=0;$c<count($fiba);$c++)
{
    $fibalist .= "<option value='$fiba[$c]'>$fiba[$c]</option>";
}

$listarr = Array('Inside Scoring','Jump Shot','Free Throw','Three Point','Handling','Passing','Offensive Rebounding','Defensive Rebounding','Post Defense','Perimeter Defense','Stealing','Blocking','Fouling','Quickness','Strength','Jumping');
$listarr_short = Array('ins','jps','ft','3ps','han','pas','orb','drb','psd','prd','stl','blk','fl','qkn','str','jmp');
$list = "";
for ($a=0;$a<count($listarr);$a++)
{
    $list .= "<option value='$listarr_short[$a]'>$listarr[$a]</option>";
}

if(isset($_POST['plast']) AND isset($_POST['pfirst']) AND $_POST['plast'] != '' AND $_POST['pfirst'] != '') 
{
    $capArr = Array($_POST['c90'],$_POST['c801'],$_POST['c802'],$_POST['c803'],$_POST['c804'],$_POST['c70']);
    $capArr2 = array_unique($capArr);
    $sql = "SELECT * FROM players WHERE user_fk='$id' AND league='$sublg' AND active ='1'";
    $result = mysqli_query($con,$sql);
    $count = mysqli_num_rows($result);
    $sql = "SELECT * FROM players WHERE user_fk='$id' AND league='$mainlg' AND active ='1'";
    $result2 = mysqli_query($con,$sql);
    $count2 = mysqli_num_rows($result2);
    $count3 = $count+$count2;
    if($count == 0 && $hostlg == 'SBA')
    {
        if(($capArr[5] == '3ps'))
        {
            $_GET['alert'] = 'threepoint';
        }
        else
        {
            if(in_array('prd',$capArr) && in_array('psd',$capArr))
            {
                $_GET['alert'] = 'defensecaps';
            }
            else
            {
                if((in_array($listarr_short[0],$capArr) || in_array($listarr_short[1],$capArr) || in_array($listarr_short[2],$capArr) || in_array($listarr_short[3],$capArr) || in_array($listarr_short[4],$capArr) || in_array($listarr_short[5],$capArr) || in_array($listarr_short[6],$capArr)) && (in_array($listarr_short[7],$capArr) || in_array($listarr_short[8],$capArr) || in_array($listarr_short[9],$capArr) || in_array($listarr_short[10],$capArr) || in_array($listarr_short[11],$capArr) || in_array($listarr_short[12],$capArr)))
                {
                    if(count($capArr) == count($capArr2))
                    {
                        if(($_POST['build'] == 'Freak' && (in_array($listarr_short[13],$capArr) || in_array($listarr_short[14],$capArr) || in_array($listarr_short[15],$capArr))) || $_POST['build'] == 'Standard')
                        {
                            if($build == 'Standard')
                            {
                                $checkquery = "SELECT * FROM players WHERE plast = \"$plast\" AND pfirst = \"$pfirst\"";
                                $checkrows = mysqli_num_rows(mysqli_query($con,$checkquery));
                                if($checkrows > 0)
                                {
                                    $_GET['alert'] = 'nameused';
                                }
                                else if((strlen($pfirst) < 3) || (strlen($plast) < 3))
                                {
                                    $_GET['alert'] = 'namelength';
                                }
                                else
                                {
                                    $query = "INSERT INTO players (user_fk,plast,pfirst,position,position2,build,num,height,weight,country,league,fl,qkn,str,jmp,c90,c801,c802,c803,c804,c70) VALUES ('$id','$plast','$pfirst','$position','$position2','$build','$num','$height','$weight','$country','SBDL','50','35','35','35','$c90','$c801','$c802','$c803','$c804','$c70')";
                                    mysqli_query($con,$query);
                                    $pid = mysqli_insert_id($con); //gets the id of the player added
                                    //Send SBAO notifications to ADs
                                    cap_notification($con,$pid);
                                    header("location:/forms/player_create.php?alert=success");
                                }
                            }
                            else
                            {
                                $checkquery = "SELECT * FROM players WHERE plast = \"$plast\" AND pfirst = \"$pfirst\"";
                                $checkrows = mysqli_num_rows(mysqli_query($con,$checkquery));
                                if($checkrows > 0)
                                {
                                    $_GET['alert'] = 'nameused';
                                }
                                else if((strlen($pfirst) == 0) || (strlen($plast) == 0))
                                {
                                    $_GET['alert'] = 'namelength';
                                }
                                else
                                {
                                    $query = "INSERT INTO players (user_fk,plast,pfirst,position,position2,build,num,height,weight,country,league,fl,qkn,str,jmp,c90,c801,c802,c803,c804,c70) VALUES ('$id','$plast','$pfirst','$position','$position2','$build','$num','$height','$weight','$country','SBDL','20','20','20','20','$c90','$c801','$c802','$c803','$c804','$c70')";
                                    mysqli_query($con,$query);
                                    $pid = mysqli_insert_id($con); //gets the id of the player added
                                    //Send SBAO notifications to ADs
                                    cap_notification($con,$pid);
                                    header("location:/forms/player_create.php?alert=success");
                                } 
                            }
                        }
                        else
                        {
                            $_GET['alert'] = 'freak';
                        }
                    }
                    else
                    {
                        $_GET['alert'] = 'dupes';
                        $d = count($capArr);
                        $e = count($capArr2);
                    }
                }
                else
                {
                    $_GET['alert'] = 'offanddef';
                }
            }
        }
    }
    // EFL Player Creation
    else if(($count < 2) && ($count3 < 3) && ($hostlg == 'EFL'))
    {
        // Archetype caps
        if($build == 'Dual-Threat')
        {
            $c90 = 'acc';
            $c901 = 'spd';
            $c801 = 'int';
            $c802 = 'han';
        }
        else
        {
            $c802 = '';
            $c901 = '';
            if($build == 'Pocket Passer')
            {
                $c90 = 'acc';
                $c801 = 'spd';
            }
            else if($build == 'Field General')
            {
                $c90 = 'arm';
                $c801 = 'spd';
            }
            else if($build == 'Gunslinger')
            {
                $c90 = 'int';
                $c801 = 'spd';
            }
            else if($build == 'One-Cut' || $build == 'Deep Threat' || $build == 'Vertical Threat')
            {
                $c90 = 'han';
                $c801 = 'str';
            }
            else if($build == 'Workhorse' || $build == 'Running' || $build == 'Balanced' || $build == 'Run Support')
            {
                $c90 = 'spd';
                $c801 = 'han';
            }
            else if($build == 'Scat Back' || $build == 'Coverage')
            {
                $c90 = 'agi';
                $c801 = 'str';
            }
            else if($build == 'Blocking')
            {
                $c90 = 'han';
                $c801 = 'spd';
            }
            else if($build == 'HBack' || $build == 'Possession')
            {
                $c90 = 'spd';
                $c801 = 'str';
            }
            else if($build == 'Red Zone')
            {
                $c90 = 'spd';
                $c801 = 'agi';
            }
            else if($build == '3/4 Tackling' || $build == 'Pass Rushing')
            {
                $c90 = 'spd';
                $c801 = 'int';
            }
            else if($build == 'Zone Cover')
            {
                $c90 = 'han';
                $c801 = 'int';
            }
            else if($build == 'Man Cover')
            {
                $c90 = 'han';
                $c801 = 'tkl';
            }
            else if($build == 'Kicker')
            {
                $c90 = '';
                $c801 = '';
            }
        }
        //
        $checkquery = "SELECT * FROM players WHERE plast = \"$plast\" AND pfirst = \"$pfirst\"";
        $checkrows = mysqli_num_rows(mysqli_query($con,$checkquery));
        if($checkrows > 0)
        {
            $_GET['alert'] = 'nameused';
        }
        else if((strlen($pfirst) == 0) || (strlen($plast) == 0))
        {
            $_GET['alert'] = 'namelength';
        }
        else
        {
            $stmt = $conn->prepare("SELECT export_id FROM players WHERE league='ECFA' ORDER BY export_id DESC LIMIT 1");
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            $export = $row['export_id'];
            $export++;

            $stmt = $conn->prepare("INSERT INTO efl_online.players (user_fk,export_id,plast,pfirst,position,build,num,height,weight,country,league,c90,c901,c801,c802) VALUES (:user,:export,:last,:first,:pos,:build,:num,:height,:weight,:country,:league,:c90,:c901,:c801,:c802)");
            $stmt->execute([':user' => $id,':export' => $export,':last' => $plast,':first' => $pfirst,':pos' => $position,':build' => $build,':num' => $num,':height' => stripslashes($height),':weight' => $weight,':country' => $country,':league' => 'ECFA',':c90' => $c90,':c901' => $c901,':c801' => $c801,':c802' => $c802]);
            $pid = $conn->lastInsertId(); // gets the id of the player added
            //Send SBAO notifications to ADs
            cap_notification($con,$pid);
            header("location:/forms/player_create.php?alert=success");
        } 
    }
    else
    {
        $_GET['alert'] = 'playerexists';
    }
}
else if(isset($_POST['plast']) AND isset($_POST['pfirst']) AND ($_POST['plast'] == '' OR $_POST['pfirst'] == '')) 
{
    $_GET['alert'] = 'lackingname';
}
if(isset($_GET['alert'])) 
{
    if ($_GET['alert'] == 'playerexists') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You already have an NCAA player.</strong></div>";
    }
    else if ($_GET['alert'] == 'threepoint') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You cannot have Three Point as a 90 Cap or a 70 Cap.</strong></div>";
    }
    else if ($_GET['alert'] == 'defensecaps') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You cannot have both Perimeter Defense and Post Defense capped.</strong></div>";
    }
    else if ($_GET['alert'] == 'offanddef') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You must have at least one Offensive and Defensive attribute capped each.</strong></div>";
    }
    else if ($_GET['alert'] == 'freak') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Freaks must have at least one physical attribute capped.</strong></div>";
    }
    else if ($_GET['alert'] == 'dupes') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You cannot use an attribute for more than one weakness or strength. $e out of $d are unique.</strong></div>";
    }
    else if ($_GET['alert'] == 'nameused') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>There is already a player with this name.</strong></div>";
    }
    else if ($_GET['alert'] == 'namelength') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>One or more names assigned to this player are empty, or do not meet a minimum length. Please give this player a first and last name, and re-submit.</strong></div>";
    }
    else if ($_GET['alert'] == 'lackingname') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You do not have both a first and last name entered for this player. Please provide a first and last name and try again.</strong></div>";
    }
    else if ($_GET['alert'] == 'success') 
    {
        $alert = "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Your player has been created successfully!</strong></div>";
    }
}
else
{$alert = '';}

?>
<!-- Body Start -->
<script>
function togglePanel (){
   var w = $(window).width();
   if (w <= 1199) {
      $('#instructions').removeClass('in');
   } else {
      $('#instructions').addClass('in');
   }
}

$(window).ready(function(){
     togglePanel();
});

togglePanel();
</script>
<body>
<div id="wrapper" style="position:relative;top:0;left:0;">
    <?php include $path.'/includes/nav.php'; ?>
    <div class="container">
        <?=HeaderImagePrint($hostlg,$_SESSION['style'])?>
        <?=$alert?>
        <div class="row">
            <div class="panel panel-primary">
                <div class="panel-heading">Create A Player</div>
                <div class="panel-body">
                    <div class="panel-group col-xs-12">
                        <div class="panel-group col-xs-12 col-sm-6 pull-right">
                            <div class="panel panel-primary">
                                <div class="panel-heading"><h3 class="panel-title" align='center'><a data-toggle="collapse" href="#instructions">Instructions (Click to Reveal)</a></h3></div>
                                <div id="instructions" class="panel-collapse collapse in">
                                    <ul class="list-group">
                                        <li class="list-group-item">3PT Shot cannot be capped at 70 or 90 (can be cappd at 80).</li>
                                        <li class="list-group-item">You may only choose Perimeter Defense or Post Defense as a capped attribute. Not both.</li>
                                        <li class="list-group-item">You must have at least one Offensive Attribute and one Defensive Attribute capped. Additionally, the Freak build requires a Physical Attribute to be capped. You may find a list of Offensive, Defensive, and Physical Attributes <a href='https://sba.today/forums/index.php?/topic/21128-new-player-handbook/&do=findComment&comment=379724' target='_blank'>here</a>.</li>
                                        <li class="list-group-item">Standard Flex Archetypes must select a primary position and a secondary position. Secondary position must meet height requirements and can only be one position up or down from your primary. Ex. you cannot have a SF secondary position as a PG.</li>
                                        <li class="list-group-item">Player may make changes to their archetype once before entering the SBA Draft.</li>
                                        <li class="list-group-item">If you are a Freak, list your secondary position as the same position as your primary.</li>
                                        <li class="list-group-item">When you change you height or position, your height will default back to 5'0. Please remember to change your height if you change one of these values.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel-group col-xs-12 col-sm-6">
                            <form class="form-horizontal" method='POST'>
                                <div class="form-group">
                                    <div class="input-group col-xs-12">
                                        <span class='input-group-addon' style='width:150px;text-align:left;'>League</span>
                                        <input type="text" class="form-control" name="league" value="<?=$hostlg?>" disabled>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group col-xs-12">
                                        <span class='input-group-addon' style='width:150px;text-align:left;'>First Name</span>
                                        <input type="text" class="form-control" name="pfirst" value="<?=$pfirst?>" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group col-xs-12">
                                        <span class='input-group-addon' style='width:150px;text-align:left;'>Last Name</span>
                                        <input type="text" class="form-control" name="plast" value="<?=$plast?>" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group col-xs-12">
                                        <span class='input-group-addon' style='width:150px;text-align:left;'>Primary Position</span>
                                        <select class="form-control" name="position" id="position" onchange="getArchetypeDropdown();getHeightDropdown();" required>
                                            <?=$listpos?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group" id="divpos2">
                                    <div class="input-group col-xs-12">
                                        <span class='input-group-addon' style='width:150px;text-align:left;'>Secondary Position</span>
                                        <select class="form-control" name="position2" id="position2" onchange="getHeightDropdown()" required>
                                            <?=$listpos2?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group col-xs-12">
                                        <span class='input-group-addon' style='width:150px;text-align:left;'>Build</span>
                                        <select class="form-control" name="build" id="build" onchange="getHeightDropdown()" required>
                                            <?=$listbuild?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group col-xs-12">
                                        <span class='input-group-addon' style='width:150px;text-align:left;'>Number</span>
                                        <input type="text" class="form-control" name="num" value="<?=$num?>" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group col-xs-12" id="divheight">
                                        <span class='input-group-addon' style='width:150px;text-align:left;'>Height</span>
                                        <select class='form-control' name='height' id='height'>
                                            <?=$listheight?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group col-xs-12">
                                        <span class='input-group-addon' style='width:150px;text-align:left;'>Weight (Pounds)</span>
                                        <input type="text" class="form-control" name="weight" value="<?=$weight?>" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group col-xs-12">
                                        <span class='input-group-addon' style='width:150px;text-align:left;'>Country</span>
                                        <select class='form-control' name='country' required>
                                            <?=$fibalist?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group" name="attcaps">
                                    <div class="input-group col-xs-12">
                                        <span class='input-group-addon' style='width:150px;text-align:left;'>90 Cap</span>
                                        <select class='form-control' name='c90' required>
                                            <?=$list?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group" name="attcaps">
                                    <div class="input-group col-xs-12">
                                        <span class='input-group-addon' style='width:150px;text-align:left;'>70 Cap</span>
                                        <select class='form-control' name='c70' required>
                                            <?=$list?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group" name="attcaps">
                                    <div class="input-group col-xs-12">
                                        <span class='input-group-addon' style='width:150px;text-align:left;'>80 Cap</span>
                                        <select class='form-control' name='c801' required>
                                            <?=$list?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group" name="attcaps">
                                    <div class="input-group col-xs-12">
                                        <span class='input-group-addon' style='width:150px;text-align:left;'>80 Cap</span>
                                        <select class='form-control' name='c802' required>
                                            <?=$list?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group" name="attcaps">
                                    <div class="input-group col-xs-12">
                                        <span class='input-group-addon' style='width:150px;text-align:left;'>80 Cap</span>
                                        <select class='form-control' name='c803' required>
                                            <?=$list?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group" name="attcaps">
                                    <div class="input-group col-xs-12">
                                        <span class='input-group-addon' style='width:150px;text-align:left;'>80 Cap</span>
                                        <select class='form-control' name='c804' required>
                                            <?=$list?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <a href="" class="btn btn-primary col-xs-12" data-toggle="modal" data-target="#warning" onclick="submit()">Submit</a>
                                </div>
                                <div class='modal fade' id='warning' role='dialog'>
                                    <div class='modal-dialog modal-lg'>
                                        <div class='modal-content'>
                                            <div class='modal-header'>
                                                <button type='button' class='close' data-dismiss='modal'>&times;</button>
                                                <h4 class='modal-title'>Create Player</h4>
                                            </div>
                                            <div class='modal-body'>
                                                <p>This will create this player as is. Please check the following information to ensure its accuracy:</p>
                                                <label class="control-label col-xs-3" for="warningheight">Height(inches):</label>
                                                <p class="form-control-static" name="warningheight" id='warningheight'></p>
                                                <p>If this information is not correct, please press Close and correct the information before proceeding.</p>
                                            </div>
                                            <div class='modal-footer'>
                                                <button type='submit' class='btn btn-danger'>Create Player</button>
                                                <button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>                 
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Body End -->
<?php
include $path.'/footer.php';
?>
<script>
var hostlg = '<?=$hostlg?>';
$(document).ready(function(){
    if(hostlg == 'EFL')
    {
        $("div[name='attcaps']").hide();
        $("#divpos2").hide();
    }
    $("#height").change(function(){
        $("#divheight").removeClass("has-error");
    });
});
function getArchetypeDropdown()
{
    pos = $('#position').val();
    build = encodeURIComponent($('#build').val());
    $.ajax({url: "../ajax/get_archetype_dropdown.php?pos="+pos+"&build="+build,async:false, success: function(result){
      $("#build").html(result);
    }});
}
function getHeightDropdown()
{
    pos = $('#position').val();
    pos2 = $('#position2').val();
    build = encodeURIComponent($('#build').val());
    height = $("#height").val();
    $.ajax({url: "../ajax/get_height_dropdown.php?pos="+pos+"&pos2="+pos2+"&build="+build+"&height="+height,async:false, success: function(result){
      $("#height").html(result);
      //alert(result);
    }});
    height2 = $("#height").val();
    if(height2 != height)
    {
        $("#divheight").addClass("has-error");
    }
}   
function submit()
{
    document.getElementById('warningheight').innerHTML = document.getElementById('height').value;
}
</script>