<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
$user=$_SESSION['user'];

if(isset($_POST['year']))
{$year = $_POST['year'];} else {$year = '1';}
if(isset($_POST['league']))
{$league = $_POST['league'];} else {$league = 'sba';}
if(isset($_POST['type']))
{$type = $_POST['type'];} else {$type = 'season';}
if(isset($_POST['limit']))
{$limit = sanitize($con,$_POST['limit']);if(is_numeric($limit)){}else{$limit = '25';}} else {$limit = '25';}

$yquery = "SELECT season FROM SBA_Years";
$yresult = mysqli_query($con,$yquery);
$yecho = '';
$echo = '';
while ($r = mysqli_fetch_array($yresult)) {
    if($year == $r['season']) {$echo = "selected";} else {$echo = "";}
    $yecho .= "<option value = '".$r['season']."' $echo>".$r['season']."</option>";
}
?>

<!-- Body Start -->
<body>
<div id="wrapper" style="position:relative;top:0;left:0;">
    <?php include $path.'/includes/nav.php'; ?>
    <div class="container">
        <?=HeaderImagePrint($hostlg,$_SESSION['style'])?>
        <form class="form-horizontal" method='POST'>
            <div class="panel-group">
                <div class="panel panel-primary">
                    <div class="panel-heading">League Leader Filters</div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="stat">Set Season:</label>
                            <div class="col-xs-2">
                                <select name="year" class="form-control"><?=$yecho?></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="league">Set League:</label>
                            <div class="col-xs-2">
                                <select name="league" class="form-control">
                                    <option value='sba' <?php if ($league == 'sba') echo "selected";?>>SBA</option>
                                    <option value='ncaa' <?php if ($league == 'ncaa') echo "selected";?>>NCAA</option>
                                    <option value='fiba' <?php if ($league == 'fiba') echo "selected";?>>FIBA</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="type">Set Type:</label>
                            <div class="col-xs-2">
                                <select name="type" class="form-control">
                                    <option value='season' <?php if ($type == 'season') echo "selected";?>>Regular Season</option>
                                    <option value='playoff' <?php if ($type == 'playoff') echo "selected";?>>Playoff</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="limit">Set Limit:</label>
                            <div class="col-xs-2">
                                <input type="text" name="limit" class="form-control" value="<?=$limit;?>"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-default">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <div class="row">
            <div class="col-xs-4">
                <h4>Minutes</h4>
                <?=printleagueleader($con,'Minutes',$league,$type,$year,$limit);?>
            </div>
            <div class="col-xs-4">
                <h4>Points</h4>
                <?=printleagueleader($con,'Points',$league,$type,$year,$limit);?>
            </div>
            <div class="col-xs-4">
                <h4>Rebounds</h4>
                <?=printleagueleader($con,'Rebounds',$league,$type,$year,$limit);?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-4">
                <h4>Assists</h4>
                <?=printleagueleader($con,'Assists',$league,$type,$year,$limit);?>
            </div>
            <div class="col-xs-4">
                <h4>Steals</h4>
                <?=printleagueleader($con,'Steals',$league,$type,$year,$limit);?>
            </div>
            <div class="col-xs-4">
                <h4>Blocks</h4>
                <?=printleagueleader($con,'Blocks',$league,$type,$year,$limit);?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-4">
                <h4>Turnovers</h4>
                <?=printleagueleader($con,'Turnovers',$league,$type,$year,$limit);?>
            </div>
            <div class="col-xs-4">
                <h4>FG%</h4>
                <?=printleagueleader($con,'FG',$league,$type,$year,$limit);?>
            </div>
            <div class="col-xs-4">
                <h4>FT%</h4>
                <?=printleagueleader($con,'FT',$league,$type,$year,$limit);?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-4">
                <h4>3P%</h4>
                <?=printleagueleader($con,'3P',$league,$type,$year,$limit);?>
            </div>
            <div class="col-xs-4">
                <h4>Double-Doubles</h4>
                <?=printleagueleader($con,'DoubleDoubles',$league,$type,$year,$limit);?>
            </div>
            <div class="col-xs-4">
                <h4>Triple-Doubles</h4>
                <?=printleagueleader($con,'TripleDoubles',$league,$type,$year,$limit);?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-4">
                <h4>TS%</h4>
                <?=printleagueleader($con,'TSP',$league,$type,$year,$limit);?>
            </div>
            <div class="col-xs-4">
                <h4>PPS</h4>
                <?=printleagueleader($con,'PPSFTM',$league,$type,$year,$limit);?>

            </div>
            <div class="col-xs-4">
                <h4>PPS(Real)</h4>
                <?=printleagueleader($con,'PPS',$league,$type,$year,$limit);?>

            </div>
        </div>
    </div>
</div>
<!-- Body End -->

<?php
include $path.'/footer.php';
?>
