<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
// get user
$user=$_SESSION['user'];
//get year
$system = new System();
$year = $system->get_year();
$preyear = $year - 1;

if(isset($_POST['update']))
{
    $champ = sanitize($con,$_POST['champion']);
    $runner = sanitize($con,$_POST['runnerup']);
    $exec = sanitize($con,$_POST['exec']);
    $mvp = sanitize($con,$_POST['mvp']);
    $op = sanitize($con,$_POST['opoty']);
    $dp = sanitize($con,$_POST['dpoty']);
    $sm = sanitize($con,$_POST['smoty']);
    $pg = sanitize($con,$_POST['pgoty']);
    $sg = sanitize($con,$_POST['sgoty']);
    $sf = sanitize($con,$_POST['sfoty']);
    $pf = sanitize($con,$_POST['pfoty']);
    $ce = sanitize($con,$_POST['coty']);
    $ro = sanitize($con,$_POST['roty']);
    $mip = sanitize($con,$_POST['mip']);
    $sql = "UPDATE sbdl_predictions SET numcorrect = numcorrect+1 WHERE year='$preyear' and champion = '$champ'";
    mysqli_query($con,$sql);
    $sql = "UPDATE sbdl_predictions SET numcorrect = numcorrect+1 WHERE year='$preyear' and runnerup = '$runner'";
    mysqli_query($con,$sql);
    $sql = "UPDATE sbdl_predictions SET numcorrect = numcorrect+1 WHERE year='$preyear' and eoty = '$exec'";
    mysqli_query($con,$sql);
    $sql = "UPDATE sbdl_predictions SET numcorrect = numcorrect+1 WHERE year='$preyear' and mvp = '$mvp'";
    mysqli_query($con,$sql);
    $sql = "UPDATE sbdl_predictions SET numcorrect = numcorrect+1 WHERE year='$preyear' and opoty = '$op'";
    mysqli_query($con,$sql);
    $sql = "UPDATE sbdl_predictions SET numcorrect = numcorrect+1 WHERE year='$preyear' and dpoty = '$dp'";
    mysqli_query($con,$sql);
    $sql = "UPDATE sbdl_predictions SET numcorrect = numcorrect+1 WHERE year='$preyear' and smoty = '$sm'";
    mysqli_query($con,$sql);
    $sql = "UPDATE sbdl_predictions SET numcorrect = numcorrect+1 WHERE year='$preyear' and pgoty = '$pg'";
    mysqli_query($con,$sql);
    $sql = "UPDATE sbdl_predictions SET numcorrect = numcorrect+1 WHERE year='$preyear' and sgoty = '$sg'";
    mysqli_query($con,$sql);
    $sql = "UPDATE sbdl_predictions SET numcorrect = numcorrect+1 WHERE year='$preyear' and sfoty = '$sf'";
    mysqli_query($con,$sql);
    $sql = "UPDATE sbdl_predictions SET numcorrect = numcorrect+1 WHERE year='$preyear' and pfoty = '$pf'";
    mysqli_query($con,$sql);
    $sql = "UPDATE sbdl_predictions SET numcorrect = numcorrect+1 WHERE year='$preyear' and coty = '$ce'";
    mysqli_query($con,$sql);
    $sql = "UPDATE sbdl_predictions SET numcorrect = numcorrect+1 WHERE year='$preyear' and roty = '$ro'";
    mysqli_query($con,$sql);
    $sql = "UPDATE sbdl_predictions SET numcorrect = numcorrect+1 WHERE year='$preyear' and mip = '$mip'";
    mysqli_query($con,$sql);
    process_predictions($con,'sbdl_season',$preyear);
}

$query = "SELECT name FROM teams where league = 'SBDL' ORDER BY name ASC";
$result = mysqli_query($con,$query);
$team = '';
$echo = '';
while ($r = mysqli_fetch_array($result)) 
{
    $team .= "<option value = '".$r['name']."' $echo>".$r['name']."</option>";
}
$query = "SELECT name FROM teams where league = 'SBDL' ORDER BY name ASC";
$result = mysqli_query($con,$query);
$team2 = '';
$echo = '';
while ($r = mysqli_fetch_array($result)) 
{
    $team2 .= "<option value = '".$r['name']."' $echo>".$r['name']."</option>";
}
$query = "SELECT name FROM coaches where league = 'SBDL' ORDER BY name ASC";
$result = mysqli_query($con,$query);
$coach = '';
$echo = '';
while ($r = mysqli_fetch_array($result)) 
{
$coach .= "<option value = '".$r['name']."' $echo>".$r['name']."</option>";
}
$query = "SELECT CONCAT(plast,', ',pfirst) as name FROM players where league = 'SBDL' || (league='SBA' && experience='1') ORDER BY name ASC";
$result = mysqli_query($con,$query);
$mvp = '';
$opoty = '';
$dpoty = '';
$smoty = '';
$pgoty = '';
$sgoty = '';
$sfoty = '';
$pfoty = '';
$coty = '';
$roty = '';
$mip = '';
$echo = '';
while ($r = mysqli_fetch_array($result)) 
{
    $mvp .= "<option value = \"".$r['name']."\" $echo>".$r['name']."</option>";
    $opoty .= "<option value = \"".$r['name']."\" $echo>".$r['name']."</option>";
    $dpoty .= "<option value = \"".$r['name']."\" $echo>".$r['name']."</option>";
    $smoty .= "<option value = \"".$r['name']."\" $echo>".$r['name']."</option>";
    $pgoty .= "<option value = \"".$r['name']."\" $echo>".$r['name']."</option>";
    $sgoty .= "<option value = \"".$r['name']."\" $echo>".$r['name']."</option>";
    $sfoty .= "<option value = \"".$r['name']."\" $echo>".$r['name']."</option>";
    $pfoty .= "<option value = \"".$r['name']."\" $echo>".$r['name']."</option>";
    $coty .= "<option value = \"".$r['name']."\" $echo>".$r['name']."</option>";
    $roty .= "<option value = \"".$r['name']."\" $echo>".$r['name']."</option>";
    $mip .= "<option value = \"".$r['name']."\" $echo>".$r['name']."</option>";
}
if(isset($_GET['alert'])) 
{
    if($_GET['alert'] == 'success') 
    {
        $echo = "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Predictions Saved.</strong></div>";
    }
    else if($_GET['alert'] == 'closed')
    {
        $echo = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Predictions Closed, Submission Not Saved.</strong></div>";
    }
    else if($_GET['alert'] == 'norecord')
    {
        $echo = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You must save your own Predictions before you can import someone else's.</strong></div>";
    }
    else 
    {
        $echo = '';
    }
}
else
{$echo = '';}
?>

<!-- Body Start -->
<body>
<div id="wrapper" style="position:relative;top:0;left:0;">
    <?php include $path.'/includes/nav.php'; ?>
    <div class="container">
        <?=HeaderImagePrint($hostlg,$_SESSION['style'])?>
        <?=$echo?>
        <div class="panel panel-primary">
            <div class="panel-heading">SBDL Season Predictions Processing</div>
            <div class="panel-body">
                <div class="panel-group col-xs-12">
                    <form class="form-horizontal" method="post" >
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>Champion</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>Champion</span>
                                <select class='form-control' name='champion'>
                                    <?=$team?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>Runner-Up</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>Runner-Up</span>
                                <select class='form-control' name='runnerup'>
                                    <?=$team2?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>Coach of the Year</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>EotY</span>
                                <select class='form-control' name='exec'>
                                    <?=$coach?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>Most Valuable Player</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>MVP</span>
                                <select class='form-control' name='mvp'>
                                    <?=$mvp?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>Offensive Player of the Year</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>OPotY</span>
                                <select class='form-control' name='opoty'>
                                    <?=$opoty?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>Defensive Player of the Year</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>DPotY</span>
                                <select class='form-control' name='dpoty'>
                                    <?=$dpoty?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>Sixth Man of the Year</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>6MotY</span>
                                <select class='form-control' name='smoty'>
                                    <?=$smoty?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>Rookie of the Year</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>RotY</span>
                                <select class='form-control' name='roty'>
                                    <?=$roty?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>Most Improved Player</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>MIP</span>
                                <select class='form-control' name='mip'>
                                    <?=$mip?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>Point Guard of the Year</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>PGotY</span>
                                <select class='form-control' name='pgoty'>
                                    <?=$pgoty?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>Shooting Guard of the Year</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>SGotY</span>
                                <select class='form-control' name='sgoty'>
                                    <?=$sgoty?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>Small Forward of the Year</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>SMotY</span>
                                <select class='form-control' name='sfoty'>
                                    <?=$sfoty?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>Power Forward of the Year</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>pfoty</span>
                                <select class='form-control' name='pfoty'>
                                    <?=$pfoty?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>Center of the Year</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>CotY</span>
                                <select class='form-control' name='coty'>
                                    <?=$coty?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" name="update" class="btn btn-primary col-xs-12">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Body End -->

<?php
include $path.'/footer.php';
?>

