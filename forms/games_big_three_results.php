<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
$user=$_SESSION['user'];

// year
$system = new System();
$year = $system->get_year();

$sql = "SELECT a.username,CONCAT(p.pfirst,' ',p.plast) as pname,CONCAT(q.pfirst,' ',q.plast) as qname,CONCAT(r.pfirst,' ',r.plast) as rname FROM auth_user a INNER JOIN games_big_3 g ON a.ID=g.a_fk INNER JOIN players p ON g.player1=p.id INNER JOIN players q ON g.player2=q.id INNER JOIN players r ON g.player3=r.id WHERE g.year='$year' ORDER BY a.username";
$result = mysqli_query($con,$sql);
$concat = '';
$i = 0;
while($r = mysqli_fetch_array($result))
{
    $concat .= "
    <div class=\"panel-group col-xs-12 col-sm-6\">
        <div class=\"panel panel-primary\">
            <div class='panel-heading'><h4 class='panel-title'><a data-toggle='collapse' href='#collapse$i'>".$r['username']."</a></h4></div>
            <div class='panel-collapse collapse' id='collapse$i'>
                <div class=\"panel-body\">
                    <div class=\"input-group col-xs-12\">
                        <span class='input-group-addon'>Player 1</span>
                        <input type=\"text\" class=\"form-control\" readonly=\"readonly\"' value=\"".$r['pname']."\">
                    </div>
                    <div class=\"input-group col-xs-12\">
                        <span class='input-group-addon'>Player 2</span>
                        <input type=\"text\". class=\"form-control\" readonly=\"readonly\"' value=\"".$r['qname']."\">
                    </div>

                    <div class=\"input-group col-xs-12\">
                        <span class='input-group-addon'>Player 3</span>
                        <input type=\"text\" class=\"form-control\" readonly=\"readonly\"' value=\"".$r['rname']."\">
                    </div>
                </div>
            </div>
        </div>
    </div>";
    $i++;
}






if(isset($_GET['alert'])) 
{
    if($_GET['alert'] == 'success') 
    {
        $echo = "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Big 3 Submission Saved.</strong></div>";
    }
    else if($_GET['alert'] == 'closed')
    {
        $echo = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Big 3 Closed, Submission Not Saved.</strong></div>";
    }
    else if($_GET['alert'] == 'norecord')
    {
        $echo = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You must save your own Predictions before you can import someone else's.</strong></div>";
    }
    else 
    {
        $echo = '';
    }
}
else
{$echo = '';}
?>

<!-- Body Start -->
<body>
<div id="wrapper" style="position:relative;top:0;left:0;">
    <?php include $path.'/includes/nav.php'; ?>
    <div class="container">
        <?=HeaderImagePrint($hostlg,$_SESSION['style'])?>
        <?=$echo?>
        <div class="panel panel-primary">
            <div class="panel-heading">SBAO Big Three Challenge - Teams</div>
            <div class="panel-body">
                <?=$concat?>
            </div>
        </div>
    </div>
</div>
<!-- Body End -->

<?php
include $path.'/footer.php';
?>