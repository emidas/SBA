<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
$user=$_SESSION['user'];
$system = new System();

if(isset($_POST['award']))
{
    $award = $_POST['award'];
}
else
{
    $award = "mvp";
} 
$awardArr = Array('Most Valuable Player','Playoff MVP','Offensive Player of the Year','Defensive Player of the Year','Sixth Man of the Year','Point Guard of the Year','Shooting Guard of the Year','Small Forward of the Year','Power Forward of the Year','Center of the Year','Rookie of the Year','Most Improved Player');
$awardArr_short = Array('mvp','pmvp','opoty','dpoty','smoty','pgoty','sgoty','sfoty','pfoty','coty','roty','mip');
$aecho = "";
for($f=0;$f<count($awardArr);$f++)
{
    if ($awardArr_short[$f] == $award){$echo = "selected";} else{$echo = '';}
    $aecho .= "<option value = '".$awardArr_short[$f]."' $echo>".$awardArr[$f]."</option>";
}

$stmt = $conn->prepare("SELECT a.year, CONCAT(p.pfirst,' ',p.plast) as pname,t.name as tname FROM award_winners a INNER JOIN players p ON a.pid=p.id INNER JOIN teams t ON a.tid=t.id WHERE award=:award ORDER BY a.year DESC");
$stmt->execute([':award' => $award]);
$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
$atable = "";
foreach ($result as $row)
{
    $stmt = $conn->prepare("SELECT y.year,TRUNCATE(`Points`/`Games`,2) as Points,TRUNCATE(`Assists`/`Games`,2) as Assists,TRUNCATE(`Rebounds`/`Games`,2) as Rebounds,TRUNCATE(`Steals`/`Games`,2) as Steals,TRUNCATE(`Blocks`/`Games`,2) as Blocks FROM SBA_Player p INNER JOIN SBA_SeasonStats s ON p.ID=s.ID INNER JOIN year y on s.Season = y.sba_season WHERE p.Name = :name AND year=:year UNION SELECT yr.year,TRUNCATE(`Points`/`Games`,2) as Points,TRUNCATE(`Assists`/`Games`,2) as Assists,TRUNCATE(`Rebounds`/`Games`,2) as Rebounds,TRUNCATE(`Steals`/`Games`,2) as Steals,TRUNCATE(`Blocks`/`Games`,2) as Blocks FROM SBAR_Player pr INNER JOIN SBAR_SeasonStats sr on pr.ID = sr.ID INNER JOIN year yr on sr.Season = yr.sba_season WHERE pr.Name = :name AND year=:year ORDER BY year DESC");
    $stmt->execute([':name' => $row['pname'],':year' => $row['year']]);
    $r = $stmt->fetch(PDO::FETCH_ASSOC);
    $atable .= "
    <tr>
        <td>".$row['year']."</td>
        <td>".$row['pname']."</td>
        <td>".$row['tname']."</td>
        <td>".$r['Points']."</td>
        <td>".$r['Assists']."</td>
        <td>".$r['Rebounds']."</td>
        <td>".$r['Steals']."</td>
        <td>".$r['Blocks']."</td>
    </tr>";
}

include $path.'/includes/head.php';  
?>
<body>
<div id="wrapper" style="position:relative;top:0;left:0;">
    <?php include $path.'/includes/nav.php'; ?>
    <div class="container">
        <?=HeaderImagePrint($hostlg,$_SESSION['style'])?>
        <?=$echo?>
        <!-- Season selector -->
        <div class="panel-group">
            <div class="panel panel-primary">
                <div class="panel-heading">Awards Filters</div>
                <div class="panel-body">
                    <!--<div class="form-group">
                        <label class="control-label col-sm-2" for="league">Set League:</label>
                        <div class="col-xs-2">
                            <select name="league" class="form-control">
                                <option value='sba' <?php if ($league == 'sba') echo "selected";?>>SBA</option>
                                <option value='ncaa' <?php if ($league == 'ncaa') echo "selected";?>>NCAA</option>
                                <option value='fiba' <?php if ($league == 'fiba') echo "selected";?>>FIBA</option>
                            </select>
                        </div>
                    </div>-->
                    <div class="panel-group col-xs-12">
                        <form class="form-horizontal" method="POST">
                            <div class="form-group">
                                <div class="input-group col-xs-12">
                                    <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>Award</span>
                                    <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>Award</span>
                                    <select name="award" class="form-control">
                                        <?=$aecho?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary col-xs-12">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-group">
            <div class="panel panel-primary">
                <div class="panel-heading">Awards</div>
                <div class="panel-body">
                    <div class='table-responsive'>
                        <table class="table compact" id='viewsba2' style="width:100%;">
                            <thead>
                                <th>Year</th>
                                <th>Player</th>
                                <th>Team</th>
                                <th>Pts</th>
                                <th>Ast</th>
                                <th>Reb</th>
                                <th>Stl</th>
                                <th>Blk</th>
                            </thead>
                            <tbody>
                                <?=$atable?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Body End -->

<?php
include $path.'/footer.php';
?>