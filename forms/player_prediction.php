<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
$user = $_SESSION['user'];
$mid = $_SESSION['memberid'];
if(isset($_GET['pid']) && isset($_GET['season']) && isset($_GET['pe']) && isset($_GET['type']))
{
    $pid = sanitize($con,$_GET['pid']);
    $season = sanitize($con,$_GET['season']);
    $type = sanitize($con,$_GET['type']);
    if($type == 0)
    {
        $form = 'season';
    }
    else if($type == 1)
    {
        $form = 'playoff';
    }
    else if($type == 2)
    {
        $form = 'sbdl_season';
    }
    else
    {
        $form = 'sbdl_playoff';
    }
    $sql = "SELECT id FROM player_prediction WHERE p_fk='$pid' AND season='$season' AND type='$form' AND claimed='1'";
    $numsult = mysqli_query($con,$sql);
    $num = mysqli_num_rows($numsult);
    if($num == 0)
    {
        $sql = "SELECT pe FROM player_prediction WHERE p_fk='$pid' AND season='$season' AND type='$form' AND claimed='0'";
        $yrq=mysqli_fetch_array(mysqli_query($con,$sql));
        $pe=$yrq['pe'];
        $sql = "UPDATE player_prediction SET claimed='1',timestamp=NOW() WHERE p_fk='$pid' AND season='$season' AND type='$form'";
        mysqli_query($con,$sql);
        $sql = "UPDATE players SET tpe=tpe+'$pe',ape=ape+'$pe',bank=bank+'$pe' WHERE id='$pid'";
        mysqli_query($con,$sql);
        header("location:player_dashboard.php?alert=predictionsuccess");
        exit();
    }
    else
    {
        echo "You already have claimed this Predictions this season. Stop cheating.";
    }
}
else
{
    header("location:javascript://history.go(-1)");
    exit();
}
?>
