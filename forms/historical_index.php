<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
$user=$_SESSION['user'];

$yquery = "SELECT year,isactive FROM year WHERE sba_season != 0 AND year > 13 ORDER BY year DESC";
$yresult = mysqli_query($con,$yquery);
$sbaecho = "<table id='sba' class='table table-striped compact' style='width:100%;'><thead><tr><th>Season</th><th>Index</th></tr></thead><tbody>";
while ($r = mysqli_fetch_array($yresult)) 
{
    if($r['isactive'] == '1')
    {

    }
    else
    {
        $sbaecho .= "<tr><td>".$r['year']."</td><td><a href='https://sbahistory.com/sba/s".$r['year']."/' target='_blank'>Season ".$r['year']." Index</a></td></tr>";
    }
}
$sbaecho .= "</tbody></table>";
$yquery = "SELECT year,isactive FROM year WHERE sbdl_season != 0 AND year > 41 ORDER BY year DESC";
$yresult = mysqli_query($con,$yquery);
$sbdlecho = "<table id='sbdl' class='table table-striped compact' style='width:100%;'><thead><tr><th>Season</th><th>Index</th></tr></thead><tbody>";
while ($r = mysqli_fetch_array($yresult)) 
{
    if($r['isactive'] == '1')
    {

    }
    else
    {
        $sbdlecho .= "<tr><td>".$r['year']."</td><td><a href='https://sbahistory.com/sbdl/s".$r['year']."/' target='_blank'>Season ".$r['year']." Index</a></td></tr>";
    }
}
$sbdlecho .= "</tbody></table>";
$yquery = "SELECT year,isactive FROM year WHERE ncaa_season != 0 AND year > 22 ORDER BY year DESC";
$yresult = mysqli_query($con,$yquery);
$ncaaecho = "<table id='ncaa' class='table table-striped compact' style='width:100%;'><thead><tr><th>Season</th><th>Index</th></tr></thead><tbody>";
while ($r = mysqli_fetch_array($yresult)) 
{
    if($r['isactive'] == '1')
    {

    }
    else
    {
        $ncaaecho .= "<tr><td>".$r['year']."</td><td><a href='https://sbahistory.com/ncaa/s".$r['year']."/' target='_blank'>Season ".$r['year']." Index</a></td></tr>";
    }
}
$ncaaecho .= "</tbody></table>";
$yquery = "SELECT year,isactive FROM year WHERE fiba_season != 0 ORDER BY year DESC";
$yresult = mysqli_query($con,$yquery);
$fibaecho = "<table id='fiba' class='table table-striped compact' style='width:100%;'><thead><tr><th>Season</th><th>Index</th></tr></thead><tbody>";
while ($r = mysqli_fetch_array($yresult)) 
{
    if($r['isactive'] == '1')
    {

    }
    else
    {
        $fibaecho .= "<tr><td>".$r['year']."</td><td><a href='https://sbahistory.com/fiba/s".$r['year']."/' target='_blank'>Season ".$r['year']." Index</a></td></tr>";
    }
}
$fibaecho .= "</tbody></table>";
?>

<!-- Body Start -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/colreorder/1.5.1/js/dataTables.colReorder.min.js"></script>
<body>
<div id="wrapper" style="position:relative;top:0;left:0;">
    <?php include $path.'/includes/nav.php'; ?>
    <div class="container-fluid">
        <?=HeaderImagePrint($hostlg,$_SESSION['style'])?>
        <div class="row">
            <div class="panel-group col-xs-12">
                <div class="panel-group col-xs-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading"><h3 class="panel-title">SBA Indexes</h3></div>
                        <div class="panel-body">
                            <?=$sbaecho?>
                        </div>
                    </div>
                </div>
                <div class="panel-group col-xs-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading"><h3 class="panel-title">SBDL Indexes</h3></div>
                        <div class="panel-body">
                            <?=$sbdlecho?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-group col-xs-12">
                <div class="panel-group col-xs-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading"><h3 class="panel-title">NCAA Indexes</h3></div>
                        <div class="panel-body">
                            <?=$ncaaecho?>
                        </div>
                    </div>
                </div>
                <div class="panel-group col-xs-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading"><h3 class="panel-title">FIBA Indexes</h3></div>
                        <div class="panel-body">
                            <?=$fibaecho?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Body End -->

<?php
include $path.'/footer.php';
?>
<script>
$(document).ready(function() {
    $('#sba').DataTable( {
        "order": [[ 0, "desc" ]]
    } );
} );
$(document).ready(function() {
    $('#sbdl').DataTable( {
        "order": [[ 0, "desc" ]]
    } );
} );
$(document).ready(function() {
    $('#ncaa').DataTable( {
        "order": [[ 0, "desc" ]]
    } );
} );
$(document).ready(function() {
    $('#fiba').DataTable( {
        "order": [[ 0, "desc" ]]
    } );
} );
</script>