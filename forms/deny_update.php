<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
$id = '';
$denypm = '';
$pname = '';
if(isset($_POST['id']))
{
    $id = sanitize($con,$_POST['id']);
    $denypm = sanitize($con,$_POST['denypm']);
    $pname = sanitize($con,$_POST['pname']);
}
else
{
    header("location:update_dashboard.php");
}
$pm = str_replace('\r\n' , '<br>', $denypm);
$pm = str_replace('\r' , '<br>', $pm);
$pm = str_replace('\n' , '<br>', $pm);
$pm = stripslashes($pm);
$user=$_SESSION['user'];
$mid = $_SESSION['memberid'];
$sql = "UPDATE player_updates SET status = '0' WHERE id = '$id'";
mysqli_query($con,$sql);
$to = array();
$tosql = "
SELECT memberid 
FROM auth_user a
INNER JOIN players p ON a.ID = p.user_fk
INNER JOIN player_updates u ON p.id = u.pid_fk
WHERE u.id = '$id' ";
$toresult = mysqli_query($con,$tosql);
while($torow = mysqli_fetch_array($toresult))
{
    array_push($to,$torow['memberid']);
}
$curl_post_data = array(
    'from' => $mid,
    'to' => $to,
    'title' => "Update Denied",
    'body' => "<h1>SBA Online</h1><p>Your update for ".$pname." has been denied by ".$user." for the following reason(s):<br><br>".$pm."</p>"
);
createMessage($curl_post_data);
header("location:update_dashboard.php");
?>

