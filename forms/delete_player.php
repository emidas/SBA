<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
$pid = '';
if(isset($_GET['pid']))
{
    $pid = sanitize($con,$_GET['pid']);
    // Delete Player Updates - Start
    $sql = "SELECT id FROM player_updates WHERE pid_fk = '$pid'";
    $result = mysqli_query($con,$sql);
    while($r = mysqli_fetch_array($result))
    {
        $puid = $r['id'];
        $sql = "DELETE FROM player_updates_tasks WHERE puid_fk = '$puid'";
        mysqli_query($con,$sql);
        $sql = "DELETE FROM player_updates_update WHERE puid_fk = '$puid'";
        mysqli_query($con,$sql);
    }
    $sql = "DELETE FROM player_updates WHERE pid_fk = '$pid'";
    mysqli_query($con,$sql);
    // Delete Player Updates - End

    // Delete Earn/Purchase History - Start
    $sql = "DELETE FROM player_earn_history WHERE pid_fk = '$pid'";
    mysqli_query($con,$sql);
    $sql = "DELETE FROM player_purchase_history WHERE pid_fk = '$pid'";
    mysqli_query($con,$sql);
    // Delete Earn/Purchase History - End

    // Delete Player - Start
    $sql = "DELETE FROM players WHERE id = '$pid'";
    mysqli_query($con,$sql);
    // Delete Player - End

    header("location:/home.php");
    exit();
}
else
{
    header("location:javascript://history.go(-1)");
    exit();
}
?>
