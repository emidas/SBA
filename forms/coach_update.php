<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
if(isset($_GET['league']))
{
    $league = sanitize($con,$_GET['league']);
}
else
{
    header("location:/home.php?alert=noleague");
    exit();
}
$user=$_SESSION['user'];
$query = "SELECT * FROM coaches WHERE (name=\"$user\" || name2=\"$user\") AND league='$league'";
$cresult = mysqli_query($con,$query);
$sbacount=mysqli_num_rows($cresult);
if($sbacount == 0)
{
    header("location:/home.php?alert=nocoach");
    exit();
}
$cid = mysqli_fetch_array($cresult);
$cid = $cid['id'];
//get tid
$sql = "SELECT t.id
FROM teams t
INNER JOIN coaches c ON t.id=c.t_fk
WHERE (c.name=\"$user\" || c.name2=\"$user\") AND t.league='$league'";
$numsult = mysqli_query($con,$sql);
$num = mysqli_num_rows($numsult);
if($num == 0)
{
header("location:/home.php?alert=adminpermission");
exit();
}
else
{
    $tarr=mysqli_fetch_array(mysqli_query($con,$sql));
    $tid=$tarr['id'];
}
// Team Object, Info
$team = new Team($tid);
$tname = $team->get_('name');
$tleague = $team->get_('league');
$tsalary = $team->get_('team_salary');
$tsalary2 = $team->get_('team_salary2');
$tsalary3 = $team->get_('team_salary3');
$tcapspace = $team->get_('cap_space');
$troster = $team->get_('roster_size');
$trosterfiller = $team->get_('roster_size_filler');
$teaminfo = "
<div class='input-group col-xs-12'><input type='text' class='form-control text-c' readonly='readonly' value=\"".$tname."\"></div>
<div class='input-group col-xs-12'><span class='input-group-addon' style='width:95px;text-align:left;'>League</span><input type='text' class='form-control'vreadonly='readonly' value=\"".$tleague."\"></div>
<div class='input-group col-xs-12'><span class='input-group-addon' style='width:95px;text-align:left;'>Salary</span><input type='text' class='form-control'vreadonly='readonly' value=\"$".number_format($tsalary,0)."\"></div>
<div class='input-group col-xs-12'><span class='input-group-addon' style='width:95px;text-align:left;'>Cap Space</span><input type='text' class='form-control'vreadonly='readonly' value=\"$".number_format($tcapspace,0)."\"></div>
<div class='input-group col-xs-12'><span class='input-group-addon' style='width:95px;text-align:left;'>Roster</span><input type='text' class='form-control'vreadonly='readonly' value=\"".$troster."/12\"></div>
<div class='input-group col-xs-12'><span class='input-group-addon' style='width:95px;text-align:left;'>Filler</span><input type='text' class='form-control'vreadonly='readonly' value=\"".$trosterfiller."/6\"></div>";

// get year
$system = new System();
$year = $system->get_year();
$nextyr = $year + 1;

$sql = "SELECT * FROM coach_updates cu INNER JOIN coach_updates_update cuu ON cu.id=cuu.cuid_fk WHERE cu.cid_fk='$cid' AND (status = '1' || status = '0')";
$numsult = mysqli_query($con,$sql);
$num = mysqli_num_rows($numsult);
if($num > 0)
{
    $sql = "SELECT * FROM coach_updates cu INNER JOIN coach_updates_update cuu ON cu.id=cuu.cuid_fk WHERE cu.cid_fk='$cid' AND (status = '1' || status = '0')";
    $numsult = mysqli_query($con,$sql);
    $t = mysqli_fetch_array($numsult);
    $name = $t['strat_name'];
}
else
{
    $query = "SELECT * FROM coaches WHERE (name=\"$user\" || name2=\"$user\") AND league='$league'";
    $cresult = mysqli_query($con,$query);
    $t = mysqli_fetch_array($cresult);
    $name = $t['strat_name'];
}
$sql = "SELECT cu.id,strat_name FROM coach_updates cu WHERE cid_fk='$cid'";
$sresult = mysqli_query($con,$sql);
$secho = '';
$echo = '';
while ($r = mysqli_fetch_array($sresult)) {
    if($name == $r['strat_name']) {$echo = "selected";} else {$echo = "";}
    $secho .= "<option value = \"".$r['id']."\" $echo>".$r['id']." - ".$r['strat_name']."</option>";
}
$dc_pos = Array('c','pf','sf','sg','pg','6','7','8','9','10','11','12');
$dc_arr = Array('pnamec','pnamepf','pnamesf','pnamesg','pnamepg','pname6','pname7','pname8','pname9','pname10','pname11','pname12');
$dc_arr2 = Array('posc','pospf','possf','possg','pospg','pos6','pos7','pos8','pos9','pos10','pos11','pos12');
$dc_arr3 = Array('minc','minpf','minsf','minsg','minpg','min6','min7','min8','min9','min10','min11','min12');
$dc_key = Array('key1','key2','key3');
$dc_echo = '';
for($v=0;$v<count($dc_pos);$v++) 
{
    $sql = "SELECT CONCAT(pfirst,' ',plast) as pname FROM players WHERE team='$cid' ORDER BY pname ASC";
    $presult = mysqli_query($con,$sql);
    $dcecho = "<option></option>";
    while ($r = mysqli_fetch_array($presult)) 
    {
        if($t[$dc_arr[$v]] == $r['pname']) {$echo = "selected";} else {$echo = "";}
        $dcecho .= "<option value = \"".$r['pname']."\" $echo>".$r['pname']."</option>";
    }
    $dc_echo .= "
    <tr>
        <td>".strtoupper($dc_pos[$v])."</td>
        <td><select class='form-control' name=\"pname".$dc_pos[$v]."\">".$dcecho."</select></td>
        <td><input class='form-control' type='text' name='pos".$dc_pos[$v]."' value=\"".$t[$dc_arr2[$v]]."\"></td>
        <td><input class='form-control' type='text' name='min".$dc_pos[$v]."' value=\"".$t[$dc_arr3[$v]]."\"></td>
    </tr>";
}
$dc_optecho = '<tr><td>Options:</td>';
for($v=0;$v<count($dc_key);$v++) 
{
    $sql = "SELECT CONCAT(pfirst,' ',plast) as pname FROM players WHERE team='$cid' ORDER BY pname ASC";
    $presult = mysqli_query($con,$sql);
    $dcecho = '<option></option>';
    while ($r = mysqli_fetch_array($presult)) 
    {
        if($t[$dc_key[$v]] == $r['pname']) {$echo = "selected";} else {$echo = "";}
        $dcecho .= "<option value = \"".$r['pname']."\" $echo>".$r['pname']."</option>";
    }
    $dc_optecho .="<td><select class='form-control' name=\"".$dc_key[$v]."\">".$dcecho."</select></td>";
}
$dc_optecho .= "</tr>";

if($hostlg == 'EFL')
{
    $strat_echo = '';
    $strat_arr = Array('First and Ten','First and Short','First and Long','Second and Short','Second and Long','Third and Short','Third and Long','Fourth and Short','Fourth and Long','Goal Line','Last 2 Min(Ahead)','Last 2 Min(Behind)');
    $play_off = Array('Balanced','West Coast','Power','Vertical','Spread');
    $off_arr = '';
    for($v=0;$v<count($play_off);$v++)
    {
        $off_arr .= "<option value = \"".$play_off[$v]."\">".$play_off[$v]."</option>";
    }
    $play_def = Array('3-4','3-3-5','4-3','Nickel','Dime','Target Run','Target Mixed','Target Pass');
    $def_arr = '';
    for($v=0;$v<count($play_def);$v++)
    {
        $def_arr .= "<option value = \"".$play_def[$v]."\">".$play_def[$v]."</option>";
    }
    $z = 0;
    for($v=0;$v<count($strat_arr);$v++)
    {
        $strat_echo .= "
        <tr>
            <td>".$strat_arr[$v]."</td>
            <td><select class='form-control' name='row_off[".$v."][play]'>".$off_arr."</select></td>
            <td><input type=\"text\" id=\"slider".$z."\" class=\"form-control\" name='row_off[".$v."][per]' value=\"50\" data-slider-min=\"0\" data-slider-max=\"100\" data-slider-steps=\"1\" data-slider-value=\"50\"></td>
        </tr>";
        $z++;
    }
    for($v=0;$v<count($strat_arr);$v++)
    {
        $strat_echo .= "
        <tr>
            <td>".$strat_arr[$v]."</td>
            <td><select class='form-control' name='row_def[".$v."][play]'>".$def_arr."</select></td>
            <td><input type=\"text\" id=\"slider".$z."\" class=\"form-control\" name='row_def[".$v."][per]' value=\"50\" data-slider-min=\"0\" data-slider-max=\"100\" data-slider-steps=\"1\" data-slider-value=\"50\"></td>
        </tr>";
        $z++;
    }
    $play_time = Array('Mostly Starters','Balanced','Mostly Bench');
    $play_echo = '';
    for($v=0;$v<count($play_time);$v++)
    {
        $play_echo .= "<option value = \"".$play_time[$v]."\">".$play_time[$v]."</option>";
    }
    $strat_echo .= "
        <tr>
            <td>Playing Time</td>
            <td><select class='form-control' name='playtime'>".$play_echo."</select></td>
        </tr>";
    $backfield = Array('Primary Back','Back By Committee');
    $back_echo = '';
    for($v=0;$v<count($backfield);$v++)
    {
        $back_echo .= "<option value = \"".$backfield[$v]."\">".$backfield[$v]."</option>";
    }
    $strat_echo .= "
        <tr>
            <td>Backfield Strategy</td>
            <td><select class='form-control' name='backfield'>".$back_echo."</select></td>
        </tr>";
    $sql = "SELECT CONCAT(pfirst,' ',plast) as pname FROM players WHERE team='$cid' ORDER BY pname ASC";
    $presult = mysqli_query($con,$sql);
    $rec_echo = '';
    while ($r = mysqli_fetch_array($presult)) 
    {
        //if($t[$dc_key[$v]] == $r['pname']) {$echo = "selected";} else {$echo = "";}
        $rec_echo .= "<option value = \"".$r['pname']."\">".$r['pname']."</option>";
    }
    $strat_echo .= "
        <tr>
            <td>Primary Receiver</td>
            <td><select class='form-control' name='prirec'>".$rec_echo."</select></td>
        </tr>";
    $tempo = Array('Very Slow','Slow','Normal','Fast','Very Fast');
    $tempo_echo = '';
    for($v=0;$v<count($tempo);$v++)
    {
        $tempo_echo .= "<option value = \"".$tempo[$v]."\">".$tempo[$v]."</option>";
    }
    $strat_echo .= "
        <tr>
            <td>Tempo</td>
            <td><select class='form-control' name='tempo'>".$tempo_echo."</select></td>
        </tr>";
}
if($hostlg == 'SBA')
{
    $strat_arr = Array('Pace','Motion','3P Usage','Crash Off. Boards','Primary D Usage','Secondary D Usage','Full Court Press','Crash Def. Boards');
    $strat_name_arr = Array('pace','motion','tpuse','crasho','primaryd','secondd','fullcourt','crashd');
    $play_arr = Array('man','help','trap','zone');
    $pri_drop = '';
    $sec_drop = '';
    for($v=0;$v<count($play_arr);$v++)
    {
        if($t['setprimary'] == $play_arr[$v]) {$echo = "selected";} else {$echo = "";}
        $pri_drop .= "<option value = \"".$play_arr[$v]."\" $echo>".ucfirst($play_arr[$v])."</option>";
    }
    for($v=0;$v<count($play_arr);$v++)
    {
        if($t['setsecondary'] == $play_arr[$v]) {$echo = "selected";} else {$echo = "";}
        $sec_drop .= "<option value = \"".$play_arr[$v]."\" $echo>".ucfirst($play_arr[$v])."</option>";
    }
    $strat_echo = '';
    $z = 0;
    for($v=0;$v<count($strat_arr);$v++)
    {
        $strat_echo .= "
        <tr>
            <td>".$strat_arr[$v].":</td>
            <td><input type=\"text\" id=\"slider".$z."\" class=\"form-control\" name=\"".$strat_name_arr[$v]."\" value=\"".$t[$strat_name_arr[$v]]."\" data-slider-min=\"0\" data-slider-max=\"10\" data-slider-steps=\"1\" data-slider-value=\"".$t[$strat_name_arr[$v]]."\"></td>
        </tr>";
        $z++;
    }
    $strat_echo .= "
    <tr>
        <td>Focus:</td>
        <td><input type=\"text\" id=\"sliderfocus\" class=\"form-control\" name='focus' value=\"".$t['focus']."\" data-slider-ticks=\"[0,1,2]\" data-slider-ticks-snap-bounds=\"1\" data-slider-ticks-labels='[\"Inside\",\"Balanced\",\"Outside\"]' data-slider-value=\"".$t['focus']."\"></td>
    </tr>
    <tr>
        <td>Set Primary D:</td>
        <td><select name=\"setprimary\" class=\"form-control\">".$pri_drop."</select></td>
    </tr>
    <tr>
        <td>Set Primary D:</td>
        <td><select name=\"setsecondary\" class=\"form-control\">".$sec_drop."</select></td>
    </tr>";
}

// Team Dropdown
$sbateams = '';
$sql = "SELECT id,name FROM teams WHERE league='$league' ORDER BY name";
$result = mysqli_query($con,$sql);
while($r = mysqli_fetch_array($result))
{
    $sbateams .= "<option value = \"".$r['id']."\">".$r['name']."</option>";
}

//CONTRACTS
$players = '';
$myplayers = '';
$bonusplayers = '';
$tradeplayers = '';
$fillers = '';
$salary = '';
$bonussalary = '';
$cotable = '';
$cctable = '';
$ectable = '';
$rptable = '';
// create player dropdown - new contract
$sql = "SELECT p.id,CONCAT(plast,', ',pfirst) as pname,u.active as uactive
FROM auth_user u
INNER JOIN players p ON u.ID=p.user_fk
WHERE (p.active = '1' || p.active = '3') AND team = '0' AND league='$league' ORDER BY pname ASC";
$result = mysqli_query($con,$sql);
while($r = mysqli_fetch_array($result))
{
    $players .= "<option value = \"".$r['id']."\">".$r['pname'].(($r['uactive'] == '0') ? "**" : "")."</option>";
}
// create player dropdown - extension
$sql = "SELECT p.id,CONCAT(plast,', ',pfirst) as pname
FROM players p
WHERE p.team='$tid' and yrs_remain = '1' AND (active='1')";
$result = mysqli_query($con,$sql);
$num = mysqli_num_rows($result);
if($num > 0)
{
    while($r = mysqli_fetch_array($result))
    {
        $myplayers .= "<option value = \"".$r['id']."\">".$r['pname']."</option>";
    }
}
else
{
    $myplayers .= "";
}
// create player dropdown - bonus
$sql = "SELECT p.id,CONCAT(plast,', ',pfirst) as pname
FROM players p
WHERE p.team='$tid' AND (active='1' || active='3')";
$result = mysqli_query($con,$sql);
$num = mysqli_num_rows($result);
if($num > 0)
{
    while($r = mysqli_fetch_array($result))
    {
        $bonusplayers .= "<option value = \"".$r['id']."\">".$r['pname']."</option>";
    }
}
else
{
    $bonusplayers .= "";
}
// create player dropdowwn - trade
$sql = "SELECT p.id,CONCAT(plast,', ',pfirst) as pname
FROM players p
WHERE p.team='$tid'";
$result = mysqli_query($con,$sql);
while($r = mysqli_fetch_array($result))
{
    $tradeplayers .= "<option value = \"".$r['id']."\">".$r['pname']."</option>";
}
// create player dropdown - fillers
$sql = "SELECT id,CONCAT(plast,', ',pfirst) as pname
FROM players
WHERE (active = '2') AND team = '0' AND league='$league' ORDER BY pname ASC";
$result = mysqli_query($con,$sql);
while($r = mysqli_fetch_array($result))
{
    $fillers .= "<option value = \"".$r['id']."\">".$r['pname']."</option>";
}

// create salary dropdown
for($v=2;$v<15;$v++)
{
    $opt = 500000 * $v;
    $salary .= "<option value='".$opt."'>$".number_format($opt,0)."</option>";
}
// create salary dropdown - bonus
for($v=1;$v<15;$v++)
{
    $opt = 500000 * $v;
    $bonussalary .= "<option value='".$opt."'>$".number_format($opt,0)."</option>";
}
// view contract offers
$sql = "SELECT t.name,CONCAT(plast,', ',pfirst) as pname,c.type,c.years,c.salary,c.salary2,c.salary3,c.opt,c.status,c.id as cid
FROM players p
INNER JOIN contracts c ON p.id=c.p_fk
INNER JOIN teams t ON c.t_fk=t.id
WHERE c.t_fk = '$tid' AND status < '2'";
$result = mysqli_query($con,$sql);
while($r = mysqli_fetch_array($result))
{
    if($r['status'] == 1)
    {
        $status = 'Offered';
    }
    else if($r['status'] == 2 || $r['status'] == 3)
    {
        $status = 'Accepted';
    }
    else
    {
        $status = 'Rejected';
    }
    $cotable .= "
    <tr>
        <td>".$r['pname']."</td>
        <td>".ucwords($r['type'])."</td>
        <td>".$r['years']."</td>
        <td>$".number_format($r['salary'],0)."</td>
        <td>$".number_format($r['salary2'],0)."</td>
        <td>$".number_format($r['salary3'],0)."</td>
        <td>".ucwords($r['opt'])."</td>
        <td>".$status."</td>
        <td><a href='contract_offer.php?cid=".$r['cid']."&delete=1' class='btn btn-primary'><i class=\"fas fa-trash\"></i></a></td>
    </tr>";
}
// View Contract Options
$coptable = '';
$sql = "SELECT CONCAT(plast,', ',pfirst) as pname,c.salary,c.opt,c.id as cid
FROM players p
INNER JOIN contracts c ON p.id=c.p_fk
INNER JOIN teams t ON c.t_fk=t.id
WHERE c.t_fk = '$tid' AND status = '3' AND c.yrs_remain = '1' AND (opt = 'team' || opt = 'mutual')";
$result = mysqli_query($con,$sql);
while($r = mysqli_fetch_array($result))
{
    $coptable .= "
    <tr>
        <td>".$r['pname']."</td>
        <td>$".number_format($r['salary'],0)."</td>
        <td>".ucwords($r['opt'])."</td>
        <td><a href='contract_offer.php?cid=".$r['cid']."&status=2&opt=2&type=".$r['opt']."' class='btn btn-primary'>Accept</a><a href='contract_offer.php?cid=".$r['cid']."&status=0&opt=2&type=".$r['opt']."' class='btn btn-danger'>Reject</a></td>
    </tr>";
}
// view current contracts
$sql = "SELECT t.name,CONCAT(plast,', ',pfirst) as pname,CONCAT(pfirst,' ',plast) as pname2,c.type,c.years,c.salary,c.salary2,c.salary3,c.opt,c.yrs_remain,p.active,p.id as pid,c.id as cid,c.status
FROM players p
INNER JOIN contracts c ON p.id=c.p_fk
INNER JOIN teams t ON c.t_fk=t.id
WHERE c.t_fk = '$tid' AND (status = '2' || status = '3')
ORDER BY CASE WHEN (type='Free Agent' || (type='Extension' && status='3') || (type='Scholarship')) THEN 1
WHEN type='Bonus' THEN 2
WHEN type='Filler' THEN 3
WHEN (type='Extension' && status='2') THEN 4
WHEN type='Waived' THEN 5
END, salary DESC";
$result = mysqli_query($con,$sql);
$cctable2 = '';
while($r = mysqli_fetch_array($result))
{
    if($r['type'] == 'Extension' && $r['status'] == 2)
    {
        $cctable .= "
        <tr>
            <td>".$r['pname']."</td>
            <td>".ucwords($r['type'])."</td>
            <td>".$r['years']."</td>
            <td>".$r['yrs_remain']."</td>
            <td>-</td>
            <td>$".number_format($r['salary'],0)."</td>
            <td>$".number_format($r['salary2'],0)."</td>
            <td>".ucwords($r['opt'])."</td>
            ".((($r['active'] == '1' || $r['active'] == '3') && ($r['type'] != 'Waived') && ($r['type'] != 'Extension')) ? "<td><button type='button' onclick=\"javascript:waive_player('".$r['pid']."','".addslashes($r['pname2'])."','".$r['cid']."','2')\" class='btn btn-primary'><i class=\"fas fa-trash\"></i></button></td>" : "".((($r['active'] == '2') && ($r['type'] != 'Waived')) ? "<td><button type='button' onclick=\"javascript:waive_player('".$r['pid']."','".addslashes($r['pname2'])."','".$r['cid']."','1')\" class='btn btn-primary'><i class=\"fas fa-trash\"></i></button></td>" : "<td></td>")."")."
        </tr>";
    }
    else
    {
        $cctable .= "
        <tr>
            <td>".$r['pname']."</td>
            <td>".ucwords($r['type'])."</td>
            <td>".$r['years']."</td>
            <td>".$r['yrs_remain']."</td>
            <td>$".number_format($r['salary'],0)."</td>
            <td>$".number_format($r['salary2'],0)."</td>
            <td>$".number_format($r['salary3'],0)."</td>
            <td>".ucwords($r['opt'])."</td>
            ".((($r['active'] == '1' || $r['active'] == '3') && ($r['type'] != 'Waived')) ? "<td><button type='button' onclick=\"javascript:waive_player('".$r['pid']."','".addslashes($r['pname2'])."','".$r['cid']."','2')\" class='btn btn-primary'><i class=\"fas fa-trash\"></i></button></td>" : "".((($r['active'] == '2') && ($r['type'] != 'Waived')) ? "<td><button type='button' onclick=\"javascript:waive_player('".$r['pid']."','".addslashes($r['pname2'])."','".$r['cid']."','1')\" class='btn btn-primary'><i class=\"fas fa-trash\"></i></button></td>" : "<td></td>")."")."
        </tr>";
    }
}
$cctable2 .= "<tr><td colspan='4'></td><td>$".number_format($tsalary,0)."</td><td>$".number_format($tsalary2,0)."</td><td>$".number_format($tsalary3,0)."</td><td colspan='2'></td></tr>";

// view trade block
$tbtable = "";
$sql = "SELECT e.name as tname, CONCAT(pfirst,' ',plast) as pname,salary,yrs_remain,position,position2 FROM team_trade_block t INNER JOIN teams e ON t.t_fk=e.id INNER JOIN players p ON t.p_fk=p.id ORDER BY tname ASC";
$result = mysqli_query($con,$sql);
while($r = mysqli_fetch_array($result))
{
    if($r['position2'] == "")
    {
        $positions = $r['position'];
    }
    else
    {
        $positions = $r['position']."/".$r['position2'];
    }
    $tbtable .= "
    <tr>
        <td>".$r['pname']."</td>
        <td>".$positions."</td>
        <td>".$r['tname']."</td>
        <td>$".number_format($r['salary'],0)."</td>
        <td>".ucwords($r['yrs_remain'])."</td>
    </tr>";
}


// view expiring contracts
$sql = "SELECT p.id,CONCAT(plast,', ',pfirst) as pname,p.salary
FROM players p
WHERE p.team='$tid' and yrs_remain = '1'";
$result = mysqli_query($con,$sql);
while($r = mysqli_fetch_array($result))
{
    $ectable .= "
    <tr>
        <td>".$r['pname']."</td>
        <td>$".number_format($r['salary'],0)."</td>
    </tr>";

}

// view retiring players
$sql = "SELECT p.id,CONCAT(plast,', ',pfirst) as pname,p.salary
FROM players p
WHERE p.team='$tid' and active = '3'";
$result = mysqli_query($con,$sql);
while($r = mysqli_fetch_array($result))
{
    $rptable .= "
    <tr>
        <td>".$r['pname']."</td>
        <td>$".number_format($r['salary'],0)."</td>
    </tr>";

}

if(isset($_GET['alert'])) 
{
    if ($_GET['alert'] == 'maxroster') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You are already at the maximum roster size. Please make a trade or release a player if you want to sign a new player.</strong></div>";
    }
    else if ($_GET['alert'] == 'maxrosterfiller') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You are already at the maximum roster size for fillers. Please make a trade or release a filler if you want to sign a new filler.</strong></div>";
    }
    else if ($_GET['alert'] == 'fillersigningapproved') 
    {
        $alert = "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You have successfully signed your filler.</strong></div>";
    }
    else if ($_GET['alert'] == 'inactivesigningapproved') 
    {
        $alert = "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You have successfully signed this player to an Inactive contract.</strong></div>";
    }
    else if ($_GET['alert'] == 'offerexists') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You already have an outstanding offer to this player.</strong></div>";
    }
    else if ($_GET['alert'] == 'minsalnotmet') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You offered less than the minimum salary value for this player. Please offer again and ensure your offer is compliant with their minimum salary.</strong></div>";
    }
    else if ($_GET['alert'] == 'cosuccess') 
    {
        $alert = "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You have successfully made an offer to a player.</strong></div>";
    }
    else if ($_GET['alert'] == 'stratsaved') 
    {
        $alert = "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You have successfully saved your strategies.</strong></div>";
    }
    else if ($_GET['alert'] == 'templateloaded') 
    {
        $alert = "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You have successfully loaded a previous strategy.</strong></div>";
    }
    else if ($_GET['alert'] == 'contractdeleted') 
    {
        $alert = "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You have successfully deleted your contract offer.</strong></div>";
    }
    else if ($_GET['alert'] == 'movesuccess') 
    {
        $alert = "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You have successfully moved a player to another team.</strong></div>";
    }
    else if ($_GET['alert'] == 'teamhasnocap') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Your team does not have the cap space. to complete this transaction.</strong></div>";
    }
    else if ($_GET['alert'] == 'playerexceedsmax') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>The player you are attempting to give a bonus to would exceed the maximum allowed salary for a season with a bonus of this amount.</strong></div>";
    }
    else if ($_GET['alert'] == 'bonusgiven') 
    {
        $alert = "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You have successfully given a bonus to your player.</strong></div>";
    }
    else if ($_GET['alert'] == 'optiondeclined') 
    {
        $alert = "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You have successfully declined this option.</strong></div>";
    }
    else if ($_GET['alert'] == 'mutualtoplayer') 
    {
        $alert = "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You have successfully accepted your portion of this Mutual Option.</strong></div>";
    }
    else if ($_GET['alert'] == 'teamoptionsuccess') 
    {
        $alert = "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You have successfully accepted your Option for this player. They have now been paid for this contract.</strong></div>";
    }
    else if ($_GET['alert'] == 'mutualdupe') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>This option has been updated since you last refreshed the page. Please try again.</strong></div>";
    }
}
else
{$alert = '';}
?>

<!-- Body Start -->
<body>
<div id="wrapper" style="position:relative;top:0;left:0;">
    <?php include $path.'/includes/nav.php'; ?>
    <div class="container">
        <?=HeaderImagePrint($hostlg,$_SESSION['style'])?>
        <?=$alert?>
        <div class="panel-group">
            <div class="panel panel-primary">
                <div class="panel-heading"><?=$tname?> Strategies</div>
                    <div class="panel-body">
                        <div class="col-xs-12 col-sm-9">
                            <form class="form-horizontal" method='POST' action="coach_strategies_process.php">
                                <div class="form-group">
                                    <div class='input-group col-xs-12 col-sm-4'><span class='input-group-addon' style='width:120px;text-align:left;'>Strategy</span><input id='strat_name' type='text' class='form-control' name='strat_name' value="<?=$t['strat_name']?>"></div>
                                </div>
                                <table class='table'>
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Name</th>
                                            <th>Position(s)</th>
                                            <th>Minutes</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?=$dc_echo?>
                                    </tbody>
                                </table>
                                <table class='table'>
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?=$dc_optecho?>
                                    </tbody>
                                </table>
                                <table class='table'>
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?=$strat_echo?>
                                    </tbody>
                                </table>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <input type='text' hidden='hidden' name='league' value="<?=$league?>">
                                        <input type='text' hidden='hidden' name='cid' id='cid' value="<?=$cid?>">
                                        <button type="submit" name="strat_submit" class="btn btn-default">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-xs-12 col-sm-3">
                            <div class="col-sm-12 pt bg-basic border">
                                <div class="card-header bordertr">
                                    <h4 class="text-center bordertr">Team Info</h4>
                                </div>
                                <div class="card-body text-left padlr">
                                    <?=$teaminfo?>
                                </div>
                            </div>
                            <strong class="text-center text-uppercase col-xs-12 puheader">Strategies</strong>
                            <button class="btn btn-primary col-xs-12 mb pubutton" id="loadstrategy">Load</button>
                            <button class="btn btn-primary col-xs-12 mb pubutton" id="deletestrategy">Delete</button>
                            <strong class="text-center text-uppercase col-xs-12 puheader">Manage</strong>
                            <?=
                            (($tleague == 'SBA') ? "<button class=\"btn btn-primary col-xs-12 mb4th pubutton\" id=\"offercontract\">Offer New Contract</button>
                            <button class=\"btn btn-primary col-xs-12 mb4th pubutton\" id=\"offerextension\">Offer Extension</button>
                            <button class=\"btn btn-primary col-xs-12 mb4th pubutton\" id=\"offerbonus\">Offer Bonus</button>
                            <button class=\"btn btn-primary col-xs-12 mb4th pubutton\" id=\"offerfiller\">Offer Filler</button>
                            <button class=\"btn btn-primary col-xs-12 mb4th pubutton\" id=\"viewcontractoffers\">View Contract Offers</button>
                            <button class=\"btn btn-primary col-xs-12 mb4th pubutton\" id=\"viewcontractoptions\">View Contract Options</button>
                            <button class=\"btn btn-primary col-xs-12 mb4th pubutton\" id=\"viewcurrentcontracts\">View Current Contracts</button>
                            <button class=\"btn btn-primary col-xs-12 mb4th pubutton\" id=\"newteam\">Move Player</button>
                            <button class=\"btn btn-primary col-xs-12 mb4th pubutton\" disabled>Offer Trade</button>
                            <button class=\"btn btn-primary col-xs-12 mb4th pubutton\" disabled>View Trade Offers</button>
                            <button class=\"btn btn-primary col-xs-12 mb4th pubutton\" id=\"modifytradeblock\">Modify Trading Block</button>
                            <button class=\"btn btn-primary col-xs-12 mb4th pubutton\" id=\"viewtradeblock\">View Trading Blocks</button>
                            <button class=\"btn btn-primary col-xs-12 mb4th pubutton\" id=\"viewexpiring\">View Expiring Contracts</button>
                            <button class=\"btn btn-primary col-xs-12 mb4th pubutton\" id=\"viewretiring\">View Retiring Players</button>
                            <strong class=\"text-center text-uppercase col-xs-12 puheader\">Draft</strong>
                            <button class=\"btn btn-primary col-xs-12 mb4th pubutton\" disabled>Select Pick</button>
                            <button class=\"btn btn-primary col-xs-12 mb4th pubutton\" disabled>Update Auto-Select List</button>
                            <button class=\"btn btn-primary col-xs-12 mb4th pubutton\" disabled>View Upcoming Prospects</button>
                            <button class=\"btn btn-primary col-xs-12 mb4th pubutton\" disabled>View Team Draft Picks</button>" 
                            : "<button class=\"btn btn-primary col-xs-12 mb4th pubutton\" id=\"offerscholarship\">Offer Scholarship</button>
                            <button class=\"btn btn-primary col-xs-12 mb4th pubutton\" id=\"viewcurrentcontracts\">View Current Scholarships</button>
                            <button class=\"btn btn-primary col-xs-12 mb4th pubutton\" id=\"newteam\">Move Player</button>") ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class='modal fade' id='#loadstrategy' role='dialog'>
    <div class='modal-dialog modal-lg'>
        <div class='modal-content'>
            <form action='coach_strategies_process.php' method='post'>
                <div class='modal-header'>
                    <button type='button' class='close' data-dismiss='modal'>&times;</button>
                    <h4 class='modal-title'>Load Strategy</h4>
                </div>
                <div class='modal-body'>
                    <div class='form-group'>
                        <div class='input-group'><span class='input-group-addon' style='width:120px;text-align:left;'>Load Previous Strategy</span><select class='form-control' name='template' id="template"><?=$secho?></select></div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <input type='text' hidden='hidden' name='cid' id='cid' value="<?=$cid?>">
                            <input type='text' hidden='hidden' name='league' id='league' value="<?=$league?>">
                        </div>
                    </div> 
                </div>
                <div class='modal-footer'>
                    <button type='submit' class='btn btn-primary'>Submit</button>
                    <button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class='modal fade' id='#deletestrategy' role='dialog'>
    <div class='modal-dialog modal-lg'>
        <div class='modal-content'>
            <form action='coach_strategies_process.php' method='post'>
                <div class='modal-header'>
                    <button type='button' class='close' data-dismiss='modal'>&times;</button>
                    <h4 class='modal-title'>Delete Strategy</h4>
                </div>
                <div class='modal-body'>
                    <div class='form-group'>
                        <div class='input-group'><span class='input-group-addon' style='width:120px;text-align:left;'>Delete Strategy</span><select class='form-control' name='template' id="template"><?=$secho?></select></div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <input type='text' hidden='hidden' name='cid' id='cid' value="<?=$cid?>">
                            <input type='text' hidden='hidden' name='league' id='league' value="<?=$league?>">
                        </div>
                    </div> 
                </div>
                <div class='modal-footer'>
                    <button type="button" class="btn btn-default" onclick="javascript:delete_strat()">Delete</button>
                    <button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class='modal fade' id='#offercontract' role='dialog'>
    <div class='modal-dialog modal-lg'>
        <div class='modal-content'>
            <form action='contract_offer.php' method='post'>
                <div class='modal-header'>
                    <button type='button' class='close' data-dismiss='modal'>&times;</button>
                    <h4 class='modal-title'>Contract Offer</h4>
                </div>
                <div class='modal-body'>
                    <div class='form-group'>
                        <div class='input-group'><span class='input-group-addon' style='width:120px;text-align:left;'>Player</span><select class='form-control' name='player'><?=$players?></select></div>
                    </div>
                    <div class='form-group'>
                        <div class='input-group'><span class='input-group-addon' style='width:120px;text-align:left;'>Years</span><select class='form-control' name='numyrs' id='numyrs'><option>1</option><option>2</option><option>3</option></select></div>
                    </div>
                    <div class='form-group'>
                        <div class='input-group'><span class='input-group-addon' style='width:120px;text-align:left;'>Year 1 Salary:</span><select class='form-control' name='salary' id='salary'><?=$salary?></select></div>
                    </div>
                    <div class='form-group' id='sal2'>
                        <div class='input-group'><span class='input-group-addon' style='width:120px;text-align:left;'>Year 2 Salary:</span><select class='form-control' name='salary2' id='salary2'><?=$salary?></select></div>
                    </div>
                    <div class='form-group' id='sal3'>
                        <div class='input-group'><span class='input-group-addon' style='width:120px;text-align:left;'>Year 3 Salary:</span><select class='form-control' name='salary3' id='salary3'><?=$salary?></select></div>
                    </div>
                    <div class='form-group'>
                        <div class='input-group'><span class='input-group-addon' style='width:120px;text-align:left;'>Option:</span><select class='form-control' name='option'><option value='none'>None</option><option value='player'>Player</option><option value='team'>Team</option><option value='mutual'>Mutual</option></select></div>
                    </div>
                    <input type='text' name='tid' value='<?=$tid?>' hidden>
                    <input type='text' name='type' value='Free Agent' hidden>
                </div>
                <div class='modal-footer'>
                    <button type='submit' class='btn btn-primary'>Submit</button>
                    <button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class='modal fade' id='#offerextension' role='dialog'>
    <div class='modal-dialog modal-lg'>
        <div class='modal-content'>
            <form action='contract_offer.php' method='post'>
                <div class='modal-header'>
                    <button type='button' class='close' data-dismiss='modal'>&times;</button>
                    <h4 class='modal-title'>Extension Offer</h4>
                </div>
                <div class='modal-body'>
                    <div class='form-group'>
                        <div class='input-group'><span class='input-group-addon' style='width:120px;text-align:left;'>Player</span><select class='form-control' name='player'><?=$myplayers?></select></div>
                    </div>
                    <div class='form-group'>
                        <div class='input-group'><span class='input-group-addon' style='width:120px;text-align:left;'>Years</span><select class='form-control' name='numyrs' id='numyrs2'><option>1</option><option>2</option><option>3</option></select></div>
                    </div>
                    <div class='form-group'>
                        <div class='input-group'><span class='input-group-addon' style='width:120px;text-align:left;'>Year 1 Salary:</span><select class='form-control' name='salary' id='salaryext'><?=$salary?></select></div>
                    </div>
                    <div class='form-group' id='sal2ext'>
                        <div class='input-group'><span class='input-group-addon' style='width:120px;text-align:left;'>Year 2 Salary:</span><select class='form-control' name='salary2' id='salary2ext'><?=$salary?></select></div>
                    </div>
                    <div class='form-group' id='sal3ext'>
                        <div class='input-group'><span class='input-group-addon' style='width:120px;text-align:left;'>Year 3 Salary:</span><select class='form-control' name='salary3' id='salary3ext'><?=$salary?></select></div>
                    </div>
                    <div class='form-group'>
                        <div class='input-group'><span class='input-group-addon' style='width:120px;text-align:left;'>Option:</span><select class='form-control' name='option'><option value='none'>None</option><option value='player'>Player</option><option value='team'>Team</option><option value='mutual'>Mutual</option></select></div>
                    </div>
                    <input type='text' name='tid' value='<?=$tid?>' hidden>
                    <input type='text' name='type' value='Extension' hidden>
                </div>
                <div class='modal-footer'>
                    <button type='submit' class='btn btn-primary'>Submit</button>
                    <button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class='modal fade' id='#offerbonus' role='dialog'>
    <div class='modal-dialog modal-lg'>
        <div class='modal-content'>
            <form action='contract_offer.php' method='post'>
                <div class='modal-header'>
                    <button type='button' class='close' data-dismiss='modal'>&times;</button>
                    <h4 class='modal-title'>Bonus Offer</h4>
                </div>
                <div class='modal-body'>
                    <div class='form-group'>
                        <div class='input-group'><span class='input-group-addon' style='width:120px;text-align:left;'>Player</span><select class='form-control' name='player'><?=$bonusplayers?></select></div>
                    </div>
                    <div class='form-group'>
                        <div class='input-group'><span class='input-group-addon' style='width:120px;text-align:left;'>Bonus Amount</span><select class='form-control' name='salary'><?=$bonussalary?></select></div>
                    </div>
                    <input type='text' name='tid' value='<?=$tid?>' hidden>
                    <input type='text' name='type' value='Bonus' hidden>
                </div>
                <div class='modal-footer'>
                    <button type='submit' class='btn btn-primary'>Submit</button>
                    <button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class='modal fade' id='#offerfiller' role='dialog'>
    <div class='modal-dialog modal-lg'>
        <div class='modal-content'>
            <form action='contract_offer.php' method='post'>
                <div class='modal-header'>
                    <button type='button' class='close' data-dismiss='modal'>&times;</button>
                    <h4 class='modal-title'>Filler Offer</h4>
                </div>
                <div class='modal-body'>
                    <div class='form-group'>
                        <div class='input-group'><span class='input-group-addon' style='width:120px;text-align:left;'>Player</span><select class='form-control' name='player'><?=$fillers?></select></div>
                    </div>
                    <div class='form-group'>
                        <div class='input-group'><span class='input-group-addon' style='width:120px;text-align:left;'>Years</span><select class='form-control' name='numyrs' id='numyrs2'><option>1</option><option>2</option><option>3</option></select></div>
                    </div>
                    <input type='text' name='tid' value='<?=$tid?>' hidden>
                    <input type='text' name='type' value='Filler' hidden>
                </div>
                <div class='modal-footer'>
                    <button type='submit' class='btn btn-primary'>Submit</button>
                    <button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class='modal fade' id='#viewcontractoffers' role='dialog'>
    <div class='modal-dialog modal-lg'>
        <div class='modal-content'>
            <form action='contract_offer.php' method='post'>
                <div class='modal-header'>
                    <button type='button' class='close' data-dismiss='modal'>&times;</button>
                    <h4 class='modal-title'>View Contract Offers</h4>
                </div>
                <div class='modal-body'>
                    <div class='table-responsive'>
                        <table class="table">
                            <thead>
                                <th>Name</th>
                                <th>Type</th>
                                <th>Years</th>
                                <th>Year 1 Salary</th>
                                <th>Year 2 Salary</th>
                                <th>Year 3 Salary</th>
                                <th>Option</th>
                                <th>Status</th>
                                <th></th>
                            </thead>
                            <tbody>
                                <?=$cotable?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class='modal-footer'>
                    <button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class='modal fade' id='#viewcontractoptions' role='dialog'>
    <div class='modal-dialog modal-lg'>
        <div class='modal-content'>
            <form action='contract_offer.php' method='post'>
                <div class='modal-header'>
                    <button type='button' class='close' data-dismiss='modal'>&times;</button>
                    <h4 class='modal-title'>View Contract Options</h4>
                </div>
                <div class='modal-body'>
                    <div class='table-responsive'>
                        <table class="table">
                            <thead>
                                <th>Name</th>
                                <th>Salary</th>
                                <th>Option</th>
                                <th></th>
                            </thead>
                            <tbody>
                                <?=$coptable?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class='modal-footer'>
                    <button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class='modal fade' id='#viewcurrentcontracts' role='dialog'>
    <div class='modal-dialog modal-lg'>
        <div class='modal-content'>
            <form action='contract_offer.php' method='post'>
                <div class='modal-header'>
                    <button type='button' class='close' data-dismiss='modal'>&times;</button>
                    <h4 class='modal-title'>View Current Contracts</h4>
                </div>
                <div class='modal-body'>
                    <div class='table-responsive'>
                        <table class="table">
                            <thead>
                                <th>Name</th>
                                <th>Type</th>
                                <th>Years</th>
                                <th>Years Left</th>
                                <th>Year 1 Salary</th>
                                <th>Year 2 Salary</th>
                                <th>Year 3 Salary</th>
                                <th>Option</th>
                            </thead>
                            <tbody>
                                <?=$cctable?>
                            </tbody>
                            <tfoot>
                                <?=$cctable2?>
                            </tfoot>
                        </table>
                    </div>
                    <div id="confirmwaive"></div>
                </div>
                <div class='modal-footer'>
                    <button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class='modal fade' id='#modifytradeblock' role='dialog'>
    <div class='modal-dialog modal-lg'>
        <div class='modal-content'>
            <div class='modal-header'>
                <button type='button' class='close' data-dismiss='modal'>&times;</button>
                <h4 class='modal-title'>Modify Trade Block</h4>
            </div>
            <div class='modal-body'>
                <div id="ajaxtradeblock"></div>
            </div>
            <div class='modal-footer'>
                <button type='submit' class='btn btn-primary' name='trade_block'>Submit</button>
                <button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button>
            </div>
        </div>
    </div>
</div>
<div class='modal fade' id='#viewtradeblock' role='dialog'>
    <div class='modal-dialog modal-lg'>
        <div class='modal-content'>
            <div class='modal-header'>
                <button type='button' class='close' data-dismiss='modal'>&times;</button>
                <h4 class='modal-title'>View Trade Block</h4>
            </div>
            <div class='modal-body'>
                <div class='table-responsive'>
                    <table class="table">
                        <thead>
                            <th>Name</th>
                            <th>Position</th>
                            <th>Team</th>
                            <th>Salary</th>
                            <th>Years</th>
                        </thead>
                        <tbody>
                            <?=$tbtable?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class='modal-footer'>
                <button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button>
            </div>
        </div>
    </div>
</div>
<div class='modal fade' id='#viewexpiring' role='dialog'>
    <div class='modal-dialog modal-lg'>
        <div class='modal-content'>
            <form action='contract_offer.php' method='post'>
                <div class='modal-header'>
                    <button type='button' class='close' data-dismiss='modal'>&times;</button>
                    <h4 class='modal-title'>View Expiring Contracts</h4>
                </div>
                <div class='modal-body'>
                    <div class='table-responsive'>
                        <table class="table">
                            <thead>
                                <th>Name</th>
                                <th>Salary</th>
                            </thead>
                            <tbody>
                                <?=$ectable?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class='modal-footer'>
                    <button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class='modal fade' id='#viewretiring' role='dialog'>
    <div class='modal-dialog modal-lg'>
        <div class='modal-content'>
            <form action='contract_offer.php' method='post'>
                <div class='modal-header'>
                    <button type='button' class='close' data-dismiss='modal'>&times;</button>
                    <h4 class='modal-title'>View Retiring Players</h4>
                </div>
                <div class='modal-body'>
                    <div class='table-responsive'>
                        <table class="table">
                            <thead>
                                <th>Name</th>
                                <th>Salary</th>
                            </thead>
                            <tbody>
                                <?=$rptable?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class='modal-footer'>
                    <button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class='modal fade' id='#newteam' role='dialog'>
    <div class='modal-dialog modal-lg'>
        <div class='modal-content'>
            <form action='change_team.php' method='post'>
                <div class='modal-header'>
                    <button type='button' class='close' data-dismiss='modal'>&times;</button>
                    <h4 class='modal-title'>Change Team</h4>
                </div>
                <div class='modal-body'>
                    <div class='form-group'>
                        <div class='input-group'><span class='input-group-addon' style='width:120px;text-align:left;'>New Team:</span>
                            <select class='form-control' name='team' id='newteamteam'>
                                <?=$sbateams?>
                            </select>
                        </div>
                    </div>
                    <div class='form-group'>
                        <div class='input-group'><span class='input-group-addon' style='width:120px;text-align:left;'>Player</span><select class='form-control' name='pid'><?=$tradeplayers?></select></div>
                    </div>
                    <input type='text' name='sbacoach' id='newteamtid' value='1' hidden>
                </div>
                <div class='modal-footer'>
                    <button type='submit' class='btn btn-primary'>Submit</button>
                    <button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
// JQuery
$(document).ready(function(){
    $('#sal2').hide();
    $('#sal3').hide();
    $('#salary2').val(0);
    $('#salary3').val(0);
    $('#sal2ext').hide();
    $('#sal3ext').hide();
    $('#salary2ext').val(0);
    $('#salary3ext').val(0);
    $('#numyrs').change(function() 
    {
        var yrs = $('#numyrs').val();
        if(yrs == 3)
        {
            $('#sal2').show();
            $('#salary2').val(1000000);
            $('#sal3').show();
            $('#salary3').val(1000000);
        }
        else if(yrs == 2)
        {
            $('#sal2').show();
            $('#salary2').val(1000000);
            $('#sal3').hide();
            $('#salary3').val(0);
        }
        else
        {
            $('#sal2').hide();
            $('#salary2').val(0);
            $('#sal3').hide();
            $('#salary3').val(0);
        }
    });
    $('#numyrs2').change(function() 
    {
        var yrs = $('#numyrs2').val();
        if(yrs == 3)
        {
            $('#sal2ext').show();
            $('#salary2ext').val(1000000);
            $('#sal3ext').show();
            $('#salary3ext').val(1000000);
        }
        else if(yrs == 2)
        {
            $('#sal2ext').show();
            $('#salary2ext').val(1000000);
            $('#sal3ext').hide();
            $('#salary3ext').val(0);
        }
        else
        {
            $('#sal2ext').hide();
            $('#salary2ext').val(0);
            $('#sal3ext').hide();
            $('#salary3ext').val(0);
        }
    });
});
$('#loadstrategy').click(function() {
    $('#\\#loadstrategy').modal('show');
});
$('#deletestrategy').click(function() {
    $('#\\#deletestrategy').modal('show');
});
$('#offercontract').click(function() {
    $('#\\#offercontract').modal('show');
});
$('#offerextension').click(function() {
    $('#\\#offerextension').modal('show');
});
$('#offerbonus').click(function() {
    $('#\\#offerbonus').modal('show');
});
$('#offerfiller').click(function() {
    $('#\\#offerfiller').modal('show');
});
$('#viewcontractoffers').click(function() {
    $('#\\#viewcontractoffers').modal('show');
});
$('#viewcontractoptions').click(function() {
    $('#\\#viewcontractoptions').modal('show');
});
$('#viewcurrentcontracts').click(function() {
    $('#\\#viewcurrentcontracts').modal('show');
});
$('#modifytradeblock').click(function() {
    $.ajax({url: "../ajax/get_tradeblock.php", success: function(result){
      $("#ajaxtradeblock").html(result);
    }});
    $('#\\#modifytradeblock').modal('show');
});
$('#viewtradeblock').click(function() {
    $('#\\#viewtradeblock').modal('show');
});
$('#viewexpiring').click(function() {
    $('#\\#viewexpiring').modal('show');
});
$('#viewretiring').click(function() {
    $('#\\#viewretiring').modal('show');
});
$('#newteam').click(function() {
    $('#\\#newteam').modal('show');
});
$("input[id^='slider']").each(function(){
    $(this).slider();
}); 
$("#sliderfocus").slider({
    ticks: [0, 1, 2],
    ticks_labels: ['Inside', 'Balanced', 'Outside'],
    ticks_snap_bounds: 1
}); 
function delete_strat()
{
    sid = document.getElementById('template').value;
    theurl = "strategy_delete.php?sid="+sid+"&league=sba";
    window.location.href = theurl;
}
function waive_player(pid,pname,cid,del)
{
    $('#confirmwaive').empty();   
    if(del == 2)
    {
        $('#confirmwaive').html("<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>By clicking confirm, you will waive "+pname+" and will pay half of their salary (including bonuses) in penalties this season, and then half of their salary in penalties for each additional year on their contract if they are not picked up by another team while on waivers. If you wish to continue, please proceed.<br><br>Note: This transaction is <i>irreversible</i>.<br><br><a href='contract_offer.php?cid="+cid+"&pid="+pid+"&delete="+del+"' class='btn btn-primary'>Waive "+pname+"</a></strong></div>");
    }
    else if(del == 1)
    {
        $('#confirmwaive').html("<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>By clicking confirm, you will waive "+pname+". If you wish to continue, please proceed.<br><br>Note: This transaction is <i>irreversible</i>.<br><br><a href='contract_offer.php?cid="+cid+"&pid="+pid+"&delete="+del+"' class='btn btn-primary'>Waive "+pname+"</a></strong></div>");
    }
}
</script>
<!-- Body End -->

<?php
include $path.'/footer.php';
?>
