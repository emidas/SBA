<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
// get user and mid
$user=$_SESSION['user'];
$mid = $_SESSION['memberid'];

// get pid
$pid = sanitize($con,$_GET['pid']);

// get year
$system = new System();
$year = $system->get_year();
$preyear = $year - 1;

// Create Player Class
$player = new Player($pid);
$pname = $player->get_('name');
$pmbank = $player->get_('p_bank');
$pleague = $player->get_('league');
$ptpe = $player->get_('tpe');

// Development
if($pleague == $sublg)
{
    $sql = "SELECT id FROM forms WHERE form='rollover' AND isactive='1'";
    $numsult = mysqli_query($con,$sql);
    $num = mysqli_num_rows($numsult);
    if($num > 0)
    {
        $development = 'disabled';
    }
    else
    {
        $week = date("m/d/Y", strtotime('Sunday'));
        $sql = "SELECT id FROM player_development WHERE p_fk='$pid' AND week='$week'";
        $numsult = mysqli_query($con,$sql);
        $num = mysqli_num_rows($numsult);
        if($num == '1')
        {
            $development = 'disabled';
        }
        else
        {
            $development = '';
        }
    }
}
else
{
    $development = 'disabled';
}

// Training/Workout
$sql = "SELECT id FROM forms WHERE form='rollover' AND isactive='1'";
$numsult = mysqli_query($con,$sql);
$num = mysqli_num_rows($numsult);
if($num > 0)
{
    $training = 'disabled';
}
else
{
    $sql = "SELECT id FROM player_training WHERE p_fk='$pid' AND season='$year'";
    $numsult = mysqli_query($con,$sql);
    $num = mysqli_num_rows($numsult);
    if($num == '1')
    {
        $training = 'disabled';
    }
    else
    {
        $training = '';
    }
}

// Predictions
$sql = "SELECT id FROM player_prediction WHERE p_fk='$pid' AND season='$preyear' AND claimed = '0'";
$numsult = mysqli_query($con,$sql);
$num = mysqli_num_rows($numsult);
if($num == '0')
{
    $prediction = 'disabled';
}
else
{
    $prediction = '';
}

// Giveaways

// Carryover
if($pleague == $mainlg)
{
    $sql = "SELECT a.id,pe FROM account_carryover a INNER JOIN auth_user u ON u.ID=a.a_fk WHERE username=\"$user\" AND claimed='0'";
    $numsult = mysqli_query($con,$sql);
    $num = mysqli_num_rows($numsult);
    $cid = '';
    $cpe = '';
    if($num > 0)
    {
        $w = mysqli_fetch_array($numsult);
        $cid = $w['id'];
        $cpe = $w['pe'];
        $carryover = '';
    }
    else
    {
        $carryover = 'disabled';
    }
}
else
{
    $carryover = 'disabled';
}

// Player Store
$store = "
    <div class='form-group'>
        <div class='col-xs-1 col-sm-3'></div><div class='input-group col-xs-10 col-sm-6'><span class='input-group-addon' style='width:120px;text-align:left;'>Cash</span><input id='p_bank' type='text' class='form-control' name='p_bank' readonly='readonly' value='$".number_format($pmbank,0)."'></div>
    </div>";
if($pleague == 'SBA')
{
    $store .= "
    <div class='form-group'>
        <div class='col-xs-1 col-sm-3'></div><a href='player_store_processing.php?pid=".$pid."&package=1' class='btn btn-primary col-xs-10 col-sm-6' style='padding-left:0;padding-right:0;'>Shane Banks Training ($7,000,000, 22PE)</a>
    </div>
    <div class='form-group'>
        <div class='col-xs-1 col-sm-3'></div><a href='player_store_processing.php?pid=".$pid."&package=2' class='btn btn-primary col-xs-10 col-sm-6' style='padding-left:0;padding-right:0;'>Blue Dads Training ($5,500,000, 14PE)</a>
    </div>
    <div class='form-group'>
        <div class='col-xs-1 col-sm-3'></div><a href='player_store_processing.php?pid=".$pid."&package=3' class='btn btn-primary col-xs-10 col-sm-6' style='padding-left:0;padding-right:0;'>Gregor Clegane Training ($4,000,000, 10PE)</a>
    </div>
    <div class='form-group'>
        <div class='col-xs-1 col-sm-3'></div><a href='player_store_processing.php?pid=".$pid."&package=4' class='btn btn-primary col-xs-10 col-sm-6' style='padding-left:0;padding-right:0;'>Nick Catania Training ($2,500,000, 6PE)</a>
    </div>
    <div class='form-group'>
        <div class='col-xs-1 col-sm-3'></div><a href='player_store_processing.php?pid=".$pid."&package=5' class='btn btn-primary col-xs-10 col-sm-6' style='padding-left:0;padding-right:0;'>Jack Salt Training ($1,000,000, 2PE)</a>
    </div>
    <div class='form-group'>
        <div class='col-xs-1 col-sm-3'></div><a href='player_store_processing.php?pid=".$pid."&package=6' class='btn btn-primary col-xs-10 col-sm-6' style='padding-left:0;padding-right:0;'>Keenan Jefferson Training ($500,000, 1PE)</a>
    </div>";
}
else
{
    $store .= "
    <div class='form-group'>
        <div class='col-xs-1 col-sm-3'></div><a href='player_store_processing.php?pid=".$pid."&package=7' class='btn btn-primary col-xs-10 col-sm-6' style='padding-left:0;padding-right:0;'>Phillycheese Training ($5,000,000, 12PE)</a>
    </div>
    <div class='form-group'>
        <div class='col-xs-1 col-sm-3'></div><a href='player_store_processing.php?pid=".$pid."&package=8' class='btn btn-primary col-xs-10 col-sm-6' style='padding-left:0;padding-right:0;'>Shaka Training ($3,000,000, 7PE)</a>
    </div>
    <div class='form-group'>
        <div class='col-xs-1 col-sm-3'></div><a href='player_store_processing.php?pid=".$pid."&package=9' class='btn btn-primary col-xs-10 col-sm-6' style='padding-left:0;padding-right:0;'>Leonidas Training ($2,000,000, 5PE)</a>
    </div>";
}

// View Offers
$cotable = '';
$sql = "SELECT c.id as cid,t.name as tname,CONCAT(plast,', ',pfirst) as pname,c.type,c.years,c.salary,c.salary2,c.salary3,c.opt
FROM players p 
INNER JOIN contracts c ON p.id=c.p_fk
INNER JOIN teams t ON c.t_fk=t.id
WHERE p.id='$pid'
AND status = '1'";
$result = mysqli_query($con,$sql);
while($r = mysqli_fetch_array($result))
{
    $cotable .= "
    <tr>
        <td>".ucwords($r['type'])."</td>
        <td>".$r['tname']."</td>
        <td>".$r['years']."</td>
        <td>$".number_format($r['salary'],0)."</td>
        <td>$".number_format($r['salary2'],0)."</td>
        <td>$".number_format($r['salary3'],0)."</td>
        <td>".$r['opt']."</td>
        <td><a href='contract_offer.php?cid=".$r['cid']."&status=2' class='btn btn-primary'>Accept</a><a href='contract_offer.php?cid=".$r['cid']."&status=0' class='btn btn-danger'>Reject</a></td>
    </tr>";
}

// View Options
$coptable = '';
$sql = "SELECT c.id as cid,t.name as tname,c.salary,c.opt
FROM players p 
INNER JOIN contracts c ON p.id=c.p_fk
INNER JOIN teams t ON c.t_fk=t.id
WHERE p.id='$pid'
AND status = '3'
AND c.yrs_remain = '1'
AND (opt = 'player' || opt = 'mutual')";
$result = mysqli_query($con,$sql);
while($r = mysqli_fetch_array($result))
{
    $coptable .= "
    <tr>
        <td>".$r['tname']."</td>
        <td>$".number_format($r['salary'],0)."</td>
        <td>".ucwords($r['opt'])."</td>
        <td><a href='contract_offer.php?cid=".$r['cid']."&status=2&opt=1&type=".$r['opt']."' class='btn btn-primary'>Accept</a><a href='contract_offer.php?cid=".$r['cid']."&status=0&opt=1&type=".$r['opt']."' class='btn btn-danger'>Reject</a></td>
    </tr>";
}

// View Contracts
$cctable = '';
$sql = "SELECT t.name as tname,CONCAT(plast,', ',pfirst) as pname,c.type,c.years,c.salary,c.salary2,c.salary3,c.opt,c.yrs_remain,status
FROM players p
INNER JOIN contracts c ON p.id=c.p_fk
INNER JOIN teams t ON c.t_fk=t.id
WHERE c.p_fk = '$pid'
ORDER BY status DESC";
$result = mysqli_query($con,$sql);
while($r = mysqli_fetch_array($result))
{
    if($r['status'] == '4')
    {
        $constat = "Completed";
    }
    else if($r['status'] == '3')
    {
        $constat = "Ongoing";
    }
    else if($r['status'] == '2')
    {
        $constat = "Accepted";
    }
    else if($r['status'] == '1')
    {
        $constat = "Offered";
    }
    else
    {
        $constat = "Rejected";
    }
    $cctable .= "
    <tr>
        <td>".ucwords($r['type'])."</td>
        <td>".$r['tname']."</td>
        <td>".$r['years']."</td>
        <td>".$r['yrs_remain']."</td>
        <td>$".number_format($r['salary'],0)."</td>
        <td>$".number_format($r['salary2'],0)."</td>
        <td>$".number_format($r['salary3'],0)."</td>
        <td>".ucwords($r['opt'])."</td>
        <td>".$constat."</td>
    </tr>";
}

// Reroll 
if($pleague == $sublg)
{
    $reroll = '';
}
else
{
    $reroll = 'disabled';
}

// Combo Box
$comboarr = Array('Affiliate PT','Biography','Graphic','Job Pay','Media Spot','Mini-PT','Network PT','Other','Podcast','Press Conference','Quote','Rookie Profile','SBAO Adjustment','Sim Attendance','Simmer/SBAO','Welfare - Inactive');
$combo = "
<td><select class='form-control input-sm' name='row[0][task]' onchange='autope(0,this.value)' required><option value=''></option>";
for($v=0;$v<count($comboarr);$v++)
{
    $combo .= "<option value=\"$comboarr[$v]\">$comboarr[$v]</option>";
}
$combo .= "</select></td>";

// Date logic
if($hostlg=='SBA'){$goodweek = date("m/d/Y", strtotime('Sunday'));}
else if($hostlg=='EFL'){$goodweek = date("m/d/Y", strtotime('Saturday'));}
$currday = date('j');
$currmonth = date('n');
$weekend = (date('N') >0) ? $currday+(7-date('N')) : $currday;
$echoweek = "Current Updates will be processed $goodweek.";

// Player Update Scale
$sql = "SELECT `range`,pe FROM update_scale WHERE league='$hostlg' ORDER BY `range` ASC";
$result = mysqli_query($con,$sql);
$addscale = "";
$subtractscale = "";
$upconcat = "";
$f = 0;
while($r = mysqli_fetch_array($result))
{
    if($f == 0)
    {
        $addscale .= "
        if(document.getElementById(current).value<".$r['range'].")
        {
            b = Number(document.getElementById(cost).innerHTML);
            document.getElementById(cost).innerHTML = b + ".$r['pe'].";
        }";
        $subtractscale .= "
        if(document.getElementById(current).value<=".$r['range'].")
        {
            b = Number(document.getElementById(cost).innerHTML);
            b = noNeg(b-".$r['pe'].");
            document.getElementById(cost).innerHTML = b;
        }";
    }
    else
    {
        $addscale .= "
        else if(document.getElementById(current).value<".$r['range'].")
        {
            b = Number(document.getElementById(cost).innerHTML);
            document.getElementById(cost).innerHTML = b + ".$r['pe'].";
        }";
        $subtractscale .= "
        else if(document.getElementById(current).value<=".$r['range'].")
        {
            b = Number(document.getElementById(cost).innerHTML);
            b = noNeg(b-".$r['pe'].");
            document.getElementById(cost).innerHTML = b;
        }";
    }
    $upconcat .= "<div class='input-group' id=\"addr".$f."\"><input type=\"number\" class=\"form-control\" value=\"".$r['range']."\"><span class=\"input-group-addon\">=</span><input type=\"number\" class=\"form-control\" value=\"".$r['pe']."\"></div>";
    $f++;
}

if($hostlg == 'EFL')
{
    // Temporary EFLO disabling
    $development = 'disabled';
    $training = 'disabled';
    $prediction = 'disabled';
    $giveaways = 'disabled';
    $carryover = 'disabled';
    $purchase = 'disabled';
    $reroll = 'disabled';
}
else
{
    $purchase = '';
    $giveaways = '';
}

if(isset($_GET['alert'])) 
{
    if ($_GET['alert'] == 'cantafford') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Your update has more spent TPE than you currently have available. Please correct this issue and re-submit.</strong></div>";
    }
    else if ($_GET['alert'] == 'success') 
    {
        $alert = "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Your update has been submitted successfully.</strong></div>";
    }
    else if ($_GET['alert'] == 'dupesuccess') 
    {
        $alert = "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Task was duplicated successfully.</strong></div>";
    }
    else if ($_GET['alert'] == 'locked') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Your update has been approved by an Updater and is awaiting being input into the sim. Please wait until this occurs before attempting to add a new Update.</strong></div>";
    }
    else if ($_GET['alert'] == 'cheater') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You have been caught cheating and attempting to circumvent the rules. Your player has been reported and an SBAO Admin will review this shortly.</strong></div>";
    }
    else if($_GET['alert'] == 'purchasesuccessful') 
    {
        $alert = "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Store Purchase Successful.</strong></div>";
    }
    else if ($_GET['alert'] == 'purchasedenied') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Store Purchase Denied.</strong></div>";
    }
    else if ($_GET['alert'] == 'somethingwentwrong') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Something went wrong. Please try again.</strong></div>";
    }
    else if($_GET['alert'] == 'carryoversuccess') 
    {
        $alert = "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Carryover has been successfully applied.</strong></div>";
    }
    else if ($_GET['alert'] == 'teamhasnocap') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>The team you are attempting to accept an offer with does not have enough Salary Cap space to sign you currently.</strong></div>";
    }
    else if($_GET['alert'] == 'contractapproved') 
    {
        $alert = "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You have successfully signed your contract.</strong></div>";
    }
    else if ($_GET['alert'] == 'contractrejected') 
    {
        $alert = "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You have successfully rejected your contract offer.</strong></div>";
    }
    else if ($_GET['alert'] == 'optiondeclined') 
    {
        $alert = "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You have successfully declined this option.</strong></div>";
    }
    else if ($_GET['alert'] == 'mutualtoteam') 
    {
        $alert = "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You have successfully accepted your portion of this Mutual Option.</strong></div>";
    }
    else if ($_GET['alert'] == 'playeroptionsuccess') 
    {
        $alert = "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You have successfully accepted your Option. You have now been paid for this contract.</strong></div>";
    }
    else if ($_GET['alert'] == 'mutualdupe') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>This option has been updated since you last refreshed the page. Please try again.</strong></div>";
    }
}
else
{$alert = '';}
?>
<!-- Body Start -->
<body onload="earnTPE();getPlayerSpend('<?=$pid?>')">
<div id="wrapper" style="position:relative;top:0;left:0;">
    <?php include $path.'/includes/nav.php'; ?>
    <div class="container">
        <?=HeaderImagePrint($hostlg,$_SESSION['style'])?>
        <?=$alert?>
        <div class="col-xs-12 pt bg-basic border mbot">
            <div class="card-header bordertr">
                <h4 class="text-center bordertr"><?=$pname?> - <?=$ptpe?> TPE</h4>
            </div>
            <div class="card-body text-center padlr">
                <div class="row">
                    <ul class="nav nav-tabs" style="border-bottom:0px;">
                        <li class="active"><a data-toggle="tab" href="#update">Update</a></li>
                        <li><a data-toggle="tab"  class="" href="#submit" onclick="getPlayerUpdate('<?=$pid?>','<?=$user?>')">Submitted</a></li>
                        <li><a data-toggle="tab" href="#history">History</a></li>
                        <li><a data-toggle="tab" href="#page">Page</a></li>
                    </ul>                
                    <div class="tab-content">
                        <div id="update" class="tab-pane fade in active col-xs-12 col-sm-9">
                            <br>
                            <div class=""><button type="button" class="btn btn-primary hidden-xs" onclick="openNav2('40%')">☰ Instructions</button><button type="button" class="btn btn-primary hidden-sm hidden-md hidden-lg" onclick="openNav2('100%')">☰ Instructions</button><button type="button" class="btn btn-primary" onclick="openNav()">☰ TPE Scale</button></div>
                            <br>
                            <form action="player_update_process.php" method="POST">
                                <div id="ajaxupdate"></div>
                                <div><input type='hidden' name ="pid" value="<?=$pid?>"></div>
                                <button id="spend_go" name="spend_submitted" type="submit" class="btn btn-primary col-xs-12 col-sm-4">Save</button>
                            </form>
                        </div>
                        <div id="submit" class="tab-pane fade col-xs-12 col-sm-9">
                            <br>
                            <div id="ajaxsubmit"></div>
                        </div>
                        <div id="history" class="tab-pane fade col-xs-12 col-sm-9">
                            <br>
                            <?=create_update_history_week($con,$pid,'')?>
                        </div>
                        <div id="page" class="tab-pane fade col-xs-12 col-sm-9">
                            <br>
                            <?=createPlayerPage($con,$pid,'0')?>
                        </div>
                        <!--<div class="col-sm-1"></div>-->
                        <div class="col-xs-12 col-sm-3">
                            <br>
                            <strong class="text-center text-uppercase col-xs-12 puheader">Update</strong>
                            <button class="btn btn-primary col-xs-12 mb pubutton" id="submit_update" onclick="getPlayerTasks('<?=$mid?>')">Submit Update</button>
                            <strong class="text-center text-uppercase col-xs-12 puheader">Claim</strong>
                            <button class="btn btn-primary col-xs-12 mb4th pubutton" id="development" <?=$development?>>Development</button>
                            <button class="btn btn-primary col-xs-12 mb4th pubutton" id="training" <?=$training?>>Training/Workout</button>
                            <button class="btn btn-primary col-xs-12 mb4th pubutton" id="prediction" onclick="getPrediction('<?=$pid?>')" <?=$prediction?>>Predictions</button>
                            <button class="btn btn-primary col-xs-12 mb4th pubutton" onclick="getGiveaway('<?=$pid?>')" <?=$giveaways?>>Giveaways</button>
                            <button class="btn btn-primary col-xs-12 mb pubutton" id="carryover" <?=$carryover?>>Carryover</button>
                            <h4 class="text-center text-uppercase col-xs-12 puheader">Store</h4>
                            <button class="btn btn-primary col-xs-12 mb pubutton" id="purchase" <?=$purchase?>>Purchase</button>
                            <h4 class="text-center text-uppercase col-xs-12 puheader">Manage</h4>
                            <button class="btn btn-primary col-xs-12 mb4th pubutton" id="viewoffers">View Offers</button>
                            <button class="btn btn-primary col-xs-12 mb4th pubutton" id="viewoptions">View Options</button>
                            <button class="btn btn-primary col-xs-12 mb4th pubutton" id="viewcontracts">View Contracts</button>
                            <a href='/forms/player_changes.php?pid=<?=$pid?>'><button class="btn btn-primary col-xs-12 mb4th pubutton" id="reroll" <?=$reroll?>>Reroll</button></a>
                            <button class="btn btn-primary col-xs-12 mb pubutton" id="retire">Retire</button>  
                        </div>
                    </div>  
                </div>
            </div>
        </div>
    </div>
</div>
<div id="mySidepanel" class="sidepanel">
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">×</a>
    <h4>TPE Scale</h4>
    <?=$upconcat?>
</div>
<div id="mySidepanel2" class="sidepanel">
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav2()">×</a>
    <h4>Instructions</h4>
    <div class='input-group col-xs-12'><span class='input-group-addon' style='width:120px;text-align:left;'>Task Name</span><input type='text' class='form-control' readonly='readonly' value='Type of Update Task completed.'></div>
    <div class='input-group col-xs-12'><span class='input-group-addon' style='width:120px;text-align:left;'>Week Ending</span><input type='text' class='form-control' readonly='readonly' value='Date of the last day(Sunday) in the Update Week.'></div>
    <div class='input-group col-xs-12'><span class='input-group-addon' style='width:120px;text-align:left;'>Link</span><input type='text' class='form-control' readonly='readonly' value='Link to your Update Task.'></div>
    <div class='input-group col-xs-12'><span class='input-group-addon' style='width:120px;text-align:left;'>PE</span><input type='text' class='form-control' readonly='readonly' value='Number of Points earned from the Task.'></div>
</div>
<div class="modal fade" id="#myModal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Draft Declaration</h4>
            </div>
            <div class="modal-body">
                <p>With this submitted update, you will go over 199 TPE. This will declare you for the upcoming draft. If you do NOT want to enter the draft, please remain at 199 TPE or lower. If you wish to declare for the draft, please proceed.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="#myModal2" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Overspending TPE</h4>
            </div>
            <div class="modal-body">
                <p>With this submitted update, you are attempting to spend more TPE than you have earned and banked. Please fix the update accordingly before you proceed, or this update will not go through.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="#submit_update" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="player_update_process.php" method="POST">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Submit Update</h4>
                </div>
                <div class="modal-body">
                    <div id="autotasks" class="text-c"></div><br>
                    <div id="updatealert"></div>
                    <div class="panel panel-primary">
                        <div class="panel-heading">Earn Points</div>
                        <div class="panel-body">
                            <div class="row clearfix" id="earn_div">
                                <div class="col-md-12 column">
                                    <table class="table table-bordered table-hover" id="tab_logic">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Task Name</th>
                                                <th class="text-center">Week Ending</th>
                                                <th class="text-center">Link</th>
                                                <th class="text-center">PE</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr id='addr0'>
                                                <?=$combo?>
                                                <td><div class="input-group date" data-provide="datepicker" name="week_picker"><input type="text" class="form-control input-sm" name='row[0][week]' id='row[0][week]' value=''><div class="input-group-addon"><i class="fas fa-calendar-alt"></i></div></div></td>
                                                <td><input type="url" name='row[0][link]' id='row[0][link]' class="form-control input-sm" value=''></td>
                                                <td><input type="number" name='row[0][pe]' id='row[0][pe]' class="form-control input-sm" value='' onchange='earnTPE()'></td>
                                                <td></td>
                                            </tr>
                                            <tr id='addr1'></tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <input type="hidden" id="numrows" value='1'>
                            <div class="input-group">
                                <span class="input-group-addon">Update Notes</span>
                                <textarea class="form-control" name="notes" rows='2' placeholder="Put any additional information that your Updater should be aware of here."></textarea>
                            </div>
                            <br>
                            <a id="add_row" class="btn btn-primary">Add Task</a><a id='delete_row' class="btn btn-primary" onmouseout='earnTPE()'>Remove Task</a>
                            <br><br>
                            <div class='input-group col-xs-12 col-sm-4'><span class='input-group-addon' style='width:120px;text-align:left;'>Earned TPE</span><input id='earned_tpe' type='text' class='form-control' name='earned_tpe' readonly='readonly' value='0'></div>
                            <div><input type='hidden' id='weektpe' value='<?=$goodweek?>'></div>
                            <div><input type='hidden' name ="pid" value="<?=$pid?>"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-default" name="submitted" id="go">Save & Close</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close Without Saving</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="#developmentmodal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Claim Development</h4>
            </div>
            <div class="modal-body">
                <div id="devalert"></div>
                <p>Clicking Claim will claim your Development for the current week, and automatically insert it into your banked tpe.</p>
            </div>
            <div class="modal-footer">
                <button type='button' class='btn btn-primary' onclick="javascript:player_development('<?=$pid?>')">Claim Development</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="#trainingmodal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Claim Training/Workout</h4>
            </div>
            <div class="modal-body">
                <p>Clicking Claim will claim your Workout/Training for the current season, and automatically insert it into your banked tpe.</p>
            </div>
            <div class="modal-footer">
                <button type='button' class='btn btn-primary' onclick="javascript:player_training('<?=$pid?>')">Claim Training</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="#predictionmodal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Claim Predictions</h4>
            </div>
            <div class="modal-body">
                <div id="ajaxprediction"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="#giveawaymodal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Claim Giveaways</h4>
            </div>
            <div class="modal-body">
                <div id="ajaxgiveaway"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="#carryovermodal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Claim Carryover</h4>
            </div>
            <div class="modal-body">
                <p>Clicking Claim will claim your carryover in the amount of <?=$cpe?> TPE, and automatically insert it into your banked tpe. This process is not reversible.</p>
            </div>
            <div class="modal-footer">
                <button type='button' class='btn btn-primary' onclick="javascript:player_carryover('<?=$pid?>','<?=$cid?>')">Claim Carryover</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="#purchasemodal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Store Purchase</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <?=$store?>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class='modal fade' id='#viewoffers' role='dialog'>
    <div class='modal-dialog modal-lg'>
        <div class='modal-content'>
            <form action='contract_offer.php' method='post'>
                <div class='modal-header'>
                    <button type='button' class='close' data-dismiss='modal'>&times;</button>
                    <h4 class='modal-title'>View Contracts</h4>
                </div>
                <div class='modal-body'>
                    <div class='table-responsive'>
                        <table class="table">
                            <thead>
                                <th>Type</th>
                                <th>Team</th>
                                <th>Years</th>
                                <th>Year 1 Salary</th>
                                <th>Year 2 Salary</th>
                                <th>Year 3 Salary</th>
                                <th>Option</th>
                                <th></th>
                            </thead>
                            <tbody>
                                <?=$cotable?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class='modal-footer'>
                    <button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class='modal fade' id='#viewoptions' role='dialog'>
    <div class='modal-dialog modal-lg'>
        <div class='modal-content'>
            <form action='contract_offer.php' method='post'>
                <div class='modal-header'>
                    <button type='button' class='close' data-dismiss='modal'>&times;</button>
                    <h4 class='modal-title'>View Options</h4>
                </div>
                <div class='modal-body'>
                    <div class='table-responsive'>
                        <table class="table">
                            <thead>
                                <th>Team</th>
                                <th>Salary</th>
                                <th>Option</th>
                                <th></th>
                            </thead>
                            <tbody>
                                <?=$coptable?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class='modal-footer'>
                    <button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class='modal fade' id='#viewcontracts' role='dialog'>
    <div class='modal-dialog modal-lg'>
        <div class='modal-content'>
            <form action='contract_offer.php' method='post'>
                <div class='modal-header'>
                    <button type='button' class='close' data-dismiss='modal'>&times;</button>
                    <h4 class='modal-title'>View Contracts</h4>
                </div>
                <div class='modal-body'>
                    <div class='table-responsive'>
                        <table class="table">
                            <thead>
                                <th>Type</th>
                                <th>Team</th>
                                <th>Years</th>
                                <th>Years Left</th>
                                <th>Year 1 Salary</th>
                                <th>Year 2 Salary</th>
                                <th>Year 3 Salary</th>
                                <th>Option</th>
                                <th>Status</th>
                            </thead>
                            <tbody>
                                <?=$cctable?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class='modal-footer'>
                    <button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class='modal fade' id='#retiremodal' role='dialog'>
    <div class='modal-dialog modal-lg'>
        <div class='modal-content'>
            <div class='modal-header'>
                <button type='button' class='close' data-dismiss='modal'>&times;</button>
                <h4 class='modal-title'>Retire Player</h4>
            </div>
            <div class='modal-body'>
                <p>This will permanently retire this player at the end of the current season. You should only do this if you are 100% sure you will not want this player to play another season. Once this player is retired, you will have no control over them. Are you sure you want to do this?</p>
            </div>
            <div class='modal-footer'>
                <a href='/forms/retire_player.php?id=<?=$pid?>'><button type='button' class='btn btn-primary'>Retire Player (Immediately)</button></a>
                <a href='/forms/retire_player.php?id=<?=$pid?>&marked=1'><button type='button' class='btn btn-primary'>Retire Player (End of Season)</button></a>
                <button type='button' class='btn btn-danger' data-dismiss='modal'>Close</button>
            </div>
        </div>
    </div>
</div>
<!-- Body End -->

<?php
include $path.'/footer.php';
?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker3.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
<script>
var hostlg = '<?=$hostlg?>';
$(document).ready(function(){
    var i = $('#numrows').val();
    i = Number(i);
    $("#add_row").click(function(){
        var i = $('#numrows').val();
        i = Number(i);
        $('#addr'+i).html("<td><select class='form-control input-sm' name='row["+i+"][task]' onchange='autope("+i+",this.value)' required><option value=''></option><option value='Affiliate PT'>Affiliate PT</option><option value='Biography'>Biography</option><option value='Graphic'>Graphic</option><option value='Job Pay'>Job Pay</option><option value='Media Spot'>Media Spot</option><option value='Mini-PT'>Mini-PT</option><option value='Network PT'>Network PT</option><option value='Other'>Other</option><option value='Podcast'>Podcast</option><option value='Press Conference'>Press Conference</option><option value='Quote'>Quote</option><option value='Rookie Profile'>Rookie Profile</option><option value='SBAO Adjustment'>SBAO Adjustment</option><option value='Sim Attendance'>Sim Attendance</option><option value='Simmer/SBAO'>Simmer/SBAO</option><option value='Status Update'>Status Update</option><option value='Welfare - Inactive'>Welfare - Inactive</option></select></td><td><div class='input-group date' data-provide='datepicker' name='week_picker'><input type='text' class='form-control input-sm' name='row["+i+"][week]' id='row["+i+"][week]' value='' required><div class='input-group-addon'><i class='fas fa-calendar-alt'></i></div></div></td><td><input  name='row["+i+"][link]' id='row["+i+"][link]' type='url' class='form-control input-sm'></td><td><input  name='row["+i+"][pe]' id='row["+i+"][pe]' type='number' class='form-control input-sm' onchange='earnTPE()' required></td><td></td>");
        $('#tab_logic').append('<tr id="addr'+(i+1)+'"></tr>');
        i++; 
        document.getElementById("numrows").value = Number(document.getElementById("numrows").value) + 1;
        $("div[name^='week_picker']").datepicker({
        daysOfWeekDisabled: "<?=$daysOfWeekDisabled?>",
        daysOfWeekHighlighted: "<?=$daysOfWeekHighlighted?>",
        todayHighlight: true,
        autoclose: true,
        clearBtn: true
    });
    });
    $("#delete_row").click(function(){
        var i = $('#numrows').val();
        i = Number(i);
        if(i>1){
        $("#addr"+(i-1)).html('');
        i--;
        document.getElementById("numrows").value = Number(document.getElementById("numrows").value) - 1;
    }
    });
    $('[data-toggle="popover"]').popover();
    $("div[name^='week_picker']").datepicker({
        daysOfWeekDisabled: "<?=$daysOfWeekDisabled?>",
        daysOfWeekHighlighted: "<?=$daysOfWeekHighlighted?>",
        todayHighlight: true,
        autoclose: true,
        clearBtn: true
    });
    var popup = '0';
    var popup2 = '0';
    var btpe = $('#ptpe').val();
    $('#go').mouseover(function() {
        earnTPE();
        var ttpe = $('#total_tpe').val();
        var ctpe = $('#cost_tpe').val();
        ctpe = parseInt(ctpe);
        var stpe = $('#spend_tpe').val();
        var rows = $('#numrows').val();
        rows = parseInt(rows);
        if (ttpe > 199 && btpe < 200 && popup == 0) 
        {
            $('#\\#myModal').modal('show');
            popup = 1;
        }
        if(ctpe > stpe)
        {
            $('#\\#myModal2').modal('show');
        }

    });
    $('#submit_update').click(function() {
        $('#\\#submit_update').modal('show');
    });
    $('#development').click(function() {
        var ptpe = $('#ptpe').val();
        ptpe = parseInt(ptpe);
        var btpe = ptpe + 2;
        if(btpe > 199 && ptpe < 200)
        {
            $('#devalert').html("<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>By claiming this Development, you will go over 199 TPE. This will declare you for the upcoming draft. If you do NOT want to enter the draft, please remain at 199 TPE or lower. If you wish to declare for the draft, please proceed.<br><br>Note: This declaration is <i>irreversible</i>.</strong></div>");
        }
        $('#\\#developmentmodal').modal('show');
    });
    $('#training').click(function() {
        $('#\\#trainingmodal').modal('show');
    });
    $('#carryover').click(function() {
        $('#\\#carryovermodal').modal('show');
    });
    $('#purchase').click(function() {
        $('#\\#purchasemodal').modal('show');
    });
    $('#viewoffers').click(function() {
        $('#\\#viewoffers').modal('show');
    });
    $('#viewoptions').click(function() {
        $('#\\#viewoptions').modal('show');
    });
    $('#viewcontracts').click(function() {
        $('#\\#viewcontracts').modal('show');
    });
    $('#retire').click(function() {
        $('#\\#retiremodal').modal('show');
    });
    $("body").mouseover(function(){
        earnTPE();
    });
    $("#earn_div").focusout(function(){
        earnTPE();
    });
});
function getPlayerTasks(mid)
{
    $.ajax({url: "../ajax/get_player_tasks.php?mid="+mid, success: function(result){
      $("#autotasks").html(result);
    }});
}
function getPlayerSpend(pid)
{
    $.ajax({url: "../ajax/get_player_spend.php?pid="+pid, success: function(result){
      $("#ajaxupdate").html(result);
    }});
}
function getPlayerUpdate(pid,user)
{
    $.ajax({url: "../ajax/get_player_update.php?pid="+pid+"&user="+user+"&type=desktop", success: function(result){
      $("#ajaxsubmit").html(result);
    }});
}
function player_development(pid)
{
    theurl = "player_development.php?pid="+pid;
    window.location.href = theurl;
}
function player_training(pid,season)
{
    theurl = "player_training.php?pid="+pid;
    window.location.href = theurl;
}
function getPrediction(pid)
{
    $.ajax({url: "../ajax/get_prediction.php?pid="+pid, success: function(result){
      $("#ajaxprediction").html(result);
    }});
    $('#\\#predictionmodal').modal('show');
}
function getGiveaway(pid)
{
    $.ajax({url: "../ajax/get_giveaway.php?pid="+pid, success: function(result){
      $("#ajaxgiveaway").html(result);
    }});
    $('#\\#giveawaymodal').modal('show');
}
function player_carryover(pid,cid)
{
    theurl = "player_carryover.php?pid="+pid+"&cid="+cid;
    window.location.href = theurl;
}
</script>
<script>
function noNeg(num) {
if(num < 0)
{
num = 0;
num = Number(num);
return num;
}
else
num = Number(num);
return num;
}

function decimize(num)
{
    a = String(num);
    b = a.split(".")[1];
    b = Number("."+b);
    return b;
}
function calc()
{
    var total = 0;
    $("p[name^='cost_']").each(function(){
        var cost = $(this).html();
        total += Number(cost);
    });
    $('#cost_tpe').val(total);
}
/*function calc()
{
    document.getElementById('cost_tpe').value = Number(document.getElementById('cost_ins').innerHTML) + Number(document.getElementById('cost_jps').innerHTML) + Number(document.getElementById('cost_ft').innerHTML) + Number(document.getElementById('cost_3ps').innerHTML) + Number(document.getElementById('cost_han').innerHTML) + Number(document.getElementById('cost_pas').innerHTML) + Number(document.getElementById('cost_orb').innerHTML) + Number(document.getElementById('cost_drb').innerHTML) + Number(document.getElementById('cost_psd').innerHTML) + Number(document.getElementById('cost_prd').innerHTML) + Number(document.getElementById('cost_stl').innerHTML) + Number(document.getElementById('cost_blk').innerHTML) + Number(document.getElementById('cost_fl').innerHTML) + Number(document.getElementById('cost_qkn').innerHTML) + Number(document.getElementById('cost_str').innerHTML) + Number(document.getElementById('cost_jmp').innerHTML);
}*/
function earnTPE()
{
    i = document.getElementById("numrows").value;
    document.getElementById('earned_tpe').value = 0;
    for(x=0;x<i;x++)
    {
        document.getElementById('earned_tpe').value = Number(document.getElementById('earned_tpe').value) + Number(document.getElementById('row['+x+'][pe]').value);
    } 
    var ptpe = $('#ptpe').val();
    ptpe = parseInt(ptpe);
    var etpe = $('#earned_tpe').val();
    etpe = parseInt(etpe);
    btpe = ptpe+etpe;
    if(btpe > 199 && ptpe < 200)
    {
        $('#updatealert').html("<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>With this submitted update, you will go over 199 TPE. This will declare you for the upcoming draft. If you do NOT want to enter the draft, please remain at 199 TPE or lower. If you wish to declare for the draft, please proceed.<br><br>Note: This declaration is <i>irreversible</i>.</strong></div>");
    }
    else
    {
        $('#updatealert').empty();
    }
}
function subtract(current,increase,cost)
{
    a = Number(document.getElementById(current).value);
    a = noNeg(a-1);
    if(a < document.getElementById(current).min)
    {
        return;
    }
    c = Number(document.getElementById(increase).innerHTML);
    c = noNeg(c-1);
    document.getElementById(increase).innerHTML = c;
    <?=$subtractscale?>
    document.getElementById(current).value = a;
}

function add(current,increase,cost)
{
    if(document.getElementById(current).value >= document.getElementById(current).max)
    {
        return;
    }   
    <?=$addscale?>
    else
    {
        return;
    }
    a = Number(document.getElementById(current).value);
    document.getElementById(current).value = a + 1;
    c = Number(document.getElementById(increase).innerHTML);
    document.getElementById(increase).innerHTML = c + 1;
}
function addIncrement(current,increase,cost,increment)
{
    for(x=0;x<increment;x++)
    {
        add(current,increase,cost);

    }
}
function subtractIncrement(current,increase,cost,increment)
{
    for(x=0;x<increment;x++)
    {
        subtract(current,increase,cost);

    }
}
function calcCost(current,increase,cost)
{
    a = document.getElementById(increase).innerHTML;
    document.getElementById(increase).innerHTML = 0;
    document.getElementById(cost).innerHTML = 0;
    document.getElementById(current).value = document.getElementById(current).min;
    addIncrement(current,increase,cost,a);
}
function calcManualCost(current,increase,cost)
{
    if(document.getElementById(current).value <= document.getElementById(current).min)
    {
        document.getElementById(current).value = document.getElementById(current).min;
        document.getElementById(increase).innerHTML = 0;
        document.getElementById(cost).innerHTML = 0;
        return;
    }
    a = document.getElementById(current).value - document.getElementById(current).min;
    document.getElementById(increase).innerHTML = 0;
    document.getElementById(cost).innerHTML = 0;
    document.getElementById(current).value = document.getElementById(current).min;
    addIncrement(current,increase,cost,a);
}
function openNav() 
{
  document.getElementById("mySidepanel").style.width = "250px";
}

function closeNav() 
{
  document.getElementById("mySidepanel").style.width = "0";
}
function openNav2(screen) 
{
  document.getElementById("mySidepanel2").style.width = screen;
}

function closeNav2() 
{
  document.getElementById("mySidepanel2").style.width = "0";
}
function autope(row,val)
{
    if(hostlg == 'SBA')
    {
        url = "https://sba.today/forums/index.php?/topic/18975-official-sba-league-hierarchy/";
    }
    else if(hostlg == 'EFL')
    {
        url = "https://efl.network/forums/index.php?/topic/21-league-payroll/";
    }
    y = 0;
    switch(val)
    {
        case 'Biography':
            //
            pe = 10;
            document.getElementById('row['+row+'][link]').disabled = false;
            break;
        case 'Graphic':
            //
            pe = 6;
            document.getElementById('row['+row+'][link]').disabled = false;
            break;
        case 'Job Pay':
            //
            pe = 3;
            document.getElementById('row['+row+'][link]').value = url;
            break;
        case 'Simmer/SBAO':
            //
            pe = 12;
            document.getElementById('row['+row+'][link]').value = url;
            break;
        case 'Media Spot':
            //
            pe = 6;
            document.getElementById('row['+row+'][link]').disabled = false;
            break;
        case 'Mini-PT':
            //
            pe = 3;
            document.getElementById('row['+row+'][link]').disabled = false;
            break;
        case 'Other':
            //
            pe = 0;
            document.getElementById('row['+row+'][link]').disabled = false;
            break;
        case 'Predictions':
            //
            pe = 0;
            document.getElementById('row['+row+'][link]').disabled = false;
            break;
        case 'Press Conference':
            //
            pe = 1;
            document.getElementById('row['+row+'][link]').disabled = false;
            break;
        case 'Quote':
            //
            pe = 3;
            document.getElementById('row['+row+'][link]').disabled = false;
            break;
        case 'Rookie Profile':
            //
            pe = 8;
            document.getElementById('row['+row+'][link]').disabled = false;
            break;
        case 'Sim Attendance':
            //
            pe = 1;
            document.getElementById('row['+row+'][link]').disabled = false;
            break;
        case 'Status Update':
            //
            pe = 1;
            document.getElementById('row['+row+'][link]').disabled = false;
            break;
        case 'Welfare - Affiliate':
            //
            pe = 6;
            document.getElementById('row['+row+'][link]').disabled = false;
            break;
        case 'Welfare - Inactive':
            //
            pe = 2;
            document.getElementById('row['+row+'][link]').disabled = false;
            break;
        default:
            //
            pe = 0;
            document.getElementById('row['+row+'][link]').value = "";
            document.getElementById('row['+row+'][week]').value = "";
            document.getElementById('row['+row+'][link]').disabled = false;
            break;
    }
    document.getElementById('row['+row+'][pe]').value = Number(pe);
    if(pe != 0 || y == 1)
    {
        document.getElementById('row['+row+'][week]').value = document.getElementById('weektpe').value;
        earnTPE();
    } 
}
</script>
<style>
.sidepanel  {
  width: 0;
  position: fixed;
  z-index: 10;
  top: 0;
  left: 0;
  overflow-x: hidden;
  transition: 0.5s;
  padding-top: 30px;
}

.sidepanel a {
  padding: 8px 8px 8px 32px;
  text-decoration: none;
  font-size: 25px;
  display: block;
  transition: 0.3s;
}

.sidepanel a:hover {
}

.sidepanel .closebtn {
  position: absolute;
  top: 0;
  right: 25px;
  font-size: 36px;
}


</style>