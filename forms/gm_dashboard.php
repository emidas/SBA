<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user']))
{
header("location:/index.php");
}
if(($_SESSION['user'] != 'emidas'))
{
header("location:/home.php?alert=adminpermission");
}
if(($_SESSION['isadmin'] != 1))
{
header("location:/home.php?alert=adminpermission");
}
// get user
$user=$_SESSION['user'];
// declare variables
$alert = '';
$cotable = '';
$totable = '';
$ectable = '';
$rptable = '';
$players = '';
$myplayers = '';
$salary = '';
$tid = '';
//get tid
$sql = "SELECT t.id
FROM teams t
INNER JOIN coaches c ON t.id=c.t_fk
WHERE (c.name=\"$user\" || c.name2=\"$user\") AND t.league='SBA'";
$numsult = mysqli_query($con,$sql);
$num = mysqli_num_rows($numsult);
if($num == 0)
{
header("location:/home.php?alert=adminpermission");
exit();
}
else
{
    $tarr=mysqli_fetch_array(mysqli_query($con,$sql));
    $tid=$tarr['id'];
}
// get year
$system = new System();
$year = $system->get_year();
$nextyr = $year + 1;
// create player dropdown
$sql = "SELECT id,CONCAT(plast,', ',pfirst) as pname
FROM players
WHERE (active > 0) AND team = '0' AND league='SBA' ORDER BY pname ASC";
$result = mysqli_query($con,$sql);
while($r = mysqli_fetch_array($result))
{
    $players .= "<option value = '".$r['id']."'>".$r['pname']."</option>";
}
$sql = "SELECT p.id,CONCAT(plast,', ',pfirst) as pname
FROM players p
WHERE p.team='$tid' and yrs_remain = '1'";
$result = mysqli_query($con,$sql);
while($r = mysqli_fetch_array($result))
{
    $myplayers .= "<option value = '".$r['id']."'>".$r['pname']."</option>";
}
// create salary dropdown
for($v=2;$v<15;$v++)
{
    $opt = 500000 * $v;
    $salary .= "<option value='".$opt."'>$".number_format($opt,0)."</option>";
}
// contract offers
$sql = "SELECT t.name,CONCAT(plast,', ',pfirst) as pname,c.type,c.years,c.salary,c.salary2,c.salary3,c.opt,c.status
FROM players p
INNER JOIN contracts c ON p.id=c.p_fk
INNER JOIN teams t ON c.t_fk=t.id
WHERE c.t_fk = '$tid'";
$result = mysqli_query($con,$sql);
while($r = mysqli_fetch_array($result))
{
    if($r['status'] == 1)
    {
        $status = 'Offered';
    }
    else if($r['status'] == 2)
    {
        $status = 'Accepted';
    }
    else
    {
        $status = 'Rejected';
    }
    $cotable .= "
    <tr>
        <td>".$r['pname']."</td>
        <td>".ucwords($r['type'])."</td>
        <td>".$r['years']."</td>
        <td>$".number_format($r['salary'],0)."</td>
        <td>$".number_format($r['salary2'],0)."</td>
        <td>$".number_format($r['salary3'],0)."</td>
        <td>".ucwords($r['opt'])."</td>
        <td>".$status."</td>
    </tr>";
}
$cotable .= "
<tr>
    <td colspan='8'><a href='#newcontract' data-toggle='modal'><button type='submit' class='btn btn-primary'>Offer New Contract</button></a></td>
</tr>";
// trade offers
$sql = '';
$result = '';
    $totable .= "
    <tr>
        <td>Milwaukee</td>
        <td>Simmons,Troy<br>Dawkins,Jack</td>
        <td>Hermann,Walter<br>S39 1st Round(MIL)<br>S39 2nd Round(MIL)<br>S40 1st Round(MIL)</td>
        <td>Purple Son Swap?</td>
        <td><a href='#' class='btn btn-primary'>Accept</a><a href='#' class='btn btn-danger'>Reject</a></td>
    </tr>
    <tr>
        <td>Houston</td>
        <td>Simmons,Troy<br>Dawkins,Jack</td>
        <td>Columbus, Adrian<br>Rosenthal, Abe</td>
        <td>You up?</td>
        <td>Waiting For Response</td>
    </tr>";
    $totable .= "
    <tr>
        <td colspan='5'><a href='' class='btn btn-primary'>Offer New Trade</a></td>
    </tr>";
// expiring contracts
$sql = "SELECT p.id,CONCAT(plast,', ',pfirst) as pname,p.salary
FROM players p
WHERE p.team='$tid' and yrs_remain = '1'";
$result = mysqli_query($con,$sql);
while($r = mysqli_fetch_array($result))
{
    $ectable .= "
    <tr>
        <td>".$r['pname']."</td>
        <td>$".number_format($r['salary'],0)."</td>
    </tr>";

}
$ectable .= "
<tr>
    <td colspan='2'><a href='#newextension' data-toggle='modal'><button type='submit' class='btn btn-primary'>Offer Extension</button></a></td>
</tr>";
// retiring players
$sql = '';
$result = '';
    $rptable .= "
    <tr>
        <td>Blair, Emidas</td>
        <td>$3,500,000</td>
        <td>10</td>
    </tr>";
// alerts
if(isset($_GET['alert'])) 
{
    if($_GET['alert'] == 'success') 
    {
        $alert = "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Login Successful.</strong></div>";
    }
    else if($_GET['alert'] == 'trainingsuccess') 
    {
        $alert = "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Player Training has been successfully applied.</strong></div>";
    }
    else if($_GET['alert'] == 'cosuccess') 
    {
        $alert = "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Contract has been successfully offered.</strong></div>";
    }
    else if($_GET['alert'] == 'offerexists') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>An offer already exists for this player.</strong></div>";
    }
}
?>
<body>
<div id="wrapper" style="position:relative;top:0;left:0;">
    <?php include $path.'/includes/nav.php'; ?>
    <div class="container">
        <?=HeaderImagePrint($hostlg,$_SESSION['style'])?>
        <?=$alert?>
        <div class="panel panel-primary">
            <div class="panel-heading">GM Dashboard</div>
            <div class="panel-body">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h4 class='panel-title'><a data-toggle='collapse' href='#collapseco'>Contract Offers</a></h4></div>
                    <div class='panel-collapse collapse' id='collapseco'>
                        <div class="panel-body">
                            <div class='table-responsive'>
                                <table class="table">
                                    <thead>
                                        <th>Name</th>
                                        <th>Type</th>
                                        <th>Years</th>
                                        <th>Year 1 Salary</th>
                                        <th>Year 2 Salary</th>
                                        <th>Year 3 Salary</th>
                                        <th>Option</th>
                                        <th>Status</th>
                                    </thead>
                                    <tbody>
                                        <?=$cotable?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-primary">
                    <div class="panel-heading"><h4 class='panel-title'><a data-toggle='collapse' href='#collapseto'>Trade Offers</a></h4></div>
                    <div class='panel-collapse collapse' id='collapseto'>
                        <div class="panel-body">
                            <div class='table-responsive'>
                                <table class="table">
                                    <thead>
                                        <th>Team</th>
                                        <th>Trading</th>
                                        <th>Receiving</th>
                                        <th>Message</th>
                                        <th></th>
                                    </thead>
                                    <tbody>
                                        <?=$totable?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-primary">
                    <div class="panel-heading"><h4 class='panel-title'><a data-toggle='collapse' href='#collapseec'>Expiring Contracts</a></h4></div>
                    <div class='panel-collapse collapse' id='collapseec'>
                        <div class="panel-body">
                            <div class='table-responsive'>
                                <table class="table">
                                    <thead>
                                        <th>Name</th>
                                        <th>Salary</th>
                                    </thead>
                                    <tbody>
                                        <?=$ectable?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-primary">
                    <div class="panel-heading"><h4 class='panel-title'><a data-toggle='collapse' href='#collapserp'>Retiring Players</a></h4></div>
                    <div class='panel-collapse collapse' id='collapserp'>
                        <div class="panel-body">
                            <div class='table-responsive'>
                                <table class="table">
                                    <thead>
                                        <th>Name</th>
                                        <th>Salary</th>
                                        <th>Experience</th>
                                    </thead>
                                    <tbody>
                                        <?=$rptable?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class='modal fade' id='newcontract' role='dialog'>
                    <div class='modal-dialog modal-lg'>
                        <div class='modal-content'>
                            <form action='contract_offer.php' method='post'>
                                <div class='modal-header'>
                                    <button type='button' class='close' data-dismiss='modal'>&times;</button>
                                    <h4 class='modal-title'>Contract Offer</h4>
                                </div>
                                <div class='modal-body'>
                                    <div class='form-group'>
                                        <div class='input-group'><span class='input-group-addon' style='width:120px;text-align:left;'>Player</span><select class='form-control' name='player'><?=$players?></select></div>
                                    </div>
                                    <div class='form-group'>
                                        <div class='input-group'><span class='input-group-addon' style='width:120px;text-align:left;'>Years</span><select class='form-control' name='numyrs' id='numyrs'><option>1</option><option>2</option><option>3</option></select></div>
                                    </div>
                                    <div class='form-group'>
                                        <div class='input-group'><span class='input-group-addon' style='width:120px;text-align:left;'>Year 1 Salary:</span><select class='form-control' name='salary' id='salary'><?=$salary?></select></div>
                                    </div>
                                    <div class='form-group' id='sal2'>
                                        <div class='input-group'><span class='input-group-addon' style='width:120px;text-align:left;'>Year 2 Salary:</span><select class='form-control' name='salary2' id='salary2'><?=$salary?></select></div>
                                    </div>
                                    <div class='form-group' id='sal3'>
                                        <div class='input-group'><span class='input-group-addon' style='width:120px;text-align:left;'>Year 3 Salary:</span><select class='form-control' name='salary3' id='salary3'><?=$salary?></select></div>
                                    </div>
                                    <div class='form-group'>
                                        <div class='input-group'><span class='input-group-addon' style='width:120px;text-align:left;'>Option:</span><select class='form-control' name='option'><option value='none'>None</option><option value='player'>Player</option><option value='team'>Team</option><option value='mutual'>Mutual</option></select></div>
                                    </div>
                                    <input type='text' name='tid' value='<?=$tid?>' hidden>
                                    <input type='text' name='type' value='free agent' hidden>
                                </div>
                                <div class='modal-footer'>
                                    <button type='submit' class='btn btn-primary'>Submit</button>
                                    <button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class='modal fade' id='newextension' role='dialog'>
                    <div class='modal-dialog modal-lg'>
                        <div class='modal-content'>
                            <form action='contract_offer.php' method='post'>
                                <div class='modal-header'>
                                    <button type='button' class='close' data-dismiss='modal'>&times;</button>
                                    <h4 class='modal-title'>Extension Offer</h4>
                                </div>
                                <div class='modal-body'>
                                    <div class='form-group'>
                                        <div class='input-group'><span class='input-group-addon' style='width:120px;text-align:left;'>Player</span><select class='form-control' name='player'><?=$myplayers?></select></div>
                                    </div>
                                    <div class='form-group'>
                                        <div class='input-group'><span class='input-group-addon' style='width:120px;text-align:left;'>Years</span><select class='form-control' name='numyrs' id='numyrs2'><option>1</option><option>2</option><option>3</option></select></div>
                                    </div>
                                    <div class='form-group'>
                                        <div class='input-group'><span class='input-group-addon' style='width:120px;text-align:left;'>Year 1 Salary:</span><select class='form-control' name='salary' id='salaryext'><?=$salary?></select></div>
                                    </div>
                                    <div class='form-group' id='sal2ext'>
                                        <div class='input-group'><span class='input-group-addon' style='width:120px;text-align:left;'>Year 2 Salary:</span><select class='form-control' name='salary2' id='salary2ext'><?=$salary?></select></div>
                                    </div>
                                    <div class='form-group' id='sal3ext'>
                                        <div class='input-group'><span class='input-group-addon' style='width:120px;text-align:left;'>Year 3 Salary:</span><select class='form-control' name='salary3' id='salary3ext'><?=$salary?></select></div>
                                    </div>
                                    <div class='form-group'>
                                        <div class='input-group'><span class='input-group-addon' style='width:120px;text-align:left;'>Option:</span><select class='form-control' name='option'><option value='none'>None</option><option value='player'>Player</option><option value='team'>Team</option><option value='mutual'>Mutual</option></select></div>
                                    </div>
                                    <input type='text' name='tid' value='<?=$tid?>' hidden>
                                    <input type='text' name='type' value='extension' hidden>
                                </div>
                                <div class='modal-footer'>
                                    <button type='submit' class='btn btn-primary'>Submit</button>
                                    <button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Body End -->
<script>
$(document).ready(function(){
    $('#sal2').hide();
    $('#sal3').hide();
    $('#salary2').val(0);
    $('#salary3').val(0);
    $('#sal2ext').hide();
    $('#sal3ext').hide();
    $('#salary2ext').val(0);
    $('#salary3ext').val(0);
    $('#numyrs').change(function() 
    {
        var yrs = $('#numyrs').val();
        if(yrs == 3)
        {
            $('#sal2').show();
            $('#salary2').val(1000000);
            $('#sal3').show();
            $('#salary3').val(1000000);
        }
        else if(yrs == 2)
        {
            $('#sal2').show();
            $('#salary2').val(1000000);
            $('#sal3').hide();
            $('#salary3').val(0);
        }
        else
        {
            $('#sal2').hide();
            $('#salary2').val(0);
            $('#sal3').hide();
            $('#salary3').val(0);
        }
    });
    $('#numyrs2').change(function() 
    {
        var yrs = $('#numyrs2').val();
        if(yrs == 3)
        {
            $('#sal2ext').show();
            $('#salary2ext').val(1000000);
            $('#sal3ext').show();
            $('#salary3ext').val(1000000);
        }
        else if(yrs == 2)
        {
            $('#sal2ext').show();
            $('#salary2ext').val(1000000);
            $('#sal3ext').hide();
            $('#salary3ext').val(0);
        }
        else
        {
            $('#sal2ext').hide();
            $('#salary2ext').val(0);
            $('#sal3ext').hide();
            $('#salary3ext').val(0);
        }
    });
});
</script>

<?php
include $path.'/footer.php';
?>