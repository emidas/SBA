<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
if(isset($_SESSION['user']))
{$user = $_SESSION['user'];} else {}

// SBA Caps
$sbacaps = '';
$sql = "SELECT stat, SUM(count) as count, CONCAT(ROUND((SUM(count)/(SELECT COUNT(id) FROM players WHERE league='SBA' AND (active='1' || active='3')) * 100),2),'%') as per
FROM (SELECT c90 as stat,COUNT(id) as count
FROM players
WHERE league='SBA'
AND (active = '1' || active = '3')
GROUP BY stat
UNION ALL
SELECT c70 as stat, COUNT(id) as count
FROM players
WHERE league='SBA'
AND (active = '1' || active = '3')
GROUP BY stat
UNION ALL
SELECT c801 as stat, COUNT(id) as count
FROM players
WHERE league='SBA'
AND (active = '1' || active = '3')
GROUP BY stat
UNION ALL
SELECT c802 as stat, COUNT(id) as count
FROM players
WHERE league='SBA'
AND (active = '1' || active = '3')
GROUP BY stat
UNION ALL
SELECT c803 as stat, COUNT(id) as count
FROM players
WHERE league='SBA'
AND (active = '1' || active = '3')
GROUP BY stat
UNION ALL
SELECT c804 as stat, COUNT(id) as count
FROM players
WHERE league='SBA'
AND (active = '1' || active = '3')
GROUP BY stat) as query
WHERE stat != ''
GROUP BY stat
ORDER BY count desc";
$result = mysqli_query($con,$sql);
while($r = mysqli_fetch_array($result))
{
    $sbacaps .= "<tr>";
        $sbacaps .= "<td>".strtoupper($r['stat'])."</td>";
        $sbacaps .= "<td>".$r['count']."</td>";
        $sbacaps .= "<td>".$r['per']."</td>";
    $sbacaps .= "</tr>";
}

// SBA 70 Caps
$sba70caps = '';
$sql = "SELECT c70 as stat, COUNT(id) as count, CONCAT(ROUND((COUNT(id)/(SELECT COUNT(id) FROM players WHERE league='SBA' AND (active='1' || active='3')) * 100),2),'%') as per
FROM players
WHERE league='SBA'
AND (active = '1' || active = '3')
AND c70 != ''
GROUP BY stat
ORDER BY count desc";
$result = mysqli_query($con,$sql);
while($r = mysqli_fetch_array($result))
{
    $sba70caps .= "<tr>";
        $sba70caps .= "<td>".strtoupper($r['stat'])."</td>";
        $sba70caps .= "<td>".$r['count']."</td>";
        $sba70caps .= "<td>".$r['per']."</td>";
    $sba70caps .= "</tr>";
}

// SBA 90 Caps
$sba90caps = '';
$sql = "SELECT c90 as stat, COUNT(id) as count, CONCAT(ROUND((COUNT(id)/(SELECT COUNT(id) FROM players WHERE league='SBA' AND (active='1' || active='3')) * 100),2),'%') as per
FROM players
WHERE league='SBA'
AND (active = '1' || active = '3')
AND c90 != ''
GROUP BY stat
ORDER BY count desc";
$result = mysqli_query($con,$sql);
while($r = mysqli_fetch_array($result))
{
    $sba90caps .= "<tr>";
        $sba90caps .= "<td>".strtoupper($r['stat'])."</td>";
        $sba90caps .= "<td>".$r['count']."</td>";
        $sba90caps .= "<td>".$r['per']."</td>";
    $sba90caps .= "</tr>";
}

// SBA 80 Caps
$sba80caps = '';
$sql = "SELECT stat, SUM(count) as count, CONCAT(ROUND((SUM(count)/(SELECT COUNT(id) FROM players WHERE league='SBA' AND (active='1' || active='3')) * 100),2),'%') as per
FROM (SELECT c801 as stat, COUNT(id) as count
FROM players
WHERE league='SBA'
AND (active = '1' || active = '3')
GROUP BY stat
UNION ALL
SELECT c802 as stat, COUNT(id) as count
FROM players
WHERE league='SBA'
AND (active = '1' || active = '3')
GROUP BY stat
UNION ALL
SELECT c803 as stat, COUNT(id) as count
FROM players
WHERE league='SBA'
AND (active = '1' || active = '3')
GROUP BY stat
UNION ALL
SELECT c804 as stat, COUNT(id) as count
FROM players
WHERE league='SBA'
AND (active = '1' || active = '3')
GROUP BY stat) as query
WHERE stat != ''
GROUP BY stat
ORDER BY count desc";
$result = mysqli_query($con,$sql);
while($r = mysqli_fetch_array($result))
{
    $sba80caps .= "<tr>";
        $sba80caps .= "<td>".strtoupper($r['stat'])."</td>";
        $sba80caps .= "<td>".$r['count']."</td>";
        $sba80caps .= "<td>".$r['per']."</td>";
    $sba80caps .= "</tr>";
}

// SBA Builds
$sbabuilds = '';
$sql = "SELECT build as stat,COUNT(id) as count, CONCAT(ROUND((COUNT(id)/(SELECT COUNT(id) FROM players WHERE league='SBA' AND (active='1' || active='3')) * 100),2),'%') as per
FROM players
WHERE league='SBA'
AND (active = '1' || active = '3')
GROUP BY stat
ORDER BY count desc";
$result = mysqli_query($con,$sql);
while($r = mysqli_fetch_array($result))
{
    $sbabuilds .= "<tr>";
        $sbabuilds .= "<td>".$r['stat']."</td>";
        $sbabuilds .= "<td>".$r['count']."</td>";
        $sbabuilds .= "<td>".$r['per']."</td>";
    $sbabuilds .= "</tr>";
}

// NCAA Caps
$ncaacaps = '';
$sql = "SELECT stat, SUM(count) as count, CONCAT(ROUND((SUM(count)/(SELECT COUNT(id) FROM players WHERE league='SBDL' AND (active='1' || active='3')) * 100),2),'%') as per
FROM (SELECT c90 as stat,COUNT(id) as count
FROM players
WHERE league='SBDL'
AND (active = '1' || active = '3')
GROUP BY stat
UNION ALL
SELECT c70 as stat, COUNT(id) as count
FROM players
WHERE league='SBDL'
AND (active = '1' || active = '3')
GROUP BY stat
UNION ALL
SELECT c801 as stat, COUNT(id) as count
FROM players
WHERE league='SBDL'
AND (active = '1' || active = '3')
GROUP BY stat
UNION ALL
SELECT c802 as stat, COUNT(id) as count
FROM players
WHERE league='SBDL'
AND (active = '1' || active = '3')
GROUP BY stat
UNION ALL
SELECT c803 as stat, COUNT(id) as count
FROM players
WHERE league='SBDL'
AND (active = '1' || active = '3')
GROUP BY stat
UNION ALL
SELECT c804 as stat, COUNT(id) as count
FROM players
WHERE league='SBDL'
AND (active = '1' || active = '3')
GROUP BY stat) as query
WHERE stat != ''
GROUP BY stat
ORDER BY count desc";
$result = mysqli_query($con,$sql);
while($r = mysqli_fetch_array($result))
{
    $ncaacaps .= "<tr>";
        $ncaacaps .= "<td>".strtoupper($r['stat'])."</td>";
        $ncaacaps .= "<td>".$r['count']."</td>";
        $ncaacaps .= "<td>".$r['per']."</td>";
    $ncaacaps .= "</tr>";
}

// NCAA 70 Caps
$ncaa70caps = '';
$sql = "SELECT c70 as stat, COUNT(id) as count, CONCAT(ROUND((COUNT(id)/(SELECT COUNT(id) FROM players WHERE league='SBDL' AND (active='1' || active='3')) * 100),2),'%') as per
FROM players
WHERE league='SBDL'
AND (active = '1' || active = '3')
AND c70 != ''
GROUP BY stat
ORDER BY count desc";
$result = mysqli_query($con,$sql);
while($r = mysqli_fetch_array($result))
{
    $ncaa70caps .= "<tr>";
        $ncaa70caps .= "<td>".strtoupper($r['stat'])."</td>";
        $ncaa70caps .= "<td>".$r['count']."</td>";
        $ncaa70caps .= "<td>".$r['per']."</td>";
    $ncaa70caps .= "</tr>";
}

// NCAA 90 Caps
$ncaa90caps = '';
$sql = "SELECT c90 as stat, COUNT(id) as count, CONCAT(ROUND((COUNT(id)/(SELECT COUNT(id) FROM players WHERE league='SBDL' AND (active='1' || active='3')) * 100),2),'%') as per
FROM players
WHERE league='SBDL'
AND (active = '1' || active = '3')
AND c90 != ''
GROUP BY stat
ORDER BY count desc";
$result = mysqli_query($con,$sql);
while($r = mysqli_fetch_array($result))
{
    $ncaa90caps .= "<tr>";
        $ncaa90caps .= "<td>".strtoupper($r['stat'])."</td>";
        $ncaa90caps .= "<td>".$r['count']."</td>";
        $ncaa90caps .= "<td>".$r['per']."</td>";
    $ncaa90caps .= "</tr>";
}

// NCAA 80 Caps
$ncaa80caps = '';
$sql = "SELECT stat, SUM(count) as count, CONCAT(ROUND((SUM(count)/(SELECT COUNT(id) FROM players WHERE league='SBDL' AND (active='1' || active='3')) * 100),2),'%') as per
FROM (SELECT c801 as stat, COUNT(id) as count
FROM players
WHERE league='SBDL'
AND (active = '1' || active = '3')
GROUP BY stat
UNION ALL
SELECT c802 as stat, COUNT(id) as count
FROM players
WHERE league='SBDL'
AND (active = '1' || active = '3')
GROUP BY stat
UNION ALL
SELECT c803 as stat, COUNT(id) as count
FROM players
WHERE league='SBDL'
AND (active = '1' || active = '3')
GROUP BY stat
UNION ALL
SELECT c804 as stat, COUNT(id) as count
FROM players
WHERE league='SBDL'
AND (active = '1' || active = '3')
GROUP BY stat) as query
WHERE stat != ''
GROUP BY stat
ORDER BY count desc";
$result = mysqli_query($con,$sql);
while($r = mysqli_fetch_array($result))
{
    $ncaa80caps .= "<tr>";
        $ncaa80caps .= "<td>".strtoupper($r['stat'])."</td>";
        $ncaa80caps .= "<td>".$r['count']."</td>";
        $ncaa80caps .= "<td>".$r['per']."</td>";
    $ncaa80caps .= "</tr>";
}

// NCAA Builds
$ncaabuilds = '';
$sql = "SELECT build as stat,COUNT(id) as count, CONCAT(ROUND((COUNT(id)/(SELECT COUNT(id) FROM players WHERE league='SBDL' AND (active='1' || active='3')) * 100),2),'%') as per
FROM players
WHERE league='SBDL'
AND (active = '1' || active = '3')
GROUP BY stat
ORDER BY count desc";
$result = mysqli_query($con,$sql);
while($r = mysqli_fetch_array($result))
{
    $ncaabuilds .= "<tr>";
        $ncaabuilds .= "<td>".$r['stat']."</td>";
        $ncaabuilds .= "<td>".$r['count']."</td>";
        $ncaabuilds .= "<td>".$r['per']."</td>";
    $ncaabuilds .= "</tr>";
}


if(isset($_GET['alert'])) 
{
    if($_GET['alert'] == 'success') 
    {
        $alert = "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Login Successful.</strong></div>";
    }
    else if ($_GET['alert'] == 'adminpermission') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You are not an Admin.</strong></div>";
    }
    else if ($_GET['alert'] == 'simmerpermission') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You are not a Simmer.</strong></div>";
    }
    else if ($_GET['alert'] == 'gmpermission') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You are not a General Manager.</strong></div>";
    }
    else if ($_GET['alert'] == 'adpermission') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You are not an Athletic Director.</strong></div>";
    }
    else if ($_GET['alert'] == 'betapermission') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>No Peeking.</strong></div>";
    }
    else if ($_GET['alert'] == 'playerexists') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You already have an SBDL player.</strong></div>";
    }
}
else
{$alert = '';}
?>

<!-- Body Start -->
<body>
<div id="wrapper" style="position:relative;top:0;left:0;">
    <?php include $path.'/includes/nav.php'; ?>
    <div class="container">
        <?=HeaderImagePrint($hostlg,$_SESSION['style'])?>
        <?=$alert?>
        <div class='row'>
            <div class="panel panel-primary">
                <div class="panel-heading">Player Build Statistics</div>
                <div class="panel-body">
                    <div class="row no-gutter">
                        <div class='col-xs-12 col-sm-4'>
                            <div class="table-responsive">
                                <table class="table compact">
                                    <thead>
                                        <tr>
                                            <th colspan='3' class='active'>SBA Builds</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?=$sbabuilds?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class='col-xs-12 col-sm-4'>
                            <div class="table-responsive">
                                <table class="table compact">
                                    <thead>
                                        <tr>
                                            <th colspan='3' class='active'>SBA Caps</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?=$sbacaps?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class='col-sm-4'>
                            
                        </div>
                    </div>
                    <div class="row no-gutter">
                        <div class='col-xs-12 col-sm-4'>
                            <div class="table-responsive">
                                <table class="table compact">
                                    <thead>
                                        <tr>
                                            <th colspan='3' class='active'>SBA 90 Caps</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?=$sba90caps?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class='col-xs-12 col-sm-4'>
                            <div class="table-responsive">
                                <table class="table compact">
                                    <thead>
                                        <tr>
                                            <th colspan='3' class='active'>SBA 80 Caps</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?=$sba80caps?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class='col-xs-12 col-sm-4'>
                            <div class="table-responsive">
                                <table class="table compact">
                                    <thead>
                                        <tr>
                                            <th colspan='3' class='active'>SBA 70 Caps</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?=$sba70caps?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row no-gutter">
                        <div class='col-xs-12 col-sm-4'>
                            <div class="table-responsive">
                                <table class="table compact">
                                    <thead>
                                        <tr>
                                            <th colspan='3' class='active'>SBDL Builds</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?=$ncaabuilds?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class='col-xs-12 col-sm-4'>
                            <div class="table-responsive">
                                <table class="table compact">
                                    <thead>
                                        <tr>
                                            <th colspan='3' class='active'>SBDL Caps</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?=$ncaacaps?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class='col-sm-4'>

                        </div>
                    </div>
                    <div class="row no-gutter">
                        <div class='col-xs-12 col-sm-4'>
                            <div class="table-responsive">
                                <table class="table compact">
                                    <thead>
                                        <tr>
                                            <th colspan='3' class='active'>SBDL 90 Caps</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?=$ncaa90caps?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class='col-xs-12 col-sm-4'>
                            <div class="table-responsive">
                                <table class="table compact">
                                    <thead>
                                        <tr>
                                            <th colspan='3' class='active'>SBDL 80 Caps</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?=$ncaa80caps?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class='col-xs-12 col-sm-4'>
                            <div class="table-responsive">
                                <table class="table compact">
                                    <thead>
                                        <tr>
                                            <th colspan='3' class='active'>SBDL 70 Caps</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?=$ncaa70caps?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Body End -->

<?php
include $path.'/footer.php';
mysqli_close($con);
?>
<script>
    $('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>