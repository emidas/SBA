<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
$pid = '';
$numyrs = '';
$team = '';
$tid = '';
if(isset($_POST['pid']) && isset($_POST['numyrs']) && isset($_POST['tid']))
{
    $pid = sanitize($con,$_POST['pid']);
    $tid = sanitize($con,$_POST['tid']);
    $numyrs = sanitize($con,$_POST['numyrs']);
    $team = sanitize($con,$_POST['team']);
    $league = sanitize($con,$_POST['league']);
    $active = sanitize($con,$_POST['active']);
    if($active != '')
    {
        $active = "&active=1";
    }
    $sql = "UPDATE players SET team = '$team', yrs_remain = '$numyrs' WHERE id = '$pid'";
    mysqli_query($con,$sql);
    if($tid == 0)
    {
        $clause = "tid=".$tid."&league=".$league;
        header("location:/forms/team_page.php?".$clause);
        exit();
    }
    else
    {
        header("location:/forms/team_page.php?tid=".$tid);
        exit();
    }
}
if(isset($_POST['pid']) && isset($_POST['numyrs']))
{
    $pid = sanitize($con,$_POST['pid']);
    $numyrs = sanitize($con,$_POST['numyrs']);
    $team = sanitize($con,$_POST['team']);
    $league = sanitize($con,$_POST['league']);
    $active = sanitize($con,$_POST['active']);
    if($active != '')
    {
        $active = "&active=1";
    }
    $sql = "UPDATE players SET team = '$team', yrs_remain = '$numyrs' WHERE id = '$pid'";
    mysqli_query($con,$sql);
    header("location:/forms/manage_players.php?league=".$league."".$active);
    exit();
}
else if(isset($_POST['pid']) && isset($_POST['sbacoach']))
{
    $pid = sanitize($con,$_POST['pid']);
    $tid = sanitize($con,$_POST['team']);
    $sql = "UPDATE players SET team = '$tid' WHERE id = '$pid'";
    mysqli_query($con,$sql);
    $sql = "UPDATE contracts SET t_fk='$tid' WHERE p_fk='$pid' AND status='3'";
    mysqli_query($con,$sql);
    header("location:/forms/coach_update.php?league=sba&alert=movesuccess");
    exit();
}
else if(isset($_POST['pid']))
{
    $pid = sanitize($con,$_POST['pid']);
    $team = sanitize($con,$_POST['team']);
    $league = sanitize($con,$_POST['league']);
    $active = sanitize($con,$_POST['active']);
    if($active != '')
    {
        $active = "&active=1";
    }
    $sql = "UPDATE players SET team = '$team' WHERE id = '$pid'";
    mysqli_query($con,$sql);
    header("location:/forms/manage_players.php?league=".$league."".$active);
    exit();
}
else
{
    header("location:javascript://history.go(-1)");
}
?>
