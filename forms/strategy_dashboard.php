<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
if($_SESSION['issimmer'] != '1'){
header("location:/home.php?alert=simmerpermission");
}
$user=$_SESSION['user'];
// year
$system = new System();
$year = $system->get_year();

$query = "SELECT cu.id as cid,c.id as cidfk,c.name as cname,c.league,t.name as tname,cuu.pnamec,cuu.posc,cuu.minc,cuu.pnamepf,cuu.pospf,cuu.minpf,cuu.pnamesf,cuu.possf,cuu.minsf,cuu.pnamesg,cuu.possg,cuu.minsg,cuu.pnamepg,cuu.pospg,cuu.minpg,cuu.pname6,cuu.pos6,cuu.min6,cuu.pname7,cuu.pos7,cuu.min7,cuu.pname8,cuu.pos8,cuu.min8,cuu.pname9,cuu.pos9,cuu.min9,cuu.pname10,cuu.pos10,cuu.min10,cuu.pname11,cuu.pos11,cuu.min11,cuu.pname12,cuu.pos12,cuu.min12,cuu.key1,cuu.key2,cuu.key3,cuu.pace,cuu.motion,cuu.tpuse,cuu.focus,cuu.crasho,cuu.setprimary,cuu.primaryd,cuu.setsecondary,cuu.secondd,cuu.fullcourt,cuu.crashd FROM coaches c INNER JOIN coach_updates cu ON c.id=cu.cid_fk INNER JOIN coach_updates_update cuu ON cu.id=cuu.cuid_fk INNER JOIN teams t ON c.id=t.id WHERE cu.status = '1'";
$result = mysqli_query($con,$query);
$sba = 0;
$ncaa = 0;
$echo = '';
$echo2 = '';
$i = 0;
while ($r = mysqli_fetch_array($result))
{
    $cidfk = $r['cidfk'];
    $concat = '';
    if ($r['focus'] == '0'){$focus = 'Inside';}
    else if ($r['focus'] == '1'){$focus = 'Balanced';}
    else {$focus = 'Outside';}
    $posarr = Array('c','pf','sf','sg','pg','6','7','8','9','10','11','12');
    $poslabelarr = Array('Center','Power Forward','Small Forward','Shooting Guard','Point Guard','6th','7th','8th','9th','10th','11th','12th');
    $sql = "SELECT * FROM coaches WHERE id = '$cidfk'";
    $result2 = mysqli_query($con,$sql);
    $w = mysqli_fetch_array($result2);
    $concat .= "
    <div class='panel panel-primary'>
        <div class='panel-heading'><h4 class='panel-title'><a data-toggle='collapse' href='#collapse$i'>".$r['cname']."(".$r['tname'].")</a></h4></div>
        <div class='panel-collapse collapse' id='collapse$i'>
            <div class='panel-body'>
                <ul class='nav nav-tabs' style='border-bottom:0px;'>
                    <li class='active'><a data-toggle='tab' href='#changes$i'>Changes</a></li>
                    <li><a data-toggle='tab' href='#all$i'>All</a></li>
                </ul>
                <div class='tab-content'>
                    <div id='changes$i' class='tab-pane fade in active'>
                        <table class='table'>
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Name</th>
                                    <th>Position(s)</th>
                                    <th>Minutes</th>
                                </tr>
                            </thead>
                            <tbody>";
    for($d=0;$d<count($posarr);$d++)
    {
        $pname = "pname".$posarr[$d];
        $pos = "pos".$posarr[$d];
        $min = "min".$posarr[$d];
        if(($r[$pname] != $w[$pname]) || ($r[$pos] != $w[$pos]) || ($r[$min] != $w[$min]))
        {
            $concat .="
            <tr>
                <td>$poslabelarr[$d]:</td>";
            if($r[$pname] != $w[$pname])
            {
                $concat .= "<td><span style='color:red;'>".$r[$pname]."</span></td>";
            }
            else
            {
                $concat .= "<td>".$r[$pname]."</td>";
            }
            if($r[$pos] != $w[$pos])
            {
                $concat .= "<td><span style='color:red;'>".$r[$pos]."</span></td>";
            }
            else
            {
                $concat .= "<td>".$r[$pos]."</td>";
            }
            if($r[$min] != $w[$min])
            {
                $concat .= "<td><span style='color:red;'>".$r[$min]."</span></td>";
            }
            else
            {
                $concat .= "<td>".$r[$min]."</td>";
            }
            $concat .= "</tr>";
        }
        else
        {

        }
    }
    $concat .= "</tbody></table>";
    $concat .= "<table class='table'>
        <thead>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>";
    if($r['key1'] != $w['key1'] || $r['key2'] != $w['key2'] || $r['key3'] != $w['key3'])
    {
        $concat .="
            <tr>
                <td>Options:</td>
                <td>".$r['key1']."</td>
                <td>".$r['key2']."</td>
                <td>".$r['key3']."</td>
            </tr>";
    }
    $stratarr = Array('pace','motion','tpuse','focus','crasho','setprimary','primaryd','setsecondary','secondd','fullcourt','crashd');
    $stratlabelarr = Array('Pace','Motion','3P Usage','Focus','Crash Offensive Boards','Set Primary Defense','Primary Defense Usage','Set Secondary Defense','Secondary Defense Usage','Full Court Press','Crash Defensive Boards');
    for($d=0;$d<count($stratarr);$d++)
    {
        if($r[$stratarr[$d]] != $w[$stratarr[$d]])
        {
            $concat .="
            <tr>
                <td>$stratlabelarr[$d]:</td>
                <td>".$r[$stratarr[$d]]."</td>
                <td></td>
                <td></td>
            </tr>";
        }
        else
        {

        }
    }
    $concat .= "</tbody>
        </table>
        <div class='form-group'>
            <div class='col-sm-offset-2 col-sm-10'>
                <a href='#approve$i' data-toggle='modal'><span title='' data-toggle='popover' data-trigger='hover' data-content='Approve' data-placement='top'><button type='submit' class='btn btn-primary'>Approve</button></a>
            </div>
        </div>
        <div class='modal fade' id='approve$i' role='dialog'>
            <div class='modal-dialog modal-lg'>
                <form method='POST' action='approve_strategy.php'>
                    <div class='modal-content'>
                        <div class='modal-header'>
                            <button type='button' class='close' data-dismiss='modal'>&times;</button>
                            <h4 class='modal-title'>Approve Update</h4>
                        </div>
                        <div class='modal-body'>
                            <p>This will clear this GM's Strategy from the system and save it as their new default in the system. You can use the box below to send a PM to the GM letting them know you have updated their strategies. Are you sure you want to do this?</p>
                            <textarea class='form-control' rows='5' name='approvepm'></textarea>
                            <input type='text' hidden='hidden' name='id' value='".$r['cid']."'>
                        </div>
                        <div class='modal-footer'>
                            <button type='submit' class='btn btn-danger'>Approve Update</button>
                            <button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id='all$i' class='tab-pane fade'>
        <table class='table'>
            <thead>
                <tr>
                    <th></th>
                    <th>Name</th>
                    <th>Position(s)</th>
                    <th>Minutes</th>
                </tr>
            </thead>
            <tbody>";
            for($d=0;$d<count($posarr);$d++)
            {
                $pname = "pname".$posarr[$d];
                $pos = "pos".$posarr[$d];
                $min = "min".$posarr[$d];
                $concat .="
                    <tr>
                        <td>$poslabelarr[$d]:</td>";
                    if($r[$pname] != $w[$pname])
                    {
                        $concat .= "<td><span style='color:red;'>".$r[$pname]."</span></td>";
                    }
                    else
                    {
                        $concat .= "<td>".$r[$pname]."</td>";
                    }
                    if($r[$pos] != $w[$pos])
                    {
                        $concat .= "<td><span style='color:red;'>".$r[$pos]."</span></td>";
                    }
                    else
                    {
                        $concat .= "<td>".$r[$pos]."</td>";
                    }
                    if($r[$min] != $w[$min])
                    {
                        $concat .= "<td><span style='color:red;'>".$r[$min]."</span></td>";
                    }
                    else
                    {
                        $concat .= "<td>".$r[$min]."</td>";
                    }
                    $concat .= "</tr>";
            }
            $concat .= "</tbody></table>";
            $concat .= "<table class='table'>
                            <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>";
                            $concat .="
                                <tr>
                                    <td>Options:</td>
                                    <td>".$r['key1']."</td>
                                    <td>".$r['key2']."</td>
                                    <td>".$r['key3']."</td>
                                </tr>";    
                        $stratarr = Array('pace','motion','tpuse','focus','crasho','setprimary','primaryd','setsecondary','secondd','fullcourt','crashd');
                        $stratlabelarr = Array('Pace','Motion','3P Usage','Focus','Crash Offensive Boards','Set Primary Defense','Primary Defense Usage','Set Secondary Defense','Secondary Defense Usage','Full Court Press','Crash Defensive Boards');
                        for($d=0;$d<count($stratarr);$d++)
                        {
                            $concat .="
                            <tr>
                                <td>$stratlabelarr[$d]:</td>
                                <td>".$r[$stratarr[$d]]."</td>
                                <td></td>
                                <td></td>
                            </tr>";
                        }
                    $concat .= "
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>";
    $i++;
    if ($r['league'] == 'SBA')
    {
        $echo .= $concat;
        $sba++;
    }
    else
    {
        $echo2 .= $concat;
        $ncaa++;
    }
}
$echo = '<div class="panel panel-default">
            <div class="panel-heading"><h4>SBA ('.$sba.' New)</h4></div>
            <div class="panel-body">' . $echo;
$echo2 = '<div class="panel panel-default">
            <div class="panel-heading"><h4>NCAA ('.$ncaa.' New)</h4></div>
            <div class="panel-body">' . $echo2;
$echo .= "</div></div>";
$echo2 .= "</div></div>";
?>

<!-- Body Start -->
<body>
<div id="wrapper" style="position:relative;top:0;left:0;">
    <?php include $path.'/includes/nav.php'; ?>
    <div class="container">
        <?=HeaderImagePrint($hostlg,$_SESSION['style'])?>
        <div class="panel-group">
            <div class="panel panel-primary">
                <div class="panel-heading">Strategy Dashboard</div>
                <div class="panel-body">
                    <?=$echo?>
                    <?=$echo2?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Body End -->

<?php
include $path.'/footer.php';
?>