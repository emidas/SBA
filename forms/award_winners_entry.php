<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
$user=$_SESSION['user'];

$yr = "SELECT year from year WHERE sba_season != '0' ORDER BY year ASC";
$result = mysqli_query($con,$yr);
$year = '';
$team = '';
$echo = '';
while ($s = mysqli_fetch_array($result))
{
    $year .= "<option value = \"".$s['year']."\" $echo>".$s['year']."</option>";
}
$query = "SELECT id,name FROM teams WHERE league='SBA' ORDER BY name ASC";
$result = mysqli_query($con,$query);
while ($s = mysqli_fetch_array($result))
{
    $team .= "<option value = \"".$s['id']."\" $echo>".$s['name']."</option>";
}
$query = "SELECT id,CONCAT(plast,', ',pfirst) as name FROM players where league = 'SBA' ORDER BY name ASC";
$result = mysqli_query($con,$query);
$mvp = '';
$pmvp = '';
$opoty = '';
$dpoty = '';
$smoty = '';
$pgoty = '';
$sgoty = '';
$sfoty = '';
$pfoty = '';
$coty = '';
$roty = '';
$mip = '';
$echo = '';
while ($r = mysqli_fetch_array($result)) 
{
    $mvp .= "<option value = \"".$r['id']."\" $echo>".$r['name']."</option>";
    $pmvp .= "<option value = \"".$r['id']."\" $echo>".$r['name']."</option>";
    $opoty .= "<option value = \"".$r['id']."\" $echo>".$r['name']."</option>";
    $dpoty .= "<option value = \"".$r['id']."\" $echo>".$r['name']."</option>";
    $smoty .= "<option value = \"".$r['id']."\" $echo>".$r['name']."</option>";
    $pgoty .= "<option value = \"".$r['id']."\" $echo>".$r['name']."</option>";
    $sgoty .= "<option value = \"".$r['id']."\" $echo>".$r['name']."</option>";
    $sfoty .= "<option value = \"".$r['id']."\" $echo>".$r['name']."</option>";
    $pfoty .= "<option value = \"".$r['id']."\" $echo>".$r['name']."</option>";
    $coty .= "<option value = \"".$r['id']."\" $echo>".$r['name']."</option>";
    $roty .= "<option value = \"".$r['id']."\" $echo>".$r['name']."</option>";
    $mip .= "<option value = \"".$r['id']."\" $echo>".$r['name']."</option>";
}
if(isset($_GET['alert'])) 
{
    if($_GET['alert'] == 'success') 
    {
        $echo = "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Predictions Saved.</strong></div>";
    }
    else if($_GET['alert'] == 'closed')
    {
        $echo = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Predictions Closed, Submission Not Saved.</strong></div>";
    }
    else if($_GET['alert'] == 'norecord')
    {
        $echo = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You must save your own Predictions before you can import someone else's.</strong></div>";
    }
    else 
    {
        $echo = '';
    }
}
else
{$echo = '';}
?>

<!-- Body Start -->
<body>
<div id="wrapper" style="position:relative;top:0;left:0;">
    <?php include $path.'/includes/nav.php'; ?>
    <div class="container">
        <?=HeaderImagePrint($hostlg,$_SESSION['style'])?>
        <?=$echo?>
        <div class="panel panel-primary">
            <div class="panel-heading">SBA Award Winners - Entry</div>
            <div class="panel-body">
                <div class="panel-group col-xs-12">
                    <form class="form-horizontal" action="check_award_winners.php" method="post" >
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>Season</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>Year</span>
                                <select class='form-control' name='season'>
                                    <?=$year?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>Most Valuable Player</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>MVP</span>
                                <select class='form-control' name='mvp'>
                                    <?=$mvp?>
                                </select>
                                <select class='form-control' name='mvpt'>
                                    <?=$team?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>Playoff MVP</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>P-MVP</span>
                                <select class='form-control' name='pmvp'>
                                    <?=$pmvp?>
                                </select>
                                <select class='form-control' name='pmvpt'>
                                    <?=$team?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>Offensive Player of the Year</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>OPotY</span>
                                <select class='form-control' name='opoty'>
                                    <?=$opoty?>
                                </select>
                                <select class='form-control' name='opotyt'>
                                    <?=$team?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>Defensive Player of the Year</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>DPotY</span>
                                <select class='form-control' name='dpoty'>
                                    <?=$dpoty?>
                                </select>
                                <select class='form-control' name='dpotyt'>
                                    <?=$team?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>Sixth Man of the Year</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>6MotY</span>
                                <select class='form-control' name='smoty'>
                                    <?=$smoty?>
                                </select>
                                <select class='form-control' name='smotyt'>
                                    <?=$team?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>Rookie of the Year</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>RotY</span>
                                <select class='form-control' name='roty'>
                                    <?=$roty?>
                                </select>
                                <select class='form-control' name='rotyt'>
                                    <?=$team?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>Most Improved Player</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>MIP</span>
                                <select class='form-control' name='mip'>
                                    <?=$mip?>
                                </select>
                                <select class='form-control' name='mipt'>
                                    <?=$team?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>Point Guard of the Year</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>PGotY</span>
                                <select class='form-control' name='pgoty'>
                                    <?=$pgoty?>
                                </select>
                                <select class='form-control' name='pgotyt'>
                                    <?=$team?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>Shooting Guard of the Year</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>SGotY</span>
                                <select class='form-control' name='sgoty'>
                                    <?=$sgoty?>
                                </select>
                                <select class='form-control' name='sgotyt'>>=
                                    <?=$team?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>Small Forward of the Year</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>SMotY</span>
                                <select class='form-control' name='sfoty'>
                                    <?=$sfoty?>
                                </select>
                                <select class='form-control' name='sfotyt'>
                                    <?=$team?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>Power Forward of the Year</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>pfoty</span>
                                <select class='form-control' name='pfoty'>
                                    <?=$pfoty?>
                                </select>
                                <select class='form-control' name='pfotyt'>
                                    <?=$team?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>Center of the Year</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>CotY</span>
                                <select class='form-control' name='coty'>
                                    <?=$coty?>
                                </select>
                                <select class='form-control' name='cotyt'>
                                    <?=$team?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type='text' hidden='hidden' name='league' value='sba'>
                            <button type="submit" class="btn btn-primary col-xs-12">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Body End -->

<?php
include $path.'/footer.php';
?>

