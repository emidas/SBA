<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
$user = $_SESSION['user'];
$mid = $_SESSION['memberid'];
if(isset($_POST['tid']) && isset($_POST['player']))
{
    $tid = sanitize($con,$_POST['tid']);
    $pid = sanitize($con,$_POST['player']);
    $type = sanitize($con,$_POST['type']);
    $yrs = isset($_POST['numyrs']) ? sanitize($con,$_POST['numyrs']) : '0';
    $sal = isset($_POST['salary']) ? sanitize($con,$_POST['salary']) : '0';
    $sal2 = isset($_POST['salary2']) ? sanitize($con,$_POST['salary2']) : '0';
    $sal3 = isset($_POST['salary3']) ? sanitize($con,$_POST['salary3']) : '0';
    $opt = isset($_POST['option']) ? sanitize($con,$_POST['option']) : 'none';
    $system = new System();
    $year = $system->get_year();
    $player = new Player($pid);
    $team = new Team($tid);
    if($type == 'Filler')
    {
        // Check if team has too many players
        $num = $team->get_('roster_size');
        if($num >= 12)
        {
            header("location:coach_update.php?league=sba&alert=maxroster");
            exit();
        }

        // Check if team has too many fillers
        $num = $team->get_('roster_size_filler');
        if($num >= 6)
        {
            header("location:coach_update.php?league=sba&alert=maxrosterfiller");
            exit();
        }

        // Create Filler Contract
        $sql = "INSERT INTO contracts (t_fk,p_fk,start,type,years,yrs_remain,salary,salary2,salary3,opt,status) VALUES ('$tid','$pid','$year','$type','$yrs','$yrs','$sal','$sal2','$sal3','$opt','3')";
        mysqli_query($con,$sql);

        // Update Player
        $sql = "UPDATE players SET yrs_remain='$yrs',salary='$sal',p_bank=p_bank+'$sal',team='$tid' WHERE id='$pid'";
        mysqli_query($con,$sql);

        // Create Filler Forum Post
        $pname = $player->get_('name');
        $tname = $team->get_('name');
        $body = "The ".$tname." have signed ".$pname." to a ".$yrs." year filler deal.";
        $curl_post_data = array(
            'forum' => '50',
            'title' => $tname." has signed ".$pname." to a Filler contract!",
            'author' => '2574',
            'post' => $body
        );
        createTopic($curl_post_data);

        header("location:coach_update.php?league=sba&alert=fillersigningapproved");
        exit();
    }
    else if($type == 'Bonus')
    {
        // Grab Current Salary Cap
        $system = new System();
        $salarycap = $system->get_salary_cap();

        // Check Team Salary
        $teamcap = $team->get_('team_salary');

        // Grab Current Player Salary
        $currsal = $player->get_('total_salary');

        // Generate Potential Player salary
        $potsal = $currsal + $sal;

        // Grab Current Player Experience
        $currexp = $player->get_('experience');

        // Generate Potential Team Cap Number
        $potcap = $sal + $teamcap;

        // Check if team has cap to sign
        if($potcap > $salarycap)
        {
            header("location:coach_update.php?league=sba&alert=teamhasnocap");
            exit();
        }
        // Check if Prime/Vet Player would make too much
        else if($currexp > 3 && $potsal > 7000000)
        {
            header("location:coach_update.php?league=sba&alert=playerexceedsmax");
            exit();
        }
        // Check if Rookie Player would make too much
        else if($currexp <= 3 && $potsal > 4000000)
        {
            header("location:coach_update.php?league=sba&alert=playerexceedsmax");
            exit();
        }
        else
        {
            $sql = "INSERT INTO contracts (t_fk,p_fk,start,type,years,yrs_remain,salary,salary2,salary3,opt,status) VALUES ('$tid','$pid','$year','$type','1','1','$sal','$sal2','$sal3','$opt','3')";
            mysqli_query($con,$sql);
            $sql = "UPDATE players SET salary_bonus = salary_bonus+'$sal',p_bank=p_bank+'$sal' WHERE id='$pid'";
            mysqli_query($con,$sql);
            header("location:coach_update.php?league=sba&alert=bonusgiven");
            exit();

        }
    }
    else if($type == 'Free Agent')
    {
        // Check if team has too many players
        $num = $team->get_('roster_size');
        if($num >= 12)
        {
            header("location:coach_update.php?league=sba&alert=maxroster");
            exit();
        }

        // Check if Contract meets Minimum Salary Requirements
        $sql = "SELECT min_salary FROM players WHERE id='$pid'";
        $result = mysqli_fetch_array(mysqli_query($con,$sql));
        $minsal = $result['min_salary'];
        if(($sal < $minsal) || (($sal2 > 0) && ($sal2 < $minsal)) || (($sal3 > 0) && ($sal3 < $minsal)))
        {
            header("location:coach_update.php?league=sba&alert=minsalnotmet");
            exit();
        }

        // Inactive Free Agent Signing
        $username = $player->username;
        $user = new User($username);
        $uactive = $user->active;

        if($uactive == 0)
        {
            // Check for Outstanding Offers
            $sql = "SELECT p_fk FROM contracts WHERE p_fk='$pid' AND type='$type' AND t_fk='$tid' AND status = '1'";
            $numsult = mysqli_query($con,$sql);
            $num = mysqli_num_rows($numsult);
            if($num > 0)
            {
                header("location:coach_update.php?league=sba&alert=offerexists");
                exit();
            }

            // Add Inactive Contract
            $sql = "INSERT INTO contracts (t_fk,p_fk,start,type,years,yrs_remain,salary,salary2,salary3,opt,status) VALUES ('$tid','$pid','$year','$type','1','1','$sal','0','0','none','3')";
            mysqli_query($con,$sql);

            // Update Player
            $sql = "UPDATE players SET yrs_remain='1',salary='$sal',p_bank=p_bank+'$sal',team='$tid' WHERE id='$pid'";
            mysqli_query($con,$sql);

            // Create Forum Post
            $pname = $player->get_('name');
            $tname = $team->get_('name');
            $body = "The ".$tname." have signed ".$pname." to a 1 year inactive deal.";
            $curl_post_data = array(
                'forum' => '50',
                'title' => $tname." has signed ".$pname." to an Inactive contract!",
                'author' => '2574',
                'post' => $body
            );
            createTopic($curl_post_data);

            header("location:coach_update.php?league=sba&alert=inactivesigningapproved");
            exit();
        }
    }
    else if($type == 'Extension')
    {
        $year = $year + 1;
    }

    // Check for Outstanding Offers
    $sql = "SELECT p_fk FROM contracts WHERE p_fk='$pid' AND type='$type' AND t_fk='$tid' AND status = '1'";
    $numsult = mysqli_query($con,$sql);
    $num = mysqli_num_rows($numsult);
    if($num > 0)
    {
        header("location:coach_update.php?league=sba&alert=offerexists");
        exit();
    }

    // Add Contract Offer
    $sql = "INSERT INTO contracts (t_fk,p_fk,start,type,years,yrs_remain,salary,salary2,salary3,opt) VALUES ('$tid','$pid','$year','$type','$yrs','$yrs','$sal','$sal2','$sal3','$opt')";
    mysqli_query($con,$sql);
    header("location:coach_update.php?league=sba&alert=cosuccess");
    exit();
}
else if(isset($_GET['cid']) && isset($_GET['status']) && isset($_GET['opt']) && isset($_GET['type']))
{
    // Generate Contract Information
    $cid = sanitize($con,$_GET['cid']);
    $status = sanitize($con,$_GET['status']);
    $opt = sanitize($con,$_GET['opt']);
    $type = sanitize($con,$_GET['type']);

    $sql = "SELECT t_fk, p_fk,salary,opt FROM contracts WHERE id='$cid'";
    $result = mysqli_query($con,$sql);
    while($r = mysqli_fetch_array($result))
    {
        $tid = $r['t_fk'];
        $pid = $r['p_fk'];
        $salary = $r['salary'];
        $option = $r['opt'];
    }

    // If Option is declined by any party
    if($status == 0)
    {
        // Complete Contract and Release Player
        complete_contract($cid,$pid);
        // Create Free Agent Forum Post
        $player = new Player($pid);
        $pname = $player->name;
        $body = $pname."'s Option has been declined, and is now a Free Agent.";
        $curl_post_data = array(
            'forum' => '50',
            'title' => $pname." Option Declined",
            'author' => '2574',
            'post' => $body
        );
        createTopic($curl_post_data);
        if($opt == 1)
        {
            header("location:player_update.php?pid=".$pid."&alert=optiondeclined");
            exit();
        }
        else if($opt == 2)
        {
            header("location:coach_update.php?league=sba&alert=optiondeclined");
            exit();
        }
    }
    // If Option is accepted by any party
    else if($status == 2)
    {
        // If Option is Mutual
        if($option == 'mutual')
        {
            if($option == $type)
            {
                if($opt == 1)
                {
                    $sql = "UPDATE contracts SET opt = 'team' WHERE id = '$cid'";
                    mysqli_query($con,$sql);
                    header("location:player_update.php?pid=".$pid."&alert=mutualtoteam");
                    exit();
                }
                else if($opt == 2)
                {
                    $sql = "UPDATE contracts SET opt = 'player' WHERE id = '$cid'";
                    mysqli_query($con,$sql);
                    header("location:coach_update.php?league=sba&alert=mutualtoplayer");
                    exit();
                }
            }
            else
            {
                // Someone already accepted this mutual option, reload page
                if($opt == 1)
                {
                    header("location:player_update.php?pid=".$pid."&alert=mutualdupe");
                    exit();
                }
                else if($opt == 2)
                {
                    header("location:coach_update.php?league=sba&alert=mutualdupe");
                    exit();
                }
            }
        }

        // If Option is Player
        else if($option == 'player')
        {
            if($option == $type)
            {
                if($opt == 1)
                {
                    // Alter Contract
                    $sql = "UPDATE contracts SET opt = 'none' WHERE id = '$cid'";
                    mysqli_query($con,$sql);

                    // Alter Player
                    $sql = "UPDATE players SET team='$tid',salary='$salary',yrs_remain='1' WHERE id = '$pid'";
                    mysqli_query($con,$sql);

                    // Pay Player
                    $system = new System();
                    $year = $system->get_year();
                    $player = new Player($pid);
                    $player->set_cash($salary);
                    $player->add_earn_history($salary,'S'.$year.' Salary');
                    $pname = $player->name;

                    // Make Forum Post
                    $body = $pname."'s Option has been accepted.";
                    $curl_post_data = array(
                        'forum' => '50',
                        'title' => $pname." Option Accepted",
                        'author' => '2574',
                        'post' => $body
                    );
                    createTopic($curl_post_data);

                    header("location:player_update.php?pid=".$pid."&alert=playeroptionsuccess");
                    exit();
                }
                else
                {
                    // Someone trying to mess with code, add
                    echo "opt doesn't match";
                }
            }
            else
            {
                // Someone already accepted this mutual option, reload page
                echo "option and type don't match";
            }
        }

        // If Option is Team
        else if($option == 'team')
        {
            if($option == $type)
            {
                if($opt == 2)
                {
                    // Alter Contract
                    $sql = "UPDATE contracts SET opt = 'none' WHERE id = '$cid'";
                    mysqli_query($con,$sql);

                    // Alter Player
                    $sql = "UPDATE players SET team='$tid',salary='$salary',yrs_remain='1' WHERE id = '$pid'";
                    mysqli_query($con,$sql);

                    // Pay Player
                    $system = new System();
                    $year = $system->get_year();
                    $player = new Player($pid);
                    $player->set_cash($salary);
                    $player->add_earn_history($salary,'S'.$year.' Salary');
                    $pname = $player->name;

                    // Make Forum Post
                    // Make Forum Post
                    $body = $pname."'s Option has been accepted.";
                    $curl_post_data = array(
                        'forum' => '50',
                        'title' => $pname." Option Accepted",
                        'author' => '2574',
                        'post' => $body
                    );
                    createTopic($curl_post_data);
                    header("location:coach_update.php?league=sba&alert=teamoptionsuccess");
                    exit();
                }
                else
                {
                    // Someone trying to mess with code, add
                }
            }
            else
            {
                // Someone already accepted this mutual option, reload page
            }
        }
    }

}
else if(isset($_GET['cid']) && isset($_GET['status']))
{
    // Generate Contract Information
    $cid = sanitize($con,$_GET['cid']);
    $status = sanitize($con,$_GET['status']);

    // If Player has Accepted
    if($status == 2)
    {
        $sql = "SELECT c.t_fk as tid,p_fk as pid,type,start,years,c.salary,c.salary2,c.salary3,CONCAT(p.pfirst,' ',p.plast) as pname,t.name as tname FROM contracts c INNER JOIN players p ON c.p_fk=p.id INNER JOIN teams t ON c.t_fk=t.id WHERE c.id='$cid'";
        $r = mysqli_fetch_array(mysqli_query($con,$sql));
        $pid = $r['pid'];
        $tid = $r['tid'];
        $type = $r['type'];
        $start = $r['start'];
        $yrs = $r['years'];
        $sal = $r['salary'];
        $sal2 = $r['salary2'];
        $sal3 = $r['salary3'];
        $pname = $r['pname'];
        $tname = $r['tname'];

        // Grab Current Salary Cap
        $system = new System();
        $salarycap = $system->get_salary_cap();
        $predictcap = $system->get_predicted_cap();

        // Check Team Salary
        $team = new Team($tid);
        $teamcap = $team->get_('team_salary');
        $teamcap2 = $team->team_salary2;

        if($type == 'Extension')
        {
            // Generate Potential Team Cap Numaber
            $potcap = $sal + $teamcap2;

            // Body Message
            $body = "The ".$tname." have signed ".$pname." to a ".$yrs." year extension, paying $".number_format($sal)." starting in S".$start.".";

            if($potcap > $predictcap)
            {
                header("location:player_update.php?pid=".$pid."&alert=teamhasnocap");
                exit();
            }
            else
            {
                // Update Contract
                $sql = "UPDATE contracts SET status = '2',answered=now() WHERE id='$cid'";
                mysqli_query($con,$sql);

                // Terminate All Other Offers
                $sql = "UPDATE contracts SET status = '0',answered=now() WHERE status = '1' AND p_fk = '$pid'";
                mysqli_query($con,$sql);

                // Create Free Agent Forum Post
                $curl_post_data = array(
                    'forum' => '50',
                    'title' => "The ".$tname." have signed ".$pname." to an Extension!",
                    'author' => '2574',
                    'post' => $body
                );
                createTopic($curl_post_data);

                header("location:player_update.php?pid=".$pid."&alert=contractapproved");
                exit();
            }
        }
        else if($type == 'Free Agent')
        {
            // Check if Player is worth $0
            $player = new Player($pid);
            $minsal = $player->min_salary;

            if($minsal == 0)
            {
                $salary = 0;
            }
            else
            {
                $salary = $sal;
            }

            // Generate Potential Team Cap Numaber
            $potcap = $salary + $teamcap;

            // Body Message
            $body = "The ".$tname." have signed ".$pname." to a ".$yrs." year contract, paying $".number_format($salary)." this season.";

            if($potcap > $salarycap)
            {
                header("location:player_update.php?pid=".$pid."&alert=teamhasnocap");
                exit();
            }
            else
            {
                // Update Contract
                $sql = "UPDATE contracts SET status = '3',salary='$salary',answered=now() WHERE id='$cid'";
                mysqli_query($con,$sql);

                // Terminate All Other Offers
                $sql = "UPDATE contracts SET status = '0',answered=now() WHERE status = '1' AND p_fk = '$pid'";
                mysqli_query($con,$sql);

                // Update Player
                $sql = "UPDATE players SET yrs_remain='$yrs',salary='$salary',team='$tid' WHERE id='$pid'";
                mysqli_query($con,$sql);

                // Pay Player
                $player = new Player($pid);
                $player->set_cash($sal);
                $player->add_earn_history($sal,'S'.$year.' Salary');

                // Create Free Agent Forum Post
                $curl_post_data = array(
                    'forum' => '50',
                    'title' => $tname." has signed ".$pname." to a Free Agent contract!",
                    'author' => '2574',
                    'post' => $body
                );
                createTopic($curl_post_data);

                header("location:player_update.php?pid=".$pid."&alert=contractapproved");
                exit();
            }
        }
    }
    // If Player has Rejected
    else
    {
        $sql = "SELECT t_fk as tid,p_fk as pid,years,salary FROM contracts WHERE id='$cid'";
        $r = mysqli_fetch_array(mysqli_query($con,$sql));
        $pid = $r['pid'];
        // Update Contract
        $sql = "UPDATE contracts SET status = '0',answered=now() WHERE id='$cid'";
        mysqli_query($con,$sql);

        header("location:player_update.php?pid=".$pid."&alert=contractrejected");
        exit();
    }
}
else if(isset($_GET['cid']) && isset($_GET['pid']) && isset($_GET['delete']))
{
    // Generate Contract Information
    $cid = sanitize($con,$_GET['cid']);
    $delete = sanitize($con,$_GET['delete']);
    $pid = sanitize($con,$_GET['pid']);
    if($delete == 2)
    {
        $player = new Player($pid);
        $tid = $player->team;
        $total_salary = $player->total_salary;
        $sal = $total_salary / 2;
        $salary = $player->salary;
        $yrs_remain = $player->yrs_remain;
        if($yrs_remain == 2)
        {
            $sal2 = $salary / 2;
            $sal3 = 0;
        }
        else if($yrs_remain == 3)
        {
            $sal2 = $salary / 2;
            $sal3 = $sal2;
        }
        else
        {
            $sal2 = 0;
            $sal3 = 0;
        } 
        $system = new System();
        $year = $system->get_year();
        // Complete Contract
        $sql = "UPDATE contracts SET status = '4' WHERE id='$cid'";
        mysqli_query($con,$sql);

        // Create Waived Contract
        $sql = "INSERT INTO contracts (t_fk,p_fk,start,type,years,yrs_remain,salary,salary2,salary3,opt,status) VALUES ('$tid','$pid','$year','Waived','$yrs_remain','$yrs_remain','$sal','$sal2','$sal3','none','3')";
        mysqli_query($con,$sql);

        // Make Player Free Agent
        $sql = "UPDATE players SET team = '0', yrs_remain = '0' WHERE id = '$pid'";
        mysqli_query($con,$sql);

        header("location:coach_update.php?league=sba&alert=contractdeleted");
        exit();
    }
    else if($delete == 1)
    {
        // Delete Contract Offer
        $sql = "DELETE FROM contracts WHERE id='$cid'";
        mysqli_query($con,$sql);

        // Make Player Free Agent(Filler)
        $sql = "UPDATE players SET team = '0', yrs_remain = '0' WHERE id = '$pid'";
        mysqli_query($con,$sql);

        header("location:coach_update.php?league=sba&alert=contractdeleted");
        exit();
    }
}
else if(isset($_GET['cid']) && isset($_GET['delete']))
{
    // Generate Contract Information
    $cid = sanitize($con,$_GET['cid']);
    $delete = sanitize($con,$_GET['delete']);
    if($delete == 1)
    {
        // Delete Contract Offer
        $sql = "DELETE FROM contracts WHERE id='$cid'";
        mysqli_query($con,$sql);

        header("location:coach_update.php?league=sba&alert=contractdeleted");
        exit();
    }

}
else
{
    header("location:javascript://history.go(-1)");
    exit();
}
?>
