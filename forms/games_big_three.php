<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
$user = new User($_SESSION['user']);
$username = $user->username;
$userid = $user->id;
$player1 = '';
$player2 = '';
$player3 = '';

// year
$system = new System();
$year = $system->get_year();

// Begin Import Predictions from Another User
$query = "SELECT p.a_fk,a.username FROM auth_user a INNER JOIN games_big_3 p ON a.ID=p.a_fk where year = '$year' ORDER BY username ASC";
$result = mysqli_query($con,$query);
$users = '';
while ($q = mysqli_fetch_array($result)) 
{
    $users .= "<option value = '".$q['a_fk']."'>".$q['username']."</option>";
}
if(isset($_POST['import']))
{
    $sql = "SELECT isactive FROM forms WHERE form = 'big_3'";
    $result = mysqli_fetch_array(mysqli_query($con,$sql));
    $active = $result['isactive'];
    if($active == 1)
    {
        $import = $_POST['import'];
        $query = "SELECT * FROM games_big_3 WHERE a_fk='$import' AND year='$year'";
        $p = mysqli_fetch_array(mysqli_query($con,$query));
        $player1 = sanitize($con,$p['player1']);
        $player2 = sanitize($con,$p['player2']);
        $player3 = sanitize($con,$p['player3']);
        $query = "SELECT * FROM games_big_3 WHERE a_fk='$userid' AND year='$year'";
        $importcheck = mysqli_num_rows(mysqli_query($con,$query));
        if($importcheck > 0)
        {
            $sql = "UPDATE games_big_3 SET player1='$player1',player2='$player2',player3='$player3' WHERE a_fk='$id' AND year='$year'";
            mysqli_query($con,$sql);
            $audit = "INSERT INTO audit_history (username,task,ipaddress,audittime) VALUES (\"$username\",'Imported Big 3','$_SERVER[REMOTE_ADDR]',now())";
            $result=mysqli_query($con,$audit);
        }
        else
        {
            $sql = "INSERT INTO games_big_3 (a_fk,year,player1,player2,player3) VALUES ('$userid','$year','$player1','$player2','$player3')";
            mysqli_query($con,$sql);
            game_submission_notification($con,$username,'SBAO Big Three');
            $audit = "INSERT INTO audit_history (username,task,ipaddress,audittime) VALUES (\"$username\",'Imported Big 3','$_SERVER[REMOTE_ADDR]',now())";
            $result=mysqli_query($con,$audit);
        }
    }
    else
    {
        header("location:games_big_three.php?alert=closed");
    }
}
// End Import Predictions from Another User

if(isset($_POST['submit']))
{
    $sql = "SELECT isactive FROM forms WHERE form = 'big_3'";
    $result = mysqli_fetch_array(mysqli_query($con,$sql));
    $active = $result['isactive'];
    if($active == 1)
    {
        $id = isset($_POST['id']) ? sanitize($con,$_POST['id']) : '';
        $player1 = isset($_POST['player1']) ? sanitize($con,$_POST['player1']) : '';
        $player2 = isset($_POST['player2']) ? sanitize($con,$_POST['player2']) : '';
        $player3 = isset($_POST['player3']) ? sanitize($con,$_POST['player3']) : '';
        $sql = "SELECT id FROM games_big_3 WHERE a_fk ='$userid' AND year='$year'";
        $result = mysqli_query($con,$sql);
        $count = mysqli_num_rows($result);
        if($count == 0)
        {
            $sql = "INSERT INTO games_big_3 (a_fk,year,player1,player2,player3) VALUES ('$userid','$year','$player1','$player2','$player3')";
            mysqli_query($con,$sql);
            game_submission_notification($con,$username,'SBAO Big Three');
        }
        else
        {
            $sql = "UPDATE games_big_3 SET player1='$player1',player2='$player2',player3='$player3' WHERE a_fk='$userid' AND year='$year'";
            mysqli_query($con,$sql);
        }
    }
    else
    {
        header("location:games_big_three.php?alert=closed");
    }
}
else
{
    $sql = "SELECT id FROM games_big_3 WHERE a_fk ='$userid' AND year='$year'";
    $result = mysqli_query($con,$sql);
    $count = mysqli_num_rows($result);
    if($count == 0)
    {
        $player1 = '';
        $player2 = '';
        $player3 = '';
    }
    else
    {
        $sql = "SELECT player1,player2,player3 FROM games_big_3 WHERE a_fk='$userid' AND year='$year'";
        $result = mysqli_query($con,$sql);
        while($r = mysqli_fetch_array($result))
        {
            $player1 = $r['player1'];
            $player2 = $r['player2'];
            $player3 = $r['player3'];
        }
    }
}


$sql = "SELECT p.id,CONCAT(plast,', ',pfirst) as name,username,t.name as tname FROM auth_user a INNER JOIN players p ON a.ID=p.user_fk INNER JOIN teams t ON p.team=t.id where p.league = 'SBDL' AND (p.active='1' || p.active='3') AND (experience = '1') ORDER BY name ASC";
$result = mysqli_query($con,$sql);
$sql = "SELECT p.id,CONCAT(plast,', ',pfirst) as name,username,t.name as tname FROM auth_user a INNER JOIN players p ON a.ID=p.user_fk INNER JOIN teams t ON p.team=t.id where p.league = 'SBDL' AND (p.active='1' || p.active='3') ORDER BY name ASC";
$result2 = mysqli_query($con,$sql);

$echo = '';
$players1 = "<option value = \"0\">None</option>";
$players2 = "<option value = \"0\">None</option>";
$players3 = "<option value = \"0\">None</option>";
while ($r = mysqli_fetch_array($result)) 
{
    if($player1 == $r['id']) {$echo = "selected";} else {$echo = "";}
    $players1 .= "<option value = \"".$r['id']."\" $echo>".$r['name']." (".$r['username'].") - ".$r['tname']."</option>";
}
while ($r = mysqli_fetch_array($result2)) 
{
    if($player2 == $r['id']) {$echo = "selected";} else {$echo = "";}
    $players2 .= "<option value = \"".$r['id']."\" $echo>".$r['name']." (".$r['username'].") - ".$r['tname']."</option>";

    if($player3 == $r['id']) {$echo = "selected";} else {$echo = "";}
    $players3 .= "<option value = \"".$r['id']."\" $echo>".$r['name']." (".$r['username'].") - ".$r['tname']."</option>";
}
if(isset($_GET['alert'])) 
{
    if($_GET['alert'] == 'success') 
    {
        $echo = "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Big 3 Submission Saved.</strong></div>";
    }
    else if($_GET['alert'] == 'closed')
    {
        $echo = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Big 3 Closed, Submission Not Saved.</strong></div>";
    }
    else if($_GET['alert'] == 'norecord')
    {
        $echo = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You must save your own Predictions before you can import someone else's.</strong></div>";
    }
    else 
    {
        $echo = '';
    }
}
else
{$echo = '';}
?>

<!-- Body Start -->
<body>
<div id="wrapper" style="position:relative;top:0;left:0;">
    <?php include $path.'/includes/nav.php'; ?>
    <div class="container">
        <?=HeaderImagePrint($hostlg,$_SESSION['style'])?>
        <?=$echo?>
        <div class="panel panel-primary">
            <div class="panel-heading">Import SBAO Big Three</div>
            <div class="panel-body">
                <div class="panel-group col-xs-12">
                    <form class="form-horizontal" method="POST">
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>User</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>User</span>
                                <select class='form-control' name='import'>
                                    <?=$users?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary col-xs-12">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="panel panel-primary">
            <div class="panel-heading">SBAO Big Three Challenge</div>
            <div class="panel-body">
                <div class="panel-group col-xs-12">
                    <form class='form-horizontal' method="POST">
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon' style='width:50px;'>Player 1</span>
                                <select class='form-control' name='player1' id='player1'>
                                    <?=$players1?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon' style='width:50px;'>Player 2</span>
                                <select class='form-control' name='player2' id='player2'>
                                    <?=$players2?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon' style='width:50px;'>Player 3</span>
                                <select class='form-control' name='player3' id='player3'>
                                    <?=$players3?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <input type='hidden' class='form-control' name='id' value="<?=$id?>">
                                <button type="submit" name="submit" class="btn btn-default col-xs-2">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Body End -->

<?php
include $path.'/footer.php';
?>