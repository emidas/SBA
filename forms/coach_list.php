<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
$user=$_SESSION['user'];
$memberid=$_SESSION['memberid'];
$echo = '';

//coach list
$query = "SELECT username FROM auth_user ORDER BY username ASC";
$result = mysqli_query($con,$query);
$coachlist = "<option value = \"\">None</option>";
$echo = '';
while ($r = mysqli_fetch_array($result)) 
{
    $coachlist .= "<option value = \"".$r['username']."\">".$r['username']."</option>";
}

// Generate Results
$sql = "SELECT t.name as team,t.id as tid, c.name as coach,c.name2 as coach2,c.id as cid,t.league AS league
FROM teams t
INNER JOIN coaches c ON t.id=c.id
WHERE t.league = ?
ORDER BY team ASC";
$stmt = mysqli_stmt_init($con);
$sba = $mainlg;
$ncaa = 'ncaa';
$fiba = 'fiba';
$sbdl = $sublg;
if (mysqli_stmt_prepare($stmt, $sql))
{
    mysqli_stmt_bind_param($stmt,'s',$sba);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    mysqli_stmt_bind_param($stmt,'s',$ncaa);
    mysqli_stmt_execute($stmt);
    $result2 = mysqli_stmt_get_result($stmt);
    mysqli_stmt_bind_param($stmt,'s',$fiba);
    mysqli_stmt_execute($stmt);
    $result3 = mysqli_stmt_get_result($stmt);
    mysqli_stmt_bind_param($stmt,'s',$sbdl);
    mysqli_stmt_execute($stmt);
    $result4 = mysqli_stmt_get_result($stmt);
    mysqli_stmt_close($stmt);
}

// SBA Tab (mainlg)
$sbatable = '';
while ($row = mysqli_fetch_array($result)) 
{
    $sbatable .= "<tr>";
        $sbatable .= "<td><a href='team_page.php?tid=".$row['tid']."'>".$row['team']."</a></td>";
        $sbatable .= "<td>".$row['coach']."".(($_SESSION['isadmin'] == '1') ? "<a href='#' onclick=\"newcoach('".$row['cid']."','".$row['league']."','primary')\"> <i class='fas fa-cog'></i></a>" : "")."</td>";
        $sbatable .= "<td>".$row['coach2']."".(($_SESSION['isadmin'] == '1') ? "<a href='#' onclick=\"newcoach('".$row['cid']."','".$row['league']."','secondary')\"> <i class='fas fa-cog'></i></a>" : "")."</td>";
    $sbatable .= "</tr>";
}

// NCAA Tab
$ncaatable = '';
while ($row = mysqli_fetch_array($result2)) 
{
    $ncaatable .= "<tr>";
        $ncaatable .= "<td><a href='team_page.php?tid=".$row['tid']."'>".$row['team']."</a></td>";
        $ncaatable .= "<td>".$row['coach']."".(($_SESSION['isadmin'] == '1') ? "<a href='#' onclick=\"newcoach('".$row['cid']."','".$row['league']."','primary')\"> <i class='fas fa-cog'></i></a>" : "")."</td>";
        $ncaatable .= "<td>".$row['coach2']."".(($_SESSION['isadmin'] == '1') ? "<a href='#' onclick=\"newcoach('".$row['cid']."','".$row['league']."','secondary')\"> <i class='fas fa-cog'></i></a>" : "")."</td>";
    $ncaatable .= "</tr>";
}

// SBDL Tab (sublg)
$sbdltable = '';
while ($row = mysqli_fetch_array($result4)) 
{
    $sbdltable .= "<tr>";
        $sbdltable .= "<td><a href='team_page.php?tid=".$row['tid']."'>".$row['team']."</a></td>";
        $sbdltable .= "<td>".$row['coach']."".(($_SESSION['isadmin'] == '1') ? "<a href='#' onclick=\"newcoach('".$row['cid']."','".$row['league']."','primary')\"> <i class='fas fa-cog'></i></a>" : "")."</td>";
        $sbdltable .= "<td>".$row['coach2']."".(($_SESSION['isadmin'] == '1') ? "<a href='#' onclick=\"newcoach('".$row['cid']."','".$row['league']."','secondary')\"> <i class='fas fa-cog'></i></a>" : "")."</td>";
    $sbdltable .= "</tr>";
}

// FIBA Tab
$fibatable = '';
while ($row = mysqli_fetch_array($result3)) 
{
    $fibatable .= "<tr>";
        $fibatable .= "<td><a href='team_page.php?tid=".$row['tid']."'>".$row['team']."</a></td>";
        $fibatable .= "<td>".$row['coach']."".(($_SESSION['isadmin'] == '1') ? "<a href='#' onclick=\"newcoach('".$row['cid']."','".$row['league']."','primary')\"> <i class='fas fa-cog'></i></a>" : "")."</td>";
        $fibatable .= "<td>".$row['coach2']."".(($_SESSION['isadmin'] == '1') ? "<a href='#' onclick=\"newcoach('".$row['cid']."','".$row['league']."','secondary')\"> <i class='fas fa-cog'></i></a>" : "")."</td>";
    $fibatable .= "</tr>";
}
?>
<!-- Body Start -->
<body>
<div id="wrapper" style="position:relative;top:0;left:0;">
    <?php include $path.'/includes/nav.php'; ?>
    <div class="container">
        <?=HeaderImagePrint($hostlg,$_SESSION['style'])?>
        <?=$echo?>
        <div class="panel panel-primary">
            <div class="panel-heading">Coach List</div>
            <div class="panel-body">
                <ul class="nav nav-tabs" style="border-bottom:0px;">
                    <li class="active"><a data-toggle="tab" href="#sba"><?=$mainlg?></a></li>
                    <!--<li><a data-toggle="tab" href="#ncaa">NCAA</a></li>-->
                    <li><a data-toggle="tab" href="#sbdl"><?=$sublg?></a></li>
                    <?php if($hostlg == 'SBA') { echo "
                    <li><a data-toggle=\"tab\" href=\"#fiba\">FIBA</a></li>";}?>
                </ul>
                <div class="tab-content">
                    <div id="sba" class="tab-pane fade in active">
                        <div class='table-responsive'>
                            <table class="table table-striped" id='viewsba' style="width:100%;">
                                <thead>
                                    <th>Team</th>
                                    <th>Coach</th>
                                    <th>Asst. Coach</th>
                                </thead>
                                <tbody>
                                    <?=$sbatable?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="ncaa" class="tab-pane fade">
                        <div class='table-responsive'>
                            <table class="table table-striped" id='viewncaa' style="width:100%;">
                                <thead>
                                    <th>Team</th>
                                    <th>Coach</th>
                                    <th>Asst. Coach</th>
                                </thead>
                                <tbody>
                                    <?=$ncaatable?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="fiba" class="tab-pane fade">
                        <div class='table-responsive'>
                            <table class="table table-striped" id='viewfiba' style="width:100%;">
                                <thead>
                                    <th>Team</th>
                                    <th>Coach</th>
                                    <th>Asst. Coach</th>
                                </thead>
                                <tbody>
                                    <?=$fibatable?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="sbdl" class="tab-pane fade">
                        <div class='table-responsive'>
                            <table class="table table-striped" id='viewsbdl' style="width:100%;">
                                <thead>
                                    <th>Team</th>
                                    <th>Coach</th>
                                    <th>Asst. Coach</th>
                                </thead>
                                <tbody>
                                    <?=$sbdltable?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class='modal fade' id='coach' role='dialog'>
            <div class='modal-dialog modal-lg'>
                <div class='modal-content'>
                    <form action='change_coach.php' method='post'>
                        <div class='modal-header'>
                            <button type='button' class='close' data-dismiss='modal'>&times;</button>
                            <h4 class='modal-title'>Change Coach</h4>
                        </div>
                        <div class='modal-body'>
                            <div class='form-group'>
                                <div class='input-group'><span class='input-group-addon' style='width:120px;text-align:left;'>New Coach:</span><select class='form-control' name='coach'><?=$coachlist?></select></div>
                            </div>
                            <input type='text' name='cid' id='cid' value='' hidden>
                            <input type='text' name='league' id='league' value='' hidden>
                            <input type='text' name='type' id='type' value='' hidden>
                        </div>
                        <div class='modal-footer'>
                            <button type='submit' class='btn btn-primary'>Submit</button>
                            <button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Body End -->
<?php
include $path.'/footer.php';
?>
<style>
.table-responsive > .table {
    margin-bottom: 0;
}
.table-responsive > .table > thead > tr > th, .table-responsive > .table > tbody > tr > th, .table-responsive > .table > tfoot > tr > th, .table-responsive > .table > thead > tr > td, .table-responsive > .table > tbody > tr > td, .table-responsive > .table > tfoot > tr > td {
    white-space: nowrap;
}
</style>
<script>
function newcoach(cid,league,type)
{
    $('#cid').val(cid);
    $('#league').val(league);
    $('#type').val(type);
    $('#coach').modal();
}
$(document).ready(function(){
    $('[data-toggle="popover"]').popover(); 
});
</script>
<script>
    $('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>