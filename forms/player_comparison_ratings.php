<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
$user=$_SESSION['user'];

if(isset($_POST['name']))
{$name = $_POST['name'];} else {$name = '';}
if(isset($_POST['name2']))
{$name2 = $_POST['name2'];} else {$name2 = '';}
if(isset($_POST['name3']))
{$name3 = $_POST['name3'];} else {$name3 = '';}
if(isset($_POST['name4']))
{$name4 = $_POST['name4'];} else {$name4 = '';}
if(isset($_POST['name5']))
{$name5 = $_POST['name5'];} else {$name5 = '';}
if(isset($_POST['checkC']))
{$checkC = $_POST['checkC'];} else {$checkC = '';}
if(isset($_POST['checkD']))
{$checkD = $_POST['checkD'];} else {$checkD = '';}
if(isset($_POST['checkE']))
{$checkE = $_POST['checkE'];} else {$checkE = '';}


if($name != '')
{
    $aecho = printplayerratings($con,$name);
}
else
{
    $aecho = '';
}
if($name2 != '')
{
    $becho = printplayerratings($con,$name2);
}
else
{
    $becho = '';
}
if($name3 != '' && $checkC == 'on')
{
    $cecho = printplayerratings($con,$name3);
}
else
{
    $cecho = '';
}
if($name4 != '' && $checkD == 'on')
{
    $decho = printplayerratings($con,$name4);
}
else
{
    $decho = '';
}
if($name5 != '' && $checkE == 'on')
{
    $eecho = printplayerratings($con,$name5);
}
else
{
    $eecho = '';
}

// Player Dropdowns
$query = "SELECT id,CONCAT(plast,', ',pfirst) as name,CONCAT(pfirst,' ',plast) as pname FROM players ORDER BY name ASC";
$result = mysqli_query($con,$query);
$plist = '';
while ($r = mysqli_fetch_array($result)) 
{
    if($name == $r['id']) {$echo = "selected";} else {$echo = "";}
    $plist .= "<option value = '".$r['id']."' $echo>".$r['name']."</option>";
}
$result = mysqli_query($con,$query);
$plist2 = '';
while ($r = mysqli_fetch_array($result)) 
{
    if($name2 == $r['id']) {$echo = "selected";} else {$echo = "";}
    $plist2 .= "<option value = '".$r['id']."' $echo>".$r['name']."</option>";
}
$result = mysqli_query($con,$query);
$plist3 = '';
while ($r = mysqli_fetch_array($result)) 
{
    if($name3 == $r['id']) {$echo = "selected";} else {$echo = "";}
    $plist3 .= "<option value = '".$r['id']."' $echo>".$r['name']."</option>";
}
$result = mysqli_query($con,$query);
$plist4 = '';
while ($r = mysqli_fetch_array($result)) 
{
    if($name4 == $r['id']) {$echo = "selected";} else {$echo = "";}
    $plist4 .= "<option value = '".$r['id']."' $echo>".$r['name']."</option>";
}
$result = mysqli_query($con,$query);
$plist5 = '';
while ($r = mysqli_fetch_array($result)) 
{
    if($name5 == $r['id']) {$echo = "selected";} else {$echo = "";}
    $plist5 .= "<option value = '".$r['id']."' $echo>".$r['name']."</option>";
}
// End of Player Dropdowns


if(isset($_GET['alert'])) 
{
    if($_GET['alert'] == 'success') 
    {
        $echo = "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Predictions Saved.</strong></div>";
    }
    else if($_GET['alert'] == 'closed')
    {
        $echo = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Predictions Closed, Submission Not Saved.</strong></div>";
    }
    else if($_GET['alert'] == 'norecord')
    {
        $echo = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You must save your own Predictions before you can import someone else's.</strong></div>";
    }
    else 
    {
        $echo = '';
    }
}
else
{$echo = '';}
?>

<!-- Body Start -->
<body>
<div id="wrapper" style="position:relative;top:0;left:0;">
    <?php include $path.'/includes/nav.php'; ?>
    <div class="container">
        <?=HeaderImagePrint($hostlg,$_SESSION['style'])?>
        <?=$echo?>
        <form class="form-horizontal" method='POST'>
            <div class="panel-group">
                <div class="panel panel-primary">
                    <div class="panel-heading">Player Comparison Settings</div>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="col-xs-offset-5 col-xs-2"><center><input type="checkbox" name="checkC" id="checkC" onChange="enableCompare('checkC','C');" <?php if ($checkC == 'on') echo "checked='checked'";?>></center></div>
                            <div class="col-xs-2"><center><input type="checkbox" name="checkD" id="checkD" onChange="enableCompare('checkD','D');" <?php if ($checkD == 'on') echo "checked='checked'";?>></center></div>
                            <div class="col-xs-2"><center><input type="checkbox" name="checkE" id="checkE" onChange="enableCompare('checkE','E');" <?php if ($checkE == 'on') echo "checked='checked'";?>></center></div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-offset-1 col-xs-2"><h4><center>Player A</center></h4></div>
                            <div class="col-xs-2"><h4><center>Player B</center></h4></div>
                            <div class="col-xs-2"><h4><center>Player C</center></h4></div>
                            <div class="col-xs-2"><h4><center>Player D</center></h4></div>
                            <div class="col-xs-2"><h4><center>Player E</center></h4></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-xs-1" for="type">Player:</label>
                            <div class="col-xs-2">
                                <select name="name" class="form-control"><?=$plist?></select>
                            </div>
                            <div class="col-xs-2">
                                <select name="name2" class="form-control"><?=$plist2?></select>
                            </div>
                            <div class="col-xs-2">
                                <select name="name3" id="name3" class="form-control" disabled><?=$plist3?></select>
                            </div>
                            <div class="col-xs-2">
                                <select name="name4" id="name4" class="form-control" disabled><?=$plist4?></select>
                            </div>
                            <div class="col-xs-2">
                                <select name="name5" id="name5" class="form-control" disabled><?=$plist5?></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-offset-1 col-xs-11">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading">Player Comparison</div>
            <div class="panel-body">
                <?=$aecho?>
                <?=$becho?>
                <?=$cecho?>
                <?=$decho?>
                <?=$eecho?>
            </div>
        </div>
    </div>
</div>
<!-- Body End -->

<?php
include $path.'/footer.php';
?>

<script type="text/javascript">
function enableCompare(obj,id)
{
    if (document.getElementById(obj).checked)
    {
        switch(id)
        {
            case 'C':
            document.getElementById('name3').disabled = false;
            break;
            case 'D':
            document.getElementById('name4').disabled = false;
            break;
            case 'E':
            document.getElementById('name5').disabled = false;
            break;
        }
    }
    else
    {
        switch(id)
        {
            case 'C':
            document.getElementById('name3').disabled = true;
            break;
            case 'D':
            document.getElementById('name4').disabled = true;
            break;
            case 'E':
            document.getElementById('name5').disabled = true;
            break;
        }
    }
}
$(document).ready(function(){
    enableCompare('checkC','C');
    enableCompare('checkD','D');
    enableCompare('checkE','E');
});
</script>