<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
// get user
$user=$_SESSION['user'];
$alert = '';
$echo = '';

// get pid
$pid = sanitize($con,$_GET['pid']);

// get player name and tpe (replace with function)
$sql = "SELECT CONCAT(pfirst,' ',plast) as pname,p_bank FROM players WHERE id='$pid'";
$nme=mysqli_fetch_array(mysqli_query($con,$sql));
$pname=$nme['pname'];

$sql = "SELECT cash,task,audittime FROM player_earn_history WHERE pid_fk='$pid' ORDER BY id ASC";
$result = mysqli_query($con,$sql);
while($row = mysqli_fetch_array($result))
{
    $echo .= "<tr>";
        $echo .= "<td>".$row['task']."</td>";
        $echo .= "<td>$".number_format($row['cash'],0)."</td>";
        $echo .= "<td>".date("m-d-Y",strtotime($row['audittime']))."</td>";
    $echo .= "</tr>";
}
?>

<!-- Body Start -->
<body>
<div id="wrapper" style="position:relative;top:0;left:0;">
    <?php include $path.'/includes/nav.php'; ?>
    <div class="container">
        <?=HeaderImagePrint($hostlg,$_SESSION['style'])?>
        <?=$alert?>
        <div class="row">
            <div class="panel panel-primary">
                <div class="panel-heading">Player Earn History - <?=$pname?></div>
                <div class="panel-body"> 
                    <div style="overflow-x:auto;">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th colspan="5">Player Earnings</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th>Task</th>
                                    <th>Cash</th>
                                    <th >Timestamp</th>
                                </tr>
                                <?=$echo?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Body End -->

<?php
include $path.'/footer.php';
?>