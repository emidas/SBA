<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
$user = new User($_SESSION['user']);
$username = $user->username;
$userid = $user->id;
$system = new System();
$cap = $system->get_salary_cap();
$year = $system->get_year();
$capshown = number_format($cap);
$total = 0;
$player1 = '';
$player2 = '';
$player3 = '';
$player4 = '';
$player5 = '';

// Begin Import Predictions from Another User
$query = "SELECT p.a_fk,a.username FROM auth_user a INNER JOIN games_5x5 p ON a.ID=p.a_fk where year = '$year' ORDER BY username ASC";
$result = mysqli_query($con,$query);
$users = '';
while ($q = mysqli_fetch_array($result)) 
{
    $users .= "<option value = '".$q['a_fk']."'>".$q['username']."</option>";
}
if(isset($_POST['import']))
{
    $sql = "SELECT isactive FROM forms WHERE form = '5x5'";
    $result = mysqli_fetch_array(mysqli_query($con,$sql));
    $active = $result['isactive'];
    if($active == 1)
    {
        $import = $_POST['import'];
        $query = "SELECT * FROM games_5x5 WHERE a_fk='$import' AND year='$year'";
        $p = mysqli_fetch_array(mysqli_query($con,$query));
        $player1 = sanitize($con,$p['player1']);
        $player2 = sanitize($con,$p['player2']);
        $player3 = sanitize($con,$p['player3']);
        $player4 = sanitize($con,$p['player4']);
        $player5 = sanitize($con,$p['player5']);
        $total = sanitize($con,$p['total']);
        $query = "SELECT * FROM games_5x5 WHERE a_fk='$userid' AND year='$year'";
        $importcheck = mysqli_num_rows(mysqli_query($con,$query));
        if($importcheck > 0)
        {
            $sql = "UPDATE games_5x5 SET player1='$player1',player2='$player2',player3='$player3',player4='$player4',player5='$player5',total='$total' WHERE a_fk='$userid' AND year='$year'";
            mysqli_query($con,$sql);
            $audit = "INSERT INTO audit_history (username,task,ipaddress,audittime) VALUES (\"$username\",'Imported 5x5','$_SERVER[REMOTE_ADDR]',now())";
            $result=mysqli_query($con,$audit);
        }
        else
        {
            $sql = "INSERT INTO games_5x5 (a_fk,year,player1,player2,player3,player4,player5,total) VALUES ('$userid','$year','$player1','$player2','$player3','$player4','$player5','$total')";
            mysqli_query($con,$sql);
            game_submission_notification($con,$username,'SBAO 5x5');
            $audit = "INSERT INTO audit_history (username,task,ipaddress,audittime) VALUES (\"$username\",'Imported 5x5','$_SERVER[REMOTE_ADDR]',now())";
            $result=mysqli_query($con,$audit);
        }
    }
    else
    {
        header("location:games_big_three.php?alert=closed");
    }
}
// End Import Predictions from Another User




if(isset($_POST['submit']))
{
    $sql = "SELECT isactive FROM forms WHERE form = '5x5'";
    $result = mysqli_fetch_array(mysqli_query($con,$sql));
    $active = $result['isactive'];
    if($active == 1)
    {
        $id = isset($_POST['id']) ? sanitize($con,$_POST['id']) : '';
        $player1 = isset($_POST['player1']) ? sanitize($con,$_POST['player1']) : '';
        $player2 = isset($_POST['player2']) ? sanitize($con,$_POST['player2']) : '';
        $player3 = isset($_POST['player3']) ? sanitize($con,$_POST['player3']) : '';
        $player4 = isset($_POST['player4']) ? sanitize($con,$_POST['player4']) : '';
        $player5 = isset($_POST['player5']) ? sanitize($con,$_POST['player5']) : '';
        $total = isset($_POST['total']) ? sanitize($con,$_POST['total']) : '';
        $sql = "SELECT id FROM games_5x5 WHERE a_fk ='$userid' AND year='$year'";
        $result = mysqli_query($con,$sql);
        $count = mysqli_num_rows($result);
        if($count == 0)
        {
            $sql = "INSERT INTO games_5x5 (a_fk,year,player1,player2,player3,player4,player5,total) VALUES ('$userid','$year','$player1','$player2','$player3','$player4','$player5','$total')";
            mysqli_query($con,$sql);
            game_submission_notification($con,$username,'SBAO 5x5');
        }
        else
        {
            $sql = "UPDATE games_5x5 SET player1='$player1',player2='$player2',player3='$player3',player4='$player4',player5='$player5',total='$total' WHERE a_fk='$userid' AND year='$year'";
            mysqli_query($con,$sql);
        }
    }
    else
    {
        header("location:games_five_x_five.php?alert=closed");
    }
}
else
{
    $sql = "SELECT id FROM games_5x5 WHERE a_fk ='$userid' AND year='$year'";
    $result = mysqli_query($con,$sql);
    $count = mysqli_num_rows($result);
    if($count == 0)
    {
        $total = 0;
        $player1 = '';
        $player2 = '';
        $player3 = '';
        $player4 = '';
        $player5 = '';
    }
    else
    {
        $sql = "SELECT player1,player2,player3,player4,player5,total FROM games_5x5 WHERE a_fk='$userid' AND year='$year'";
        $result = mysqli_query($con,$sql);
        while($r = mysqli_fetch_array($result))
        {
            $player1 = $r['player1'];
            $player2 = $r['player2'];
            $player3 = $r['player3'];
            $player4 = $r['player4'];
            $player5 = $r['player5'];
            $total = $r['total'];
        }
    }
}

$totalshown = number_format($total);

$sql = "SELECT id,CONCAT(plast,', ',pfirst) as name,salary FROM players p where league = 'SBA' AND (p.active='1' || p.active='3') AND (position='PG' || position2='PG') ORDER BY name ASC";
$result = mysqli_query($con,$sql);
$sql = "SELECT id,CONCAT(plast,', ',pfirst) as name,salary FROM players p where league = 'SBA' AND (p.active='1' || p.active='3') AND (position='SG' || position2='SG' || (build='Freak' && (position='PG' || position2='PG'))) ORDER BY name ASC";
$result2 = mysqli_query($con,$sql);
$sql = "SELECT id,CONCAT(plast,', ',pfirst) as name,salary FROM players p where league = 'SBA' AND (p.active='1' || p.active='3') AND (position='SF' || position2='SF' || (build='Freak' && (position='PG' || position2='PG' || position='SG' || position2='SG'))) ORDER BY name ASC";
$result3 = mysqli_query($con,$sql);
$sql = "SELECT id,CONCAT(plast,', ',pfirst) as name,salary FROM players p where league = 'SBA' AND (p.active='1' || p.active='3') AND (position='PF' || position2='PF' || (build='Freak' && (position='PG' || position2='PG' || position='SG' || position2='SG' || position='SF' || position2='SF'))) ORDER BY name ASC";
$result4 = mysqli_query($con,$sql);
$sql = "SELECT id,CONCAT(plast,', ',pfirst) as name,salary FROM players p where league = 'SBA' AND (p.active='1' || p.active='3') AND (position='C' || position2='C' || (build='Freak' && (position='PG' || position2='PG' || position='SG' || position2='SG' || position='SF' || position2='SF' || position='PF' || position2='PF'))) ORDER BY name ASC";
$result5 = mysqli_query($con,$sql);


$echo = '';
$players1 = "<option value = \"0\">None</option>";
$players2 = "<option value = \"0\">None</option>";
$players3 = "<option value = \"0\">None</option>";
$players4 = "<option value = \"0\">None</option>";
$players5 = "<option value = \"0\">None</option>";
while ($r = mysqli_fetch_array($result)) 
{
    if($player1 == $r['id']) {$echo = "selected";} else {$echo = "";}
    $players1 .= "<option value = \"".$r['id']."\" $echo>".$r['name']." - $".number_format($r['salary'])."</option>";
}
while ($r = mysqli_fetch_array($result2)) 
{
    if($player2 == $r['id']) {$echo = "selected";} else {$echo = "";}
    $players2 .= "<option value = \"".$r['id']."\" $echo>".$r['name']." - $".number_format($r['salary'])."</option>";
}
while ($r = mysqli_fetch_array($result3)) 
{
    if($player3 == $r['id']) {$echo = "selected";} else {$echo = "";}
    $players3 .= "<option value = \"".$r['id']."\" $echo>".$r['name']." - $".number_format($r['salary'])."</option>";
}
while ($r = mysqli_fetch_array($result4)) 
{
    if($player4 == $r['id']) {$echo = "selected";} else {$echo = "";}
    $players4 .= "<option value = \"".$r['id']."\" $echo>".$r['name']." - $".number_format($r['salary'])."</option>";
}
while ($r = mysqli_fetch_array($result5)) 
{
    if($player5 == $r['id']) {$echo = "selected";} else {$echo = "";}
    $players5 .= "<option value = \"".$r['id']."\" $echo>".$r['name']." - $".number_format($r['salary'])."</option>";
}
if(isset($_GET['alert'])) 
{
    if($_GET['alert'] == 'success') 
    {
        $echo = "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Big 3 Submission Saved.</strong></div>";
    }
    else if($_GET['alert'] == 'closed')
    {
        $echo = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Big 3 Closed, Submission Not Saved.</strong></div>";
    }
    else if($_GET['alert'] == 'norecord')
    {
        $echo = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You must save your own Predictions before you can import someone else's.</strong></div>";
    }
    else 
    {
        $echo = '';
    }
}
else
{$echo = '';}
?>

<!-- Body Start -->
<body>
<div id="wrapper" style="position:relative;top:0;left:0;">
    <?php include $path.'/includes/nav.php'; ?>
    <div class="container">
        <?=HeaderImagePrint($hostlg,$_SESSION['style'])?>
        <?=$echo?>
        <div class="panel panel-primary">
            <div class="panel-heading">Import SBAO 5x5</div>
            <div class="panel-body">
                <div class="panel-group col-xs-12">
                    <form class="form-horizontal" method="POST">
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>User</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>User</span>
                                <select class='form-control' name='import'>
                                    <?=$users?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary col-xs-12">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="panel panel-primary">
            <div class="panel-heading">SBAO 5x5 Challenge</div>
            <div class="panel-body">
                <div class="panel-group col-xs-12">
                    <form class='form-horizontal' method="POST">
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon' style='width:50px;'>PG</span>
                                <select class='form-control' name='player1' id='player1' onchange="salary(this.value,'val1')">
                                    <?=$players1?>
                                </select>
                                <input type='text' class='form-control' name='salary1' id='val1' readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon' style='width:50px;'>SG</span>
                                <select class='form-control' name='player2' id='player2' onchange="salary(this.value,'val2')">
                                    <?=$players2?>
                                </select>
                                <input type='text' class='form-control' name='salary2' id='val2' readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon' style='width:50px;'>SF</span>
                                <select class='form-control' name='player3' id='player3' onchange="salary(this.value,'val3')">
                                    <?=$players3?>
                                </select>
                                <input type='text' class='form-control' name='salary3' id='val3' readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon' style='width:50px;'>PF</span>
                                <select class='form-control' name='player4' id='player4' onchange="salary(this.value,'val4')">
                                    <?=$players4?>
                                </select>
                                <input type='text' class='form-control' name='salary4' id='val4' readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon' style='width:50px;'>C</span>
                                <select class='form-control' name='player5' id='player5' onchange="salary(this.value,'val5')">
                                    <?=$players5?>
                                </select>
                                <input type='text' class='form-control' name='salary5' id='val5' readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon' style='width:125px;'>Salary Spent</span>
                                <input type='text' class='form-control' id='totalshown' value="<?=$totalshown?>" readonly>
                                <input type='hidden' class='form-control' name='total' id='total' value="<?=$total?>" readonly>
                            </div>
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon' style='width:125px;'>Salary Cap</span>
                                <input type='text' class='form-control' id='cap' value="<?='$'.$capshown?>" readonly>
                                <input type='hidden' class='form-control' id='caphidden' value='<?=$cap?>'>
                            </div>
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon' style='width:125px;'>Cap Remaining</span>
                                <input type='text' class='form-control' id='caproom' value="<?='$'.$capshown?>" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-offset-2 col-xs-2">
                                <input type='hidden' class='form-control' name='id' value="<?=$id?>">
                                <button type="submit" name="submit" class="btn btn-default">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Body End -->

<?php
include $path.'/footer.php';
?>
<script>
$(document).ready(function(){
    salary($('#player1').val(),'val1');
    salary($('#player2').val(),'val2');
    salary($('#player3').val(),'val3');
    salary($('#player4').val(),'val4');
    salary($('#player5').val(),'val5');
});
$("body").mouseover(function(){
    
});
function salary(pid,pos) {
    if(pid == 0)
    {
        $("#"+pos).val(0);
        return;
    }
    $.ajax({url: "../ajax/get_salary.php?pid="+pid, success: function(result){
      $("#"+pos).val(result);
      calculate();
    }});
}
function calculate()
{
    var total = 0;
    $("input[name^='salary']").each(function(){
        var sal = $(this).val();
        total += Number(sal);
    });
    $('#totalshown').val('$'+total.toLocaleString());
    $('#total').val(total);
    var cap = $('#caphidden').val();
    var caproom = cap - total;
    $('#caproom').val('$'+caproom.toLocaleString());
}
</script>