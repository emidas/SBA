<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
$user=$_SESSION['user'];

$league = '';
$type = '';
$stat = '';
if(isset($_POST['season']))
{$season = $_POST['season'];} else {$season = '1';}
if(isset($_POST['team']))
{$team = $_POST['team'];} else {$team = '5';}
if(isset($_POST['category']))
{$category = $_POST['category'];} else {$category = 'GameAssists';}

$yquery = "SELECT season FROM SBA_Years";
$yresult = mysqli_query($con,$yquery);
$yecho = '';
$echo = '';
while ($r = mysqli_fetch_array($yresult)) 
{
    if($season == $r['season']) {$echo = "selected";} else {$echo = "";}
    $yecho .= "<option value = '".$r['season']."' $echo>".$r['season']."</option>";
}
// years
$yquery = "SELECT year FROM year";
$yresult = mysqli_query($con,$yquery);
$yecho = '';
$echo = '';
while ($r = mysqli_fetch_array($yresult)) {
    if($season == $r['year']){$echo = 'selected';}else{$echo = '';}
    $yecho .= "<option value = '".$r['year']."' ".$echo.">".$r['year']."</option>";
}
?>

<!-- Body Start -->
<body>
<div id="wrapper" style="position:relative;top:0;left:0;">
    <?php include $path.'/includes/nav.php'; ?>
    <div class="container-fluid">
        <?=HeaderImagePrint($hostlg,$_SESSION['style'])?>
        <div class="panel panel-primary">
            <div class="panel-heading">Player Statistics</div>
            <div class="panel-body">
                <div class="panel-group">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Player Statistics Filters</div>
                        <div class="panel-body">
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>League</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>League</span>
                                <select name="league" id="league" class="form-control">
                                    <option value='sba' <?php if ($league == 'sba') echo "selected";?>>SBA</option>
                                    <option value='ncaa' <?php if ($league == 'ncaa') echo "selected";?>>NCAA</option>
                                    <option value='fiba' <?php if ($league == 'fiba') echo "selected";?>>FIBA</option>
                                </select>
                            </div>
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>Category</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>Category</span>
                                <select name="category" id="category" class="form-control">
                                    <option value='season' <?php if ($category == 'season') echo "selected";?>>Season</option>
                                    <option value='alltime' <?php if ($category == 'alltime') echo "selected";?>>All-Time</option>
                                </select>
                            </div>
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>Type</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>Type</span>
                                <select name="type" id="type" class="form-control">
                                    <option value='season' <?php if ($type == 'season') echo "selected";?>>Regular Season</option>
                                    <option value='playoff' <?php if ($type == 'playoff') echo "selected";?>>Playoff</option>
                                </select>
                            </div>
                            <div class="input-group col-xs-12">
                                <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>Season</span>
                                <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>Season</span>
                                <select name="season" id="season" class="form-control">
                                    <?=$yecho?>
                                </select>
                            </div>
                            <button type="button" class="btn btn-primary col-xs-12" onclick="getPlayerStatistics('0')">Submit</button>
                            <button type="submit" class="btn btn-primary col-xs-12" onclick="getPlayerStatistics('1')">Export</button>
                        </div>
                    </div>
                </div>
                <div class="panel-group">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Statistics</div>
                        <div class="panel-body">
                            <div id="playerstatistics">
                                <!--Ajax Call inserts records here -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>       
    </div>
</div>
<!-- Body End -->
<script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<?php
include $path.'/footer.php';
?>
<style>
.table-responsive > .table {
    margin-bottom: 0;
}
.table-responsive > .table > thead > tr > th, .table-responsive > .table > tbody > tr > th, .table-responsive > .table > tfoot > tr > th, .table-responsive > .table > thead > tr > td, .table-responsive > .table > tbody > tr > td, .table-responsive > .table > tfoot > tr > td {
    white-space: nowrap;
}
table.dataTable thead .sorting, 
table.dataTable thead .sorting_asc, 
table.dataTable thead .sorting_desc {
    background : none;
}
</style>
<script>
$(document).ready(function() {
    $('#seasonstats').DataTable( {
        scrollX: true,
        paging:   false,
        select: true,
        columnDefs: 
        [
            {"orderSequence": ['asc','desc'], "targets": [0]},
            {"orderSequence": ['desc','asc'], "targets": '_all'}
        ]
    } );
    $('#alltimestats').DataTable( {
        scrollX: true,
        paging:   false,
        select: true,
        columnDefs: 
        [
            {"orderSequence": ['asc','desc'], "targets": [0,1]},
            {"orderSequence": ['desc','asc'], "targets": '_all'}
        ]
    } );
} );
$('.table-responsive').on('show.bs.dropdown', function () {
    $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
    $('.table-responsive').css( "overflow", "auto" );
});
$('#category').change(function() {
    if($('#category').val() == 'alltime')
    {
        $('#season').prop("disabled",true);
        $('#season').val('');
    }
    else
    {
        $('#season').prop("disabled",false);
        $('#season').val('1');
    }
});
function getPlayerStatistics(csv)
{
    var category = $('#category').val();
    var league = $('#league').val();
    var type = $('#type').val();
    var season = $('#season').val();
    if(csv == '0')
    {
        $.ajax({url: "../ajax/get_player_statistics.php?category="+category+"&league="+league+"&type="+type+"&season="+season+"&export="+csv, success: function(result){
            $("#playerstatistics").html(result);
        }});
    }
    else
    {
        window.open("../ajax/get_player_statistics.php?category="+category+"&league="+league+"&type="+type+"&season="+season+"&export="+csv);
    }
}
</script>