<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
$user=$_SESSION['user'];
$mid = $_SESSION['memberid'];

if(isset($_GET['pid']))
{
    $pid = sanitize($con,$_GET['pid']);
}
else{$pid ='%';}

// year
$system = new System();
$year = $system->get_year();
// get id
$userobj = new User($user);
$id = $userobj->get_('id');

if(isset($_POST['plast']) AND isset($_POST['pfirst'])) 
{
    $pid = isset($_POST['pid']) ? sanitize($con,$_POST['pid']) : '';
    $pfirst = isset($_POST['pfirst']) ? sanitize($con,$_POST['pfirst']) : '';
    $plast = isset($_POST['plast']) ? sanitize($con,$_POST['plast']) : '';
    $position = isset($_POST['position']) ? sanitize($con,$_POST['position']) : '';
    $position2 = isset($_POST['position2']) ? sanitize($con,$_POST['position2']) : '';
    $build = isset($_POST['build']) ? sanitize($con,$_POST['build']) : '';
    $num = isset($_POST['num']) ? sanitize($con,$_POST['num']) : '';
    $height = isset($_POST['height']) ? sanitize($con,$_POST['height']) : '';
    $weight = isset($_POST['weight']) ? sanitize($con,$_POST['weight']) : '';
    $country = isset($_POST['country']) ? sanitize($con,$_POST['country']) : '';
    $c90 = isset($_POST['c90']) ? sanitize($con,$_POST['c90']) : '';
    $c801 = isset($_POST['c801']) ? sanitize($con,$_POST['c801']) : '';
    $c802 = isset($_POST['c802']) ? sanitize($con,$_POST['c802']) : '';
    $c803 = isset($_POST['c803']) ? sanitize($con,$_POST['c803']) : '';
    $c804 = isset($_POST['c804']) ? sanitize($con,$_POST['c804']) : '';
    $c70 = isset($_POST['c70']) ? sanitize($con,$_POST['c70']) : '';
    $capArr = Array($_POST['c90'],$_POST['c801'],$_POST['c802'],$_POST['c803'],$_POST['c804'],$_POST['c70']);
    $capArr2 = array_unique($capArr);
    $count = 0;
}
else
{
    $sql = "SELECT * FROM players WHERE id = '$pid'";
    $result = mysqli_query($con,$sql);
    while($row = mysqli_fetch_array($result))
    {
        $pfirst = $row['pfirst'];
        $plast = $row['plast'];
        $position = $row['position'];
        $position2 = $row['position2'];
        $build = $row['build'];
        $num = $row['num'];
        $height = $row['height'];
        $weight = $row['weight'];
        $country = $row['country'];
        $c90 = $row['c90'];
        $c801 = $row['c801'];
        $c802 = $row['c802'];
        $c803 = $row['c803'];
        $c804 = $row['c804'];
        $c70 = $row['c70'];
        $count = 1;  
    }
}
$listarr = Array('Inside Scoring','Jump Shot','Free Throw','Three Point','Handling','Passing','Offensive Rebounding','Defensive Rebounding','Post Defense','Perimeter Defense','Stealing','Blocking','Fouling','Quickness','Strength','Jumping');
$listarr_short = Array('ins','jps','ft','3ps','han','pas','orb','drb','psd','prd','stl','blk','fl','qkn','str','jmp');
$listarr_pos = Array('PG','SG','SF','PF','C');
$listarr_country = Array('Africa','Greece','Argentina','Spain','Canada','Mexico','China','Germany','Italy','New Zealand','United States','Australia','Japan','Brazil','United Kingdom + Ireland','Latvia','France','Russia','Croatia','Lithuania');
$listarr_height = Array("5'0","5'1","5'2","5'3","5'4","5'5","5'6","5'7","5'8","5'9","5'10","5'11","6'0","6'1","6'2","6'3","6'4","6'5","6'6","6'7","6'8","6'9","6'10","6'11","7'0","7'1","7'2","7'3","7'4");
sort($listarr_country);
$listpos = "";
for ($a=0;$a<count($listarr_pos);$a++)
{
    if($position == $listarr_pos[$a]) {$echo = "selected";} else {$echo = "";}
    $listpos .= "<option value='$listarr_pos[$a]' $echo>$listarr_pos[$a]</option>";
}
$listpos2 = "";
for ($a=0;$a<count($listarr_pos);$a++)
{
    if($position2 == $listarr_pos[$a]) {$echo = "selected";} else {$echo = "";}
    $listpos2 .= "<option value='$listarr_pos[$a]' $echo>$listarr_pos[$a]</option>";
}
$listheight = "";
for ($a=0;$a<count($listarr_height);$a++)
{
    if($height == $listarr_height[$a]) {$echo = "selected";} else {$echo = "";}
    $listheight .= "<option value=\"$listarr_height[$a]\" $echo>$listarr_height[$a]</option>";
}
$listcountry = "";
for ($a=0;$a<count($listarr_country);$a++)
{
    if($country == $listarr_country[$a]) {$echo = "selected";} else {$echo = "";}
    $listcountry .= "<option value='$listarr_country[$a]' $echo>$listarr_country[$a]</option>";
}
$list90 = "";
for ($a=0;$a<count($listarr);$a++)
{
    if($c90 == $listarr_short[$a]) {$echo = "selected";} else {$echo = "";}
    $list90 .= "<option value='$listarr_short[$a]' $echo>$listarr[$a]</option>";
}
$list801 = "";
for ($a=0;$a<count($listarr);$a++)
{
    if($c801 == $listarr_short[$a]) {$echo = "selected";} else {$echo = "";}
    $list801 .= "<option value='$listarr_short[$a]' $echo>$listarr[$a]</option>";
}
$list802 = "";
for ($a=0;$a<count($listarr);$a++)
{
    if($c802 == $listarr_short[$a]) {$echo = "selected";} else {$echo = "";}
    $list802 .= "<option value='$listarr_short[$a]' $echo>$listarr[$a]</option>";
}
$list803 = "";
for ($a=0;$a<count($listarr);$a++)
{
    if($c803 == $listarr_short[$a]) {$echo = "selected";} else {$echo = "";}
    $list803 .= "<option value='$listarr_short[$a]' $echo>$listarr[$a]</option>";
}
$list804 = "";
for ($a=0;$a<count($listarr);$a++)
{
    if($c804 == $listarr_short[$a]) {$echo = "selected";} else {$echo = "";}
    $list804 .= "<option value='$listarr_short[$a]' $echo>$listarr[$a]</option>";
}
$list70 = "";
for ($a=0;$a<count($listarr);$a++)
{
    if($c70 == $listarr_short[$a]) {$echo = "selected";} else {$echo = "";}
    $list70 .= "<option value='$listarr_short[$a]' $echo>$listarr[$a]</option>";
}

if($count == 0)
{
    if($capArr[5] == '3ps')
    {
        $_GET['alert'] = 'threepoint';
    }
    else
    {
        if(in_array('prd',$capArr) && in_array('psd',$capArr))
        {
            $_GET['alert'] = 'defensecaps';
        }
        else
        {
            if((in_array($listarr_short[0],$capArr) || in_array($listarr_short[1],$capArr) || in_array($listarr_short[2],$capArr) || in_array($listarr_short[3],$capArr) || in_array($listarr_short[4],$capArr) || in_array($listarr_short[5],$capArr) || in_array($listarr_short[6],$capArr)) && (in_array($listarr_short[7],$capArr) || in_array($listarr_short[8],$capArr) || in_array($listarr_short[9],$capArr) || in_array($listarr_short[10],$capArr) || in_array($listarr_short[11],$capArr) || in_array($listarr_short[12],$capArr)))
            {
                if(count($capArr) == count($capArr2))
                {
                    if(($_POST['build'] == 'Freak' && (in_array($listarr_short[13],$capArr) || in_array($listarr_short[14],$capArr) || in_array($listarr_short[15],$capArr))) || $_POST['build'] == 'Standard')
                    {
                        if($build == 'Standard')
                        {
                            $checkquery = "SELECT * FROM players WHERE plast = '$plast' AND pfirst = '$pfirst' AND id != '$pid'";
                            $checkrows = mysqli_num_rows(mysqli_query($con,$checkquery));
                            if($checkrows > 0)
                            {
                                $_GET['alert'] = 'nameused';
                            }
                            else
                            {
                                $query = "UPDATE players SET plast='$plast',pfirst='$pfirst',position='$position',position2='$position2',build='$build',num='$num',height='$height',weight='$weight',country='$country',c90='$c90',c801='$c801',c802='$c802',c803='$c803',c804='$c804',c70='$c70' WHERE id='$pid'";
                                mysqli_query($con,$query);
                                header("location:/forms/player_edit.php?alert=success&pid=".$pid."");
                            }
                        }
                        else
                        {
                            $checkquery = "SELECT * FROM players WHERE plast = '$plast' AND pfirst = '$pfirst' AND id != '$pid'";
                            $checkrows = mysqli_num_rows(mysqli_query($con,$checkquery));
                            if($checkrows > 0)
                            {
                                $_GET['alert'] = 'nameused';
                            }
                            else
                            {
                                $query = "UPDATE players SET plast='$plast',pfirst='$pfirst',position='$position',position2='$position2',build='$build',num='$num',height='$height',weight='$weight',country='$country',c90='$c90',c801='$c801',c802='$c802',c803='$c803',c804='$c804',c70='$c70' WHERE id='$pid'";
                                mysqli_query($con,$query);
                                header("location:/forms/player_edit.php?alert=success&pid=".$pid."");
                            } 
                        }
                    }
                    else
                    {
                        $_GET['alert'] = 'freak';
                    }
                }
                else
                {
                    $_GET['alert'] = 'dupes';
                    $d = count($capArr);
                    $e = count($capArr2);
                }
            }
            else
            {
                $_GET['alert'] = 'offanddef';
            }
        }
    }
}
else
{

}
if(isset($_GET['alert'])) 
{
    if ($_GET['alert'] == 'playerexists') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You already have an NCAA player.</strong></div>";
    }
    else if ($_GET['alert'] == 'threepoint') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You cannot have Three Point as a 90 Cap or a 70 Cap.</strong></div>";
    }
    else if ($_GET['alert'] == 'defensecaps') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You cannot have both Perimeter Defense and Post Defense capped.</strong></div>";
    }
    else if ($_GET['alert'] == 'offanddef') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You must have at least one Offensive and Defensive attribute capped each.</strong></div>";
    }
    else if ($_GET['alert'] == 'freak') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Freaks must have at least one physical attribute capped.</strong></div>";
    }
    else if ($_GET['alert'] == 'dupes') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You cannot use an attribute for more than one weakness or strength.$d $e</strong></div>";
    }
    else if ($_GET['alert'] == 'nameused') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>There is already a player with this name.</strong></div>";
    }
    else if ($_GET['alert'] == 'success') 
    {
        $alert = "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Your player has been created successfully!</strong></div>";
    }
}
else
{$alert = '';}

?>
<!-- Body Start -->
<body>
<div id="wrapper" style="position:relative;top:0;left:0;">
    <?php include $path.'/includes/nav.php'; ?>
    <div class="container">
        <?=HeaderImagePrint($hostlg,$_SESSION['style'])?>
        <?=$alert?>
        <div class="row">
            <div class="panel panel-primary">
                <div class="panel-heading">Create A Player</div>
                <div class="panel-body">
                    <div class="panel-group col-xs-12">
                        <div class="panel-group col-xs-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading"><h3 class="panel-title" align='center'><a data-toggle="collapse" href="#instructions">Instructions (Click to Reveal)</a></h3></div>
                                <div id="instructions" class="panel-collapse collapse">
                                    <ul class="list-group">
                                        <li class="list-group-item">Your 90 cap and your 70 cap will be considered your weaknesses. Weaknesses are attributes that regress at a significantly higher rate than any other attribute.</li>
                                        <li class="list-group-item">Strengths will not play a part for you until regression. These attributes will regress at a significantly lower rate than any other attribute. Freaks get one Strength, Standard gets two.</li>
                                        <li class="list-group-item">3PT Shot cannot be a weakness, due to it being an adjustable rating (can still be an 80 cap).</li>
                                        <li class="list-group-item">You may only choose Perimeter Defense or Post Defense as a capped attribute. Not both.</li>
                                        <li class="list-group-item">You must have at least one Offensive Attribute and one Defensive Attribute capped. Additionally, the Freak build requires a Physical Attribute to be capped. You may find a list of Offensive, Defensive, and Physical Attributes <a href='https://sba.today/forums/index.php?/topic/21128-new-player-handbook/&do=findComment&comment=379724' target='_blank'>here</a>.</li>
                                        <li class="list-group-item">Standard Flex Archetypes must select a primary position and a secondary position. Secondary position must meet height requirements and can only be one position up or down from your primary. Ex. you cannot have a SF secondary position as a PG.</li>
                                        <li class="list-group-item">Player may make changes to their archetype once before entering the SBA Draft.</li>
                                        <li class="list-group-item">If you are a Freak, list your secondary position as the same position as your primary.</li>
                                        <li class="list-group-item">When you change you height or position, your height will default back to 5'0. Please remember to change your height if you change one of these values.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel-group col-xs-12">
                            <form class="form-horizontal" method='POST'>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="pfirst">First Name:</label>
                                    <div class="col-xs-4">
                                        <input type="text" class="form-control" name="pfirst" value="<?=$pfirst?>" required>
                                    </div>
                                    <div class="col-xs-5"></div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="plast">Last Name:</label>
                                    <div class="col-xs-4">
                                        <input type="text" class="form-control" name="plast" value="<?=$plast?>" required>
                                    </div>
                                    <div class="col-xs-5"></div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="position">Primary Position:</label>
                                    <div class="col-xs-4">
                                        <select class="form-control" name="position" id="position" value="<?=$position?>" onchange="playerHeight('build','position','position2')" required>
                                            <?=$listpos?>
                                        </select>
                                    </div>
                                    <div class="col-xs-5"></div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="position2">Secondary Position:</label>
                                    <div class="col-xs-4">
                                        <select class="form-control" name="position2" id="position2" value="<?=$position2?>" onchange="playerHeight('build','position','position2')" required>
                                            <?=$listpos2?>
                                        </select>
                                    </div>
                                    <div class="col-xs-5"></div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="build">Build:</label>
                                    <div class="col-xs-4">
                                        <select class="form-control" name="build" id="build" value="<?=$build?>" onchange="playerHeight('build','position','position2')" required>
                                            <option value="Standard">Standard</option>
                                            <option value="Freak">Freak</option>
                                        </select>
                                    </div>
                                    <div class="col-xs-5"></div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="num">Number:</label>
                                    <div class="col-xs-4">
                                        <input type="text" class="form-control" name="num" value="<?=$num?>" required>
                                    </div>
                                    <div class="col-xs-5"></div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="height">Height(inches):</label>
                                    <div class="col-xs-4" id="divheight">
                                        <select class='form-control' name='height' id='height'>
                                            <?=$listheight?>
                                        </select>
                                    </div>
                                    <div class="col-xs-5"></div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="weight">Weight(pounds):</label>
                                    <div class="col-xs-4">
                                        <input type="text" class="form-control" name="weight" value="<?=$weight?>" required>
                                    </div>
                                    <div class="col-xs-5"></div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="country">Country:</label>
                                    <div class="col-xs-4">
                                        <select class='form-control' name='country' required>
                                            <?=$listcountry?>
                                        </select>
                                    </div>
                                    <div class="col-xs-5"></div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="c90">90 Cap:</label>
                                    <div class="col-xs-4">
                                        <select class='form-control' name='c90' required>
                                            <?=$list90?>
                                        </select>
                                    </div>
                                    <div class="col-xs-5"></div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="c70">70 Cap:</label>
                                    <div class="col-xs-4">
                                        <select class='form-control' name='c70' required>
                                            <?=$list70?>
                                        </select>
                                    </div>
                                    <div class="col-xs-5"></div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="c801">80 Cap:</label>
                                    <div class="col-xs-4">
                                        <select class='form-control' name='c801' required>
                                            <?=$list801?>
                                        </select>
                                    </div>
                                    <div class="col-xs-5"></div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="c802">80 Cap:</label>
                                    <div class="col-xs-4">
                                        <select class='form-control' name='c802' required>
                                            <?=$list802?>
                                        </select>
                                    </div>
                                    <div class="col-xs-5"></div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="c803">80 Cap:</label>
                                    <div class="col-xs-4">
                                        <select class='form-control' name='c803' required>
                                            <?=$list803?>
                                        </select>
                                    </div>
                                    <div class="col-xs-5"></div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" for="c804">80 Cap:</label>
                                    <div class="col-xs-4">
                                        <select class='form-control' name='c804' required>
                                            <?=$list804?>
                                        </select>
                                    </div>
                                    <div class="col-xs-5"></div>
                                </div>
                                <div class="form-group">
                                    <input type="text" name="pid" value="<?=$pid?>" hidden>
                                    <a href="" class="btn btn-primary" data-toggle="modal" data-target="#warning" onclick="submit()">Submit</a>
                                </div>
                                <div class='modal fade' id='warning' role='dialog'>
                                    <div class='modal-dialog modal-lg'>
                                        <div class='modal-content'>
                                            <div class='modal-header'>
                                                <button type='button' class='close' data-dismiss='modal'>&times;</button>
                                                <h4 class='modal-title'>Create Player</h4>
                                            </div>
                                            <div class='modal-body'>
                                                <p>This will create this player as is. Please check the following information to ensure its accuracy:</p>
                                                <label class="control-label col-xs-3" for="warningheight">Height(inches):</label>
                                                <p class="form-control-static" name="warningheight" id='warningheight'></p>
                                                <p>If this information is not correct, please press Close and correct the information before proceeding.</p>
                                            </div>
                                            <div class='modal-footer'>
                                                <button type='submit' class='btn btn-danger'>Create Player</button>
                                                <button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>                 
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Body End -->
<?php
include $path.'/footer.php';
?>
<script>
function submit()
{
    document.getElementById('warningheight').innerHTML = document.getElementById('height').value;
}
function playerHeight(build,position,position2)
{
    a = document.getElementById('build').value;
    b = document.getElementById('position').value;
    c = document.getElementById('position2').value;
    d = '';
    if(a == 'Standard')
    {
        if(b == 'PG' || c == 'PG')
        {
            d = '1';
        }
        else if(b == 'SG' || c == 'SG')
        {
            d = '2';
        }
        else if(b == 'SF' || c == 'SF')
        {
            d = '3';
        }
        else if(b == 'PF' || c == 'PF')
        {
            d = '4';
        }
        else
        {
            d = '5';
        }
        switch(d)
        {
            case '1':
                document.getElementById('divheight').innerHTML = "<select class='form-control' name='height'><option>5'0</option><option>5'1</option><option>5'2</option><option>5'3</option><option>5'4</option><option>5'5</option><option>5'6</option><option>5'7</option><option>5'8</option><option>5'9</option><option>5'10</option><option>5'11</option><option>6'0</option><option>6'1</option><option>6'2</option><option>6'3</option><option>6'4</option><option>6'5</option></select>";
                break;
            case '2':
                document.getElementById("divheight").innerHTML = "<select class='form-control' name='height'><option>5'0</option><option>5'1</option><option>5'2</option><option>5'3</option><option>5'4</option><option>5'5</option><option>5'6</option><option>5'7</option><option>5'8</option><option>5'9</option><option>5'10</option><option>5'11</option><option>6'0</option><option>6'1</option><option>6'2</option><option>6'3</option><option>6'4</option><option>6'5</option><option>6'6</option><option>6'7</option></select>";
                break;
            case '3':
                document.getElementById('divheight').innerHTML = "<select class='form-control' name='height'><option>5'0</option><option>5'1</option><option>5'2</option><option>5'3</option><option>5'4</option><option>5'5</option><option>5'6</option><option>5'7</option><option>5'8</option><option>5'9</option><option>5'10</option><option>5'11</option><option>6'0</option><option>6'1</option><option>6'2</option><option>6'3</option><option>6'4</option><option>6'5</option><option>6'6</option><option>6'7</option><option>6'8</option><option>6'9</option></select>";
                break;
            case '4':
                document.getElementById('divheight').innerHTML = "<select class='form-control' name='height'><option>5'0</option><option>5'1</option><option>5'2</option><option>5'3</option><option>5'4</option><option>5'5</option><option>5'6</option><option>5'7</option><option>5'8</option><option>5'9</option><option>5'10</option><option>5'11</option><option>6'0</option><option>6'1</option><option>6'2</option><option>6'3</option><option>6'4</option><option>6'5</option><option>6'6</option><option>6'7</option><option>6'8</option><option>6'9</option><option>6'10</option><option>6'11</option></select>";
                break;
            case '5':
                document.getElementById('divheight').innerHTML = "<select class='form-control' name='height'><option>5'0</option><option>5'1</option><option>5'2</option><option>5'3</option><option>5'4</option><option>5'5</option><option>5'6</option><option>5'7</option><option>5'8</option><option>5'9</option><option>5'10</option><option>5'11</option><option>6'0</option><option>6'1</option><option>6'2</option><option>6'3</option><option>6'4</option><option>6'5</option><option>6'6</option><option>6'7</option><option>6'8</option><option>6'9</option><option>6'10</option><option>6'11</option><option>7'0</option><option>7'1</option></select>";
                break;
        }
    }
    else
    {
        switch(b)
        {
            case 'PG':
                document.getElementById('divheight').innerHTML = "<select class='form-control' name='height'><option>5'0</option><option>5'1</option><option>5'2</option><option>5'3</option><option>5'4</option><option>5'5</option><option>5'6</option><option>5'7</option><option>5'8</option><option>5'9</option><option>5'10</option><option>5'11</option><option>6'0</option><option>6'1</option><option>6'2</option><option>6'3</option><option>6'4</option><option>6'5</option><option>6'6</option><option>6'7</option><option>6'8</option></select>";
                break;
            case 'SG':
                document.getElementById("divheight").innerHTML = "<select class='form-control' name='height'><option>5'0</option><option>5'1</option><option>5'2</option><option>5'3</option><option>5'4</option><option>5'5</option><option>5'6</option><option>5'7</option><option>5'8</option><option>5'9</option><option>5'10</option><option>5'11</option><option>6'0</option><option>6'1</option><option>6'2</option><option>6'3</option><option>6'4</option><option>6'5</option><option>6'6</option><option>6'7</option><option>6'8</option><option>6'9</option><option>6'10</option></select>";
                break;
            case 'SF':
                document.getElementById('divheight').innerHTML = "<select class='form-control' name='height'><option>5'0</option><option>5'1</option><option>5'2</option><option>5'3</option><option>5'4</option><option>5'5</option><option>5'6</option><option>5'7</option><option>5'8</option><option>5'9</option><option>5'10</option><option>5'11</option><option>6'0</option><option>6'1</option><option>6'2</option><option>6'3</option><option>6'4</option><option>6'5</option><option>6'6</option><option>6'7</option><option>6'8</option><option>6'9</option><option>6'10</option><option>6'11</option><option>7'0</option></select>";
                break;
            case 'PF':
                document.getElementById('divheight').innerHTML = "<select class='form-control' name='height'><option>5'0</option><option>5'1</option><option>5'2</option><option>5'3</option><option>5'4</option><option>5'5</option><option>5'6</option><option>5'7</option><option>5'8</option><option>5'9</option><option>5'10</option><option>5'11</option><option>6'0</option><option>6'1</option><option>6'2</option><option>6'3</option><option>6'4</option><option>6'5</option><option>6'6</option><option>6'7</option><option>6'8</option><option>6'9</option><option>6'10</option><option>6'11</option><option>7'0</option><option>7'1</option><option>7'2</option></select>";
                break;
            case 'C':
                document.getElementById('divheight').innerHTML = "<select class='form-control' name='height'><option>5'0</option><option>5'1</option><option>5'2</option><option>5'3</option><option>5'4</option><option>5'5</option><option>5'6</option><option>5'7</option><option>5'8</option><option>5'9</option><option>5'10</option><option>5'11</option><option>6'0</option><option>6'1</option><option>6'2</option><option>6'3</option><option>6'4</option><option>6'5</option><option>6'6</option><option>6'7</option><option>6'8</option><option>6'9</option><option>6'10</option><option>6'11</option><option>7'0</option><option>7'1</option><option>7'2</option><option>7'3</option><option>7'4</option></select>";
                break;
        }
    }
}
</script>
