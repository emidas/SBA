<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
$user=$_SESSION['user'];
$memberid=$_SESSION['memberid'];
$echo = '';

// SBA and NCAA Data
$sql = "SELECT t.name as name,t.abr as abr,sum(tpe) as sumt,round(avg(tpe),2) as avgt, sum(ape) as suma,round(avg(ape),2) as avga,t.id as tid
FROM players p
INNER JOIN teams t ON p.team = t.id
WHERE p.league = ? AND p.team != '0' AND (active = '1' OR active ='3')
GROUP BY team
ORDER BY sumt";
$stmt = mysqli_stmt_init($con);
$lg1 = $mainlg;
$lg2 = $sublg;
if (mysqli_stmt_prepare($stmt, $sql))
{
    mysqli_stmt_bind_param($stmt,'s',$lg1);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    mysqli_stmt_bind_param($stmt,'s',$lg2);
    mysqli_stmt_execute($stmt);
    $result2 = mysqli_stmt_get_result($stmt);
    mysqli_stmt_close($stmt);
}

// FIBA Data
$sql = "SELECT t.name as name,t.abr as abr,sum(tpe) as sumt,round(avg(tpe),2) as avgt, sum(ape) as suma,round(avg(ape),2) as avga
FROM players p
INNER JOIN teams t ON p.country = t.name
WHERE (active ='1' || active = '3') AND p.league = 'SBA'
GROUP BY name
ORDER BY sumt";
$result3 = mysqli_query($con,$sql);

// SBA Tab
$sbatable = '';
while ($row = mysqli_fetch_array($result)) 
{
    $team = $row['tid'];
    $sql = "SELECT sum(tpe) as sumt,round(avg(tpe),2) as avgt, sum(ape) as suma,round(avg(ape),2) as avga
            FROM (SELECT tpe,ape FROM players p
            INNER JOIN teams t ON p.team = t.id
            WHERE p.league = 'sba' AND p.team = '$team' AND (active = '1' OR active ='3') ORDER BY TPE DESC LIMIT 5) as sub";
    $row2 = mysqli_fetch_array(mysqli_query($con,$sql));
    $sbatable .= "<tr>";
        $sbatable .= "<td><span class='hidden-xs'>".$row['name']."</span><span class='hidden-sm hidden-md hidden-lg'>".$row['abr']."</span></td>";
        $sbatable .= "<td>".$row2['sumt']."</td>";
        $sbatable .= "<td>".$row2['avgt']."</td>";
        $sbatable .= "<td>".$row2['suma']."</td>";
        $sbatable .= "<td>".$row2['avga']."</td>";
        $sbatable .= "<td>".$row['sumt']."</td>";
        $sbatable .= "<td>".$row['avgt']."</td>";
        $sbatable .= "<td>".$row['suma']."</td>";
        $sbatable .= "<td>".$row['avga']."</td>";
    $sbatable .= "</tr>";
}

// SBDL Tab
$sbdltable = '';
while ($row = mysqli_fetch_array($result2)) 
{
    $sbdltable .= "<tr>";
        $sbdltable .= "<td><span class='hidden-xs'>".$row['name']."</span><span class='hidden-sm hidden-md hidden-lg'>".$row['abr']."</span></td>";
        $sbdltable .= "<td>".$row['sumt']."</td>";
        $sbdltable .= "<td>".$row['avgt']."</td>";
        $sbdltable .= "<td>".$row['suma']."</td>";
        $sbdltable .= "<td>".$row['avga']."</td>";
    $sbdltable .= "</tr>";
}

// FIBA Tab
$fibatable = '';
while ($row = mysqli_fetch_array($result3)) 
{
    $fibatable .= "<tr>";
        $fibatable .= "<td><span class='hidden-xs'>".$row['name']."</span><span class='hidden-sm hidden-md hidden-lg'>".$row['abr']."</span></td>";
        $fibatable .= "<td>".$row['sumt']."</td>";
        $fibatable .= "<td>".$row['avgt']."</td>";
        $fibatable .= "<td>".$row['suma']."</td>";
        $fibatable .= "<td>".$row['avga']."</td>";
    $fibatable .= "</tr>";
}

?>
<!-- Body Start -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/colreorder/1.5.1/js/dataTables.colReorder.min.js"></script>
<body>
<div id="wrapper" style="position:relative;top:0;left:0;">
    <?php include $path.'/includes/nav.php'; ?>
    <div class="container">
        <?=HeaderImagePrint($hostlg,$_SESSION['style'])?>
        <?=$echo?>
        <div class="panel panel-primary">
            <div class="panel-heading">PE Index</div>
            <div class="panel-body">
                <ul class="nav nav-tabs" style="border-bottom:0px;">
                    <li class="active"><a data-toggle="tab" href="#sba">SBA</a></li>
                    <li><a data-toggle="tab" href="#sbdl">SBDL</a></li>
                    <li><a data-toggle="tab" href="#fiba">FIBA</a></li>
                </ul>
                <div class="tab-content">
                    <div id="sba" class="tab-pane fade in active">
                        <div class='table-responsive'>
                            <table class="table table-striped" id='viewsba' style="width:100%;">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th colspan="4" style="text-align:center;border-style:solid;border-width: 0px 1px;">Top 5</th>
                                        <th colspan="4" style="text-align:center;border-style:solid;border-width: 0px 1px 0px 0px;">Total</th>
                                    </tr>
                                    <tr>
                                        <th>Name</th>
                                        <th>TPE</th>
                                        <th>AVG</th>
                                        <th>APE</th>
                                        <th>AVG</th>
                                        <th>TPE</th>
                                        <th>AVG</th>
                                        <th>APE</th>
                                        <th>AVG</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?=$sbatable?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="sbdl" class="tab-pane fade">
                        <div class='table-responsive'>
                            <table class="table table-striped" id='viewsbdl' style="width:100%;">
                                <thead>
                                    <th>Name</th>
                                    <th>TPE</th>
                                    <th>AVG</th>
                                    <th>APE</th>
                                    <th>AVG</th>
                                </thead>
                                <tbody>
                                    <?=$sbdltable?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="fiba" class="tab-pane fade">
                        <div class='table-responsive'>
                            <table class="table table-striped" id='viewfiba' style="width:100%;">
                                <thead>
                                    <th>Name</th>
                                    <th>TPE</th>
                                    <th>AVG</th>
                                    <th>APE</th>
                                    <th>AVG</th>
                                </thead>
                                <tbody>
                                    <?=$fibatable?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Body End -->

<?php
include $path.'/footer.php';
?>
<style>
.table-responsive > .table {
    margin-bottom: 0;
}
.table-responsive > .table > thead > tr > th, .table-responsive > .table > tbody > tr > th, .table-responsive > .table > tfoot > tr > th, .table-responsive > .table > thead > tr > td, .table-responsive > .table > tbody > tr > td, .table-responsive > .table > tfoot > tr > td {
    white-space: nowrap;
}
</style>
<script>
$(document).ready(function() {
    $('#viewsba').DataTable( {
        paging:   false,
        select: true,
        order: [1,'desc'],
        columnDefs: 
        [
            {"orderSequence": ['desc','asc'], "targets": [1,2,3,4,5,6,7,8]}
        ]
    } );
    $('#viewsbdl').DataTable( {
        paging:   false,
        select: true,
        order: [1,'desc'],
        columnDefs: 
        [
            {"orderSequence": ['desc','asc'], "targets": [1,2,3,4]}
        ]
    } );
    $('#viewfiba').DataTable( {
        paging:   false,
        select: true,
        order: [1,'desc'],
        columnDefs: 
        [
            {"orderSequence": ['desc','asc'], "targets": [1,2,3,4]}
        ]
    } );
} );
$(document).ready(function(){
    $('[data-toggle="popover"]').popover(); 
});
</script>
<script>
    $('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>