<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
$user=$_SESSION['user'];

if(isset($_POST['season']))
{$season = $_POST['season'];} else {$season = '1';}
if(isset($_POST['league']))
{$league = $_POST['league'];} else {$league = 'sba';}
if(isset($_POST['type']))
{$type = $_POST['type'];} else {$type = 'season';}
if(isset($_POST['export']))
{$export = 1;} else {$export = 0;}

$yquery = "SELECT season FROM SBA_Years";
$yresult = mysqli_query($con,$yquery);
$yecho = '';
$echo = '';
while ($r = mysqli_fetch_array($yresult)) 
{
    if($season == $r['season']) {$echo = "selected";} else {$echo = "";}
    $yecho .= "<option value = '".$r['season']."' $echo>".$r['season']."</option>";
}
?>

<!-- Body Start -->
<body>
<div id="wrapper" style="position:relative;top:0;left:0;">
    <?php include $path.'/includes/nav.php'; ?>
    <div class="container-fluid">
        <?=HeaderImagePrint($hostlg,$_SESSION['style'])?>
        <form class="form-horizontal" method='POST'>
            <div class="panel-group">
                <div class="panel panel-primary">
                    <div class="panel-heading">Season Stats Filters</div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="league">Set League:</label>
                            <div class="col-xs-2">
                                <select name="league" class="form-control">
                                    <option value='sba' <?php if ($league == 'sba') echo "selected";?>>SBA</option>
                                    <option value='ncaa' <?php if ($league == 'ncaa') echo "selected";?>>NCAA</option>
                                    <option value='fiba' <?php if ($league == 'fiba') echo "selected";?>>FIBA</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="type">Set Type:</label>
                            <div class="col-xs-2">
                                <select name="type" class="form-control">
                                    <option value='season' <?php if ($type == 'season') echo "selected";?>>Regular Season</option>
                                    <option value='playoff' <?php if ($type == 'playoff') echo "selected";?>>Playoff</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="season">Season:</label>
                            <div class="col-xs-2">
                                <select name="season" class="form-control"><?=$yecho?></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <button type="submit" name="export" class="btn btn-primary">Export</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <?=printplayersingleseason($con,'%',$league,$type,$season,$export);?>
    </div>
</div>
<!-- Body End -->
<script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

<?php
include $path.'/footer.php';
?>
<style>
.table-responsive > .table {
    margin-bottom: 0;
}
.table-responsive > .table > thead > tr > th, .table-responsive > .table > tbody > tr > th, .table-responsive > .table > tfoot > tr > th, .table-responsive > .table > thead > tr > td, .table-responsive > .table > tbody > tr > td, .table-responsive > .table > tfoot > tr > td {
    white-space: nowrap;
}
table.dataTable thead .sorting, 
table.dataTable thead .sorting_asc, 
table.dataTable thead .sorting_desc {
    background : none;
}
</style>
<script>
$(document).ready(function() {
    $('#season').DataTable( {
        scrollX: true,
        paging:   false,
        select: true,
        columnDefs: 
        [
            {"orderSequence": ['asc','desc'], "targets": [0]},
            {"orderSequence": ['desc','asc'], "targets": '_all'}
        ]
    } );
} );
</script>