<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
$user=$_SESSION['user'];
if(isset($_POST['year']))
{$year = $_POST['year'];} else {$yr = "SELECT year from year WHERE isactive='1'";$yrq=mysqli_fetch_array(mysqli_query($con,$yr));$year=$yrq['year'];$year=$year-1;}
$echo = '';

$sql = "SELECT a.year,a.award,a.pid,a.tid,CONCAT(p.pfirst,' ',p.plast) as pname,t.name AS tname FROM award_winners a INNER JOIN players p ON a.pid=p.id INNER JOIN teams t ON a.tid=t.id WHERE year='$year'"; //$season
$result = mysqli_query($con,$sql);
$w = 0;
$awardecho = "<div class='row'>";
while($r = mysqli_fetch_array($result))
{
    if($w == '4')
    {
        $awardecho .= "</div><div class='row'>";
        $w = 0;
    }
    $awardecho .= "
    <div class='col-xs-3'>
        <div class='text-center'>
            <div class='award-header'>
                <h5>".strtoupper($r['award'])."</h5>
            </div>
            <div>
                <img src='/images/trophy.png' alt='Award image'>
                <p>".$r['pname']."<br>".$r['tname']."</p>
            </div>
        </div>
    </div>";
    $w++;
}
$awardecho .= "</div>";


// Generate season dropdown
$yquery = "SELECT year FROM year";
$yresult = mysqli_query($con,$yquery);
$yecho = '';
$echo = '';
while ($r = mysqli_fetch_array($yresult)) 
{
    if($year == $r['year']) {$echo = "selected";} else {$echo = "";}
    $yecho .= "<option value = '".$r['year']."' $echo>".$r['year']."</option>";
}

include $path.'/includes/head.php';  
?>
<body>
<div id="wrapper" style="position:relative;top:0;left:0;">
    <?php include $path.'/includes/nav.php'; ?>
    <div class="container">
        <?=HeaderImagePrint($hostlg,$_SESSION['style'])?>
        <?=$echo?>
        <!-- Season selector -->
        <div class="panel-group">
            <div class="panel panel-primary">
                <div class="panel-heading">Awards Filters</div>
                <div class="panel-body">
                    <!--<div class="form-group">
                        <label class="control-label col-sm-2" for="league">Set League:</label>
                        <div class="col-xs-2">
                            <select name="league" class="form-control">
                                <option value='sba' <?php if ($league == 'sba') echo "selected";?>>SBA</option>
                                <option value='ncaa' <?php if ($league == 'ncaa') echo "selected";?>>NCAA</option>
                                <option value='fiba' <?php if ($league == 'fiba') echo "selected";?>>FIBA</option>
                            </select>
                        </div>
                    </div>-->
                    <div class="panel-group col-xs-12">
                        <form class="form-horizontal" method="POST">
                            <div class="form-group">
                                <div class="input-group col-xs-12">
                                    <span class='input-group-addon hidden-xs' style='width:200px;text-align:left;'>Season</span>
                                    <span class='input-group-addon hidden-sm hidden-md hidden-lg' style='width:100px;text-align:left;'>Season</span>
                                    <select name="year" class="form-control">
                                        <?=$yecho?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary col-xs-12">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-group">
            <div class="panel panel-primary">
                <div class="panel-heading">Awards</div>
                <div class="panel-body">
                    <?=$awardecho?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Body End -->

<?php
include $path.'/footer.php';
?>