<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
$user=$_SESSION['user'];
$memberid=$_SESSION['memberid'];
$echo = '';
$lge = '';
$tname = '';
$attyheaders = '';
for ($a=0;$a<count($lgatty);$a++)
{
    $attyheaders .= "<th>".strtoupper($lgatty[$a])."</th>";
}

// Get Year
$system = new System();
$year = $system->get_year();
$season = "S".$year;

if(isset($_GET['tid']))
{
    $team = sanitize($con,$_GET['tid']);
    if($team != '0')
    {
        $sql = "SELECT t_fk,name,league FROM teams WHERE id='$team'";
        $sqlr=mysqli_fetch_array(mysqli_query($con,$sql));
        $tfk=$sqlr['t_fk'];
        $tname = $sqlr['name'];
        $lge=strtolower($sqlr['league']);
        if($hostlg == 'SBA')
        {
            $history = printfranchise($con,$tfk,$lge);
        }
        else
        {
            $history = '';
        }
    }
    else
    {
        $history = '';
    }
}
else{$team ='%';}
if($team == '0' && isset($_GET['league']))
{
    $league = sanitize($con,$_GET['league']);
    if($league == $mainlg)
    {
        $clause = "AND p.league='".$league."'";
    }
    else if($league == 'fiba')
    {
        $clause = "AND p.league='SBA'";
    }
    else
    {
        $clause = "AND p.league='".$league."' AND (created_time < DATE_SUB(NOW(),INTERVAL 12 HOUR))";
    }
}
else{$clause ='';}
if($team != '%' && $team != '0')
{
    $sql = "SELECT id FROM coaches WHERE id='$team' AND (name=\"$user\" || name2=\"$user\")";
    $numsult = mysqli_query($con,$sql);
    $num = mysqli_num_rows($numsult);
    if($num > 0)
    {
        $manage = '1';
    }
    else
    {
        $manage = '0';
    }
}
else if($team != '%' && $team == '0')
{
    $sql = "SELECT id FROM coaches WHERE (name=\"$user\" || name2=\"$user\") AND league='$league'";
    $numsult = mysqli_query($con,$sql);
    $num = mysqli_num_rows($numsult);
    if($num > 0)
    {
        $manage = '1';
    }
    else
    {
        $manage = '0';
    }
}
else
{
    $manage = '0';
}

// Initialize Team
$oteam = new Team($team);
$tsalary = $oteam->get_('team_salary');
$tsalary2 = $oteam->get_('team_salary2');
$tsalary3 = $oteam->get_('team_salary3');
$tcapspace = $oteam->get_('cap_space');

//SBA Team Dropdown
$query = "SELECT id,name FROM teams where league = '$mainlg' || league = 'FA' ORDER BY name ASC";
$result = mysqli_query($con,$query);
$sbateams = '';
$echo = '';
while ($r = mysqli_fetch_array($result)) 
{
    $sbateams .= "<option value = '".$r['id']."'>".$r['name']."</option>";
}

//SBDL Team Dropdown
$query = "SELECT id,name FROM teams where league = '$sublg' || league = 'FA' ORDER BY name ASC";
$result = mysqli_query($con,$query);
$sbdlteams = '';
$echo = '';
while ($r = mysqli_fetch_array($result)) 
{
    $sbdlteams .= "<option value = '".$r['id']."'>".$r['name']."</option>";
}

// Numyrs Dropdown
$numyrs = '';
$numyrarr = Array('0','1','2','3');
for ($a=0;$a<count($numyrarr);$a++)
{
    $numyrs .= "<option value = '".$numyrarr[$a]."'>".$numyrarr[$a]."</option>";
}

$sql = "SELECT t.name,t.league,c.name as coach,c.name2 as coach2 FROM teams t INNER JOIN coaches c ON t.id=c.id WHERE t.id='$team'";
$result = mysqli_fetch_array(mysqli_query($con,$sql));
$team_name = $result['name'] ?? '';
$team_league = $result['league'] ?? '';
$team_coach = $result['coach'] ?? '';
$team_coach2 = $result['coach2'] ?? '';

$sql = "SELECT year, award, CONCAT(pfirst,' ',plast) as pname
FROM players p
INNER JOIN award_winners s ON p.id=s.pid
WHERE tid = '$team'
ORDER BY year DESC";
$result = mysqli_query($con,$sql);
$awtable = '';
while($a = mysqli_fetch_array($result))
{
    $awtable .= "<tr>";
        $awtable .= "<td>".$a['year']."</td>";
        $awtable .= "<td>".strtoupper($a['award'])."</td>";
        $awtable .= "<td>".$a['pname']."</td>";
    $awtable .= "</tr>";
}

if($lge == 'fiba')
{
    $query = "SELECT CONCAT(plast,', ',pfirst) AS pname,u.username,CONCAT(pfirst,' ',plast) AS tname,position,position2,build,height,weight,p.league,p.active,p.id,p.user_fk,tpe,ape,ins,jps,ft,3ps,han,pas,orb,drb,psd,prd,stl,blk,fl,qkn,str,jmp,t.name AS team,p.league,experience,FORMAT(salary,0) as salary,u.memberid as toid,yrs_remain,u.active as uactive
    FROM auth_user u 
    INNER JOIN players p ON u.ID = p.user_fk 
    INNER JOIN teams t ON p.team = t.id
    INNER JOIN molholt_sba.core_members cm ON u.username = cm.name
    WHERE p.country ='$tname' AND (p.active ='1' || p.active = '2' || p.active = '3') AND (p.league = 'SBA')
    ORDER BY pname";
}
else if($hostlg == 'EFL')
{
    $query = "SELECT CONCAT(plast,', ',pfirst) AS pname,u.username,CONCAT(pfirst,' ',plast) AS tname,position,position2,build,height,weight,p.league,p.active,p.id,p.user_fk,tpe,ape,str,agi,arm,`int`,acc,tkl,spd,han,rbk,pbk,kdi,kac,t.name AS team,p.league,experience,FORMAT((salary+salary_bonus),0) as salary,u.memberid as toid,yrs_remain,u.active as uactive
    FROM auth_user u 
    INNER JOIN players p ON u.ID = p.user_fk 
    INNER JOIN teams t ON p.team = t.id
    WHERE p.team ='$team' AND (p.active ='1' || p.active = '2' || p.active = '3') $clause
    ORDER BY pname";
}
else
{
    $query = "SELECT CONCAT(plast,', ',pfirst) AS pname,u.username,CONCAT(pfirst,' ',plast) AS tname,position,position2,build,height,weight,p.league,p.active,p.id,p.user_fk,tpe,ape,ins,jps,ft,3ps,han,pas,orb,drb,psd,prd,stl,blk,fl,qkn,str,jmp,t.name AS team,p.league,experience,FORMAT((salary+salary_bonus),0) as salary,u.memberid as toid,yrs_remain,u.active as uactive
    FROM auth_user u 
    INNER JOIN players p ON u.ID = p.user_fk 
    INNER JOIN teams t ON p.team = t.id
    INNER JOIN molholt_sba.core_members cm ON u.username = cm.name
    WHERE p.team ='$team' AND (p.active ='1' || p.active = '2' || p.active = '3') $clause
    ORDER BY pname";
}
$result = mysqli_query($con,$query);
$playercount = mysqli_num_rows($result);
$ptable = '';
$atable = '';
$stable = '';
$f = '0';
while ($row = mysqli_fetch_array($result)) 
{
    if(($manage == '1' || $_SESSION['isadmin'] == '1') && ($row['league'] == $mainlg))
    {
        $managebuttons = "<li><a href='#' onclick=\"addcash('".$row['id']."','".$team."','".$row['league']."')\" class='btn btn-primary'>Add Cash <span class='badge'>HC</span></a></li><li><a href='#' onclick=\"newteam('".$row['id']."','".$team."','".$row['league']."')\" class='btn btn-primary'>Change Team <span class='badge'>HC</span></a></li>";
    }
    else if(($manage == '1' || $_SESSION['isadmin'] == '1') && ($row['league'] == $sublg))
    {
        $managebuttons = "<li><a href='#' onclick=\"addcash('".$row['id']."','".$team."','".$row['league']."')\" class='btn btn-primary'>Add Cash <span class='badge'>HC</span></a></li><li><a href='#' onclick=\"newteam('".$row['id']."','".$team."','".$row['league']."')\" class='btn btn-primary'>Change Team <span class='badge'>HC</span></a></li><li><button class='btn btn-primary' disabled style='width:100%;'>Redshirt Freshman <span class='badge'>HC</span></button></li>";
    }
    else
    {
        $managebuttons = "<li><button class='btn btn-primary' disabled style='width:100%;'>Add Cash <span class='badge'>GM/HC</span></button></li><li><button class='btn btn-primary' disabled style='width:100%;'>Change Team <span class='badge'>GM/HC</span></button></li>";
    }
    $alltable = "<tr>";
        $alltable .= "<td><div class='dropdown'><a href='#' class='dropdown-toggle' data-toggle='dropdown'>".$row['pname']." ".(($row['uactive'] == '0') ? "<span class='badge'>IA</span>" : "")."<span class='caret'></span></a><ul class='dropdown-menu'><li><a href='player_page.php?pid=".$row['id']."' class='btn btn-primary'>Player Page</a></li><li><a href='view_updates.php?pid=".$row['id']."' class='btn btn-primary'>Update History</a></li>".$managebuttons."</ul></div></td>";
        $ptable .= $alltable;
        $atable .= $alltable;
        $alltable = "<td><div class='dropdown'><a href='#' class='dropdown-toggle' data-toggle='dropdown'>".$row['username']."<span class='caret'></span></a><ul class='dropdown-menu'><li><a href='view_players.php?user=".$row['username']."' class='btn btn-primary'>View Players</a></li><li><a href='#' onclick=\"sendpm('".$row['username']."','".$row['toid']."','".$memberid."')\" class='btn btn-primary'>PM Player</a></li></ul></div></td>";
        $ptable .= $alltable;
        $ptable .= "<td>".$row['position'];
            if($row['position2'] != ''){$ptable .= "/".$row['position2'];}else{}
        $ptable .= "</td>";
        $ptable .= "<td>".$row['tpe']."</td>";
        $ptable .= "<td>".$row['ape']."</td>";
        $ptable .= "<td>".$row['experience']."</td>";
        for ($a=0;$a<count($lgatty);$a++)
        {
            $atable .= "<td>".$row[$lgatty[$a]]."</td>";
        }
    $ptable .= "</tr>";
    $atable .= "</tr>";
    $f++;
}
if($team_league == 'SBA')
{
    $cap = $system->get_salary_cap();
    $stable2 = "
    <table class='table table-striped'>
        <thead>
            <th></th>
            <th></th>
        </thead>
        <tbody>
            <tr>
                <td>Salary Cap:</td><td>$".number_format($cap,0)."</td>
            </tr>
            <tr>
                <td>Cap Used:</td><td>$".number_format($tsalary,0)."</td>
            </tr>
            <tr>
                <td>Cap Space:</td><td>$".number_format($tcapspace,0)."</td>
            </tr>
        </tbody>
    </table>";
}
else
{
    $stable2 = '';
}
// Expenses
$cctable = '';
$sql = "SELECT t.name,CONCAT(plast,', ',pfirst) as pname,CONCAT(pfirst,' ',plast) as pname2,c.type,c.years,c.salary,c.salary2,c.salary3,c.opt,c.yrs_remain,p.active,p.id as pid,c.id as cid,c.status
FROM players p
INNER JOIN contracts c ON p.id=c.p_fk
INNER JOIN teams t ON c.t_fk=t.id
WHERE c.t_fk = '$team' AND (status = '2' || status = '3')
ORDER BY CASE WHEN (type='Free Agent' || (type='Extension' && status='3')) THEN 1
WHEN type='Bonus' THEN 2
WHEN type='Filler' THEN 3
WHEN (type='Extension' && status='2') THEN 4
WHEN type='Waived' THEN 5
END, salary DESC";
$result = mysqli_query($con,$sql);
$cctable2 = '';
while($r = mysqli_fetch_array($result))
{
    if($r['type'] == 'Extension' && $r['status'] == 2)
    {
        $cctable .= "
        <tr>
            <td>".$r['pname']."</td>
            <td>".ucwords($r['type'])."</td>
            <td>".$r['years']."</td>
            <td>".$r['yrs_remain']."</td>
            <td>-</td>
            <td>$".number_format($r['salary'],0)."</td>
            <td>$".number_format($r['salary2'],0)."</td>
            <td>".ucwords($r['opt'])."</td>
            <td></td>
        </tr>";
    }
    else
    {
        $cctable .= "
        <tr>
            <td>".$r['pname']."</td>
            <td>".ucwords($r['type'])."</td>
            <td>".$r['years']."</td>
            <td>".$r['yrs_remain']."</td>
            <td>$".number_format($r['salary'],0)."</td>
            <td>$".number_format($r['salary2'],0)."</td>
            <td>$".number_format($r['salary3'],0)."</td>
            <td>".ucwords($r['opt'])."</td>
            <td></td>
        </tr>";
    }
}
$cctable2 .= "<tr><td colspan='4'></td><td>$".number_format($tsalary,0)."</td><td>$".number_format($tsalary2,0)."</td><td>$".number_format($tsalary3,0)."</td><td colspan='2'></td></tr>";

// Expenses
$etable = '';
$sql = "SELECT CONCAT(plast,', ',pfirst) as pname, e.cash,e.task
FROM players p 
INNER JOIN player_earn_history e ON p.id=e.pid_fk
INNER JOIN teams t ON p.team=t.id
WHERE e.task LIKE '%$season%'
AND t.id = '$team'
ORDER BY CASE WHEN task='$season SBA Salary' THEN 1
WHEN task='$season SBDL Scholarship' THEN 2
WHEN task='$season Contract Bonus' THEN 3
WHEN task='$season FIBA Payout' THEN 4
ELSE 5 END, cash desc";
$result = mysqli_query($con,$sql);
while($r = mysqli_fetch_array($result))
{
    $etable .= "</tr>";
        $etable .= "<td>".$r['pname']."</td>";
        $etable .= "<td>".number_format($r['cash'],0)."</td>";
        $etable .= "<td>".$r['task']."</td>";
    $etable .= "</tr>";
}

$alert = '';
if($manage == '1' && $playercount < 10 && $team_league == 'SBA')
{
    $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You do not have 10 players signed to your roster. Please pick up additional players to meet the roster minimum.</strong></div>";
}

?>

<!-- Body Start -->
<body>
<div id="wrapper" style="position:relative;top:0;left:0;">
    <?php include $path.'/includes/nav.php'; ?>
    <div class="container">
        <?=HeaderImagePrint($hostlg,$_SESSION['style'])?>
        <?=$alert?>
        <div class="panel panel-primary">
            <div class="panel-heading"><?=$team_name?> Team Page<br>Coach: <?=$team_coach?><br>Assistant: <?=$team_coach2?></div>
            <div class="panel-body">
                <img src='/images/team-images/<?=$team?>.png' style='height:50px;width:50px;'>
                <ul class="nav nav-tabs" style="border-bottom:0px;">
                    <li class="active"><a data-toggle="tab" href="#home">Roster</a></li>
                    <li><a data-toggle="tab" href="#attributes">Attributes</a></li>
                    <li><a data-toggle="tab" href="#finances">Finances</a></li>
                    <li><a data-toggle="tab" href="#awards">Awards</a></li>
                    <li><a data-toggle="tab" href="#history">History</a></li>
                </ul>
                <div class="tab-content">
                    <div id="home" class="tab-pane fade in active">
                        <div class='table-responsive'>
                            <table class="table table-striped" id='viewsba' style="width:100%;">
                                <thead>
                                    <th>Name</th>
                                    <th>Username</th>
                                    <th>Position</th>
                                    <th>TPE</th>
                                    <th>APE</th>
                                    <th>Exp.</th>
                                </thead>
                                <tbody>
                                    <?=$ptable?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="attributes" class="tab-pane fade">
                        <div class='table-responsive'>
                            <table class="table compact" id='viewsba2' style="width:100%;">
                                <thead>
                                    <th>Name</th>
                                    <?=$attyheaders?>
                                </thead>
                                <tbody>
                                    <?=$atable?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="finances" class="tab-pane fade">
                        <div class='table-responsive'>
                            <table class="table">
                                <thead>
                                    <th>Name</th>
                                    <th>Type</th>
                                    <th>Years</th>
                                    <th>Years Left</th>
                                    <th>Year 1 Salary</th>
                                    <th>Year 2 Salary</th>
                                    <th>Year 3 Salary</th>
                                    <th>Option</th>
                                </thead>
                                <tbody>
                                    <?=$cctable?>
                                </tbody>
                                <tfoot>
                                    <?=$cctable2?>
                                </tfoot>
                            </table>
                        </div>
                        <?=$stable2?>
                    </div>
                    <div id="awards" class="tab-pane fade">
                        <div class='table-responsive'>
                            <table class="table table-striped" id='viewsba3' style="width:100%;">
                                <thead>
                                    <th>Season</th>
                                    <th>Award</th>
                                    <th>Player</th>
                                </thead>
                                <tbody>
                                    <?=$awtable?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="history" class="tab-pane fade">
                        <?=$history?>
                    </div>
                </div>
            </div>
        </div>
        <div class='modal fade' id='PM' role='dialog'>
            <div class='modal-dialog modal-lg'>
                <div class='modal-content'>
                    <form method='POST' action='send_pm.php'>
                        <div class='modal-content'>
                            <div class='modal-header'>
                                <button type='button' class='close' data-dismiss='modal'>&times;</button>
                                <h4 class='modal-title'>PM Player</h4>
                            </div>
                            <div class='modal-body'>
                                <p>To: <span id='toname'></span></p>
                                Title:<input type='text' name='title'><br><br>
                                <textarea class='form-control' rows='5' name='pm' id='pm'></textarea>
                                <input type='text' hidden='hidden' name='toid' id='toid' value=''>
                                <input type='text' hidden='hidden' name='fromid' id='fromid' value=''>
                            </div>
                            <div class='modal-footer'>
                                <button type='submit' class='btn btn-danger'>Send PM</button>
                                <button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class='modal fade' id='addcash' role='dialog'>
            <div class='modal-dialog modal-lg'>
                <div class='modal-content'>
                    <form action='add_cash.php' method='POST'>
                        <div class='modal-header'>
                            <button type='button' class='close' data-dismiss='modal'>&times;</button>
                            <h4 class='modal-title'>Add Cash</h4>
                        </div>
                        <div class='modal-body'>
                            <div id='cashoption'></div>
                            <div class='form-group'>
                                <div class='input-group'>
                                    <span class='input-group-addon' style='background-color:#337ab7;color:white;width:120px;text-align:left;'>Reason</span>
                                    <select class='form-control' name='reason'>
                                        <option>Contract Bonus</option>
                                        <option>FIBA Payout</option>
                                        <option><?=$sublg?> Scholarship</option>
                                        <option>Other</option>
                                        <option><?=$mainlg?> Salary</option>
                                    </select>
                                </div>
                            </div>
                            <input type='text' name='pid' id='addcashpid' value='' hidden>
                            <input type='text' name='tid' id='addcashtid' value='' hidden>
                            <input type='text' name='league' id='addcashleague' value='' hidden>
                        </div>
                        <div class='modal-footer'>
                            <button type='submit' class='btn btn-primary'>Submit</button>
                            <button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class='modal fade' id='newteam' role='dialog'>
            <div class='modal-dialog modal-lg'>
                <div class='modal-content'>
                    <form action='change_team.php' method='post'>
                        <div class='modal-header'>
                            <button type='button' class='close' data-dismiss='modal'>&times;</button>
                            <h4 class='modal-title'>Change Team</h4>
                        </div>
                        <div class='modal-body'>
                            <div class='form-group'>
                                <div class='input-group'><span class='input-group-addon' style='width:120px;text-align:left;'>New Team:</span>
                                    <select class='form-control' name='team' id='newteamteam'>
                                        
                                    </select>
                                </div>
                            </div>
                            <div class='form-group'>
                                <div class='input-group'><span class='input-group-addon' style='width:120px;text-align:left;'>Years</span>
                                    <select class='form-control' name='numyrs'>
                                        <?=$numyrs?>
                                    </select>
                                </div>
                            </div>
                            <input type='text' name='pid' id='newteampid' value='' hidden>
                            <input type='text' name='tid' id='newteamtid' value='' hidden>
                            <input type='text' name='league' id='newteamleague' value='' hidden>
                        </div>
                        <div class='modal-footer'>
                            <button type='submit' class='btn btn-primary'>Submit</button>
                            <button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Body End -->
<script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

<?php
include $path.'/footer.php';
mysqli_close($con);
?>
<style>
.table-responsive > .table {
    margin-bottom: 0;
}
.table-responsive > .table > thead > tr > th, .table-responsive > .table > tbody > tr > th, .table-responsive > .table > tfoot > tr > th, .table-responsive > .table > thead > tr > td, .table-responsive > .table > tbody > tr > td, .table-responsive > .table > tfoot > tr > td {
    white-space: nowrap;
}
table.dataTable thead .sorting, 
table.dataTable thead .sorting_asc, 
table.dataTable thead .sorting_desc {
    background : none;
}
</style>
<script>
var mainlg = '<?=$mainlg?>';
var sublg = '<?=$sublg?>';
function newteam(pid,tid,league)
{
    if(league == sublg)
    {
        $('#newteamteam').html("<?=$sbdlteams?>");
    }
    if(league == mainlg)
    {
        $('#newteamteam').html("<?=$sbateams?>");
    }
    $('#newteampid').val(pid);
    $('#newteamtid').val(tid);
    $('#newteamleague').val(league);

    $('#newteam').modal();
}
function addcash(pid,tid,league)
{
    if(league == sublg)
    {
        $('#cashoption').html("<div class='form-group'><div class='input-group'><span class='input-group-addon' style='background-color:#337ab7;color:white;width:120px;text-align:left;'>Amount</span><select class='form-control' name='cash'><option value='2000000'>$2,000,000</option><option value='3000000'>$3,000,000</option><option value='5000000'>$5,000,000</option></select></div></div>");
    }
    if(league == mainlg)
    {
        $('#cashoption').html("<div class='form-group'><div class='input-group'><span class='input-group-addon' style='background-color:#337ab7;color:white;width:120px;text-align:left;'>Amount</span><input id='cash' type='number' class='form-control' name='cash'></div></div>");
    }
    $('#addcashpid').val(pid);
    $('#addcashtid').val(tid);
    $('#addcashleague').val(league);
    $('#addcash').modal();
}
function sendpm(toname,toid,fromid)
{
    $('#toname').html(toname);
    $('#toid').val(toid);
    $('#fromid').val(fromid);
    $('#PM').modal();
}
$(document).ready(function() {
    $('#viewsba').DataTable( {
        paging:   false,
        select: true,
        columnDefs:
        [
            {"orderSequence": ['asc','asc'], "targets": [0,1,2]},
            {"orderSequence": ['desc','asc'], "targets": [3,4,5]}
        ]
    } );
    $('#viewsba2').DataTable( {
        paging:   false,
        select: true,
        columnDefs:
        [
            {"orderSequence": ['asc','asc'], "targets": [0]},
            {"orderSequence": ['desc','asc'], "targets": [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16]}
        ]
    } );
    $('#viewsba3').DataTable( {
        paging:   false,
        select: true,
        order: [[0,"desc"]],
        columnDefs:
        [
            {"orderSequence": ['desc','asc'], "targets": [0,1,2]}
        ]
    } );
    $('[data-toggle="popover"]').popover(); 
    $('.table-responsive').on('show.bs.dropdown', function () {
        $('.table-responsive').css( "overflow", "inherit" );
    });

    $('.table-responsive').on('hide.bs.dropdown', function () {
        $('.table-responsive').css( "overflow", "auto" );
    });
} );
</script>