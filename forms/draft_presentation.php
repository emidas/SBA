<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:../auto/main_login.php");
}
if($_SESSION['issimmer'] != '1'){
header("location:../auto/index.php?alert=simmerpermission");
}
$echo = '';
if(isset($_POST['to']) && isset($_POST['from']))
{
    set_time_limit(0);
    $to = sanitize($con,$_POST['to']);
    $from = sanitize($con,$_POST['from']);
    if(isset($_POST['start'])){$start = $_POST['start'];}else{$start = 1;}
    if(isset($_POST['end'])){$end = $_POST['end'];}else{$end = 30;}
    if(isset($_POST['sleep'])){$sleep = $_POST['sleep'];}else{$sleep = 10;}
    $start = $start - 1;
    $sql = "SELECT pid FROM molholt_sba.forums_posts WHERE topic_id='$from' LIMIT $start,$end";
    $result = mysqli_query($con,$sql);
    while ($r = mysqli_fetch_array($result))
    {
        $pid = $r['pid'];
        $data = getPost($pid);
        $curl_post_data = array(
        'topic' => $to,
        'author' => '2574',
        'post' => $data
        );
        createPost($curl_post_data);
        sleep($sleep);
    }
}
?>

<!-- Body Start -->
<body>
<div id="wrapper" style="position:relative;top:0;left:0;">
    <?php include $path.'/includes/nav.php'; ?>
    <div class="container">
        <?=HeaderImagePrint($hostlg,$_SESSION['style'])?>
        <div class="panel-group">
            <div class="panel panel-primary">
                <div class="panel-heading">Draft Presentation</div>
                <div class="panel-body">
                    <form class="form-horizontal" action="draft_presentation.php" method="post">
                        <div class="form-group">
                            <label for="to">Topic ID(To):</label>
                            <input type="text" class="form-control" name="to">
                        </div>
                        <div class="form-group">
                            <label for="from">Topic ID(From):</label>
                            <input type="text" class="form-control" name="from">
                        </div>
                        <div class="form-group">
                            <label for="sleep">Sleep Timer(seconds):</label>
                            <input type="text" class="form-control" name="sleep">
                        </div>
                        <div class="form-group">
                            <label for="start">Starting Post(Number):</label>
                            <input type="text" class="form-control" name="start">
                        </div>
                        <div class="form-group">
                            <label for="end">Number of Posts:</label>
                            <input type="text" class="form-control" name="end">
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-default">Submit</button>
                            </div>
                        </div>
                    </form>
                    <?=$echo?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Body End -->

<?php
include $path.'/footer.php';
?>