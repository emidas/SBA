<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
$user=$_SESSION['user'];
$memberid=$_SESSION['memberid'];

if(isset($_POST['league']))
{
    $league = sanitize($con,$_POST['league']);
    $season = sanitize($con,$_POST['season']);
    $mvp = sanitize($con,$_POST['mvp']);
    $pmvp = sanitize($con,$_POST['pmvp']);
    $op = sanitize($con,$_POST['opoty']);
    $dp = sanitize($con,$_POST['dpoty']);
    $sm = sanitize($con,$_POST['smoty']);
    $pg = sanitize($con,$_POST['pgoty']);
    $sg = sanitize($con,$_POST['sgoty']);
    $sf = sanitize($con,$_POST['sfoty']);
    $pf = sanitize($con,$_POST['pfoty']);
    $ce = sanitize($con,$_POST['coty']);
    $ro = sanitize($con,$_POST['roty']);
    $mip = sanitize($con,$_POST['mip']);
    $mvpt = sanitize($con,$_POST['mvpt']);
    $pmvpt = sanitize($con,$_POST['pmvpt']);
    $opt = sanitize($con,$_POST['opotyt']);
    $dpt = sanitize($con,$_POST['dpotyt']);
    $smt = sanitize($con,$_POST['smotyt']);
    $pgt = sanitize($con,$_POST['pgotyt']);
    $sgt = sanitize($con,$_POST['sgotyt']);
    $sft = sanitize($con,$_POST['sfotyt']);
    $pft = sanitize($con,$_POST['pfotyt']);
    $cet = sanitize($con,$_POST['cotyt']);
    $rot = sanitize($con,$_POST['rotyt']);
    $mipt = sanitize($con,$_POST['mipt']);
    $posarr = Array('mvp','pmvp','opoty','dpoty','smoty','pgoty','sgoty','sfoty','pfoty','coty','roty','mip');
    $pidarr = Array($mvp,$pmvp,$op,$dp,$sm,$pg,$sg,$sf,$pf,$ce,$ro,$mip);
    $tidarr = Array($mvpt,$pmvpt,$opt,$dpt,$smt,$pgt,$sgt,$sft,$pft,$cet,$rot,$mipt);
    if($league == 'sba')
    {
        $table = 'award_winners';
    }
    else
    {
        $table = 'ncaa_award_winners';
    }
    $sql = "SELECT year FROM $table WHERE year='$season'";
    $result=mysqli_query($con,$sql);
    $count=mysqli_num_rows($result);
    if($count > 0)
    {
        exit();
    }
    else
    {
        for($v=0;$v<count($posarr);$v++)
        {
            $sql = "INSERT INTO $table (year,award,pid,tid) VALUES ('$season','$posarr[$v]','$pidarr[$v]','$tidarr[$v]')";
            mysqli_query($con,$sql);
        }
        header("location:award_winners_entry.php");
    }
}
else
{
	header("location:predictions.php?alert=closed");
}
?>

