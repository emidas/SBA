<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
$user=$_SESSION['user'];
$echo = '';

// Get Year
$system = new System();
$year = $system->get_year();
$cap = $system->get_salary_cap();
$season = "S".$year;

$sql = "SELECT t.id as tid
FROM teams t
WHERE (t.league = 'SBA')
ORDER BY name";
$result = mysqli_query($con,$sql);
$stable = '';
while($row = mysqli_fetch_array($result))
{
    $tid = $row['tid'];
    $team = new Team($tid);
    $tname = $team->get_('name');
    $tsalary = $team->get_('team_salary');
    $tcapspace = $team->get_('cap_space');
    $troster = $team->get_('roster_size');
    $stable .= "
    <tr>
        <td><a href='team_page.php?tid=".$tid."'>".$tname."</a></td><td>".$troster."</td><td>$".number_format($tsalary,0)."</td><td>$".number_format($tcapspace,0)."</td>
    </tr>";
}
?>

<!-- Body Start -->
<body>
<div id="wrapper" style="position:relative;top:0;left:0;">
    <?php include $path.'/includes/nav.php'; ?>
    <div class="container">
        <?=HeaderImagePrint($hostlg,$_SESSION['style'])?>
        <?=$echo?>
        <div class="panel panel-primary">
            <div class="panel-heading">Team Salaries</div>
            <div class="panel-body">
                <table class="table table-striped">
                    <thead>
                        <th>Team</th>
                        <th>Players</th>
                        <th>Team Salary</th>
                        <th>Remaining Cap</th>
                    </thead>
                    <tbody>
                        <?=$stable?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- Body End -->

<?php
include $path.'/footer.php';
?>