<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
$user=$_SESSION['user'];

if(isset($_GET['pid']))
{
    $pid = sanitize($con,$_GET['pid']);
    $player = new Player($pid);
    $pname = $player->get_('name');
}
else
{
    $pid = '';
    $pname = '';
}
// year
$system = new System();
$year = $system->get_year();

$echo = create_update_history($con,$pid,1);  
$echo2 = create_update_history_week($con,$pid,'');
?>

<!-- Body Start -->
<body>
<div id="wrapper" style="position:relative;top:0;left:0;">
    <?php include $path.'/includes/nav.php'; ?>
    <div class="container">
        <?=HeaderImagePrint($hostlg,$_SESSION['style'])?>
        <div class="panel-group">
            <div class="panel-group">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h4 class="panel-title">Update History - <?=$pname?></h4></div>
                    <div class="panel-body">
                        <div class="panel-group">
                            <div class="panel panel-default">
                                <div class="panel-heading"><h4 class="panel-title"><a data-toggle='collapse' href='#collapsehbu'>History By Update</a></h4></div>
                                <div class='panel-collapse collapse' id='collapsehbu'>
                                    <div class="panel-body">
                                        <?=$echo?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-group">
                            <div class="panel panel-default">
                                <div class="panel-heading"><h4 class="panel-title"><a data-toggle='collapse' href='#collapsehbwe'>History By Week's End</a></h4></div>
                                <div class='panel-collapse collapse' id='collapsehbwe'>
                                    <div class="panel-body">
                                        <?=$echo2?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Body End -->

<?php
include $path.'/footer.php';
?>