<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
$user=$_SESSION['user'];
$echo = '';
if(isset($_GET['pid']))
{
    $pid = $_GET['pid'];
}
else
{
    $pid = '';
}
if(isset($_POST['upload']))
{
    $pid = $_POST['pid'];
    $target_dir = $path."/images/player-images/";
    $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    $newfilename = $target_dir."".$pid.".png";
    // Check if image file is a actual image or fake image
    if(isset($_POST["submit"])) 
    {
        $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
        if($check !== false) 
        {
            $echo = "File is an image - " . $check["mime"] . ".";
            $uploadOk = 1;
        } 
        else 
        {
            $echo = "File is not an image.";
            $uploadOk = 0;
        }
    }
    // Check if file already exists
    //if (file_exists($newfilename)) 
    //{
    //   echo "Sorry, file already exists.";
    //    $uploadOk = 0;
    //}
    // Check file size
    if ($_FILES["fileToUpload"]["size"] > 500000) 
    {
        $echo = "Sorry, your file is too large.";
        $uploadOk = 0;
    }
    // Allow certain file formats
    if($imageFileType != "png") 
    {
        $echo = "Sorry, only PNG files are allowed.";
        $uploadOk = 0;
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) 
    {
        //$echo = "Sorry, your file was not uploaded.";
        // if everything is ok, try to upload file
    } 
    else 
    {
        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $newfilename)) 
        {
            $echo = "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.<br>";
        } 
        else 
        {
            $echo = "Sorry, there was an error uploading your file.";
        }
    }
}
?>

<!-- Body Start -->
<body>
<div id="wrapper" style="position:relative;top:0;left:0;">
    <?php include $path.'/includes/nav.php'; ?>
    <div class="container">
        <?=HeaderImagePrint($hostlg,$_SESSION['style'])?>
        <?=$echo?>
        <div class="panel panel-primary">
            <div class="panel-heading">Add/Edit Picture</div>
            <div class="panel-body">
                <form method="post" enctype="multipart/form-data">
                    <div class="well well-sm">
                        This will upload a photo onto your Player Page. Currently, we are only accepting PNG files up to 500KB in size. If at all possible, pick a photo from the following search that meets this criteria: <a href='https://www.google.com/search?biw=1502&bih=876&tbs=isz%3Aex%2Ciszw%3A350%2Ciszh%3A254&tbm=isch&sa=1&ei=6Q0QXNq0Oqiv5wLNnZzYBA&q=nba+player&oq=nba+player&gs_l=img.3..35i39j0l2j0i67l3j0l4.3043.3937..4243...0.0..0.76.263.4......1....1..gws-wiz-img.......0i7i30.9qR5KmlBNFc' target="_blank"><u>Click Here</u></a>
                    </div>
                    <input type="text" name="pid" value='<?=$pid?>' hidden>
                    <div class="input-group">
                        <label class="input-group-btn">
                            <span class="btn btn-primary">
                                Select Image For Upload&hellip; <input type="file" name="fileToUpload" id="fileToUpload" style="display: none;" multiple>
                            </span>
                        </label>
                        <input type="text" class="form-control" readonly>
                    </div>
                    <button type="submit" name="upload" class="btn btn-primary" name="submit">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Body End -->

<?php
include $path.'/footer.php';
?>
<script>
 $(function() {

  // We can attach the `fileselect` event to all file inputs on the page
  $(document).on('change', ':file', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
  });

  // We can watch for our custom `fileselect` event like this
  $(document).ready( function() {
      $(':file').on('fileselect', function(event, numFiles, label) {

          var input = $(this).parents('.input-group').find(':text'),
              log = numFiles > 1 ? numFiles + ' files selected' : label;

          if( input.length ) {
              input.val(log);
          } else {
              if( log ) alert(log);
          }

      });
  });
  
});
</script>