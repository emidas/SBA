<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/sba_process.php';

$user = new User($_SESSION['user']);
$username = $user->username;
$userid = $user->id;

if(isset($_POST['go_cap']))
{
    global $sublg;
    if(isset($_POST['capsub'])){$cap = '1';} else {$cap = '0';}
    if(isset($_POST['capsubpm'])){$capm = '1';} else {$capm = '0';}
    $query = "UPDATE auth_user SET cap_sub = '$cap',cap_sub_pm = '$capm' WHERE username=\"$username\"";
    mysqli_query($con,$query);
    header("location:/settings.php?alert=success");  
}
else if(isset($_POST['go_style']))
{
    $user=$_SESSION['user'];
    $style = sanitize($con,$_POST['style']);
    $query = "UPDATE auth_user SET style='$style' WHERE username=\"$username\"";
    mysqli_query($con,$query);
    $_SESSION['style'] = $style;
    header("location:/settings.php?alert=success");  
}
else if(isset($_POST['go_efl']))
{
    $user = sanitize_login($con,$_POST['efl_name']);
    $password = sanitize_login($con,$_POST['efl_pass']);
    $stmt = $conn->prepare("SELECT * FROM eflmanager.Users WHERE displayname=:user");
    $stmt->execute([':user' => $user]);
    $count = $stmt->rowCount();
    if($count == 1)
    {
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        $check = password_verify($password,$row['password']);
        if($check)
        {
            $efl_fk = $row['user_ID'];
            $stmt = $conn->prepare("UPDATE auth_user SET efl_fk = :efl WHERE ID=:id");
            $stmt->execute([':id' => $userid, ':efl' => $efl_fk]);
            header("location:/settings.php?alert=success"); 
            exit();
        }
        else
        {
            header("location:/settings.php?alert=wrongpass");
            exit();
        }
    }
    else
    {
        header("location:/settings.php?alert=noeflfound");
        exit();
    }

}
else if(isset($_POST['go_deleteefl']))
{
    $stmt = $conn->prepare("UPDATE auth_user SET efl_fk = NULL WHERE ID=:id");
    $stmt->execute([':id' => $userid]);
    header("location:/settings.php?alert=success"); 
    exit();
}
else
{
    header("location:/settings.php?alert=success");   
}
?>