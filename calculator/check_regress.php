<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:../index.php");
}
if($_SESSION['isupdater'] != '1' && $_SESSION['issimmer'] != '1' && $_SESSION['isadmin'] != '1'){
header("location:../home.php?alert=betapermission");
}
if(isset($_POST['id']))
{
    $id = sanitize($con,$_POST['id']);
    $ins = sanitize($con,$_POST['reg_ins']);
    $jps = sanitize($con,$_POST['reg_jps']);
    $ft = sanitize($con,$_POST['reg_ft']);
    $tps = sanitize($con,$_POST['reg_3ps']);
    $han = sanitize($con,$_POST['reg_han']);
    $pas = sanitize($con,$_POST['reg_pas']);
    $orb = sanitize($con,$_POST['reg_orb']);
    $drb = sanitize($con,$_POST['reg_drb']);
    $psd = sanitize($con,$_POST['reg_psd']);
    $prd = sanitize($con,$_POST['reg_prd']);
    $stl = sanitize($con,$_POST['reg_stl']);
    $blk = sanitize($con,$_POST['reg_blk']);
    $fl = sanitize($con,$_POST['reg_fl']);
    $qkn = sanitize($con,$_POST['reg_qkn']);
    $str = sanitize($con,$_POST['reg_str']);
    $jmp = sanitize($con,$_POST['reg_jmp']);
    $bank = sanitize($con,$_POST['reg_bank']);
    $ape = sanitize($con,$_POST['ape']);
}
else
{
    header("location:../home.php");
}
$sql = "UPDATE players
        SET ins='$ins',
            jps='$jps',
            ft='$ft',
            3ps='$tps',
            han='$han',
            pas='$pas',
            orb='$orb',
            drb='$drb',
            psd='$psd',
            prd='$prd',
            stl='$stl',
            blk='$blk',
            fl='$fl',
            qkn='$qkn',
            str='$str',
            jmp='$jmp',
            bank=$bank,
            ape=$ape,
            bank='$bank'
        WHERE id='$id'";
mysqli_query($con,$sql);

header("location:../home.php");
?>

