<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
$user=$_SESSION['user'];
//$sql = "SELECT p.ins,p.jps,p.ft,p.3ps,p.han,p.pas,p.orb,p.drb,p.psd,p.prd,p.stl,p.blk,p.fl,p.qkn,p.str,p.jmp,p.bank,p.ape FROM players p WHERE id=\"$pid\"";
//$r = mysqli_fetch_array(mysqli_query($con,$sql));
$query = "SELECT * FROM auth_user WHERE username=\"$user\"";
$r = [];
$s = mysqli_fetch_array(mysqli_query($con,$query));
?>

<!-- Body Start -->
<body>
<div id="wrapper" style="position:relative;top:0;left:0;">
    <?php include $path.'/includes/nav.php'; ?>
    <div class="container">
        <?=HeaderImagePrint($hostlg,$_SESSION['style'])?>
        <div class="panel-group">
            <form method="get" id="regcalc" class="form-inline">
                <div class="panel panel-primary">
                    <div class="panel-heading">Regression Calculator</div>
                    <div class="panel-body">
                        <div class="form-group">          
                            <select class="form-control" onchange="template(this.value)" id="pos" data-toggle="toolip" title="Pick your Year of Regression. This will populate your Regression Percentages.">
                                <option selected value="none" disabled>Year</option>
                                <option value="9">9th</option>
                                <option value="10">10th</option>
                                <option value="11">11th</option>
                                <option value="12">12th</option>
                                <option value="13">13th</option>
                                <option value="14">14th</option>
                                <option value="15">15th+</option>
                            </select>                                    
                            <button type="button" class="btn btn-default" onclick="Regress()" data-toggle="tooltip" title="Will calculate your Regressed Attribute values based on Current Attribute, Weaknesses/Strengths, and Regression Percentage.">Regress</button>
                            <button type="button" class="btn btn-default" onclick="Swap()" data-toggle="tooltip" title="Places the current Regressed values in the Rating field. Useful for calculating multiple years.">Swap</button>
                            <button type="button" class="btn btn-default" onclick="Print()" data-toggle="tooltip" title="Will give you a popup of your Regressed attributes.">Print</button>
                            <button type="button" class="btn btn-default" onclick="Reset()" data-toggle="tooltip" title="Will reset the entire calculator.">Reset</button>
                            <button type="button" class="btn btn-default" onclick="Test()" data-toggle="tooltip" title="Template build, will populate every field with a 90 rating.">Template</button>
                        </div>
                        <table style="margin: 0 auto;padding:0;" id="regtable" class="<?=$s['view']?>">
                            <tr>
                                <th >Attribute Name</th>
                                <th data-toggle="tooltip" title="The value of your attribute prior to this regression calculation.">Rating</th>
                                <th data-toggle="tooltip" title="Weakness, Normal, or Strength. Click the corresponding option if this attribute is a weakness or a strength in your archetype.">Wk | Nrm | Str</th>
                                <th data-toggle="tooltip" title="The percentage (in decimal form) in which each attribute will be regressed.">Percentage</th>
                                <th data-toggle="tooltip" title="The value of each attribute after regression has ruined your player.">Regressed</th>
                            </tr>
                            <tr>
                                <td style="text-align:left;">Inside Scoring:</td>
                                <td><input type="text" class="cb" id="curr_ins" name="curr_ins" size="4" maxlength="2" tabindex=1 value=<?=$r['ins']?>></td>
                                <td><input type="radio" class="cb" id="w_ins" name="ins" value="weak" onclick="template(document.getElementById('pos').value)"><input type="radio" class="cb" id="n_ins" name="ins" value="normal" onclick="template(document.getElementById('pos').value)"><input type="radio" class="cb" id="s_ins" name="ins" value="strong" onclick="template(document.getElementById('pos').value)"></td>
                                <td><input type="text" class="cb" id="pct_ins"  size ="4" maxlength="3"></td>
                                <td><input type="text" class="cb" id="reg_ins"  size="4" maxlength="2" readonly="readonly"></td>
                            </tr>
                            <tr>
                                <td style="text-align:left;">Jump Shot:</td>
                                <td><input type="text" class="cb" id="curr_jps" name="curr_ins"  size="4" maxlength="2" tabindex=2 value=<?=$r['jps']?>></td>
                                <td><input type="radio" class="cb" id="w_jps" name="jps" value="weak" onclick="template(document.getElementById('pos').value)"><input type="radio" class="cb" id="n_jps" name="jps" value="normal" onclick="template(document.getElementById('pos').value)"><input type="radio" class="cb" id="s_jps" name="jps" value="strong" onclick="template(document.getElementById('pos').value)"></td>
                                <td><input type="text" class="cb" id="pct_jps" size ="4" maxlength="3"></td>
                                <td><input type="text" class="cb" id="reg_jps"  size="4" maxlength="2" readonly="readonly"></td>
                            </tr>
                            <tr>
                                <td style="text-align:left;">Free Throw:</td>
                                <td><input type="text" class="cb" id="curr_ft" name="curr_ins"  size="4" maxlength="2" tabindex=3 value=<?=$r['ft']?>></td>
                                <td><input type="radio" class="cb" id="w_ft" name="ft" value="weak" onclick="template(document.getElementById('pos').value)"><input type="radio" class="cb" id="n_ft" name="ft" value="normal" onclick="template(document.getElementById('pos').value)"><input type="radio" class="cb" id="s_ft" name="ft" value="strong" onclick="template(document.getElementById('pos').value)"></td>
                                <td><input type="text" class="cb" id="pct_ft" size ="4" maxlength="3"></td>
                                <td><input type="text" class="cb" id="reg_ft"  size="4" maxlength="2" readonly="readonly"></td>
                            </tr>
                            <tr>
                                <td style="text-align:left;">Three Point:</td>
                                <td><input type="text" class="cb" id="curr_3ps" name="curr_ins"  size="4" maxlength="2" tabindex=4 value=<?=$r['3ps']?>></td>
                                <td><input type="radio" class="cb" id="w_3ps" name="3ps" value="weak" onclick="template(document.getElementById('pos').value)"><input type="radio" class="cb" id="n_3ps" name="3ps" value="normal" onclick="template(document.getElementById('pos').value)"><input type="radio" class="cb" id="s_3ps" name="3ps" value="strong" onclick="template(document.getElementById('pos').value)"></td>
                                <td><input type="text" class="cb" id="pct_3ps" size ="4" maxlength="3"></td>
                                <td><input type="text" class="cb" id="reg_3ps"  size="4" maxlength="2" readonly="readonly"></td>
                            </tr>
                            <tr>
                                <td style="text-align:left;">Handling:</td>
                                <td><input type="text" class="cb" id="curr_han" name="curr_ins"  size="4" maxlength="2" tabindex=5 value=<?=$r['han']?>></td>
                                <td><input type="radio" class="cb" id="w_han" name="han" value="weak" onclick="template(document.getElementById('pos').value)"><input type="radio" class="cb" id="n_han" name="han" value="normal" onclick="template(document.getElementById('pos').value)"><input type="radio" class="cb" id="s_han" name="han" value="strong" onclick="template(document.getElementById('pos').value)"></td>
                                <td><input type="text" class="cb" id="pct_han" size ="4" maxlength="3"></td>
                                <td><input type="text" class="cb" id="reg_han"  size="4" maxlength="2" readonly="readonly"></td>
                            </tr>
                            <tr>
                                <td style="text-align:left;">Passing:</td>
                                <td><input type="text" class="cb" id="curr_pas" name="curr_ins"  size="4" maxlength="2" tabindex=6 value=<?=$r['pas']?>></td>
                                <td><input type="radio" class="cb" id="w_pas" name="pas" value="weak" onclick="template(document.getElementById('pos').value)"><input type="radio" class="cb" id="n_pas" name="pas" value="normal" onclick="template(document.getElementById('pos').value)"><input type="radio" class="cb" id="s_pas" name="pas" value="strong" onclick="template(document.getElementById('pos').value)"></td>
                                <td><input type="text" class="cb" id="pct_pas" size ="4" maxlength="3"></td>
                                <td><input type="text" class="cb" id="reg_pas"  size="4" maxlength="2" readonly="readonly"></td>
                            </tr>
                            <tr>
                                <td style="text-align:left;">Offensive Rebounding:</td>
                                <td><input type="text" class="cb" id="curr_orb" name="curr_ins"  size="4" maxlength="2" tabindex=7 value=<?=$r['orb']?>></td>
                                <td><input type="radio" class="cb" id="w_orb" name="orb" value="weak" onclick="template(document.getElementById('pos').value)"><input type="radio" class="cb" id="n_orb" name="orb" value="normal" onclick="template(document.getElementById('pos').value)"><input type="radio" class="cb" id="s_orb" name="orb" value="strong" onclick="template(document.getElementById('pos').value)"></td>
                                <td><input type="text" class="cb" id="pct_orb" size ="4" maxlength="3"></td>
                                <td><input type="text" class="cb" id="reg_orb"  size="4" maxlength="2" readonly="readonly"></td>
                            </tr>
                            <tr>
                                <td style="text-align:left;">Defensive Rebounding:</td>
                                <td><input type="text" class="cb" id="curr_drb" name="curr_ins"  size="4" maxlength="2" tabindex=8 value=<?=$r['drb']?>></td>
                                <td><input type="radio" class="cb" id="w_drb" name="drb" value="weak" onclick="template(document.getElementById('pos').value)"><input type="radio" class="cb" id="n_drb" name="drb" value="normal" onclick="template(document.getElementById('pos').value)"><input type="radio" class="cb" id="s_drb" name="drb" value="strong" onclick="template(document.getElementById('pos').value)"></td>
                                <td><input type="text" class="cb" id="pct_drb" size ="4" maxlength="3"></td>
                                <td><input type="text" class="cb" id="reg_drb"  size="4" maxlength="2" readonly="readonly"></td>
                            </tr>
                            <tr>
                                <td style="text-align:left;">Post Defense:</td>
                                <td><input type="text" class="cb" id="curr_psd" name="curr_ins"  size="4" maxlength="2" tabindex=9 value=<?=$r['psd']?>></td>
                                <td><input type="radio" class="cb" id="w_psd" name="psd" value="weak" onclick="template(document.getElementById('pos').value)"><input type="radio" class="cb" id="n_psd" name="psd" value="normal" onclick="template(document.getElementById('pos').value)"><input type="radio" class="cb" id="s_psd" name="psd" value="strong" onclick="template(document.getElementById('pos').value)"></td>
                                <td><input type="text" class="cb" id="pct_psd" size ="4" maxlength="3"></td>
                                <td><input type="text" class="cb" id="reg_psd"  size="4" maxlength="2" readonly="readonly"></td>
                            </tr>
                            <tr>
                                <td style="text-align:left;">Perimeter Defense:</td>
                                <td><input type="text" class="cb" id="curr_prd" name="curr_ins"  size="4" maxlength="2" tabindex=10 value=<?=$r['prd']?>></td>
                                <td><input type="radio" class="cb" id="w_prd" name="prd" value="weak" onclick="template(document.getElementById('pos').value)"><input type="radio" class="cb" id="n_prd" name="prd" value="normal" onclick="template(document.getElementById('pos').value)"><input type="radio" class="cb" id="s_prd" name="prd" value="strong" onclick="template(document.getElementById('pos').value)"></td>
                                <td><input type="text" class="cb" id="pct_prd" size ="4" maxlength="3"></td>
                                <td><input type="text" class="cb" id="reg_prd"  size="4" maxlength="2" readonly="readonly"></td>
                            </tr>
                            <tr>
                                <td style="text-align:left;">Stealing:</td>
                                <td><input type="text" class="cb" id="curr_stl" name="curr_ins"  size="4" maxlength="2" tabindex=11 value=<?=$r['stl']?>></td>
                                <td><input type="radio" class="cb" id="w_stl" name="stl" value="weak" onclick="template(document.getElementById('pos').value)"><input type="radio" class="cb" id="n_stl" name="stl" value="normal" onclick="template(document.getElementById('pos').value)"><input type="radio" class="cb" id="s_stl" name="stl" value="strong" onclick="template(document.getElementById('pos').value)"></td>
                                <td><input type="text" class="cb" id="pct_stl" size ="4" maxlength="3"></td>
                                <td><input type="text" class="cb" id="reg_stl"  size="4" maxlength="2" readonly="readonly"></td>
                            </tr>
                            <tr>
                                <td style="text-align:left;">Shot Blocking:</td>
                                <td><input type="text" class="cb" id="curr_blk" name="curr_ins"  size="4" maxlength="2" tabindex=12 value=<?=$r['blk']?>></td>
                                <td><input type="radio" class="cb" id="w_blk" name="blk" value="weak" onclick="template(document.getElementById('pos').value)"><input type="radio" class="cb" id="n_blk" name="blk" value="normal" onclick="template(document.getElementById('pos').value)"><input type="radio" class="cb" id="s_blk" name="blk" value="strong" onclick="template(document.getElementById('pos').value)"></td>
                                <td><input type="text" class="cb" id="pct_blk" size ="4" maxlength="3"></td>
                                <td><input type="text" class="cb" id="reg_blk"  size="4" maxlength="2" readonly="readonly"></td>
                            </tr>
                            <tr>
                                <td style="text-align:left;">Fouling:</td>
                                <td><input type="text" class="cb" id="curr_fl" name="curr_ins"  size="4" maxlength="2" tabindex=13 value=<?=$r['fl']?>></td>
                                <td><input type="radio" class="cb" id="w_fl" name="fl" value="weak" onclick="template(document.getElementById('pos').value)"><input type="radio" class="cb" id="n_fl" name="fl" value="normal" onclick="template(document.getElementById('pos').value)"><input type="radio" class="cb" id="s_fl" name="fl" value="strong" onclick="template(document.getElementById('pos').value)"></td>
                                <td><input type="text" class="cb" id="pct_fl" size ="4" maxlength="3"></td>
                                <td><input type="text" class="cb" id="reg_fl"  size="4" maxlength="2" readonly="readonly"></td>
                            </tr>
                            <tr>
                                <td style="text-align:left;">Quickness:</td>
                                <td><input type="text" class="cb" id="curr_qkn" name="curr_ins"  size="4" maxlength="2" tabindex=14 value=<?=$r['qkn']?>></td>
                                <td><input type="radio" class="cb" id="w_qkn" name="qkn" value="weak" onclick="template(document.getElementById('pos').value)"><input type="radio" class="cb" id="n_qkn" name="qkn" value="normal" onclick="template(document.getElementById('pos').value)"><input type="radio" class="cb" id="s_qkn" name="qkn" value="strong" onclick="template(document.getElementById('pos').value)"></td>
                                <td><input type="text" class="cb" id="pct_qkn" size ="4" maxlength="3"></td>
                                <td><input type="text" class="cb" id="reg_qkn"  size="4" maxlength="2" readonly="readonly"></td>
                            </tr>
                            <tr>
                                <td style="text-align:left;">Strength:</td>
                                <td><input type="text" class="cb" id="curr_str" name="curr_ins"  size="4" maxlength="2" tabindex=15 value=<?=$r['str']?>></td>
                                <td><input type="radio" class="cb" id="w_str" name="str" value="weak" onclick="template(document.getElementById('pos').value)"><input type="radio" class="cb" id="n_str" name="str" value="normal" onclick="template(document.getElementById('pos').value)"><input type="radio" class="cb" id="s_str" name="str" value="strong" onclick="template(document.getElementById('pos').value)"></td>
                                <td><input type="text" class="cb" id="pct_str" size ="4" maxlength="3"></td>
                                <td><input type="text" class="cb" id="reg_str"  size="4" maxlength="2" readonly="readonly"></td>
                            </tr>
                            <tr>
                                <td style="text-align:left;">Jumping:</td>
                                <td><input type="text" class="cb" id="curr_jmp" name="curr_ins"  size="4" maxlength="2" tabindex=16 value=<?=$r['jmp']?>></td>
                                <td><input type="radio" class="cb" id="w_jmp" name="jmp" value="weak" onclick="template(document.getElementById('pos').value)"><input type="radio" class="cb" id="n_jmp" name="jmp" value="normal" onclick="template(document.getElementById('pos').value)"><input type="radio" class="cb" id="s_jmp" name="jmp" value="strong" onclick="template(document.getElementById('pos').value)"></td>
                                <td><input type="text" class="cb" id="pct_jmp" size ="4" maxlength="3"></td>
                                <td><input type="text" class="cb" id="reg_jmp"  size="4" maxlength="2" readonly="readonly"></td>
                            </tr>
                            <tr>
                                <td style="text-align:left;">Banked TPE:</td>
                                <td><input type="text" class="cb" id="curr_bnk" name="bank"  size="4" maxlength="4" tabindex=17 value=<?=$r['bank']?>></td>
                                <td></td>
                                <td><input type="text" class="cb" id="pct_bnk" size ="4" maxlength="3"></td>
                                <td><input type="text" class="cb" id="reg_bnk"  size="4" maxlength="2" readonly="readonly"></td>
                            </tr>
                            <tr>
                                <td style="text-align:left;font-weight:bold;color:#ff7c19;">Current APE:</td>
                                <td><input type="text" class="cb" id="tpe" name="tpe" size="4" maxlength="4" tabindex=18 value=<?=$r['tpe']?>></td>
                            </tr>
                            <tr>
                                <td style="text-align:left;font-weight:bold;color:#ff7c19;">Regressed APE:</td>
                                <td><input type="text" readonly="readonly"; class="cb" id="ape" name="ape" size="4" maxlength="4" tabindex=18></td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip({container: 'body'});   
});
</script>
<!-- Body End -->

<?php
include $path.'/footer.php';
?>