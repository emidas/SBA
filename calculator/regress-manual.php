<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
if($_SESSION['isupdater'] != '1' && $_SESSION['issimmer'] != '1' && $_SESSION['isadmin'] != '1'){
header("location:/home.php?alert=betapermission");
}
if(isset($_GET['id']))
{
    $id = sanitize($con,$_GET['id']);
}
$user=$_SESSION['user'];
$query = "SELECT ins,jps,ft,3ps,han,pas,orb,drb,psd,prd,stl,blk,fl,qkn,str,jmp,bank,ape,experience FROM players WHERE id='$id'";
$result = mysqli_query($con,$query);
$echo = '';
$listarr = Array('Inside Scoring','Jump Shot','Free Throw','Three Point','Handling','Passing','Offensive Rebounding','Defensive Rebounding','Post Defense','Perimeter Defense','Stealing','Blocking','Fouling','Quickness','Strength','Jumping');
$listarr_short = Array('ins','jps','ft','3ps','han','pas','orb','drb','psd','prd','stl','blk','fl','qkn','str','jmp');
while($r = mysqli_fetch_array($result))
{
    for ($a=0;$a<count($listarr);$a++)
    {
        $b = $a + 1;
        $echo .="<tr>
            <td>".$listarr[$a].":</td>
            <td><input type='text' class='cb' id='curr_".$listarr_short[$a]."' size='4' maxlength='2' tabindex='$b' value='".$r[$listarr_short[$a]]."'></td>
            <td><label class='radio-inline'><input type='radio' id='w_".$listarr_short[$a]."' name='".$listarr_short[$a]."' onClick=\"template(document.getElementById('pos').value);\"></label><label class='radio-inline'><input type='radio' id='n_".$listarr_short[$a]."' name='".$listarr_short[$a]."' onClick=\"template(document.getElementById('pos').value);\"></label><label class='radio-inline'><input type='radio' id='s_".$listarr_short[$a]."' name='".$listarr_short[$a]."' onClick=\"template(document.getElementById('pos').value);\"></label></td>
            <td><input type='text' class='cb' id='pct_".$listarr_short[$a]."' size='4' readonly='readonly'></td>
            <td><input type='text' class='cb' id='reg_".$listarr_short[$a]."' name='reg_".$listarr_short[$a]."' size='4' readonly='readonly'></td>
        </tr>";
    }
    $banked = $r['bank'];
    $ape = $r['ape'];
}
?>

<!-- Body Start -->
<body>
<div id="wrapper" style="position:relative;top:0;left:0;">
    <?php include $path.'/includes/nav.php'; ?>
    <div class="container">
        <?=HeaderImagePrint($hostlg,$_SESSION['style'])?>
        <div class="panel-group">
            <form method="POST" action="check_regress.php" id="regcalc" class="form-inline">
                <div class="panel panel-primary">
                    <div class="panel-heading">Regression Calculator</div>
                    <div class="panel-body">
                        <div class="form-group">          
                            <select class="form-control" onchange="template(this.value)" id="pos" data-toggle="toolip" title="Pick your Year of Regression. This will populate your Regression Percentages.">
                                <option selected value="none" disabled>Year</option>
                                <option value="9">9th</option>
                                <option value="10">10th</option>
                                <option value="11">11th</option>
                                <option value="12">12th</option>
                                <option value="13">13th</option>
                                <option value="14">14th</option>
                                <option value="15">15th+</option>
                            </select>                                    
                        </div>
                        <table style="margin: 0 auto;padding:0;" id="regtable" class="table">
                            <tr>
                                <th >Attribute Name</th>
                                <th data-toggle="tooltip" title="The value of your attribute prior to this regression calculation.">Rating</th>
                                <th data-toggle="tooltip" title="Weakness, Normal, or Strength. Click the corresponding option if this attribute is a weakness or a strength in your archetype.">Wk | Nrm | Str</th>
                                <th data-toggle="tooltip" title="The percentage (in decimal form) in which each attribute will be regressed.">Percentage</th>
                                <th data-toggle="tooltip" title="The value of each attribute after regression has ruined your player.">Regressed</th>
                            </tr>
                            <?=$echo?>
                            <tr>
                                <td style="text-align:left;">Banked TPE:</td>
                                <td><input type="text" class="cb" id="curr_bnk" name="bank"  size="4" maxlength="4" tabindex=17 value="<?=$banked?>"></td>
                                <td></td>
                                <td><input type="text" class="cb" id="pct_bnk" size ="4" maxlength="3"></td>
                                <td><input type="text" class="cb" id="reg_bnk" name="reg_bank"  size="4" maxlength="2" readonly="readonly"></td>
                            </tr>
                            <tr>
                                <td style="text-align:left;font-weight:bold;color:#ff7c19;">APE:</td>
                                <td><input type="text" class="cb" id="tpe" name="tpe" size="4" maxlength="4" tabindex=18 value="<?=$ape?>"></td>
                                <td></td>
                                <td><input type="text" readonly="readonly" class="cb" id="lost" size="4" maxlength="4"></td>
                                <td><input type="text" readonly="readonly" class="cb" id="ape" name="ape" size="4" maxlength="4"></td>
                            </tr>
                        </table>
                        <div class="form-group col-sm-12">
                            <input type='text' name='id' value='<?=$id?>' hidden>
                            <button type="button" class="btn btn-default" onclick="Regress()" data-toggle="tooltip" title="Will calculate your Regressed Attribute values based on Current Attribute, Weaknesses/Strengths, and Regression Percentage.">Regress</button>
                            <button type="submit" class="btn btn-danger">Save</button>
                            <button type="button" class="btn btn-default" onclick="Reset()" data-toggle="tooltip" title="Will reset the entire calculator.">Reset</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip({container: 'body'});   
});
</script>
<!-- Body End -->

<?php
include $path.'/footer.php';
?>