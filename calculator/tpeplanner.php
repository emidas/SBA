<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
$user=$_SESSION['user'];
$importbuttons = '';
$sql = "SELECT CONCAT(pfirst,' ',plast) as name, p.id FROM auth_user a INNER JOIN players p ON a.ID=p.user_fk WHERE username=\"$user\" AND (p.active='1' || p.active='3')";
$result = mysqli_query($con,$sql);
while($r = mysqli_fetch_array($result))
{
    $importbuttons .= "<button type='button' class='btn btn-primary' onclick=\"ImportBuild('".$r['id']."');\" data-toggle='tooltip' title='This will import ".$r['name']."'s current build.'>Import (".$r['name'].")</button>";
}

$listarr = Array('Inside Scoring','Jump Shot','Free Throw','Three Point','Handling','Passing','Offensive Rebounding','Defensive Rebounding','Post Defense','Perimeter Defense','Stealing','Blocking','Fouling','Quickness','Strength','Jumping');
$listarr_short = Array('ins','jps','ft','3ps','han','pas','orb','drb','psd','prd','stl','blk','fl','qkn','str','jmp');
$echo = '';
for($v=0;$v<count($listarr);$v++)
{
    $echo .= "
        <tr>
            <td><div class='hidden-xs'>".$listarr[$v].":</div><div class='hidden-sm hidden-md hidden-lg'>".strtoupper($listarr_short[$v]).":</div></td>
            <td><input type='text' class='cb' id='curr_".$listarr_short[$v]."' name='curr_".$listarr_short[$v]."' value='' size='3' maxlength='2' onChange=\"calcManualCost('curr_".$listarr_short[$v]."','increase_".$listarr_short[$v]."','cost_".$listarr_short[$v]."');calc();\"></td>
            <td><button type='button' class='btn btn-primary' name='calculate' onClick=\"subtract('curr_".$listarr_short[$v]."','increase_".$listarr_short[$v]."','cost_".$listarr_short[$v]."');calc();\">-1</button><button type='button' class='btn btn-primary' name='calculate' onClick=\"subtractIncrement('curr_".$listarr_short[$v]."','increase_".$listarr_short[$v]."','cost_".$listarr_short[$v]."','5');calc();\">-5</button><button type='button' class='btn btn-primary' name='calculate' onClick=\"subtractIncrement('curr_".$listarr_short[$v]."','increase_".$listarr_short[$v]."','cost_".$listarr_short[$v]."','10');calc();\">-10</button><button type='button' class='btn btn-primary' name='calculate' onClick=\"add('curr_".$listarr_short[$v]."','increase_".$listarr_short[$v]."','cost_".$listarr_short[$v]."');calc();\">+1</button><button type='button' class='btn btn-primary' name='calculate' onClick=\"addIncrement('curr_".$listarr_short[$v]."','increase_".$listarr_short[$v]."','cost_".$listarr_short[$v]."','5');calc();\">+5</button><button type='button' class='btn btn-primary' name='calculate' onClick=\"addIncrement('curr_".$listarr_short[$v]."','increase_".$listarr_short[$v]."','cost_".$listarr_short[$v]."','10');calc();\">+10</button></td>
            <td><p class='form-control-static' id='increase_".$listarr_short[$v]."'></p></td>
            <td><p class='form-control-static' id='cost_".$listarr_short[$v]."' name='cost_".$listarr_short[$v]."'></p></td>
            <script>
            $(document).ready(function(){
                calcCost('curr_".$listarr_short[$v]."','increase_".$listarr_short[$v]."','cost_".$listarr_short[$v]."');
                calc();
            });
            </script>
        </tr>";
}
$echo .= "<tr><td><div class='hidden-xs'><b>TPE Needed:</b></div><div class='hidden-sm hidden-md hidden-lg'>TPE</div></td><td><input type='text' readonly='readonly' class='cb' id='cost_tpe' name='cost_tpe' size='4' maxlength='4' value='0'></td><td colspan='4'></td></tr>";

// Player Update Scale
$sql = "SELECT `range`,pe FROM update_scale WHERE league='$hostlg' ORDER BY `range` ASC";
$result = mysqli_query($con,$sql);
$addscale = "";
$subtractscale = "";
$x = 0;
while($r = mysqli_fetch_array($result))
{
    if($x == 0)
    {
        $addscale .= "
        if(document.getElementById(current).value<".$r['range'].")
        {
            b = Number(document.getElementById(cost).innerHTML);
            document.getElementById(cost).innerHTML = b + ".$r['pe'].";
        }";
        $subtractscale .= "
        if(document.getElementById(current).value<=".$r['range'].")
        {
            b = Number(document.getElementById(cost).innerHTML);
            b = noNeg(b-".$r['pe'].");
            document.getElementById(cost).innerHTML = b;
        }";
    }
    else
    {
        $addscale .= "
        else if(document.getElementById(current).value<".$r['range'].")
        {
            b = Number(document.getElementById(cost).innerHTML);
            document.getElementById(cost).innerHTML = b + ".$r['pe'].";
        }";
        $subtractscale .= "
        else if(document.getElementById(current).value<=".$r['range'].")
        {
            b = Number(document.getElementById(cost).innerHTML);
            b = noNeg(b-".$r['pe'].");
            document.getElementById(cost).innerHTML = b;
        }";
    }
    $x++;
}
?>

<!-- Body Start -->
<body>
<script>
attr = new Array('ins','jps','ft','3ps','han','pas','orb','drb','psd','prd','stl','blk','fl','qkn','str','jmp');
function noNeg(num) {
if(num < 0)
{
num = 0;
num = Number(num);
return num;
}
else
num = Number(num);
return num;
}

function decimize(num)
{
    a = String(num);
    b = a.split(".")[1];
    b = Number("."+b);
    return b;
}

function subtract(current,increase,cost)
{
    a = Number(document.getElementById(current).value);
    a = noNeg(a-1);
    if(a < document.getElementById(current).min)
    {
        return;
    }
    c = Number(document.getElementById(increase).innerHTML);
    c = noNeg(c-1);
    document.getElementById(increase).innerHTML = c;
    <?=$subtractscale?>
    document.getElementById(current).value = a;
}

function add(current,increase,cost)
{ 
    <?=$addscale?>
    else
    {
        return;
    }
    a = Number(document.getElementById(current).value);
    document.getElementById(current).value = a + 1;
    c = Number(document.getElementById(increase).innerHTML);
    document.getElementById(increase).innerHTML = c + 1;
}
function calc()
{
    document.getElementById('cost_tpe').value = Number(document.getElementById('cost_ins').innerHTML) + Number(document.getElementById('cost_jps').innerHTML) + Number(document.getElementById('cost_ft').innerHTML) + Number(document.getElementById('cost_3ps').innerHTML) + Number(document.getElementById('cost_han').innerHTML) + Number(document.getElementById('cost_pas').innerHTML) + Number(document.getElementById('cost_orb').innerHTML) + Number(document.getElementById('cost_drb').innerHTML) + Number(document.getElementById('cost_psd').innerHTML) + Number(document.getElementById('cost_prd').innerHTML) + Number(document.getElementById('cost_stl').innerHTML) + Number(document.getElementById('cost_blk').innerHTML) + Number(document.getElementById('cost_fl').innerHTML) + Number(document.getElementById('cost_qkn').innerHTML) + Number(document.getElementById('cost_str').innerHTML) + Number(document.getElementById('cost_jmp').innerHTML);
}
function addIncrement(current,increase,cost,increment)
{
    for(x=0;x<increment;x++)
    {
        add(current,increase,cost);

    }
}
function subtractIncrement(current,increase,cost,increment)
{
    for(x=0;x<increment;x++)
    {
        subtract(current,increase,cost);

    }
}
function calcCost(current,increase,cost)
{
    a = document.getElementById(increase).innerHTML;
    document.getElementById(increase).innerHTML = 0;
    document.getElementById(cost).innerHTML = 0;
    document.getElementById(current).value = document.getElementById(current).min;
    addIncrement(current,increase,cost,a);
}
// Function to provide Standard TPE Planner Templates
function Template(type) 
{
    document.getElementById("tpecalc").reset();
    var i = 0;
    if(type == 'Standard')
    {
        while(i < curr.length)
        {
            if(curr[i] == 'curr_fl')
            {
                $('#'+curr[i]).val(50);
            }
            else
            {
                $('#'+curr[i]).val(35);
            }
            i++;
        }
    }
    else if(type == 'Freak')
    {
        while(i < curr.length)
        {
            if((curr[i] == 'curr_fl') || (curr[i] == 'curr_qkn') || (curr[i] == 'curr_str') || (curr[i] == 'curr_jmp'))
            {
                $('#'+curr[i]).val(20);
            }
            else
            {
                $('#'+curr[i]).val(35);
            }
            i++;
        }
    }
    for(var i=0;i<attr.length;i++)
    {
        $('#increase_'+attr[i]).html('0');
        $('#cost_'+attr[i]).html('0');
    }
}
// Function to reset the Regression Calculator
function Reset() 
{
    document.getElementById("tpecalc").reset();
    
    i = 0;
    while(i < attr.length)
    {
        $('#increase_'+attr[i]).html('0');
        $('#cost_'+attr[i]).html('0');
        i++;
    }
}

// Function to Import a player's ratings
function ImportBuild(pid)
{
    var build = new Array();
    $.ajax({type: 'GET', url: "../ajax/get_player_build.php?pid="+pid,dataType:"json", success: function(result){
        console.log(result);
        console.log(result['ins']);
        build = result;
        for(var i=0;i<attr.length;i++)
        {
            $('#curr_'+attr[i]).val(build[attr[i]]);
            $('#curr_'+attr[i]).prop('min',build[attr[i]]);
            $('#increase_'+attr[i]).html('0');
            $('#cost_'+attr[i]).html('0');
        }
    }});
    document.getElementById("tpecalc").reset();
}

</script>
<div id="wrapper" style="position:relative;top:0;left:0;">
    <?php include $path.'/includes/nav.php'; ?>
    <div class="container">
        <?=HeaderImagePrint($hostlg,$_SESSION['style'])?>
        <div class="panel-group">
            <form method='ge' id="tpecalc" class="form-horizontal">
                <div class="panel panel-primary">
                    <div class="panel-heading">TPE Planner</div>
                        <div class="panel-body">
                            <div class="form-group col-xs-10 col-xs-offset-1">  
                                <?=$importbuttons?>        
                                <button type="button" class="btn btn-primary" onclick="Template('Standard');" data-toggle="tooltip" title="Template build, will populate with default Standard ratings.">Template(Standard)</button>
                                <button type="button" class="btn btn-primary" onclick="Template('Freak');" data-toggle="tooltip" title="Template build, will populate with default Freak ratings.">Template(Freak)</button>
                                <button type="button" class="btn btn-primary" disabled>Save Build</button>
                                <button type="button" class="btn btn-primary" onclick="Reset()" data-toggle="tooltip" title="Will reset the entire calculator.">Reset</button>
                            </div>
                            <div class="col-xs-12">
                                <div class="table-responsive">
                                    <table class ="table table-condensed compact" style="margin: 0 auto;" id="tpetable">
                                        <thead>
                                            <tr>
                                                <th class="hidden-xs">Attribute Name</th>
                                                <th class="hidden-sm hidden-md hidden-lg">ATT</th>
                                                <th class="hidden-xs">Rating</th>
                                                <th class="hidden-sm hidden-md hidden-lg">RTG</th>
                                                <th></th>
                                                <th class="hidden-xs">Change</th>
                                                <th class="hidden-sm hidden-md hidden-lg">CHG</th>
                                                <th class="hidden-xs">TPE Cost</th>
                                                <th class="hidden-sm hidden-md hidden-lg">TPE</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?=$echo?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Body End -->

<?php
include $path.'/footer.php';
?>
<script>
$('.table-responsive').on('show.bs.dropdown', function () {
    $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
    $('.table-responsive').css( "overflow", "auto" );
});
</script>