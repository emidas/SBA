<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
if(($_SESSION['isadmin'] != 1)){
header("location:/home.php?alert=adminpermission");
}
$user=$_SESSION['user'];

// year
$system = new System();
$year = $system->get_year();
$maint = $system->is_open('maintenance');
$rollover = $system->is_open('rollover');
$sbapred = $system->is_open('predictions');
$ncaapred = $system->is_open('sbdl_predictions');
$big_three = $system->is_open('big_3');
$fivebyfive = $system->is_open('5x5');

// Update Scale
$sql = "SELECT id,`range`,pe FROM update_scale WHERE league='$hostlg' ORDER BY `range` ASC";
$result = mysqli_query($con,$sql);
$f = 0;
$upconcat = "";
while($r = mysqli_fetch_array($result))
{
    $upconcat .= "<div class='input-group' id=\"addr".$f."\"><input type=\"number\" name='row[".$f."][range]' id='row[".$f."][range]' class=\"form-control\" value=\"".$r['range']."\"><span class=\"input-group-addon\">=</span><input type=\"number\" name='row[".$f."][pe]' id='row[".$f."][pe]' class=\"form-control\" value=\"".$r['pe']."\"><span class='input-group-addon'><a href='#rangedelete' onclick='passID(\"".$r['id']."\",\"".$r['range']."\",\"".$r['pe']."\")' data-toggle='modal'><i class='fas fa-trash'></i></a></span></div>";
    $f++;
}
$upconcat .= "<div class='input-group' id=\"addr".$f."\"></div>";

// FIBA Payouts
// generate FIBA Countries
$fiba = Array('Africa','Greece','Argentina','Canada','Mexico','China','Germany','New Zealand','United States','Australia','Japan','Brazil','United Kingdom + Ireland','Latvia','France','Russia','Croatia','Lithuania');
sort($fiba);
$fibalist = '';
for($c=0;$c<count($fiba);$c++)
{
    $fibalist .= "<option value='$fiba[$c]'>$fiba[$c]</option>";
}
// create FIBA pay list
$fibapay = '';
for($v=1;$v<15;$v++)
{
    $opt = 500000 * $v;
    $fibapay .= "<option value='".$opt."'>$".number_format($opt,0)."</option>";
}

// years
$yquery = "SELECT year FROM year";
$yresult = mysqli_query($con,$yquery);
$yecho = '';
$echo = '';
while ($r = mysqli_fetch_array($yresult)) {
    if($year == $r['year']){$echo = 'selected';}else{$echo = '';}
    $yecho .= "<option value = '".$r['year']."' ".$echo.">".$r['year']."</option>";
}
if(isset($_GET['alert'])) {
    if($_GET['alert'] == 'success') {
        $echo = "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Settings Saved.</strong></div>";
    }
    else {
        $echo = '';
    }
}
else
{$echo = '';}
?>

<!-- Body Start -->
<body>
<div id="wrapper" style="position:relative;top:0;left:0;">
    <?php include $path.'/includes/nav.php'; ?>
    <div class="container">
        <?=HeaderImagePrint($hostlg,$_SESSION['style'])?>
        <?=$echo?>
        <form class="form-horizontal" action='check_admin.php' method='POST'>
            <div class="panel panel-default">
                <div class="panel-heading"><h4 class="panel-title">Active Year</h4></div>
                <div class="panel-body">
                    <div class="form-group col-xs-12">
                        <div class='input-group'><span class='input-group-addon' style='width:120px;text-align:left;'>Change Year</span><select class="form-control" name="year"><?=$yecho?></select></div>
                    </div>
                    <div class="form-group col-xs-12 col-sm-4">
                        <button type="submit" name="go_activeyear" class="btn btn-default">Submit</button>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><h4 class="panel-title">Import/Export</h4></div>
                <div class="panel-body">
                    <div id="ioalert"></div>
                    <a href="/scripts/generate_sba_rookies.php" class="btn btn-primary col-xs-4 col-xs-offset-4 mb pubutton">Generate SBA Rookies</a>
                    <a href="/scripts/generate_sbdl_rookies.php" class="btn btn-primary col-xs-4 col-xs-offset-4 mb pubutton">Generate SBDL Rookies</a>
                    <a href="/scripts/generate_efl_players.php" class="btn btn-primary col-xs-4 col-xs-offset-4 mb pubutton">Generate EFL Export</a>
                    <a href="/scripts/generate_ecfa_players.php" class="btn btn-primary col-xs-4 col-xs-offset-4 mb pubutton">Generate ECFA Export</a>
                    <button class="btn btn-primary col-xs-4 col-xs-offset-4 mb pubutton" onclick="sequence('<?=$mainlg?>')">Sequence EFL Players</button>
                    <button class="btn btn-primary col-xs-4 col-xs-offset-4 mb pubutton" onclick="sequence('<?=$sublg?>')">Sequence ECFA Players</button>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><h4 class="panel-title">Giveaways</h4></div>
                <div class="panel-body">
                    <div class="form-group col-xs-12">
                        <div class='input-group col-sm-4'><span class='input-group-addon' style='width:130px;text-align:left;'>Giveaway Name</span><input type='text' class='form-control' name='giveaway_task'></div>
                        <div class='input-group col-sm-4'><span class='input-group-addon' style='width:130px;text-align:left;'>League</span><select class="form-control" name="giveaway_league"><option>All</option><option>SBDL</option><option>SBA</option></select></div>
                        <div class='input-group col-sm-4'><span class='input-group-addon' style='width:130px;text-align:left;'>TPE Value</span><input type='text' class='form-control' name='giveaway_pe'></div>
                        <div class="input-group date col-sm-4" data-provide="datepicker" name="week_picker"><span class='input-group-addon' style='width:130px;text-align:left;'>Beginning Date</span><input type="text" class="form-control input-sm" name='giveaway_beg_date'><div class="input-group-addon"><i class="fas fa-calendar-alt"></i></div></div>
                        <div class="input-group date col-sm-4" data-provide="datepicker" name="week_picker"><span class='input-group-addon' style='width:130px;text-align:left;'>Ending Date</span><input type="text" class="form-control input-sm" name='giveaway_end_date'><div class="input-group-addon"><i class="fas fa-calendar-alt"></i></div></div>
                    </div>
                    <div class="form-group col-xs-12 col-sm-4">
                        <button type="submit" name="go_giveaway" class="btn btn-default">Submit</button>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><h4 class="panel-title">Claims</h4></div>
                <div class="panel-body">
                    <div class="form-group col-xs-12">
                        <div class='input-group'><span class='input-group-addon' style='width:140px;text-align:left;'>Maintenance</span><input type="checkbox" <?php if($maint == '1') echo "checked" ?> data-toggle="toggle" name="maintenance"></div>
                    </div>
                    <div class="form-group col-xs-12">
                        <div class='input-group'><span class='input-group-addon' style='width:140px;text-align:left;'>Rollover</span><input type="checkbox" <?php if($rollover == '1') echo "checked" ?> data-toggle="toggle" name="rollover"></div>
                    </div>
                    <div class="form-group col-xs-12">
                        <div class='input-group'><span class='input-group-addon' style='width:140px;text-align:left;'>SBA Predictions</span><input type="checkbox" <?php if($sbapred == '1') echo "checked" ?> data-toggle="toggle" name="sbapred"></div>
                    </div>
                    <div class="form-group col-xs-12">
                        <div class='input-group'><span class='input-group-addon' style='width:140px;text-align:left;'>SBDL Predictions</span><input type="checkbox" <?php if($ncaapred == '1') echo "checked" ?> data-toggle="toggle" name="sbdlpred"></div>
                    </div>
                    <div class="form-group col-xs-12">
                        <div class='input-group'><span class='input-group-addon' style='width:140px;text-align:left;'>SBAO Big 3</span><input type="checkbox" <?php if($big_three == '1') echo "checked" ?> data-toggle="toggle" name="bigthree"></div>
                    </div>
                    <div class="form-group col-xs-12">
                        <div class='input-group'><span class='input-group-addon' style='width:140px;text-align:left;'>SBAO 5x5</span><input type="checkbox" <?php if($fivebyfive == '1') echo "checked" ?> data-toggle="toggle" name="fivebyfive"></div>
                    </div>
                    <div class="form-group col-xs-12 col-sm-4">
                        <button type="submit" name="go_predictions" class="btn btn-default">Submit</button>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><h4 class="panel-title">FIBA Payouts</h4></div>
                <div class="panel-body">
                    <div class="form-group col-xs-12">
                        <div class='input-group col-sm-4'><span class='input-group-addon' style='width:130px;text-align:left;'>Country</span><select class="form-control" name="fiba_country"><?=$fibalist?></select></div>
                        <div class='input-group col-sm-4'><span class='input-group-addon' style='width:130px;text-align:left;'>Payout</span><select class="form-control" name="fiba_payout"><?=$fibapay?></select></div>
                    </div>
                    <div class="form-group col-xs-12 col-sm-4">
                        <button type="submit" name="go_fibapayout" class="btn btn-default">Submit</button>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><h4 class="panel-title">Update Scale</h4></div>
                <div class="panel-body">
                    <div class="form-group col-xs-12 col-sm-4" id="tab_logic">
                        <?=$upconcat?>
                    </div>
                    <br><br>
                    <input type="hidden" id="numrows" value='<?=$f?>'>
                    <div class="form-group col-xs-12">
                        <a id="add_row" class="btn btn-primary">Add Range</a><a id='delete_row' class="btn btn-primary" onmouseout='earnTPE()'>Remove Range</a>
                    </div>
                    <div class="form-group col-xs-12 col-sm-4">
                        <button type="submit" name="go_updatescale" class="btn btn-default">Submit</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<div class='modal fade' id='rangedelete' role='dialog'>
    <div class='modal-dialog modal-lg'>
        <div class='modal-content'>
            <form method='POST' action='check_admin.php'>
                <div class='modal-header'>
                    <button type='button' class='close' data-dismiss='modal'>&times;</button>
                    <h4 class='modal-title'>Delete Range</h4>
                </div>
                <div class='modal-body'>
                    <div id="deleterange"></div>
                    <input type="hidden" name="rid" id="rid">
                </div>
                <div class='modal-footer'>
                    <button type="submit" name="go_deleterange" class="btn btn-default">Delete</button>
                    <button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Body End -->

<?php
include $path.'/footer.php';
?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker3.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
<script>
$(document).ready(function(){
    $("div[name^='week_picker']").datepicker({
        daysOfWeekHighlighted: "0",
        todayHighlight: true,
        autoclose: true,
        clearBtn: true
    });
    var i = $('#numrows').val();
    i = Number(i);
    $("#add_row").click(function(){
        var i = $('#numrows').val();
        i = Number(i);
        $('#addr'+i).html("<input type=\"number\" name='row["+i+"][range]' id='row["+i+"][range]' class=\"form-control\" value='' onchange='earnTPE()'><span class=\"input-group-addon\">-</span><input type=\"number\" name='row["+i+"][pe]' id='row["+i+"][pe]' class=\"form-control\" value='' onchange='earnTPE()'>");
        $('#tab_logic').append('<div class="input-group" id="addr'+(i+1)+'"></div>');
        i++; 
        document.getElementById("numrows").value = Number(document.getElementById("numrows").value) + 1;
    });
    $("#delete_row").click(function(){
        var i = $('#numrows').val();
        i = Number(i);
        if(i>1){
        $("#addr"+(i-1)).html('');
        i--;
        document.getElementById("numrows").value = Number(document.getElementById("numrows").value) - 1;
    }
    });
});
function passID(id,range,pe)
{
    $('#rid').val(id);
    $('#deleterange').empty();
    $('#deleterange').html("You are about to delete your Update Range ending in "+range+", worth "+pe+" TPE. Do you wish to continue? This action is <i>irreversible</i>.");   
}
function sequence(league) 
{
    $.ajax({url: "../ajax/sequence_league.php?league="+league, success: function(result){
      $("#ioalert").val(league+" successfully Sequenced!");
    }});
}
</script>