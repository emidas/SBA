<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
$user = new User($_SESSION['user']);
$username = $user->username;
$userid = $user->id;
$reg_date = $user->reg_date;
$beta = $user->isbeta;
$efl_fk = $user->efl_fk;
$sbapreds = $user->has_submitted('predictions');
$sbdlpreds = $user->has_submitted('sbdl_predictions');
$bigthrees = $user->has_submitted('games_big_3');
$fives = $user->has_submitted('games_5x5');
$set_sba = ($sbapreds > 0) ? "<i class='fas fa-check'></i>" : "<i class='fas fa-times'></i>";
$sba_class = ($sbapreds > 0) ? "has-success" : "has-error";
$set_sbdl = ($sbdlpreds > 0) ? "<i class='fas fa-check'></i>" : "<i class='fas fa-times'></i>";
$sbdl_class = ($sbdlpreds > 0) ? "has-success" : "has-error";
$set_bigthree = ($bigthrees > 0) ? "<i class='fas fa-check'></i>" : "<i class='fas fa-times'></i>";
$bigthree_class = ($bigthrees > 0) ? "has-success" : "has-error";
$set_fivexfive = ($fives > 0) ? "<i class='fas fa-check'></i>" : "<i class='fas fa-times'></i>";
$fivexfive_class = ($fives > 0) ? "has-success" : "has-error";
$system = new System();
$year = $system->get_year();
if(isset($efl_fk))
{
    $efl_username = $user->check_efl_name();
    $efl_integration = "
    <div class=\"form-group col-xs-12\">
        <div class='input-group col-sm-4'><span class='input-group-addon' style='width:130px;text-align:left;'>EFL Username</span><input type='text' class='form-control' name='efl_name' value=".$efl_username." disabled><span class='input-group-addon'><a href='#efldelete' data-toggle='modal'><i class='fas fa-trash'></i></a></span></div>
    </div>";
}
else
{
    $efl_integration = "
    <div class=\"form-group col-xs-12\">
        <div class='input-group col-sm-4'><span class='input-group-addon' style='width:130px;text-align:left;'>Username</span><input type='text' class='form-control' name='efl_name'></div>
        <div class='input-group col-sm-4'><span class='input-group-addon' style='width:130px;text-align:left;'>Password</span><input type='password' class='form-control' name='efl_pass'></div>
    </div>
    <div class=\"form-group col-xs-12 col-sm-4\">
        <button type=\"submit\" name=\"go_efl\" class=\"btn btn-default\">Connect to EFL</button>
    </div>";
}

// CAP Subscriptions
$query = "SELECT cap_sub,cap_sub_pm FROM auth_user WHERE username=\"$username\"";
$caps=mysqli_fetch_array(mysqli_query($con,$query));
$capsub=$caps['cap_sub'];
$capsubpm=$caps['cap_sub_pm'];
$cap = "";
$cap .= "   
<div class='panel panel-default'>
    <div class='panel-heading'><h4 class='panel-title'>Create-A-Player Notification Subscription</h4></div>
    <div class='panel-body'>
        <div class='form-group col-xs-12'>
            <div class='input-group col-sm-4'><span class='input-group-addon' style='width:150px;text-align:left;'>".$hostlg."O Notifications</span><input type='checkbox' ".(($capsub == '1') ? "checked" : "")." data-toggle=\"toggle\" name='capsub'></div>
        </div>
        <div class='form-group col-xs-12'>
            <div class='input-group col-sm-4'><span class='input-group-addon' style='width:150px;text-align:left;'>Forum PMs</span><input type='checkbox' ".(($capsubpm == '1') ? "checked" : "")." data-toggle=\"toggle\" name='capsubpm'></div>
        </div>
        <div class=\"form-group col-xs-12 col-sm-4\">
            <button type=\"submit\" name=\"go_cap\" class=\"btn btn-default\">Submit</button>
        </div>
    </div>
</div>";

$query = "SELECT * FROM auth_user WHERE username=\"$username\"";
$s = mysqli_fetch_array(mysqli_query($con,$query));


if($beta == '1')
{
    $betastyles = "<option value = 'mist'".(($s['style'] == 'mist') ? "selected" : "").">Mist</option>";
}
else
{
    $betastyles = '';
}


if(isset($_GET['alert'])) 
{
    if($_GET['alert'] == 'success') 
    {
        $echo = "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Settings Saved.</strong></div>";
    }
    else if ($_GET['alert'] == 'wrongpass') 
    {
        $echo = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Wrong Password.</strong></div>";
    }
    else if ($_GET['alert'] == 'noeflfound') 
    {
        $echo = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>There was no EFL account found with this username.</strong></div>";
    }
    else 
    {
        $echo = '';
    }
}
else
{$echo = '';}
?>

<!-- Body Start -->
<body>
<div id="wrapper" style="position:relative;top:0;left:0;">
    <?php include $path.'/includes/nav.php'; ?>
    <div class="container">
        <?=HeaderImagePrint($hostlg,$_SESSION['style'])?>
        <?=$echo?>
        <form class="form-horizontal" action='/check_settings.php' method='POST'>
            <div class="panel panel-default">
                <div class="panel-heading"><h4 class="panel-title">User Information</h4></div>
                <div class="panel-body">
                    <div class="form-group col-xs-12">
                        <div class='input-group col-sm-4'><span class='input-group-addon' style='width:130px;text-align:left;'>Username</span><input type='text' class='form-control' name='userinfo_name' value="<?=$username?>" disabled></div>
                        <div class='input-group col-sm-4'><span class='input-group-addon' style='width:130px;text-align:left;'>Persona Level</span><input type='text' class='form-control' name='userinfo_datereg' value="1" disabled></div>
                        <div class='input-group col-sm-4'><span class='input-group-addon' style='width:130px;text-align:left;'>Date Registered</span><input type='text' class='form-control' name='userinfo_datereg' value="<?=$reg_date?>" disabled></div>
                        <div class='input-group col-sm-4'><span class='input-group-addon' style='width:130px;text-align:left;'><?=$hostlg?> Premium</span><input type='text' class='form-control' name='userinfo_datereg' value="Not Subscribed" disabled></div>
                        <a href="/forms/predictions.php"><div class='input-group col-sm-4 <?=$sba_class?>'><span class='input-group-addon' style='width:110px;text-align:left;'><b>S<?=$year?> <?=$mainlg?> Predictions</b></span><span class='input-group-addon'><?=$set_sba?></span></div></a>
                        <a href="/forms/sbdl_predictions.php"><div class='input-group col-sm-4 <?=$sbdl_class?>'><span class='input-group-addon' style='width:110px;text-align:left;'><b>S<?=$year?> <?=$sublg?> Predictions</b></span><span class='input-group-addon'><?=$set_sbdl?></span></div></a>
                        <a href="/forms/games_big_three.php"><div class='input-group col-sm-4 <?=$bigthree_class?>'><span class='input-group-addon' style='width:110px;text-align:left;'><b>S<?=$year?> <?=$hostlg?>O Big 4</b></span><span class='input-group-addon'><?=$set_bigthree?></span></div></a>
                        <a href="/forms/games_five_x_five.php"><div class='input-group col-sm-4 <?=$fivexfive_class?>'><span class='input-group-addon' style='width:110px;text-align:left;'><b>S<?=$year?> <?=$hostlg?>O 8x8</b></span><span class='input-group-addon'><?=$set_fivexfive?></span></div></a>
                    </div>
                </div>
            </div>
            <?=$cap?>
            <div class="panel panel-default">
                <div class="panel-heading"><h4 class="panel-title">Style</h4></div>
                <div class="panel-body">
                    <div class="form-group col-xs-12">
                        <div class='input-group col-sm-4'><span class='input-group-addon' style='width:130px;text-align:left;'>Style</span>
                            <select name="style" class="form-control">
                                <option value = "default" <?php if($s['style'] == 'default') echo "selected" ?>>Default</option>
                                <option value = "dark" <?php if($s['style'] == 'dark') echo "selected" ?>>Dark</option>
                                <option value = "darklime" <?php if($s['style'] == 'darklime') echo "selected" ?>>Dark - Lime Variant</option>
                                <option value = "hotdog" <?php if($s['style'] == 'hotdog') echo "selected" ?>>Hot Dog</option>
                                <option value = "pumpkin" <?php if($s['style'] == 'pumpkin') echo "selected" ?>>Pumpkin</option>
                                <?=$betastyles?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-xs-12 col-sm-4">
                        <button type="submit" name="go_style" class="btn btn-default">Submit</button>
                    </div>
                </div>
            </div>
            <?php if($hostlg == 'SBA') { echo "
            <div class=\"panel panel-default\">
                <div class=\"panel-heading\"><h4 class=\"panel-title\">EFL Integration</h4></div>
                <div class=\"panel-body\">
                    ".$efl_integration."
                </div>
            </div>";}?>
        </form>
    </div>
</div>
<div class='modal fade' id='efldelete' role='dialog'>
    <div class='modal-dialog modal-lg'>
        <div class='modal-content'>
            <form method='POST' action='check_settings.php'>
                <div class='modal-header'>
                    <button type='button' class='close' data-dismiss='modal'>&times;</button>
                    <h4 class='modal-title'>Delete EFL Integration</h4>
                </div>
                <div class='modal-body'>
                    <div id="deleterange">Continuing will remove your EFL integration in SBAO. If you are sure you want to continue, please click Delete below.</div>
                </div>
                <div class='modal-footer'>
                    <button type="submit" name="go_deleteefl" class="btn btn-default">Delete</button>
                    <button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Body End -->

<?php
include $path.'/footer.php';
?>