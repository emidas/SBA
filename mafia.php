<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
$roles = Array('Sheriff','Godfather','Werewolf','Coven Leader','Vampire','Lookout');
$ti = Array('Investigator','Lookout','Psychic','Sheriff','Spy','Tracker');
$ts = Array('Escort','Mayor','Medium','Retributionist','Transporter');
$tp = Array('Bodyguard','Crusader','Doctor','Trapper');
$tk = Array('Jailor','Vampire Hunter','Veteran','Vigilante');
$ms = Array('Blackmailer','Consigliere','Consort');
$md = Array('Disguiser','Forger','Framer','Hypnotist','Janitor');
$mk = Array('Ambusher','Mafioso','Godfather');
$nb = Array('Amnesiac','Guardian Angel','Survivor');
$ne = Array('Executioner','Jester'); // No Witch
$nk = Array('Arsonist','Juggernaut','Serial Killer','Werewolf');
$nc = Array('Pirate','Plaguebearer'); // No Pestilence as Plaguebearer transforms into it. Vampire is pushed into $neutral if Vampire Hunter exists.
$coven = Array('Coven Leader','Hex Master','Medusa','Necromancer','Poisoner','Potion Master');
$unique = Array('Mayor','Jailor','Retributionist','Veteran','Ambusher','Godfather','Mafioso','Juggernaut','Pirate','Plaguebearer','Werewolf','Coven Leader','Hex Master','Medusa','Necromancer','Poisoner','Potion Master');
$town = array_merge($tp,$ts,$ti,$tk);
$mafia = array_merge($ms,$md,$mk);
$neutral = array_merge($nb,$ne,$nk,$nc);
$users = Array('jhatty8','zachary','okochastar','mmflex','jcole194','noahcole1103','omgitshim','reality96','zekethatbeast25','sixersfan594','mr. splash man','ace','pennywise','taiwo','bigzouzou','psanchez55','oscartheswagdude','symmetrik','thelastolympian07','ptyrell','abaddon','robotastic');
$used = Array();
$arr = Array();
// 22 users
// 15 town
// 6 faction
// 4 neutral

// Guardian Angel target cannot be Executioner, Jester, or Guardian Angel.
// Executioner target must be a Townie. Cannot be Mayor or Jailor.
// Amnesiac cannot choose Unique Town roles such as Mayor, Jailor, Retributionist, and Veteran.

// shuffle users
shuffle($users);

$echo = '';
$i = 0;
// Town Investigative Logic
$ti_limit = 3;
$echo .= "<b>Town Investigative Roles</b><br>";
for($x=0;$x<$ti_limit;$x++)
{
    shuffle($ti);
    $role = $ti[$x];
    while(in_array($role,$used) && in_array($role,$unique))
    {
        shuffle($ti);
        $role = $ti[$x];
    }
    $echo .= $role.": ".$users[$i]."<br>";
    $arrs = ['name' => $users[$i], 'role' => $role];
    array_push($arr,$arrs);
    array_push($used,$role);
    $i++;
}
$echo .= "<br>";

// Town Support Logic
$ts_limit = 2;
$echo .= "<b>Town Support Roles</b><br>";
for($x=0;$x<$ts_limit;$x++)
{
    shuffle($ts);
    $role = $ts[$x];
    while(in_array($role,$used) && in_array($role,$unique))
    {
        shuffle($ts);
        $role = $ts[$x];
    }
    $echo .= $role.": ".$users[$i]."<br>";
    $arrs = ['name' => $users[$i], 'role' => $role];
    array_push($arr,$arrs);
    array_push($used,$role);
    $i++;
}
$echo .= "<br>";

// Town Protective Logic
$tp_limit = 2;
$echo .= "<b>Town Protective Roles</b><br>";
for($x=0;$x<$tp_limit;$x++)
{
    shuffle($tp);
    $role = $tp[$x];
    while(in_array($role,$used) && in_array($role,$unique))
    {
        shuffle($tp);
        $role = $tp[$x];
    }
    $echo .= $role.": ".$users[$i]."<br>";
    $arrs = ['name' => $users[$i], 'role' => $role];
    array_push($arr,$arrs);
    array_push($used,$role);
    $i++;
}
$echo .= "<br>";

// Town Killing Logic
$tk_limit = 2;
$echo .= "<b>Town Killing Roles</b><br>";
for($x=0;$x<$tk_limit;$x++)
{
    if($x == 0)
    {
        $role = 'Jailor';
    }
    else
    {
        shuffle($tk);
        $role = $tk[$x];
    }
    while(in_array($role,$used) && in_array($role,$unique))
    {
        shuffle($tk);
        $role = $tk[$x];
    }
    $echo .= $role.": ".$users[$i]."<br>";
    $arrs = ['name' => $users[$i], 'role' => $role];
    array_push($arr,$arrs);
    array_push($used,$role);
    $i++;
}
$echo .= "<br>";

// Random Town Logic
$rt_limit = 4;
$echo .= "<b>Random Town Roles</b><br>";
for($x=0;$x<$rt_limit;$x++)
{
    shuffle($town);
    $role = $town[$x];
    while(in_array($role,$used) && in_array($role,$unique))
    {
        shuffle($town);
        $role = $town[$x];
    }
    $echo .= $role.": ".$users[$i]."<br>";
    $arrs = ['name' => $users[$i], 'role' => $role];
    array_push($arr,$arrs);
    array_push($used,$role);
    $i++;
}
$echo .= "<br>";

// Mafia Logic
$ma_limit = 5;
$echo .= "<b>Mafia Roles</b><br>";
for($x=0;$x<$ma_limit;$x++)
{
    //if($x == 0)
    {
        $role = 'Godfather';
    }
    //else if($x == 1)
    {
        $role = 'Mafioso';
    }
    //else
    if(1==1)
    {
        shuffle($mafia);
        $role = $mafia[$x];
    }
    while(in_array($role,$used) && in_array($role,$unique))
    {
        shuffle($mafia);
        $role = $mafia[$x];
    }
    $echo .= $role.": ".$users[$i]."<br>";
    $arrs = ['name' => $users[$i], 'role' => $role];
    array_push($arr,$arrs);
    array_push($used,$role);
    $i++;
}
$echo .= "<br>";

// Coven Logic
$co_limit = 0;
$echo .= "<b>Coven Roles</b><br>";
for($x=0;$x<$co_limit;$x++)
{
    //if($x == 0)
    {
        $role = 'Coven Leader';
    }
    //else if($x == 1)
    {
        $role = 'Medusa';
    }
    //else
    if(1==1)
    {
        shuffle($coven);
        $role = $coven[$x];
    }
    while(in_array($role,$used) && in_array($role,$unique))
    {
        shuffle($coven);
        $role = $coven[$x];
    }
    $echo .= $role.": ".$users[$i]."<br>";
    $arrs = ['name' => $users[$i], 'role' => $role];
    array_push($arr,$arrs);
    array_push($used,$role);
    $i++;
}
$echo .= "<br>";

// Neutral Logic
$ne_limit = 4;
$echo .= "<b>Neutral Roles</b><br>";
for($x=0;$x<$ne_limit;$x++)
{
    // Check if Vampire Hunter exists, push Vampire
    if(in_array('Vampire Hunter',$used))
    {
        array_push($neutral,'Vampire');
    }
    if(($x == ($ne_limit-1)) && (in_array('Vampire Hunter',$used)) && (!in_array('Vampire',$used)))
    {
        $role = 'Vampire';
    }
    else
    {
        shuffle($neutral);
        $role = $neutral[$x];
    }
    while(in_array($role,$used) && in_array($role,$unique))
    {
        shuffle($neutral);
        $role = $neutral[$x];
    }
    $echo .= $role.": ".$users[$i]."<br>";
    $arrs = ['name' => $users[$i], 'role' => $role];
    array_push($arr,$arrs);
    array_push($used,$role);
    $i++;
}
$echo .= "<br>";

// Target Logic
$count = count($arr);
for($x=0;$x<$count;$x++)
{
    if($arr[$x]['role'] == 'Executioner')
    {
        // Executioner target must be a Townie. Cannot be Mayor or Jailor.
        $index = $count - 1;
        $y = mt_rand(0,$index);
        $player = $arr[$y];
        while((!in_array($player['role'],$town)) || (($player['role'] == 'Mayor') || ($player['role'] == 'Jailor')))
        {
            $y = mt_rand(0,$index);
            $player = $arr[$y];
        }
        $echo .= "<b>".$arr[$x]['name']."'s</b> target for execution is <b>".$player['name']."</b> the ".$player['role']."<br>";
    }
    else if($arr[$x]['role'] == 'Guardian Angel')
    {
        // Guardian Angel target cannot be Executioner, Jester, or Guardian Angel.
        $index = $count - 1;
        $y = mt_rand(0,$index);
        $player = $arr[$y];
        while(($player['role'] == 'Executioner') || ($player['role'] == 'Jester') || ($player['role'] == 'Guardian Angel'))
        {
            $y = mt_rand(0,$index);
            $player = $arr[$y];
        }
        $echo .= "<b>".$arr[$x]['name']."'s</b> protection target is <b>".$player['name']."</b> the ".$player['role']."<br>";
    }
}
$echo .= "<br>";


$alert = '';
?>
<!-- Body Start -->
<body>
<div id="wrapper" style="position:relative;top:0;left:0;">
    <?php include $path.'/includes/nav.php'; ?>
    <div class="container">
        <?=HeaderImagePrint($hostlg,$_SESSION['style'])?>
        <?=$alert?>
        <div class="row">
            <?=$echo?>
        </div>
    </div>
</div>
<!-- Body End -->

<?php
include $path.'/footer.php';
mysqli_close($con);
?>