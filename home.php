<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';

if(empty($_SESSION['user'])){
header("location:/index.php");
}

$user = new User($_SESSION['user']);
$username = $user->username;
$userid = $user->id;
$efl_fk = $user->efl_fk;
$system = new System();
$year = $system->get_year();

//Alerts.
if(isset($_GET['alert'])) 
{
    if($_GET['alert'] == 'success') 
    {
        $alert = "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Login Successful.</strong></div>";
    }
    else if($_GET['alert'] == 'cookiesuccess') 
    {
        $alert = "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Login Successful. For security purposes, you will asked to login again on ".date('m/d/Y',(time() + (86400 * 60))).".</strong></div>";
    }
    else if ($_GET['alert'] == 'adminpermission') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You are not an Admin.</strong></div>";
    }
    else if ($_GET['alert'] == 'simmerpermission') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You are not a Simmer.</strong></div>";
    }
    else if ($_GET['alert'] == 'gmpermission') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You are not a General Manager.</strong></div>";
    }
    else if ($_GET['alert'] == 'nocoach') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You are not a Coach.</strong></div>";
    }
    else if ($_GET['alert'] == 'adpermission') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You are not an Athletic Director.</strong></div>";
    }
    else if ($_GET['alert'] == 'betapermission') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>No Peeking.</strong></div>";
    }
    else if ($_GET['alert'] == 'playerexists') 
    {
        $alert = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You already have an SBDL player.</strong></div>";
    }
}
else
{$alert = '';}

//Get the last 4 threads from SBA games and SBDL games.
if($hostlg == 'SBA')
{
    $simquery = "SELECT tid,title,start_date FROM molholt_sba.forums_topics t INNER JOIN molholt_sba.forums_posts p ON t.tid=p.topic_id WHERE (forum_id='17' || forum_id='243') AND new_topic='1' ORDER BY start_date DESC LIMIT 4";
}
else if($hostlg == 'EFL')
{
    $simquery = "SELECT tid,title,start_date FROM efl_forums.forums_topics t INNER JOIN efl_forums.forums_posts p ON t.tid=p.topic_id WHERE (forum_id='27' || forum_id='33') AND new_topic='1' AND starter_id != '0' ORDER BY start_date DESC LIMIT 4";
}

$simresult = mysqli_query($con, $simquery);
$simecho = "";
while ($simthreads = mysqli_fetch_array($simresult)) 
{
    $unix_time = $simthreads['start_date'];
    $simdate = time_Ago($unix_time);

    $simecho .= "<div class='sim-feed text-center'><h5 class='sim-title'><a href='https://".$forum_domain."/forums/index.php?/topic/" . $simthreads['tid'] . "-" . $simthreads['title'] . "' target='_blank'>" . $simthreads['title'] . "</a>" . $simdate ;
}

//Get the last 3 announcements.
if($hostlg == 'SBA')
{
    $aquery = "SELECT tid,title,starter_name FROM molholt_sba.forums_topics t INNER JOIN molholt_sba.forums_posts p ON t.tid=p.topic_id WHERE (forum_id='3') AND new_topic='1' AND starter_name!='' ORDER BY start_date DESC LIMIT 3";
}
else if($hostlg == 'EFL')
{
    $aquery = "SELECT tid,title,starter_name FROM efl_forums.forums_topics t INNER JOIN efl_forums.forums_posts p ON t.tid=p.topic_id WHERE (forum_id='2') AND new_topic='1' AND starter_name!='' ORDER BY start_date DESC LIMIT 3";
}
$aresult = mysqli_query($con, $aquery);
$annecho = "";
while ($announc = mysqli_fetch_array($aresult)) 
{
    $annecho .= "<div class='sim-feed'><h5 class='sim-title'><a href='https://".$forum_domain."/forums/index.php?/topic/" . $announc['tid'] . "-" . $announc['title'] . "' target='_blank'>" . $announc['title'] ."</a></div>"; 
}


//Get the last 5 $mainlg articles.
if($hostlg == 'SBA')
{
    $sbaquery = "SELECT tid,title,starter_name,post FROM molholt_sba.forums_topics t INNER JOIN molholt_sba.forums_posts p ON t.tid=p.topic_id WHERE (forum_id='20') AND new_topic='1' ORDER BY start_date DESC LIMIT 5";
}
else if($hostlg == 'EFL')
{
    $sbaquery = "SELECT tid,title,starter_name,post FROM efl_forums.forums_topics t INNER JOIN efl_forums.forums_posts p ON t.tid=p.topic_id WHERE (forum_id='36') AND new_topic='1' ORDER BY start_date DESC LIMIT 5";
}
$sbaresult = mysqli_query($con, $sbaquery);
$sbarticles = "";
while ($sbathreads = mysqli_fetch_array($sbaresult)) {
    $sbarticles .= "<div class='sim-feed'><h5 class='sim-title'><a href='https://".$forum_domain."/forums/index.php?/topic/" . $sbathreads['tid'] . "-" . $sbathreads['title'] . "' target='_blank'>" . $sbathreads['title'] . "</a></h5><h5 class='author'><small>Written by " . $sbathreads['starter_name'] . "</small></h5></div>";
}

if($hostlg == 'SBA')
{
    //Get the last 5 SBDL articles.
    $sbdlquery = "SELECT tid,title,starter_name,post FROM molholt_sba.forums_topics t INNER JOIN molholt_sba.forums_posts p ON t.tid=p.topic_id WHERE (forum_id='724') AND new_topic='1' ORDER BY start_date DESC LIMIT 5";
    $sbdlresult = mysqli_query($con, $sbdlquery);
    $sbdlarticles = "";
    while ($sbdlthreads = mysqli_fetch_array($sbdlresult)) 
    {
        $sbdlarticles .= "<div class='sim-feed'><h5 class='sim-title'><a href='https://".$forum_domain."/forums/index.php?/topic/" . $sbdlthreads['tid'] . "-" . $sbdlthreads['title'] . "' target='_blank'>" . $sbdlthreads['title'] . "</a></h5><h5 class='sim-date'><small>Written by " . $sbdlthreads['starter_name'] . "</small></h5></div>"; 
    }
}

if($hostlg == 'SBA')
{
    //TPE opportunities.
    $sql = "SELECT tid,title,title_seo
    FROM molholt_sba.forums_topics
    WHERE state = 'open'
    AND forum_id ='836'
    ORDER BY tid DESC
    LIMIT 5";
    $result = mysqli_query($con,$sql);
    $tpeopprows = mysqli_num_rows($result);
    if ($tpeopprows != 0) {
        $tpeopp = "";
        $tpeoppecho = "";
        while($r = mysqli_fetch_array($result))
        {
            $tpeopp .= "<a href='https://$forum_domain/forums/index.php?/topic/".$r['tid']."-".$r['title_seo']."/' target='_blank'>".$r['title']."</a>";

            $tpeoppecho .= "<div class='col-sm-12 pt bg-basic border mbot'><div class='card-header bordertr'><h4 class='text-center bordertr'>TPE Opportunities</h4></div><div class='card-body text-center padlr'>" . $tpeopp . "</div></div>";
        }
    }
}
else
{
    $tpeopprows = '';
}

// Player Info
$playertabs = "<div class=\"card-header bordertr\"><ul class=\"nav nav-tabs bordertr\">";
$playercontent = "<div class=\"card-body tab-content\">";
$f = 0;
$stmt = $conn->prepare("SELECT id FROM players p WHERE (active = '1' || active = '3') AND user_fk=:id");
$stmt->execute([':id' => $userid]);
$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
foreach ($result as $row)
{
    $echo = $buttons = $content = "";
    $player = new Player($row['id']);
    $pid = $player->id;
    $pname = $player->name;
    $plg = $player->league;
    $ptpe = $player->tpe;
    $pape = $player->ape;
    $pbank = $player->bank;
    $stmt = $conn->prepare("SELECT earn_tpe,status FROM player_updates WHERE pid_fk = :id AND (status < 2)");
    $stmt->execute([':id' => $pid]);
    $count = $stmt->rowCount();
    if($count == 0){$submit = "<i class='fas fa-times'></i>";$earned = '0';}
    else
    {
        $r = $stmt->fetch(PDO::FETCH_ASSOC);
        $earned = $r['earn_tpe'];
        $submit = "<i class='fas fa-check'></i>";
    }
    $echo .= "<td class='text'>".$plg."</td><td class='hidden-xs text'>".$ptpe."</td><td class='text'>".$pape."</td><td class='hidden-xs text'>".$pbank."</td><td class='text'>$submit</td><td><span class='badge'> " . $earned . "</span></td>";
    $buttons .= "<div class='col-sm-6 padtopb'><a href='/forms/player_page.php?pid=".$pid."' class='btn btn-primary btn-block'>Player Page</a></div><div class='col-sm-6 padtopb'><div class='btn-block'><a href='/forms/player_update.php?pid=".$pid."' class='btn btn-primary btn-block'>Update Page</a></div></div>";
    $content .= "<div class='col-sm-12 pt'><table class='table table-responsive'><thead><tr><th>League</th><th class='hidden-xs'>TPE</th><th>APE</th><th class='hidden-xs'>Bank</th><th class='hidden-xs'>Submitted</th><th class='hidden-sm hidden-md hidden-lg'>Sub.</th><th>P.E.</th></tr></thead><tbody>".$echo."</tbody></table></div>".$buttons;
    $playertabs .= "<li class=\"".(($f == 0) ? "active" : "")." bordertr\"><a data-toggle=\"tab\" href=\"#player$f\">".$pname."</a></li>";
    $playercontent .= "<div id=\"player$f\" class=\"tab-pane fade ".(($f == 0) ? "in active" : "")."\">$content</div>";
    $f++;
}
if($hostlg == 'SBA' && isset($efl_fk))
{
    $stmt = $conn->prepare("SELECT CONCAT(firstname,' ',lastname) as name,TPE,APE,inEFL,unique_ID FROM eflmanager.Players p WHERE user_ID=:id AND hasretired='0'");
    $stmt->execute([':id' => $efl_fk]);
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    foreach ($result as $row)
    {
        $echo = $buttons = $content = "";
        $pname = $row['name'];
        $eflpid = $row['unique_ID'];
        $plg = ($row['inEFL'] > 0) ? "EFL" : "ECFA";
        $ptpe = $row['TPE'];
        $pape = $row['APE'];
        $pbank = "-";
        $stmt = $conn->prepare("SELECT sum(taskTPE) as earn_tpe FROM eflmanager.Updates WHERE player_UID = :id AND updated = '0'");
        $stmt->execute([':id' => $eflpid]);
        $count = $stmt->rowCount();
        if($count == 0){$submit = "<i class='fas fa-times'></i>";$earned = '0';}
        else
        {
            $r = $stmt->fetch(PDO::FETCH_ASSOC);
            $earned = $r['earn_tpe'];
            $submit = "<i class='fas fa-check'></i>";
        }
        $echo .= "<td class='text'>".$plg."</td><td class='hidden-xs text'>".$ptpe."</td><td class='text'>".$pape."</td><td class='hidden-xs text'>".$pbank."</td><td class='text'>$submit</td><td><span class='badge'> " . $earned . "</span></td>";
        $buttons .= "";
        $content .= "<div class='col-sm-12 pt'><table class='table table-responsive'><thead><tr><th>League</th><th class='hidden-xs'>TPE</th><th>APE</th><th class='hidden-xs'>Bank</th><th class='hidden-xs'>Submitted</th><th class='hidden-sm hidden-md hidden-lg'>Sub.</th><th>P.E.</th></tr></thead><tbody>".$echo."</tbody></table></div>".$buttons;
        $playertabs .= "<li class=\"".(($f == 0) ? "active" : "")." bordertr\"><a data-toggle=\"tab\" href=\"#player$f\">".$pname."</a></li>";
        $playercontent .= "<div id=\"player$f\" class=\"tab-pane fade ".(($f == 0) ? "in active" : "")."\">$content</div>";
        $f++;
    }
}
$playertabs .= "</ul></div>";
$playercontent .= "</div>";

//Check if user has an active player.
$check = "SELECT * FROM players WHERE user_fk = '$userid' AND (active='1' || (league='SBDL' && active='2') || active='3')";
$checkresults = mysqli_query($con,$check);
$hasplayers = mysqli_num_rows($checkresults);
if ($hasplayers < 1) {
    $p1name = "Create a player";
    $p1content = "<div class='col-sm-12 padtopb'><p>You don't have an active player at the moment. Please create one.</p><a href='/forms/player_create.php' class='btn btn-primary btn-block'>Create player</a></div>";
}

// Get SBA Standings
if($hostlg == 'SBA')
{
    $sql = "SELECT * FROM standings WHERE league='sba'";
    $sbar = mysqli_query($con, $sql);
    if (mysqli_num_rows($sbar) > 0)
    {   
        $sbast = '<tr>';

        while ($row = mysqli_fetch_array($sbar))
        {
            $sbast .= '<td>'. $row['t_name'] .'</td>';
            $sbast .= '<td>'. $row['t_w'] .'</td>';
            $sbast .= '<td>'. $row['t_l'] .'</td>';
            $sbast .= '<td>'. $row['t_gb'] .'</td>';
            $sbast .= '<td>'. $row['t_lten'] .'</td>';
            $sbast .= '</tr>';
        }
    }

    // Get SBDL Standings
    $sql = "SELECT * FROM standings WHERE league='sbdl'";
    $sbdlr = mysqli_query($con, $sql);
    if (mysqli_num_rows($sbdlr) > 0)
    {   
        $sbdlst = '<tr>';

        while ($row = mysqli_fetch_array($sbdlr))
        {
            $sbdlst .= '<td>'. $row['t_name'] .'</td>';
            $sbdlst .= '<td>'. $row['t_w'] .'</td>';
            $sbdlst .= '<td>'. $row['t_l'] .'</td>';
            $sbdlst .= '<td>'. $row['t_gb'] .'</td>';
            $sbdlst .= '<td>'. $row['t_lten'] .'</td>';
            $sbdlst .= '</tr>';
        }
    }

    // Display the standings
    $standings = '<div class="card-header bordertr">
                    <ul class="nav nav-tabs bordertr">
                        <li class="active bordertr"><a data-toggle="tab" href="#standings-sba">SBA</a></li>
                        <li><a data-toggle="tab" href="#standings-sbdl">SBDL</a></li>
                    </ul>
                </div>
                <div class="card-body tab-content table-responsive">
                    <div id="standings-sba" class="tab-pane fade in active">
                        <table class="table table-striped text-left">
                            <thead>
                                <tr>
                                    <th>Team</th>
                                    <th>W</th>
                                    <th>L</th>
                                    <th>GB</th>
                                    <th>L10</th>
                                </tr>
                            </thead>
                            <tbody>
                                ' . $sbast . '
                            </tbody>
                        </table>
                    </div>

                    <div id="standings-sbdl" class="tab-pane fade">
                        <table class="table table-striped text-left">
                            <thead>
                                <tr>
                                    <th>Team</th>
                                    <th>W</th>
                                    <th>L</th>
                                    <th>GB</th>
                                    <th>L10</th>
                                </tr>
                            </thead>
                            <tbody>
                               ' . $sbdlst . '
                            </tbody>
                        </table>
                    </div>
                </div>';
}
else if ($hostlg == 'EFL')
{
    // EFL East standings
    $sql = "SELECT * FROM standings WHERE (league='efl') AND (conf='East')";
    $efler = mysqli_query($con, $sql);
    if (mysqli_num_rows($efler) > 0)
    {   
        $efle = '<tr>';

        while ($row = mysqli_fetch_array($efler))
        {
            $efle .= '<td>'. $row['t_name'] .'</td>';
            $efle .= '<td>'. $row['t_w'] .'</td>';
            $efle .= '<td>'. $row['t_l'] .'</td>';
            $efle .= '</tr>';
        }
    }

    // EFL West standings
    $sql = "SELECT * FROM standings WHERE (league='efl') AND (conf='West')";
    $eflwr = mysqli_query($con, $sql);
    if (mysqli_num_rows($eflwr) > 0)
    {   
        $eflw = '<tr>';

        while ($row = mysqli_fetch_array($eflwr))
        {
            $eflw .= '<td>'. $row['t_name'] .'</td>';
            $eflw .= '<td>'. $row['t_w'] .'</td>';
            $eflw .= '<td>'. $row['t_l'] .'</td>';
            $eflw .= '</tr>';
        }
    }

    // ECFA Heroes standings
    $sql = "SELECT * FROM standings WHERE (league='ecfa') AND (conf='Heroes')";
    $ecfahr = mysqli_query($con, $sql);
    if (mysqli_num_rows($ecfahr) > 0)
    {   
        $ecfah = '<tr>';

        while ($row = mysqli_fetch_array($ecfahr))
        {
            $ecfah .= '<td>'. $row['t_name'] .'</td>';
            $ecfah .= '<td>'. $row['t_w'] .'</td>';
            $ecfah .= '<td>'. $row['t_l'] .'</td>';
            $ecfah .= '</tr>';
        }
    }

    // ECFA Legends standings
    $sql = "SELECT * FROM standings WHERE (league='ecfa') AND (conf='Legends')";
    $ecfalr = mysqli_query($con, $sql);
    if (mysqli_num_rows($ecfalr) > 0)
    {   
        $ecfal = '<tr>';

        while ($row = mysqli_fetch_array($ecfalr))
        {
            $ecfal .= '<td>'. $row['t_name'] .'</td>';
            $ecfal .= '<td>'. $row['t_w'] .'</td>';
            $ecfal .= '<td>'. $row['t_l'] .'</td>';
            $ecfal .= '</tr>';
        }
    }

    $standings = '<div class="card-header bordertr">
                    <ul class="nav nav-tabs bordertr">
                        <li class="active bordertr"><a data-toggle="tab" href="#standings-efl">EFL</a></li>
                        <li><a data-toggle="tab" href="#standings-ecfa">ECFA</a></li>
                    </ul>
                </div>
                <div class="card-body tab-content pt">
                    <div id="standings-efl" class="tab-pane fade in active">
                        <div class="col-sm-6 pt br">
                            <div class="table-responsive">
                                <table class="table table-striped">                                    
                                    <thead>
                                        <tr>
                                            <th>Team</th>
                                            <th>W</th>
                                            <th>L</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        ' . $efle . '
                                    </tbody>
                                </table>  
                            </div>                          
                        </div>
                        <div class="col-sm-6 pt">
                            <div class="table-responsive">
                                <table class="table table-striped">                                   
                                    <thead>
                                        <tr>
                                            <th>Team</th>
                                            <th>W</th>
                                            <th>L</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       ' . $eflw . '
                                    </tbody>
                                </table>
                            </div>                            
                        </div>
                    </div>

                    <div id="standings-ecfa" class="tab-pane fade">
                        <div class="col-sm-6 pt br">
                            <div class="table-responsive">
                                <table class="table table-striped">                                    
                                    <thead>
                                        <tr>
                                            <th>Team</th>
                                            <th>W</th>
                                            <th>L</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        ' . $ecfah . '
                                    </tbody>
                                </table>      
                            </div>                      
                        </div>
                        <div class="col-sm-6 pt">
                            <div class="table-responsive">
                                <table class="table table-striped">                                    
                                    <thead>
                                        <tr>
                                            <th>Team</th>
                                            <th>W</th>
                                            <th>L</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        ' . $ecfal . '
                                    </tbody>
                                </table>      
                            </div>                      
                        </div>
                    </div>
                </div>
            </div>';
}

?>

<!-- Body start -->
<style>
    .border {
        border-radius: .5rem;
    }

    .header {
        font-weight: bold;
    }
    
    .pt {
        padding: 0;
    }

    .table {
        margin-bottom: .5rem;
    }
    
    .mzt {
        margin: 5rem 0 0 0;
    }

    .mb {
        margin-bottom: 1rem;
    }
    
    .bordertr {
        border-top-right-radius: .5rem;
        border-top-left-radius: .5rem;
    }

    h4 {
        margin-top: 0;
        padding: 0.625rem 0;
    }
    
    h5 {
        margin-top: 1rem;
        margin-bottom: .5rem;
    }
    .mbot {
        margin-bottom: 5rem;
    }

    .table>thead>tr>th {
        border-bottom: 0;
    }

    .padtopb {
        padding-top: 1.4rem;
        padding-bottom: 1rem;
    }

    .padlr {
        padding: 0 1.5rem .5rem;   
    }

    .sim-date, .author {
        margin: .5rem 0 1rem;
    }
</style>
<body>
<div id="wrapper" style="position:relative;top:0;left:0;">
    <?php include $path.'/includes/nav.php'; ?>
    <div class="container">
        <?=$alert?>
        <div class="row">
            <!-- Articles feed and player home -->
            <div class="col-sm-5 col-sm-push-2 col-sm-offset-1 border pt mb">
                <!-- Player info -->
                <div class="col-sm-12 pt bg-basic border mbot">
                    <?=$playertabs?>
                    <?=$playercontent?>
                </div>
                <!-- SBA+SBDL articles or EFL standings-->
                <div class="col-sm-12 pt text-center bg-basic border mbot">
                    <?php if($hostlg == 'SBA') {echo "
                        <div class=\"card-header bordertr\">
                            <ul class=\"nav nav-tabs bordertr\">
                                <li class=\"bordertr active\"><a data-toggle=\"tab\" href=\"#SBAnetwork\">".$mainlg."Network</a></li>
                                <li class=\"bordertr\"><a data-toggle=\"tab\" href=\"#SBDLnetwork\">".$sublg."Network</a></li>
                            </ul>   
                        </div>
                        <div class=\"card-body tab-content text-left\">
                            <div id=\"SBAnetwork\" class=\"tab-pane fade in active text-left padlr\">
                                ".$sbarticles."
                            </div>
                            <div id=\"SBDLnetwork\" class=\"tab-pane fade text-left padlr\">
                                ".$sbdlarticles."
                            </div>
                        </div>
                    </div>";}

                    else if($hostlg == 'EFL') { echo $standings;}?>
            </div>
            <!-- Sim threads, calendar and announcements -->
            <div class="col-sm-2 border col-sm-pull-6 pt mbot">
                <!-- TPE Opportunities -->
                <?php if ($tpeopprows != 0) { echo $tpeoppecho; } ?>
                <!-- Sim threads -->
                <div class="col-sm-12 pt bg-basic border mbot">
                    <div class="card-header bordertr">
                        <h4 class="text-center bordertr">Recent Games</h4>
                    </div>
                    <div class="card-body text-center padlr">
                        <?=$simecho?>
                    </div>
                </div>
                <!-- Announcements -->
                <div class="col-sm-12 pt bg-basic border">
                    <div class="card-header bordertr">
                        <h4 class="text-center bordertr">Announcements</h4>
                    </div>
                    <div class="card-body text-left padlr">
                        <?=$annecho?>
                    </div>
                </div>
            </div>
            <!-- Standings -->
            <div class="col-sm-3 col-sm-offset-1 border bg-basic pt mb">
                <?php if($hostlg == 'SBA') {echo $standings;}

                else if($hostlg == 'EFL') {echo "<div class=\"col-sm-12 pt text-center bg-basic border\">
                    <div class=\"card-header bordertr\">
                        <ul class=\"nav nav-tabs bordertr\">
                            <li class=\"bordertr active\"><a data-toggle=\"tab\" href=\"#SBAnetwork\">".$mainlg."Network</a></li>
                        </ul>   
                    </div>
                    <div class=\"card-body tab-content text-left\">
                        <div id=\"SBAnetwork\" class=\"tab-pane fade in active text-left padlr\">".$sbarticles."</div>";}?>
            </div>

        </div>
    </div>
</div>
</body>

<?php
include $path.'/footer.php';
mysqli_close($con);
?>

<script>
    $('.table-responsive').on('show.bs.dropdown', function () {
        $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
        $('.table-responsive').css( "overflow", "auto" );
})
</script>