<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/sba_process.php';
$position = $_GET['pos'];
$position2 = $_GET['pos2'];
$build = $_GET['build'];
$height = $_GET['height'];

if($hostlg == 'SBA')
{
    $heightarr = Array("5'0","5'1","5'2","5'3","5'4","5'5","5'6","5'7","5'8","5'9","5'10","5'11","6'0","6'1","6'2","6'3","6'4","6'5");
    if($build == 'Standard')
    {
        if($position == 'PG' || $position2 == 'PG')
        {

        }
        else if($position == 'SG' || $position2 == 'SG')
        {
            array_push($heightarr,"6'6","6'7");
        }
        else if($position == 'SF' || $position2 == 'SF')
        {
            array_push($heightarr,"6'6","6'7","6'8","6'9");
        }
        else if($position == 'PF' || $position2 == 'PF')
        {
            array_push($heightarr,"6'6","6'7","6'8","6'9","6'10","6'11");
        }
        else
        {
            array_push($heightarr,"6'6","6'7","6'8","6'9","6'10","6'11","7'0","7'1");
        }
    }
    else
    {
        if($position == 'PG' || $position2 == 'PG')
        {
            array_push($heightarr,"6'6","6'7","6'8");
        }
        else if($position == 'SG' || $position2 == 'SG')
        {
            array_push($heightarr,"6'6","6'7","6'8","6'9","6'10");
        }
        else if($position == 'SF' || $position2 == 'SF')
        {
            array_push($heightarr,"6'6","6'7","6'8","6'9","6'10","6'11","7'0");
        }
        else if($position == 'PF' || $position2 == 'PF')
        {
            array_push($heightarr,"6'6","6'7","6'8","6'9","6'10","6'11","7'0","7'1","7'2");
        }
        else
        {
            array_push($heightarr,"6'6","6'7","6'8","6'9","6'10","6'11","7'0","7'1","7'2","7'3","7'4");
        }
    }
}
else if($hostlg == 'EFL')
{
    switch ($position):
        case "QB":
            $heightarr = Array("6'0","6'1","6'2","6'3","6'4");
            if($build == 'Field General')
            {
                array_push($heightarr,"6'5");
            }
            else if($build == 'Pocket Passer')
            {
                array_push($heightarr,"6'5","6'6");
            }
            break;
        case "RB":
            $heightarr = Array("5'10","5'11","6'0");
            if($build == 'One-Cut')
            {
                array_push($heightarr,"5'8","5'9","6'1");
            }
            else if($build == 'Workhorse')
            {
                array_push($heightarr,"6'1","6'2");
            }
            else if($build == 'Scat Back')
            {
                array_push($heightarr,"5'8","5'9");
            }
            break;
        case "FB":
            $heightarr = Array("5'10","5'11","6'0","6'1","6'2");
            if($build == 'Blocking')
            {
                array_push($heightarr,"6'3");
            }
            break;
        case "WR":
            $heightarr = Array("5'10","5'11","6'0");
            if($build == 'Deep Threat')
            {
                array_push($heightarr,"5'8","5'9");
            }
            else if($build == 'Possession')
            {
                array_push($heightarr,"6'1","6'2");
            }
            else if($build == 'Red Zone')
            {
                array_push($heightarr,"6'1","6'2","6'3","6'4");
            }
            break;
        case "TE":
            $heightarr = Array();
            if($build == 'Vertical Threat')
            {
                array_push($heightarr,"6'0","6'1","6'2");
            }
            else if($build == 'Possession')
            {
                array_push($heightarr,"6'2","6'3","6'4","6'5");
            }
            else if($build == 'Blocking')
            {
                array_push($heightarr,"6'3","6'4","6'5","6'6","6'7");
            }
            break;
        case "LB":
            $heightarr = Array("6'2");
            if($build == 'Balanced')
            {
                array_push($heightarr,"6'0","6'1","6'3");
            }
            else if($build == 'Coverage')
            {
                array_push($heightarr,"5'11","6'0","6'1");
            }
            else if($build == '3/4 Tackling')
            {
                array_push($heightarr,"6'1","6'3","6'4");
            }
            else if($build == '3/4 Tackling')
            {
                array_push($heightarr,"6'1","6'3","6'4");
            }
            break;
        case "CB":
            $heightarr = Array("6'0");
            if($build == 'Man Cover')
            {
                array_push($heightarr,"5'8","5'9","5'10","5'11");
            }
            else if($build == 'Zone Cover')
            {
                array_push($heightarr,"5'10","5'11","6'1");
            }
            else if($build == 'Run Support')
            {
                array_push($heightarr,"6'1","6'2");
            }
            break;
        case "FS":
            $heightarr = Array("6'0","6'1");
            if($build == 'Man Cover')
            {
                array_push($heightarr,"5'9","5'10","5'11");
            }
            else if($build == 'Zone Cover')
            {
                array_push($heightarr,"5'10","5'11","6'2");
            }
            else if($build == 'Run Support')
            {
                array_push($heightarr,"6'2");
            }
            break;
        case "SS":
            $heightarr = Array("6'0");
            if($build == 'Man Cover')
            {
                array_push($heightarr,"5'9","5'10","5'11");
            }
            else if($build == 'Zone Cover')
            {
                array_push($heightarr,"5'10","5'11","6'1","6'2");
            }
            else if($build == 'Run Support')
            {
                array_push($heightarr,"6'1","6'2","6'3");
            }
            break;
        case "K":
            $heightarr = Array("5'7","5'8","5'9","5'10","5'11","6'0","6'1","6'2","6'3");
            break;
        default:
            $heightarr = Array("6'0","6'1","6'2","6'3","6'4");
            break;
    endswitch;
}

$listheight = "";
shuffle($heightarr);
for ($a=0;$a<count($heightarr);$a++)
{
    if(!in_array($height,$heightarr) && $a==0)
    {
        $listheight .= "<option value=\"$heightarr[$a]\" selected>$heightarr[$a]</option>";
    }
    else
    {
        if($height == $heightarr[$a]) {$echo = "selected";} else {$echo = "";}
        $listheight .= "<option value=\"$heightarr[$a]\" $echo>$heightarr[$a]</option>";
    }
}
echo $listheight;
?>