<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/sba_process.php';
$category=$_GET['category'];
$league=$_GET['league'];
$type=$_GET['type'];
$season=$_GET['season'];
$export=$_GET['export'];
if($export == '0')
{
    if($category == 'season')
    {
        echo printplayersingleseason($con,'%',$league,$type,$season,$export);
    }
    else if($category == 'alltime')
    {
        echo printalltime($con,$league,$type,$export);
    }
}
else
{
    if($category == 'season')
    {
        echo printplayersingleseason($con,'%',$league,$type,$season,$export);
        exit();
    }
    else if($category == 'alltime')
    {
        echo printalltime($con,$league,$type,$export);
        exit();
    }
}
?>