<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/sba_process.php';
$position = $_GET['pos'];
$build = $_GET['build'];

if($hostlg == 'EFL')
{
    switch ($position):
        case "QB":
            $buildarr = Array('Pocket Passer','Field General','Gunslinger','Dual-Threat');
            break;
        case "RB":
            $buildarr = Array('One-Cut','Workhorse','Scat Back');
            break;
        case "FB":
            $buildarr = Array('Runner','Blocker','HBack');
            break;
        case "WR":
            $buildarr = Array('Deep Threat','Possession','Redzone');
            break;
        case "TE":
            $buildarr = Array('Vertical Threat','Possession','Blocking');
            break;
        case "LB":
            $buildarr = Array('Balanced','Coverage','3/4 Tackling','Pass Rushing');
            break;
        case "CB":
            $buildarr = Array('Man Cover','Zone Cover','Run Support');
            break;
        case "FS":
            $buildarr = Array('Man Cover','Zone Cover','Run Support');
            break;
        case "SS":
            $buildarr = Array('Man Cover','Zone Cover','Run Support');
            break;
        case "K":
            $buildarr = Array('Kicking');
            break;
        default:
            break;
    endswitch;
}
else if($hostlg == 'SBA')
{
    $buildarr = Array('Standard','Freak');
}

$listbuild = "";
for ($a=0;$a<count($buildarr);$a++)
{
    if($build == $buildarr[$a]) {$echo = "selected";} else {$echo = "";}
    $listbuild .= "<option value=\"$buildarr[$a]\" $echo>$buildarr[$a]</option>";
}
echo $listbuild;
?>