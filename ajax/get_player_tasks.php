<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/sba_process.php';
$mid = $_GET['mid'];

$player = new User($_SESSION['user']);
$eflmid = $player->efl_memberid;

$json = json_decode(getTopics('SBA',$mid));
$count = count($json->results);
$echo = '';
for($x=0;$x<$count;$x++)
{
    $url = $json->results[$x]->url;
    $id = $json->results[$x]->id;
    $forumid = $json->results[$x]->forum->id;
    $forumname = $json->results[$x]->forum->name;
    $postDate = $json->results[$x]->firstPost->date;  
    $content = $json->results[$x]->firstPost->content;
    $contentR = strip_tags($content);
    $contentArr = explode(" ",$contentR);
    $wordCount = count($contentArr);
    $postingDate = date("m/d/Y", strtotime($postDate));
    $postingDateUnix = strtotime($postDate);
    $title = $json->results[$x]->title;
    $weekEnd = date("m/d/Y", strtotime('Sunday'));
    $weekEndUnix = strtotime($weekEnd);
    $timecheck = date('N');
    if($timecheck == '1')
    {
        $weekBegin = date("m/d/Y", strtotime('now'));
        $weekBeginUnix = strtotime('N');
    }
    else
    {
        $weekBegin = date("m/d/Y", strtotime('last Monday'));
        $weekBeginUnix = strtotime('last Monday');
    }
    switch ($forumid):
        case "456":
            $tpe=floor($wordCount/100);
            if($tpe>3){$tpe=3;}
            $pt="Mini-PT";
            break;
        case "78":
            $tpe=floor($wordCount/100);
            if($tpe>3){$tpe=3;}
            $pt="Quote"; 
            break;
        case "20":
            $tpe=floor($wordCount/100);
            if($tpe>9){$tpe=9;}
            $pt="Network PT";
            break;
        case "724":
            $tpe=floor($wordCount/100);
            if($tpe>9){$tpe=9;}
            $pt="Network PT";
            break;
        case "32":
            $tpe=floor($wordCount/100);
            if($tpe>6){$tpe=6;}
            $pt="Media Spot";
            break;
        case "34":
            $tpe=6;
            $pt="Graphic";
            break;
        case "36":
            $tpe=6;
            $pt="Podcast";
        default:
            $tpe=6;
            $pt="PT";
            break;
    endswitch;

    $sql = "SELECT id FROM player_updates_tasks WHERE link LIKE '%$url%'";
    $num = mysqli_num_rows(mysqli_query($con,$sql));
    if(($postingDateUnix > $weekBeginUnix) && ($num == 0))
    {
        //$echo .= "<div class='alert alert-info alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Your ".$pt." titled <a href=\"$url\">\"".$title."\"</a> has been posted during the week beginning ".$weekBegin." and ending ".$weekEnd." and has not yet been claimed for ".$tpe." TPE. Would you like to add this to this week's update?</strong></div>";
        $echo .= "<a class='btn btn-default m-5' onclick=\"addPlayerTask('$pt','$url','$tpe')\">Click to claim \"".$title."\" for ".$tpe." TPE!</a>";
        //$echo .= "Your ".$pt." titled \"".$title."\" has been posted during the week beginning ".$weekBegin." and ending ".$weekEnd." and has not yet been claimed for ".$tpe." TPE. Would you like to add this to this week's update? <a href=\"$url\">Here is a link to your ".$pt."</a>.<br>Word Count: ".$wordCount."<br>Post Date(Unix): ".$postingDateUnix."<br>Week Begin(Unix): ".$weekBeginUnix."<br>Week End(Unix): ".$weekEndUnix."<br><br>"; 
        $curl_post_data = array(
        'topic' => $id,
        'author' => '2574',
        'post' => "<p>This ".$pt." has been claimed in ".$hostlg."O for ".$tpe." TPE (".$wordCount." words).</p>"
        );
        createPost($curl_post_data);
    }
}
$json = json_decode(getTopics('EFL',$eflmid));
$count = count($json->results);
for($x=0;$x<$count;$x++)
{
    $url = $json->results[$x]->url;
    $id = $json->results[$x]->id;
    $forumid = $json->results[$x]->forum->id;
    $forumname = $json->results[$x]->forum->name;
    $postDate = $json->results[$x]->firstPost->date;  
    $content = $json->results[$x]->firstPost->content;
    $contentR = strip_tags($content);
    $contentArr = explode(" ",$contentR);
    $wordCount = count($contentArr);
    $postingDate = date("m/d/Y", strtotime($postDate));
    $postingDateUnix = strtotime($postDate);
    $title = $json->results[$x]->title;
    $weekEnd = date("m/d/Y", strtotime('Sunday'));
    $weekEndUnix = strtotime($weekEnd);
    $timecheck = date('N');
    if($timecheck == '1')
    {
        $weekBegin = date("m/d/Y", strtotime('now'));
        $weekBeginUnix = strtotime('N');
    }
    else
    {
        $weekBegin = date("m/d/Y", strtotime('last Monday'));
        $weekBeginUnix = strtotime('last Monday');
    }
    switch ($forumid):
        default:
            $tpe=6;
            $pt="Affiliate PT";
            break;
    endswitch;

    $sql = "SELECT id FROM player_updates_tasks WHERE link LIKE '%$url%'";
    $num = mysqli_num_rows(mysqli_query($con,$sql));
    if(($postingDateUnix > $weekBeginUnix) && ($num == 0))
    {
        //$echo .= "<div class='alert alert-info alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Your ".$pt." titled <a href=\"$url\">\"".$title."\"</a> has been posted during the week beginning ".$weekBegin." and ending ".$weekEnd." and has not yet been claimed for ".$tpe." TPE. Would you like to add this to this week's update?</strong></div>";
        $echo .= "<a class='btn btn-default m-5' onclick=\"addPlayerTask('$pt','$url','$tpe')\">Click to claim \"".$title."\" for ".$tpe." TPE!</a>";
        //$echo .= "Your ".$pt." titled \"".$title."\" has been posted during the week beginning ".$weekBegin." and ending ".$weekEnd." and has not yet been claimed for ".$tpe." TPE. Would you like to add this to this week's update? <a href=\"$url\">Here is a link to your ".$pt."</a>.<br>Word Count: ".$wordCount."<br>Post Date(Unix): ".$postingDateUnix."<br>Week Begin(Unix): ".$weekBeginUnix."<br>Week End(Unix): ".$weekEndUnix."<br><br>"; 
        $curl_post_data = array(
        'topic' => $id,
        'author' => '2574',
        'post' => "<p>This ".$pt." has been claimed in ".$hostlg."O for ".$tpe." TPE.</p>"
        );
        //createPost($curl_post_data);
    }
}
$echo .= "<script>
function addPlayerTask(pt,url,tpe)
{
    var i = $('#numrows').val();
    i = Number(i);
    var init_task = $(\"select[name='row[0][task]']\").val();
    var week = $(\"#weektpe\").val();
    if(i == 1 && init_task == '')
    {
        $(\"select[name='row[0][task]']\").val(pt);
        $(\"input[name='row[0][week]']\").val(week);
        $(\"input[name='row[0][link]']\").val(url);
        $(\"input[name='row[0][pe]']\").val(tpe);
    }
    else
    {
        $('#addr'+i).html(\"<td><select class='form-control input-sm' name='row[\"+i+\"][task]' onchange='autope(\"+i+\",this.value)' required><option value=''></option><option value='Affiliate PT'>Affiliate PT</option><option value='Biography'>Biography</option><option value='Graphic'>Graphic</option><option value='Job Pay'>Job Pay</option><option value='Media Spot'>Media Spot</option><option value='Mini-PT'>Mini-PT</option><option value='Network PT'>Network PT</option><option value='Other'>Other</option><option value='Podcast'>Podcast</option><option value='Press Conference'>Press Conference</option><option value='Quote'>Quote</option><option value='Rookie Profile'>Rookie Profile</option><option value='SBAO Adjustment'>SBAO Adjustment</option><option value='Sim Attendance'>Sim Attendance</option><option value='Simmer/SBAO'>Simmer/SBAO</option><option value='Welfare - Inactive'>Welfare - Inactive</option></select></td><td><div class='input-group date' data-provide='datepicker' name='week_picker'><input type='text' class='form-control input-sm' name='row[\"+i+\"][week]' id='row[\"+i+\"][week]' value='' required><div class='input-group-addon'><i class='fas fa-calendar-alt'></i></div></div></td><td><input  name='row[\"+i+\"][link]' id='row[\"+i+\"][link]' type='url' class='form-control input-sm'></td><td><input  name='row[\"+i+\"][pe]' id='row[\"+i+\"][pe]' type='number' class='form-control input-sm' onchange='earnTPE()' required></td><td></td>\");
        $('#tab_logic').append('<tr id=\"addr'+(i+1)+'\"></tr>');
        $(\"div[name^='week_picker']\").datepicker({
            daysOfWeekDisabled: \"".$daysOfWeekDisabled."\",
            daysOfWeekHighlighted: \"".$daysOfWeekHighlighted."\",
            todayHighlight: true,
            autoclose: true,
            clearBtn: true
        });
        $(\"select[name='row[\"+i+\"][task]']\").val(pt);
        $(\"input[name='row[\"+i+\"][week]']\").val(week);
        $(\"input[name='row[\"+i+\"][link]']\").val(url);
        $(\"input[name='row[\"+i+\"][pe]']\").val(tpe);
        i = i+1;
        $('#numrows').val(i);

    }
}</script>";
echo $echo;
?>