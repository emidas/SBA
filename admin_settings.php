<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}
if(($_SESSION['isadmin'] != 1)){
header("location:/home.php?alert=adminpermission");
}
$user = $_SESSION['user'];
$atable = '';
$f = 0;
if(isset($_POST['update'])) 
{
    $user = sanitize($con,$_POST['user']);
    if(isset($_POST['admin'])){$admin = '1';} else {$admin = '0';}
    if(isset($_POST['simmer'])){$simmer = '1';} else {$simmer = '0';}
    if(isset($_POST['updater'])){$updater = '1';} else {$updater = '0';}
    if(isset($_POST['beta'])){$beta = '1';} else {$beta = '0';}
    $sql = "UPDATE auth_user SET isadmin='$admin',issimmer='$simmer',isupdater='$updater',isbeta='$beta' WHERE username = \"$user\"";
    mysqli_query($con,$sql);
    $_GET['alert'] = 'success';
}

$sql = "SELECT username,isadmin,issimmer,isupdater,isbeta,style FROM auth_user ORDER BY username";
$result = mysqli_query($con,$sql);
while($r = mysqli_fetch_array($result))
{
    $atable .= "<tr>";
        $atable .= "<td><div class='dropdown'><a href='#' class='dropdown-toggle' data-toggle='dropdown'>".$r['username']."<span class='caret'></span></a><ul class='dropdown-menu'><li><a href='#' onclick=\"editUser('".$r['username']."','".$r['isadmin']."','".$r['issimmer']."','".$r['isupdater']."','".$r['isbeta']."','".$r['style']."')\" class='btn btn-info'>Edit User</a></li></ul></div></td>";
        $atable .= "<td>".$r['style']."</td>";
        $atable .= "<td>".(($r['isupdater'] > 0) ? "<span class='badge'>Yes</span>" : "<span class='badge'>No</span>")."</td>";
        $atable .= "<td>".(($r['issimmer'] > 0) ? "<span class='badge'>Yes</span>" : "<span class='badge'>No</span>")."</td>";
        $atable .= "<td>".(($r['isadmin'] > 0) ? "<span class='badge'>Yes</span>" : "<span class='badge'>No</span>")."</td>";
        $atable .= "<td>".(($r['isbeta'] > 0) ? "<span class='badge'>Yes</span>" : "<span class='badge'>No</span>")."</td>";
    $atable .= "</tr>";
    $atable .= "
    ";
    $f++;
}
if(isset($_GET['alert'])) 
{
    if($_GET['alert'] == 'success') 
    {
        $echo = "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Settings Saved.</strong></div>";
    }
    else {
        $echo = '';
    }
}
else
{$echo = '';}
?>
<!-- Body Start -->
<body>
<div id="wrapper" style="position:relative;top:0;left:0;">
    <?php include $path.'/includes/nav.php'; ?>
    <div class="container">
        <?=HeaderImagePrint($hostlg,$_SESSION['style'])?>
        <?=$echo?>
        <div class="panel panel-primary">
            <div class="panel-heading">User Management</div>
            <div class="panel-body">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h4 class="panel-title"><a data-toggle="collapse" href="#collapse2">Users</a></h4></div>
                    <div id="collapse2" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div class='table-responsive'>
                                <table class="table" id='view' style="width:100%;">
                                    <thead>
                                        <tr>
                                            <th>User</th>
                                            <th>Style</th>
                                            <th>Updater</th>
                                            <th>Simmer</th>
                                            <th>Admin</th>
                                            <th>Beta</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?=$atable?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class='modal fade' id='edituser' role='dialog'>
        <div class='modal-dialog modal-lg'>
            <div class='modal-content'>
                <form method='POST'>
                    <div class='modal-header'>
                        <button type='button' class='close' data-dismiss='modal'>&times;</button>
                        <h4 class='modal-title'>Edit User</h4>
                    </div>
                    <div class='modal-body'>
                        <div class='panel panel-primary'>
                            <div class='panel-heading'>Change User Information</div>
                            <div class='panel-body'>
                                <div class='form-group'>
                                    <div class='input-group col-xs-10 col-xs-offset-1'>
                                        <span class='input-group-addon' style='width:200px;text-align:left;'>Username</span>
                                        <input type='text' name='user' id='user' class='form-control' value='' readonly>
                                    </div>
                                </div>
                                <div class='form-group'>
                                    <div class='input-group col-xs-10 col-xs-offset-1'>
                                        <span class='input-group-addon' style='width:210px;text-align:left;'>Admin</span>
                                        <input type='checkbox' data-toggle='toggle' data-on='Yes' data-off='No' name='admin' id='admin'>
                                    </div>
                                </div>
                                <div class='form-group'>
                                    <div class='input-group col-xs-10 col-xs-offset-1'>
                                        <span class='input-group-addon' style='width:210px;text-align:left;'>Simmer</span>
                                        <input type='checkbox' data-toggle='toggle' data-on='Yes' data-off='No' name='simmer' id='simmer'>
                                    </div>
                                </div>
                                <div class='form-group'>
                                    <div class='input-group col-xs-10 col-xs-offset-1'>
                                        <span class='input-group-addon' style='width:210px;text-align:left;'>Updater</span>
                                        <input type='checkbox' data-toggle='toggle' data-on='Yes' data-off='No' name='updater' id='updater'>
                                    </div>
                                </div>
                                <div class='form-group'>
                                    <div class='input-group col-xs-10 col-xs-offset-1'>
                                        <span class='input-group-addon' style='width:210px;text-align:left;'>Beta Features</span>
                                        <input type='checkbox' data-toggle='toggle' data-on='Yes' data-off='No' name='beta' id='beta'>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='modal-footer'>
                        <button type='submit' name="update" class='btn btn-danger'>Confirm Changes</button>
                        <button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Body End -->
<script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<?php
include $path.'/footer.php';
?>
<style>
.dropdown-menu
{
    padding:0;
}
.modal-content
{
   border:none;
}
.table-responsive > .table {
    margin-bottom: 0;
}
.table-responsive > .table > thead > tr > th, .table-responsive > .table > tbody > tr > th, .table-responsive > .table > tfoot > tr > th, .table-responsive > .table > thead > tr > td, .table-responsive > .table > tbody > tr > td, .table-responsive > .table > tfoot > tr > td {
    white-space: nowrap;
}
table.dataTable thead .sorting, 
table.dataTable thead .sorting_asc, 
table.dataTable thead .sorting_desc {
    background : none;
}
</style>
<script>
function editUser(user,admin,simmer,updater,beta)
{
    $('#user').val(user);
    if (admin > '0') 
    {
        $('#admin').prop('checked',true).change();
    }
    else
    {
        $('#admin').prop('checked',false).change();
    }
    if (simmer > 0) 
    {
        $('#simmer').prop('checked',true).change();
    }
    else
    {
        $('#simmer').prop('checked',false).change();
    }
    if (updater > 0) 
    {
        $('#updater').prop('checked',true).change();
    }
    else
    {
        $('#updater').prop('checked',false).change();
    }
    if (beta > 0) 
    {
        $('#beta').prop('checked',true).change();
    }
    else
    {
        $('#beta').prop('checked',false).change();
    }
    $('#edituser').modal();
}
$(document).ready(function() 
{
    $('#view').DataTable( 
    {
        paging:   false,
        select: true
    } );
    $('[data-toggle="popover"]').popover(); 
    $('.table-responsive').on('show.bs.dropdown', function () 
    {
        $('.table-responsive').css( "overflow", "inherit" );
    });

    $('.table-responsive').on('hide.bs.dropdown', function () 
    {
        $('.table-responsive').css( "overflow", "auto" );
    });
});
</script>
