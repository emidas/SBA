<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/sba_process.php';
$sql = "SELECT pfirst,plast,height,weight,position,num,country,ins,jps,ft,3ps,han,pas,psd,prd,stl,blk,orb,drb,fl,str,qkn,jmp FROM players WHERE league='SBA' AND experience='1' AND active > '0'";
$result = mysqli_query($con,$sql);
$fp = fopen('php://output', 'w');
$headers = array('FirstName','LastName','Height','Weight','Position','DOB','Age','Uniform','City','State','College','InsideScoring','PotInside','JumpShot','PotJumpShot','FtShot','PotFtShot','3pShot','Pot3pShot','3pUsage','Handling','PotHandling','Passing','PotPassing','PostDefense','PotPostDefense','PerimeterDefense','PotPerimeteDefense','Stealing','PotStealing','Blocking','PotBlocking','OReb','PotOReb','DReb','PotDReb','Fouling','Strength','Quickness','Jumping','Stamina','Picname','InjuryAvoidance');
header('Content-Type: text/csv');
header('Content-Disposition: attachment; filename="sba_rookies.csv"');
header('Pragma: no-cache');
header('Expires: 0');
$yr = "SELECT sba_season FROM year WHERE isactive='1'";
$yrq=mysqli_fetch_array(mysqli_query($con,$yr));
$year=$yrq['sba_season'];
$year = $year - 25;
$dob = "1/25/".$year;
$age = 25;
fputcsv($fp,$headers);
while ($r = mysqli_fetch_assoc($result))
{
    $hei = explode("'",$r['height']);
    $height = ($hei[0]*12) + $hei[1];
    $row = array(
        $r['pfirst'],
        $r['plast'],
        $height,
        $r['weight'],
        $r['position'],
        $dob,
        $age,
        $r['num'],
        '',
        $r['country'],
        'None',
        $r['ins'],
        $r['ins'],
        $r['jps'],
        $r['jps'],
        $r['ft'],
        $r['ft'],
        $r['3ps'],
        $r['3ps'],
        $r['3ps'],
        $r['han'],
        $r['han'],
        $r['pas'],
        $r['pas'],
        $r['psd'],
        $r['psd'],
        $r['prd'],
        $r['prd'],
        $r['stl'],
        $r['stl'],
        $r['blk'],
        $r['blk'],
        $r['orb'],
        $r['orb'],
        $r['drb'],
        $r['drb'],
        $r['fl'],
        $r['str'],
        $r['qkn'],
        $r['jmp'],
        '100',
        '',
        '100'
    );
    fputcsv($fp, $row);
}
?>