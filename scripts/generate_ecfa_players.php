<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/sba_process.php';
$fp = fopen('php://output', 'w');
$headers = array('First Name','Last Name','Jersey','DDSPF Position','Height','Weight','Age','Experience','College','DDSPF TeamID','Strength','Agility','Arm','Speed','Hands','Accuracy','Run Blocking','Pass Blocking','Intelligence','Endurance','Tackling','Kick Distance','Kick Accuracy','Punt Distance','Punt Accuracy','ContractYears','Salary','Overall','PlayerID');
header('Content-Type: text/csv');
header('Content-Disposition: attachment; filename="ecfa_players.csv"');
header('Pragma: no-cache');
header('Expires: 0');
fputcsv($fp,$headers);
$stmt = $conn->prepare("SELECT RTRIM(LTRIM(pfirst)) as pfirst,RTRIM(LTRIM(plast)) as plast,RTRIM(LTRIM(num)) as num,position,height,weight,experience,t.export_id as tid,str,agi,arm,spd,han,acc,rbk,pbk,`int`,tkl,kdi,kac,p.export_id as pid FROM players p INNER JOIN teams t ON p.team=t.id WHERE p.league = :league AND active > '0' ORDER BY p.id");
$stmt->execute([':league' => $sublg]);
$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
foreach ($result as $r)
{
    $hei = explode("'",$r['height']);
    $height = ($hei[0]*12) + $hei[1];
    $row = array(
        $r['pfirst'],
        $r['plast'],
        $r['num'],
        $r['position'],
        $height,
        $r['weight'],
        '25',
        $r['experience'],
        'None',
        $r['tid'],
        $r['str'],
        $r['agi'],
        $r['arm'],
        $r['spd'],
        $r['han'],
        $r['acc'],
        $r['rbk'],
        $r['pbk'],
        $r['int'],
        '100',
        $r['tkl'],
        $r['kdi'],
        $r['kac'],
        $r['kdi'],
        $r['kac'],
        '0',
        '0',
        '0',
        $r['pid']
    );
    fputcsv($fp, $row);
}
?>