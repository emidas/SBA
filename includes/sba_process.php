<?php
class System
{
    // Properties

    // Methods
    function get_year()
    {
        global $conn;
        $stmt = $conn->prepare("SELECT year FROM year WHERE isactive='1'");
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        return $row['year'];
    }
    function get_salary_cap()
    {
        global $conn;
        $stmt = $conn->prepare("SELECT cap FROM salary_cap");
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        return $row['cap'];
    }
    function get_predicted_cap()
    {
        global $conn;
        $stmt = $conn->prepare("SELECT sum(exp_salary) as tsal FROM players WHERE active = '1'");
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        $cap = (round_up_to_nearest_n(($row['tsal']/20), 500000)) + 1500000;
        return $cap;
    }
    function is_open($form)
    {
        global $conn;
        $stmt = $conn->prepare("SELECT isactive from forms where form = :form");
        $stmt->execute([':form' => $form]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        return $row['isactive'];
    }
    function season_rollover_notification()
    {
        global $conn;
        $year = $this->get_year();
        $stmt = $conn->prepare("SELECT DISTINCT user_fk as aid,username FROM players p INNER JOIN auth_user a ON p.user_fk=a.ID  WHERE p.active ='1'");
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach ($result as $row)
        {
            $aid = $row['aid'];
            $user = $row['username'];
            $topic = "Welcome to S".$year."!";
            $message = "<p>Hey ".$user.", the offsason has been run and we are now in S".$year."! This means your cash has been deposited, new Workouts are available, and much more!</p>";
            $stmt = $conn->prepare("INSERT INTO user_messages (a_fk,topic,message) VALUES (:aid,:topic,:message)");
            $stmt->execute([':aid' => $aid, ':topic' => $topic,':message' => $message]);
        }
    }
    function season_predictions_notification()
    {
        global $conn;
        $year = $this->get_year();
        $stmt = $conn->prepare("SELECT DISTINCT user_fk as aid,username FROM players p INNER JOIN auth_user a ON p.user_fk=a.ID  WHERE p.active ='1'");
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach ($result as $row)
        {
            $aid = $row['aid'];
            $user = $row['username'];
            $topic = "S".$year." Predictions now open!";
            $message = "<p>Hey ".$row['username'].", Predictions are now available for S".$year."! You can follow the links below to fill out your Season Predictions for SBA and SBDL!</p><p><a href=\"/forms/predictions.php\">SBA Season Predictions</a>!</p><p><a href=\"/forms/sbdl_predictions.php\">SBDL Season Predictions</a>!</p>";
            $stmt = $conn->prepare("INSERT INTO user_messages (a_fk,topic,message) VALUES (:aid,:topic,:message)");
            $stmt->execute([':aid' => $aid, ':topic' => $topic,':message' => $message]);
        }
    }
    function big_three_notification()
    {
        global $conn;
        $year = $this->get_year();
        $stmt = $conn->prepare("SELECT DISTINCT user_fk as aid,username FROM players p INNER JOIN auth_user a ON p.user_fk=a.ID  WHERE p.active ='1'");
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach ($result as $row)
        {
            $aid = $row['aid'];
            $user = $row['username'];
            $topic = "S".$year." Big 3 now open!";
            $message = "<p>Hey ".$user.", SBAO Big 3 is now available for S".$year."! You can follow the link below to fill out your SBAO Big 3 team today!</p><p><a href=\"/forms/games_big_three.php\">SBAO Big 3</a>!</p>";
            $stmt = $conn->prepare("INSERT INTO user_messages (a_fk,topic,message) VALUES (:aid,:topic,:message)");
            $stmt->execute([':aid' => $aid, ':topic' => $topic,':message' => $message]);
        }
    }
    function five_x_five_notification()
    {
        global $conn;
        $year = $this->get_year();
        $stmt = $conn->prepare("SELECT DISTINCT user_fk as aid,username FROM players p INNER JOIN auth_user a ON p.user_fk=a.ID  WHERE p.active ='1'");
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach ($result as $row)
        {
            $aid = $row['aid'];
            $user = $row['username'];
            $topic = "S".$year." 5x5 now open!";
            $message = "<p>Hey ".$user.", SBAO 5x5 is now available for S".$year."! You can follow the link below to fill out your SBAO 5x5 team today!</p><p><a href=\"/forms/games_five_x_five.php\">SBAO 5x5</a>!</p>";
            $stmt = $conn->prepare("INSERT INTO user_messages (a_fk,topic,message) VALUES (:aid,:topic,:message)");
            $stmt->execute([':aid' => $aid, ':topic' => $topic,':message' => $message]);
        }
    }
}
class User
{
    // Properties
    public $id;
    public $memberid;
    public $username;
    public $reg_date;
    public $active;
    public $isadmin;
    public $issimmer;
    public $isupdater;
    public $style;
    public $efl_fk;
    public $efl_memberid;
    public $cid;

    // Methods
    function __construct($user) 
    {
        global $conn;
        $stmt = $conn->prepare("SELECT ID,memberid,username,reg_date,active,isadmin,issimmer,isupdater,isbeta,style,efl_fk FROM auth_user WHERE username=:user");
        $stmt->execute([':user' => $user]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        $this->id = $row['ID'];
        $this->memberid = $row['memberid'];
        $this->username = $row['username'];
        $this->reg_date = $row['reg_date'];
        $this->active = $row['active'];
        $this->isadmin = $row['isadmin'];
        $this->issimmer = $row['issimmer'];
        $this->isupdater = $row['isupdater'];
        $this->isbeta = $row['isbeta'];
        $this->style = $row['style'];
        $this->efl_fk = $row['efl_fk'];

        $stmt = $conn->prepare("SELECT member_id FROM efl_forums.core_members WHERE name=:user");
        $stmt->execute([':user' => $user]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        $this->efl_memberid = $row['member_id'] ?? '';

        $stmt = $conn->prepare("SELECT id FROM coaches WHERE (name=:user || name2=:user)");
        $stmt->execute([':user' => $user]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        $this->cid = $row['id'] ?? '';
    }
    function get_($attr)
    {
        return $this->$attr;
    }
    function set_active($active)
    {
        global $conn;
        $id = $this->id;
        $stmt = $conn->prepare("UPDATE auth_user SET active = :active WHERE ID=:id");
        $stmt->execute([':id' => $id, ':active' => $active]);
    }
    function player_count($type,$pid)
    {
        global $conn;
        $id = $this->id;
        if($type == 'sba_active_exclude')
        {
            $stmt = $conn->prepare("SELECT id FROM players WHERE user_fk=:id AND league='SBA' AND active = '1' AND id !=:pid");
            $stmt->execute([':id' => $id,':pid' => $pid]);
        }
        else if($type == 'sba_retiring')
        {
            $stmt = $conn->prepare("SELECT id FROM players WHERE user_fk=:id AND league='SBA' AND active = '3'");
            $stmt->execute([':id' => $id]);
        }
        $count = $stmt->rowCount();
        return $count;
    }
    function has_carryover()
    {
        global $conn;
        $id = $this->id;
        $stmt = $conn->prepare("SELECT id FROM account_carryover WHERE a_fk=:id AND claimed = '0'");
        $stmt->execute([':id' => $id]);
        $count = $stmt->rowCount(); 
        return $count;  
    }
    function has_submitted($form)
    {
        global $conn;
        $id = $this->id;
        $system = new System();
        $year = $system->get_year();
        $stmt = $conn->prepare("SELECT id FROM $form WHERE a_fk = :id AND year = :year");
        $stmt->execute([':id' => $id,':year' => $year]);
        $count = $stmt->rowCount(); 
        return $count;
    }
    function check_efl_name()
    {
        global $conn;
        $id = $this->id;
        $efl_fk = $this->efl_fk;
        $stmt = $conn->prepare("SELECT displayname FROM eflmanager.Users WHERE user_ID = :fk");
        $stmt->execute([':fk' => $efl_fk]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        return $row['displayname'];
    }
}
class Player extends User
{
    // Properties
    public $user_id;
    public $id;
    public $name;
    public $league;
    public $tpe;
    public $ape;
    public $bank;
    public $p_bank;
    public $salary;
    public $min_salary;
    public $total_salary;
    public $experience;
    public $team;
    public $yrs_remain;
    // Methods
    function __construct($id) 
    {
        global $conn;
        $stmt = $conn->prepare("SELECT p.id,CONCAT(p.pfirst,' ',p.plast) as name,p.league,p.tpe,p.ape,p.bank,p.p_bank,p.salary,p.min_salary,p.salary+p.salary_bonus as total_salary,p.experience,a.ID as user_id,team,yrs_remain,a.username FROM auth_user a INNER JOIN players p ON a.ID=p.user_fk WHERE p.id=:id");
        $stmt->execute([':id' => $id]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        $this->user_id = $row['user_id'];
        $this->username = $row['username'];
        $this->id = $row['id'];
        $this->name = $row['name'];
        $this->league = $row['league'];
        $this->tpe = $row['tpe'];
        $this->ape = $row['ape'];
        $this->bank = $row['bank'];
        $this->p_bank = $row['p_bank'];
        $this->salary = $row['salary'];
        $this->min_salary = $row['min_salary'];
        $this->total_salary = $row['total_salary'];
        $this->experience = $row['experience'];
        $this->team = $row['team'];
        $this->yrs_remain = $row['yrs_remain'];
    }
    function get_($attr)
    {
        return $this->$attr;
    }
    function set_cash($cash)
    {
        global $conn;
        $id = $this->id;
        $stmt = $conn->prepare("UPDATE players SET p_bank = p_bank+:cash WHERE id = :id");
        $stmt->execute([':cash' => $cash, ':id' => $id]);
    }
    function make_purchase($price,$tpe,$puid)
    {
        global $conn;
        $id = $this->id;
        $stmt = $conn->prepare("UPDATE players SET tpe=tpe+:tpe,ape=ape+:tpe,bank=bank+:tpe,p_bank=p_bank-:price WHERE id=:id");
        $stmt->execute([':tpe' => $tpe, ':price' => $price,':id' => $id]);
        if($puid > 0)
        {
            $stmt = $conn->prepare("UPDATE player_updates SET bank=bank+:tpe WHERE id=:puid");
            $stmt->execute([':tpe' => $tpe, ':puid' => $puid]);
        }
    }
    function add_purchase_history($price,$package)
    {
        global $conn;
        $id = $this->id;
        $stmt = $conn->prepare("INSERT INTO player_purchase_history (pid_fk,cost,item) VALUES (:id,:price,:package)");
        $stmt->execute([':id' => $id, ':price' => $price, ':package' => $package]);
    }
    function add_earn_history($price,$package)
    {
        global $conn;
        $id = $this->id;
        $stmt = $conn->prepare("INSERT INTO player_earn_history (pid_fk,cash,task) VALUES (:id,:price,:package)");
        $stmt->execute([':id' => $id, ':price' => $price, ':package' => $package]);
    }
    function graduate_player()
    {
        global $conn;
        $id = $this->id;
        $user_id = $this->user_id;
        $stmt = $conn->prepare("SELECT id FROM players WHERE league='SBA' AND active = '1' AND user_fk =:aid");
        $stmt->execute([':aid' => $user_id]);
        $count = $stmt->rowCount();
        if($count == 0)
        {
            $stmt = $conn->prepare("UPDATE players SET experience = '0',league='SBA',team='0',p_bank='0',active='1' WHERE id=:id");
            $stmt->execute([':id' => $id]);
        }
        else
        {
            $stmt = $conn->prepare("UPDATE players SET experience = '0',league='SBA',team='0',p_bank='0',active='2' WHERE id=:id");
            $stmt->execute([':id' => $id]);
        }   
    }
    function player_regress_snapshot()
    {
        global $conn;
        $id = $this->id;
        $stmt = $conn->prepare("SELECT * FROM players WHERE id=:id");
        $stmt->execute([':id' => $id]);
        $r = $stmt->fetch(PDO::FETCH_ASSOC);
        $stmt = $conn->prepare("INSERT INTO player_snapshot (p_fk,ins,jps,ft,3ps,han,pas,orb,drb,psd,prd,stl,blk,fl,qkn,str,jmp,bank,tpe) VALUES (:id,:ins,:jps,:ft,:tps,:han,:pas,:orb,:drb,:psd,:prd,:stl,:blk,:fl,:qkn,:str,:jmp,:bank,:tpe)");
        $stmt->execute([':id' => $id, ':ins' => $r['ins'], ':jps' => $r['jps'], ':ft' => $r['ft'], ':tps' => $r['3ps'], ':han' => $r['han'], ':pas' => $r['pas'], ':orb' => $r['orb'], ':drb' => $r['drb'], ':psd' => $r['psd'], ':prd' => $r['prd'], ':stl' => $r['stl'], ':blk' => $r['blk'], ':fl' => $r['fl'], ':qkn' => $r['qkn'], ':str' => $r['str'], ':jmp' => $r['jmp'], ':bank' => $r['bank'], ':tpe' => $r['tpe']]);
    }
    function regression($type)
    {
        global $conn;
        $id = $this->id;
        $regress_pct = '';
        $regress_pct_str = '';
        $regress_pct_wk = '';
        $loss = 0;
        $ape = 0;
        $attr = Array('ins','jps','ft','3ps','han','pas','orb','drb','psd','prd','stl','blk','fl','qkn','str','jmp');
        $new_attr = Array();
        $stmt = $conn->prepare("SELECT * FROM players WHERE id=:id");
        $stmt->execute([':id' => $id]);
        $r = $stmt->fetch(PDO::FETCH_ASSOC);
        $wk = Array();
        array_push($wk,$r['c70']);
        array_push($wk,$r['c90']);
        $str = Array();
        array_push($str,$r['str1']);
        if($r['build'] == 'Standard')
        {
            $dattr = Array(35,35,35,35,35,35,35,35,35,35,35,35,50,35,35,35);
        }
        else if($r['build'] == 'Freak')
        {
            $dattr = Array(35,35,35,35,35,35,35,35,35,35,35,35,20,20,20,20);
        }
        else
        {
            $dattr = Array(35,35,35,35,35,35,35,35,35,35,35,35,35,35,35,35);
        }
        if($r['str2'] != '')
        {
            array_push($str,$r['str2']);
        }
        if($type == 'offseason')
        {
            if($r['experience'] == '9')
            {
                $this->player_regress_snapshot();
            }
        }
        switch($r['experience']):
            case "9":
                $regress_pct_wk = 0.12;
                $regress_pct = 0.08;
                $regress_pct_str = 0.04;
                break;
            case "10":
                $regress_pct_wk = 0.15;
                $regress_pct = 0.08;
                $regress_pct_str = 0.04;
                break;
            case "11":
                $regress_pct_wk = 0.18;
                $regress_pct = 0.12;
                $regress_pct_str = 0.06;
                break;
            case "12":
                $regress_pct_wk = 0.24;
                $regress_pct = 0.16;
                $regress_pct_str = 0.08;
                break;
            case "13":
                $regress_pct_wk = 0.30;
                $regress_pct = 0.20;
                $regress_pct_str = 0.10;
                break;
            case "14":
                $regress_pct_wk = 0.45;
                $regress_pct = 0.30;
                $regress_pct_str = 0.15;
                break;
            case "15":
                $regress_pct_wk = 0.60;
                $regress_pct = 0.40;
                $regress_pct_str = 0.20;
                break;
            default:
                $regress_pct_wk = 0.12;
                $regress_pct = 0.08;
                $regress_pct_str = 0.04;
                break;
        endswitch;
        for($v=0;$v<count($attr);$v++)
        {
            if(in_array($attr[$v],$wk))
            {
                $newatt = round($r[$attr[$v]] - ($r[$attr[$v]] * $regress_pct_wk));
            }
            else if(in_array($attr[$v],$str))
            {
                $newatt = round($r[$attr[$v]] - ($r[$attr[$v]] * $regress_pct_str));
            }
            else
            {
                $newatt = round($r[$attr[$v]] - ($r[$attr[$v]] * $regress_pct));
            }
            $new_attr[$attr[$v]] = $newatt;
            if($new_attr[$attr[$v]] < $dattr[$v])
            {
                $new_attr[$attr[$v]] = $dattr[$v];
            }
            else
            {
                $loss += Subtract($r[$attr[$v]],$newatt);
            }
        }
        $newbank = round($r['bank'] - ($r['bank'] * $regress_pct));
        $loss += Subtract($r['bank'],$newbank);
        $ape = $r['ape'] - $loss;
        if($type == 'offseason')
        {
            $stmt = $conn->prepare("UPDATE players SET ins=:ins,jps=:jps,ft=:ft,3ps=:tps,han=:han,pas=:pas,orb=:orb,drb=:drb,psd=:psd,prd=:prd,stl=:stl,blk=:blk,fl=:fl,qkn=:qkn,str=:str,jmp=:jmp,bank=:bank,ape=:ape WHERE id=:id");
            $stmt->execute([':id' => $id, ':ins' => $new_attr['ins'], ':jps' => $new_attr['jps'], ':ft' => $new_attr['ft'], ':tps' => $new_attr['3ps'], ':han' => $new_attr['han'], ':pas' => $new_attr['pas'], ':orb' => $new_attr['orb'], ':drb' => $new_attr['drb'], ':psd' => $new_attr['psd'], ':prd' => $new_attr['prd'], ':stl' => $new_attr['stl'], ':blk' => $new_attr['blk'], ':fl' => $new_attr['fl'], ':qkn' => $new_attr['qkn'], ':str' => $new_attr['str'], ':jmp' => $new_attr['jmp'], ':bank' => $newbank, ':ape' => $ape]);
        }
        else if($type == 'expected')
        {
            return $ape;
        }
    }
    function retire()
    {
        global $conn;
        $id = $this->id;
        $stmt = $conn->prepare("UPDATE players SET active = '0',team='0',yrs_remain='0' WHERE id = :id");
        $stmt->execute([':id' => $id]);
    }
    function get_carryover()
    {
        global $conn;
        $id = $this->id;
        $stmt = $conn->prepare("SELECT a.ID as aid,tpe,experience,league FROM auth_user a INNER JOIN players p ON a.ID=p.user_fk WHERE p.id = :id");
        $stmt->execute([':id' => $id]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if($row['experience'] < 3 || $row['league'] == 'SBDL')
        {
            
        }
        else
        {
            $aid = $row['aid'];
            $tpe = $row['tpe'];
            $tpe = ceil($tpe * .05);
            if($tpe > 100)
            {
                $tpe = 100;
            }
            $stmt = $conn->prepare("INSERT INTO account_carryover (a_fk,p_fk,pe) VALUES (:aid,:id,:tpe)");
            $stmt->execute([':id' => $id, ':aid' => $aid, ':tpe' => $tpe]);
        }
    }
    function check_for_extension()
    {
        global $conn;
        $id = $this->id;
        $stmt = $conn->prepare("SELECT id FROM contracts WHERE p_fk=:id AND type='Extension' AND status='2'");
        $stmt->execute([':id' => $id]);
        $count = $stmt->rowCount();
        return $count;
    }
    function activate_extension()
    {
        global $conn;
        $id = $this->id;
        $stmt = $conn->prepare("SELECT id,t_fk,years,salary FROM contracts WHERE p_fk=:id AND type='Extension' AND status='2'");
        $stmt->execute([':id' => $id]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        $stmt = $conn->prepare("UPDATE contracts SET status='3' WHERE id=:id");
        $stmt->execute([':id' => $row['id']]);
        $stmt = $conn->prepare("UPDATE players SET team=:team,salary=:sal,yrs_remain=:yrs WHERE id=:id");
        $stmt->execute([':id' => $id, ':team' => $row['t_fk'], ':sal' => $row['salary'], ':yrs' => $row['years']]);
    }
    function has_snapshot()
    {
        global $conn;
        $id = $this->id;
        $stmt = $conn->prepare("SELECT id FROM player_snapshot WHERE p_fk=:id");
        $stmt->execute([':id' => $id]);
        $count = $stmt->rowCount();
        return $count;
    }
    function isOnBlock()
    {
        global $conn;
        $id = $this->id;
        $stmt = $conn->prepare("SELECT id FROM team_trade_block WHERE p_fk=:id");
        $stmt->execute([':id' => $id]);
        $count = $stmt->rowCount();
        return $count;
    }
    function modify_tradeblock()
    {
        global $conn;
        $id = $this->id;
        $tid = $this->team;
        $stmt = $conn->prepare("SELECT id FROM team_trade_block WHERE p_fk=:id");
        $stmt->execute([':id' => $id]);
        $count = $stmt->rowCount();
        if($count == 0)
        {
            $stmt = $conn->prepare("INSERT INTO team_trade_block (t_fk,p_fk) VALUES (:tid,:id)");
            $stmt->execute([':id' => $id, ':tid' => $tid]);
        }
        else
        {
            $stmt = $conn->prepare("DELETE FROM team_trade_block WHERE p_fk=:id");
            $stmt->execute([':id' => $id]);
        } 
    }
    function get_contractID()
    {
        global $conn;
        $id = $this->id;
        $stmt = $conn->prepare("SELECT id FROM contracts WHERE p_fk=:id AND status = '3' AND type !='Waived'");
        $stmt->execute([':id' => $id]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        return $row['id'];
    }
}
class Team
{
    // Properties
    public $id;
    public $name;
    public $abr;
    public $league;
    public $team_salary;
    public $team_salary2;
    public $team_salary3;
    public $cap_space;
    public $roster_size;
    public $roster_size_filler;
    // Methods
    function __construct($id) 
    {
        global $conn;
        $system = new System();
        $year = $system->get_year();
        $extyear = $year + 1;
        $stmt = $conn->prepare("SELECT id,name,abr,league,(SELECT sum(salary) FROM contracts c WHERE t_fk=:id AND status='3') as team_salary,((SELECT cap FROM salary_cap) - (SELECT sum(salary) FROM contracts c WHERE t_fk=:id AND status='3')) as cap_space,((SELECT IFNULL(sum(salary2),0) FROM players p INNER JOIN contracts c ON p.id=c.p_fk WHERE c.status = '3' and t_fk=:id and c.yrs_remain >= 2 and (type='Free Agent' || type='Rookie' || type='Waived') AND p.active ='1')+(SELECT IFNULL(sum(c.salary),0) FROM players p INNER JOIN contracts c ON p.id=c.p_fk WHERE c.type='Extension' AND c.start=:extyear AND c.status='2' AND team=:id)) as team_salary2,((SELECT IFNULL(sum(salary3),0) FROM players p INNER JOIN contracts c ON p.id=c.p_fk WHERE c.status = '3' and t_fk=:id and c.yrs_remain = 3 and (type='Free Agent' || type='Rookie' || type='Waived') AND p.active ='1')+(SELECT IFNULL(sum(c.salary2),0) FROM players p INNER JOIN contracts c ON p.id=c.p_fk WHERE c.type='Extension' AND c.start=:extyear AND c.status='2' AND team=:id)) as team_salary3,(SELECT count(id) FROM players WHERE team=:id) as roster_size, (SELECT count(id) FROM players WHERE team=:id AND active='2') as roster_size_filler FROM teams WHERE id=:id");
        $stmt->execute([':id' => $id,':extyear' => $extyear]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        $this->id = $row['id'];
        $this->name = $row['name'];
        $this->abr = $row['abr'];
        $this->league = $row['league'];
        $this->team_salary = $row['team_salary'];
        $this->cap_space = $row['cap_space'];
        $this->team_salary2 = $row['team_salary2'];
        $this->team_salary3 = $row['team_salary3'];
        $this->roster_size = $row['roster_size'];
        $this->roster_size_filler = $row['roster_size_filler'];
    }
    function get_($attr)
    {
        return $this->$attr;
    }
    function get_tradeblock()
    { 
        global $conn;
        $id = $this->id;
        $echo = "
        <div class='table-responsive'>
            <table class=\"table\">
                <thead>
                    <th>Name</th>
                    <th></th>
                </thead>
                <tbody>";
        $stmt = $conn->prepare("SELECT id FROM players WHERE team=:id AND active > 0");
        $stmt->execute([':id' => $id]);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach ($result as $row)
        {
            $pid = $row['id'];
            $player = new Player($pid);
            $pname = $player->name;
            $isOnBlock = $player->isOnBlock();
            $echo .= "<tr><td>".$pname."</td><td><span class='pull-right' style='background-color:transparent;top:0;padding:0;margin:0;'>".(($isOnBlock == 0) ? "<a href='#' onclick='modifyBlock(".$row['id'].")'><i class='fas fa-lock'></i></a> " : "<a href='#' onclick='modifyBlock(".$row['id'].")'><i class='fas fa-unlock'></i></a> ")."</span></td></tr>";
        }
        $echo .= "
                </tbody>
            </table>
        </div>
        <script>
            function modifyBlock(id)
            {
                $.ajax({url: '../ajax/modify_tradeblock.php?id='+id, success: function(result){
                    $('#ajaxtradeblock').html(result);
                    $('#\\#modifytradeblock').modal();
                }});
            }
        </script>";
        return $echo;
    }
}
class Offseason
{
    // Properties

    // Methods
    function reset_SBDL_bank()
    {
        global $conn;
        $stmt = $conn->prepare("UPDATE players SET p_bank = 0 WHERE league = 'SBDL'");
        $stmt->execute();
    }
    function graduation()
    {
        global $conn;
        $stmt = $conn->prepare("SELECT id,user_fk FROM players WHERE league = 'SBDL' AND (tpe >= '200' || experience >= '3') AND (active != '0')");
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach ($result as $row)
        {
            $player = new Player($row['id']);
            $player->graduate_player();
        }
    }
    function increment_experience()
    {
        global $conn;
        $stmt = $conn->prepare("UPDATE players SET experience = experience+1 WHERE active > 0");
        $stmt->execute();
    }
    function handle_retired_contracts()
    {
        global $conn;
        $stmt = $conn->prepare("SELECT c.id as cid,p.id as pid FROM players p INNER JOIN contracts c ON p.id=c.p_fk WHERE c.status != '4' AND p.active = '0'");
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach ($result as $row)
        {
            complete_contract($row['cid'],$row['pid']);
        }
    }
    function reset_salary()
    {
        global $conn;
        $stmt = $conn->prepare("UPDATE players SET salary=0, salary_bonus=0 WHERE active > 0 AND league='SBA'");
        $stmt->execute();
    }
    function decrement_contracts()
    {
        global $conn;
        $stmt = $conn->prepare("UPDATE contracts SET yrs_remain = yrs_remain-1 WHERE status = '3'");
        $stmt->execute();
    }
    function handle_outstanding_offers()
    {
        global $conn;
        $stmt = $conn->prepare("SELECT id FROM contracts WHERE (status = '1' || status = '0')");
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach ($result as $row)
        {
            delete_contract($row['id']);
        }
    }
    function handle_expired_contracts()
    {
        global $conn;
        $stmt = $conn->prepare("UPDATE contracts SET status='4' WHERE status = '3' AND type='Bonus'");
        $stmt->execute();
        $stmt = $conn->prepare("UPDATE contracts SET status='4' WHERE status = '3' AND type='Waived' AND yrs_remain='0'");
        $stmt->execute();
        $stmt = $conn->prepare("SELECT id,p_fk FROM contracts WHERE status = '3' AND yrs_remain = '0'");
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach ($result as $row)
        {
            complete_contract($row['id'],$row['p_fk']);
            $player = new Player($row['p_fk']);
            $ext = $player->check_for_extension();
            if($ext > 0)
            {
                $player->activate_extension();
            }
        }
    }
    function update_contract_salaries()
    {
        global $conn;
        $stmt = $conn->prepare("SELECT id,p_fk,type,years,yrs_remain,salary,salary2,salary3 FROM contracts WHERE status = '3' AND type !='Waived'");
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach ($result as $row)
        {
            $player = new Player($row['p_fk']);
            $min = $player->min_salary;
            $y = $row['years'];
            $x = $row['yrs_remain'];
            $z = $y - $x;
            if($row['type'] == 'Waived')
            {
                $stmt = $conn->prepare("UPDATE contracts SET salary=:sal,salary2=:sal2,salary3=0 WHERE id=:id");
                $stmt->execute([':id' => $row['id'], ':sal' => $row['salary2'],':sal2' => $row['salary3']]);   
            }
            else
            {
                if($x == 1)
                {
                    // 1 season
                    $stmt = $conn->prepare("UPDATE contracts SET salary=:sal,salary2=0,salary3=0 WHERE id=:id");
                    $stmt->execute([':id' => $row['id'], ':sal' => $min]);   
                }
                else if($x == 2)
                {
                    // 2 seasons
                    $stmt = $conn->prepare("UPDATE contracts SET salary=:sal,salary2=:sal,salary3=0 WHERE id=:id");
                    $stmt->execute([':id' => $row['id'], ':sal' => $min]);
                }
                else
                {
                    // 3 seasons
                    $stmt = $conn->prepare("UPDATE contracts SET salary=:sal,salary2=:sal,salary3=:sal WHERE id=:id");
                    $stmt->execute([':id' => $row['id'], ':sal' => $min]);
                }

                $stmt = $conn->prepare("UPDATE players SET salary=:sal,yrs_remain=:yrs WHERE id=:id");
                $stmt->execute([':id' => $row['p_fk'], ':sal' => $min,':yrs' => $row['yrs_remain']]);
            }
        }
    }
}
function delete_contract($cid)
{
    global $conn;
    $stmt = $conn->prepare("DELETE FROM contracts WHERE id=:id");
    $stmt->execute([':id' => $cid]);
}
function complete_contract($cid,$pid)
{
    global $conn;
    $stmt = $conn->prepare("UPDATE contracts SET status='4' WHERE id=:id");
    $stmt->execute([':id' => $cid]);
    $stmt = $conn->prepare("UPDATE players SET team='0',salary='0',yrs_remain='0' WHERE id=:pid");
    $stmt->execute([':pid' => $pid]);
}


function sanitize($con,$data)
{
    $data = str_replace(array("!",";","(",")"),"",$data);
    $data = stripslashes($data);
    $data = strip_tags($data);
    $data = mysqli_real_escape_string($con, $data);
    return $data;
}
function sanitize_login($con,$data)
{
    $data = stripslashes($data);
    $data = strip_tags($data);
    $data = mysqli_real_escape_string($con, $data);
    return $data;
}
function stripslashes_deep($value)
{
    $value = is_array($value) ?
                array_map('stripslashes_deep', $value) :
                stripslashes($value);

    return $value;
}
function HeaderImagePrint($league,$color) 
{
    if($league == 'EFL')
    {
        $rs="/images/eflfull.png";
    }
    else
    {
        if($color == 'light')
        {
            $rs="/images/sbaofullb.png";
        }
        else
        {
            $rs = "/includes/themes/head/".$color."-head.png";
        }
    }
    return "
    <div class='row'>
        <div class='col-md-2 col-md-offset-5' style='background-color:transparent;'><img src='$rs' style='height:50px;width:200px;margin-bottom:20px;'></div>
    </div>";
}
function NavImagePrint($league,$color) 
{
    if($league == 'EFL')
    {
        $rs="/images/eflfull.png";
        $height="30px";
    }
    else
    {
        if($color == 'light')
        {
            $rs="/images/sbaofullb.png";
        }
        else
        {
            $rs = "/includes/themes/nav/".$color."-nav.png";
        }
        $height = "20px";
    }
    return "<a class='navbar-brand' href='/home.php'><img src='$rs' style='height:".$height.";width:80px;'></a>";
}
function printpred($con,$category,$year,$league) {
if($league == 'sba') {
    $table = 'predictions';
}
else {
    $table = 'sbdl_predictions';
}
$query = "SELECT $category, count(username) AS count
FROM $table
WHERE year='$year'
GROUP BY $category
ORDER BY 2 DESC";
$result = mysqli_query($con,$query);
$echo = "<table class='table table-condensed'><tr><th colspan='2'>".$category."</th></tr>";
while ($r = mysqli_fetch_array($result)) {
    $echo .= "<tr><td>".$r[$category]."</td><td>".$r['count']."</td></tr>";
}
$echo .= "</table>";
return $echo;
}
function printres($con,$year,$league,$type) 
{
    if($league == 'sba' AND $type == 'season') 
    {
        $table = 'predictions';
    }
    else if($league == 'sba' AND $type == 'playoff')
    {
        $table = 'playoff_predictions';
    }
    else if($league == 'sbdl' AND $type == 'season')
    {
        $table = 'sbdl_predictions';
    }
    else 
    {
        $table = 'ncaa_playoff_predictions';
    }
    $query = "SELECT a.username,p.numcorrect
    FROM auth_user a INNER JOIN $table p ON a.ID=p.a_fk
    WHERE year='$year'
    ORDER BY 2 DESC";
    $result = mysqli_query($con,$query);
    $echo = "<br><br><table class='table'><tr><th>User</th><th>Points</th></tr>";
    while ($r = mysqli_fetch_array($result)) {
        $echo .= "<tr><td>".$r['username']."</td><td>".$r['numcorrect']."</td></tr>";
    }
    $echo .= "</table>";
    return $echo;
}
function printleagueleader($con,$stat,$league,$type,$season,$limit) 
{
    if($league == 'sba' AND $type == 'season') 
    {
        $table = 'FROM SBA_Player p INNER JOIN SBA_SeasonStats s on p.ID = s.ID INNER JOIN year y on s.Season = y.sba_season';
        $table2 = 'FROM SBAR_Player pr INNER JOIN SBAR_SeasonStats sr on pr.ID = sr.ID INNER JOIN year yr on sr.Season = yr.sba_season';
    }
    else if($league == 'sba' AND $type == 'playoff')
    {
        $table = 'FROM SBA_Player p INNER JOIN SBA_PlayoffStats s on p.ID = s.ID INNER JOIN year y on s.Season = y.sba_season';
        $table2 = 'FROM SBAR_Player pr INNER JOIN SBAR_PlayoffStats sr on pr.ID = sr.ID INNER JOIN year yr on sr.Season = yr.sba_season';
    }
    else if($league == 'fiba' AND $type == 'season') 
    {
        $table = 'FROM FIBA_Player p INNER JOIN FIBA_SeasonStats s on p.ID = s.ID INNER JOIN year y on s.Season = y.fiba_season';
        $table2 = 'FROM FIBAR_Player pr INNER JOIN FIBAR_SeasonStats sr on pr.ID = sr.ID INNER JOIN year yr on sr.Season = yr.fiba_season';
    }
    else if($league == 'fiba' AND $type == 'playoff')
    {
        $table = 'FROM FIBA_Player p INNER JOIN FIBA_PlayoffStats s on p.ID = s.ID INNER JOIN year y on s.Season = y.fiba_season';
        $table2 = 'FROM FIBAR_Player pr INNER JOIN FIBAR_PlayoffStats sr on pr.ID = sr.ID INNER JOIN year yr on sr.Season = yr.fiba_season';
    }
    else if($league == 'ncaa' AND $type == 'season')
    {
        $table = 'FROM NCAA_Player p INNER JOIN NCAA_SeasonStats s on p.ID = s.ID INNER JOIN year y on s.Season = y.ncaa_season';
        $table2 = 'FROM NCAAR_Player pr INNER JOIN NCAAR_SeasonStats sr on pr.ID = sr.ID INNER JOIN year yr on sr.Season = yr.ncaa_season';
    }
    else 
    {
        $table = 'FROM NCAA_Player p INNER JOIN NCAA_PlayoffStats s on p.ID = s.ID INNER JOIN year y on s.Season = y.ncaa_season';
        $table2 = 'FROM NCAAR_Player pr INNER JOIN NCAAR_PlayoffStats sr on pr.ID = sr.ID INNER JOIN year yr on sr.Season = yr.ncaa_season';
    }
    if($stat == 'DoubleDoubles' || $stat == 'TripleDoubles')
    {
        $perc= '';
        $query = "SELECT CONCAT(FirstName,' ',LastName) as name, s.Team as team, `$stat` as average
        $table
        WHERE y.year='$season'
        UNION
        SELECT CONCAT(FirstName,' ',LastName) as name, sr.Team as team, `$stat` as average
        $table2
        WHERE yr.year='$season'
        order by 3 DESC LIMIT $limit";
    }
    else if($stat == 'FT' || $stat == 'FG' || $stat == '3P')
    {
        $average = "ROUND(((".$stat."M/".$stat."A)*100),1) as average";
        $perc = '%';
        $query = "SELECT CONCAT(FirstName,' ',LastName) as name, s.Team as team, $average
        $table
        WHERE y.year='$season'
        UNION
        SELECT CONCAT(FirstName,' ',LastName) as name, sr.Team as team, $average
        $table2
        WHERE yr.year='$season'
        order by 3 DESC LIMIT $limit";   
    }
    else if($stat == 'TSP')
    {
        $average = "ROUND(`Points` / (2*(`FGA`+(.44*`FTA`))),3) as average";
        $perc = '';
        $query = "SELECT CONCAT(FirstName,' ',LastName) as name, s.Team as team, $average
        $table
        WHERE y.year='$season' AND `FGA` >= '820'
        UNION
        SELECT CONCAT(FirstName,' ',LastName) as name, sr.Team as team, $average
        $table2
        WHERE yr.year='$season' AND `FGA` >= '820'
        order by 3 DESC LIMIT $limit";
    }
    else if($stat == 'PPSFTM')
    {
        $average = "(`Points`)/(`FGA`) as average";
        $perc = '';
        $query = "SELECT CONCAT(FirstName,' ',LastName) as name, s.Team as team, $average
        $table
        WHERE y.year='$season' AND `FGA` >= '820'
        UNION
        SELECT CONCAT(FirstName,' ',LastName) as name, sr.Team as team, $average
        $table2
        WHERE yr.year='$season' AND `FGA` >= '820'
        order by 3 DESC LIMIT $limit";
    }
    else if($stat == 'PPS')
    {
        $average = "(`Points` - `FTM`)/(`FGA`) as average";
        $perc = '';
        $query = "SELECT CONCAT(FirstName,' ',LastName) as name, s.Team as team, $average
        $table
        WHERE y.year='$season' AND `FGA` >= '820'
        UNION
        SELECT CONCAT(FirstName,' ',LastName) as name, sr.Team as team, $average
        $table2
        WHERE yr.year='$season' AND `FGA` >= '820'
        order by 3 DESC LIMIT $limit";
    }
    else
    {
        $perc = '';
        $query = "SELECT CONCAT(FirstName,' ',LastName) as name, s.Team as team, ROUND((`$stat`/Games),2) as average
        $table
        WHERE y.year='$season'
        UNION
        SELECT CONCAT(FirstName,' ',LastName) as name, sr.Team as team, ROUND((`$stat`/Games),2) as average
        $table2
        WHERE yr.year='$season'
        order by 3 DESC LIMIT $limit";
    }
    $result = mysqli_query($con,$query);
    $count = 1;
    $echo = "<table class='table table-condensed'><tr><th>#</th><th>Player</th><th>Team</th><th>Average</th></tr>";
    while ($r = mysqli_fetch_array($result)) {
        $echo .= "<tr><td>$count</td><td>".$r['name']."</td><td>".$r['team']."</td><td>".$r['average']."$perc</td></tr>";
        $count++;
    }
    $echo .= "</table>";
    return $echo;
}
function printleaguestanding($con,$league,$season)
{
    if($league == 'sba') 
    {
        $table = 'FROM SBA_PastStandings s INNER JOIN year y on s.Season = y.sba_season';
    }
    else if($league == 'fiba') 
    {
        $table = 'FROM FIBA_PastStandings s INNER JOIN year y on s.Season = y.fiba_season';
    }
    else
    {
        $table = 'FROM NCAA_PastStandings s INNER JOIN year y on s.Season = y.ncaa_season';
    }
    $query = "SELECT TeamName, CONCAT(Wins,'-',Losses) as Record,ROUND(`Win%`,3) as Percentage,HomeRecord,Wins
    $table
    WHERE y.year='$season'
    ORDER BY Wins DESC";
    $result = mysqli_query($con,$query);
    $echo = "<table class='table'><thead><tr><th>Team</th><th>Record</th><th>Win %</th><th>Home</th><th>Away</th></tr></thead><tbody>";
    while ($r = mysqli_fetch_array($result)) 
    {
        $awayArr = explode("-",$r['HomeRecord']);
        $homeW = $awayArr[0];
        $awayW = $r['Wins'] - $homeW;
        $awayL = 41 - $awayW;
        $away = $awayW."-".$awayL;
        $echo .= "<tr><td>".$r['TeamName']."</td><td>".$r['Record']."</td><td>".$r['Percentage']."</td><td>".$r['HomeRecord']."</td><td>".$away."</td></tr>";
    }
    $echo .= "</tbody></table>";
    return $echo;

}
function printgame($con,$stat,$league) 
{
    if($stat == 'OffensiveRebounds'){$stat='ORebounds';}
    if($stat == '3PA'){$stat='TPA';}
    if($stat == '3PM'){$stat='TPM';}
    $stat2 = "Game".$stat;
    if($league == 'sba') 
    {
        $table = 'FROM SBA_SingleTeamPlayerRecords s INNER JOIN year y on s.Season = y.sba_season';
    }
    else if($league == 'fiba') 
    {
        $table = 'FROM FIBA_SingleTeamPlayerRecords s INNER JOIN year y on s.Season = y.fiba_season';
    }
    else
    {
        $table = 'FROM NCAA_SingleTeamPlayerRecords s INNER JOIN year y on s.Season = y.ncaa_season';
    }
    $query = "SELECT Player, Record, y.year as Season
    $table
    WHERE RecordTitle = '$stat2'
    ORDER BY CAST(Record AS UNSIGNED) DESC LIMIT 50";
    $result = mysqli_query($con,$query);
    $count = 1;
    $echo = "<br><br><div class='table-responsive'><table class='table table-condensed compact'><tr><th></th><th>Player</th><th>Season</th><th>$stat</th></tr>";
    while ($r = mysqli_fetch_array($result)) {
        $echo .= "<tr><td>$count</td><td>".$r['Player']."</td><td>S".$r['Season']."</td><td>".$r['Record']."</td></tr>";
        $count++;
    }
    $echo .= "</table></div>";
    return $echo;
}
function printseason($con,$stat,$league,$type) 
{
    if($league == 'sba' AND $type == 'season') 
    {
        $table = 'FROM SBA_Player p INNER JOIN SBA_SeasonStats s on p.ID = s.ID INNER JOIN year y on s.Season = y.sba_season';
        $table2 = 'FROM SBAR_Player pr INNER JOIN SBAR_SeasonStats sr on pr.ID = sr.ID INNER JOIN year yr on sr.Season = yr.sba_season';
    }
    else if($league == 'sba' AND $type == 'playoff')
    {
        $table = 'FROM SBA_Player p INNER JOIN SBA_PlayoffStats s on p.ID = s.ID INNER JOIN year y on s.Season = y.sba_season';
        $table2 = 'FROM SBAR_Player pr INNER JOIN SBAR_PlayoffStats sr on pr.ID = sr.ID INNER JOIN year yr on sr.Season = yr.sba_season';
    }
    else if($league == 'fiba' AND $type == 'season') 
    {
        $table = 'FROM FIBA_Player p INNER JOIN FIBA_SeasonStats s on p.ID = s.ID INNER JOIN year y on s.Season = y.fiba_season';
        $table2 = 'FROM FIBAR_Player pr INNER JOIN FIBAR_SeasonStats sr on pr.ID = sr.ID INNER JOIN year yr on sr.Season = yr.fiba_season';
    }
    else if($league == 'fiba' AND $type == 'playoff')
    {
        $table = 'FROM FIBA_Player p INNER JOIN FIBA_PlayoffStats s on p.ID = s.ID INNER JOIN year y on s.Season = y.fiba_season';
        $table2 = 'FROM FIBAR_Player pr INNER JOIN FIBAR_PlayoffStats sr on pr.ID = sr.ID INNER JOIN year yr on sr.Season = yr.fiba_season';
    }
    else if($league == 'ncaa' AND $type == 'season')
    {
        $table = 'FROM NCAA_Player p INNER JOIN NCAA_SeasonStats s on p.ID = s.ID INNER JOIN year y on s.Season = y.ncaa_season';
        $table2 = 'FROM NCAAR_Player pr INNER JOIN NCAAR_SeasonStats sr on pr.ID = sr.ID INNER JOIN year yr on sr.Season = yr.ncaa_season';
    }
    else 
    {
        $table = 'FROM NCAA_Player p INNER JOIN NCAA_PlayoffStats s on p.ID = s.ID INNER JOIN year y on s.Season = y.ncaa_season';
        $table2 = 'FROM NCAAR_Player pr INNER JOIN NCAAR_PlayoffStats sr on pr.ID = sr.ID INNER JOIN year yr on sr.Season = yr.ncaa_season';
    }
    if($stat == 'TS%')
    {
        $average = "ROUND(`Points` / (2*(`FGA`+(.44*`FTA`))),3) as category";
        $query = "SELECT CONCAT(FirstName,' ',LastName) as name, y.year as Season, $average, '---' as average
        $table
        WHERE `FGA` >= '820'
        UNION
        SELECT CONCAT(FirstName,' ',LastName) as name, yr.year as Season, $average, '---' as average
        $table2
        WHERE `FGA` >= '820'
        order by 3 DESC LIMIT 25";
    }
    else if($stat == 'PPS')
    {
        $average = "(`Points`)/(`FGA`) as category";
        $query = "SELECT CONCAT(FirstName,' ',LastName) as name, y.year as Season, $average, '---' as average
        $table
        WHERE `FGA` >= '820'
        UNION
        SELECT CONCAT(FirstName,' ',LastName) as name, yr.year as Season, $average, '---' as average
        $table2
        WHERE `FGA` >= '820'
        order by 3 DESC LIMIT 25";
    }
    else if($stat == 'PPS(Real)')
    {
        $average = "(`Points` - `FTM`)/(`FGA`) as category";
        $query = "SELECT CONCAT(FirstName,' ',LastName) as name, y.year as Season, $average, '---' as average
        $table
        WHERE `FGA` >= '820'
        UNION
        SELECT CONCAT(FirstName,' ',LastName) as name, yr.year as Season, $average, '---' as average
        $table2
        WHERE `FGA` >= '820'
        order by 3 DESC LIMIT 25";
    }
    else
    {
        $query = "SELECT CONCAT(FirstName,' ',LastName) as name, y.year as Season, `$stat` as category, (`$stat`/Games) as average
        $table
        UNION
        SELECT CONCAT(FirstName,' ',LastName) as name, yr.year as Season, `$stat` as category, (`$stat`/Games) as average
        $table2
        order by 3 DESC LIMIT 500";
    }
    $result = mysqli_query($con,$query);
    $count = 1;
    $echo = "<div class='table-responsive'><table class='table table-condensed compact'><tr><th></th><th>Player</th><th>Season</th><th>$stat</th><th>Average</th></tr>";
    while ($r = mysqli_fetch_array($result)) {
        $echo .= "<tr><td>$count</td><td>".$r['name']."</td><td>S".$r['Season']."</td><td>".$r['category']."</td><td>".$r['average']."</td></tr>";
        $count++;
    }
    $echo .= "</table></div>";
    return $echo;
}
function printplayerseason($con,$name,$league,$type,$season) 
{
    $name = sanitize($con,$name);
    if($season != '')
    {
        $clause = "AND y.year='".$season."'";
        $clause2 = "AND yr.year='".$season."'";
    }
    else
    {
        $clause = '';
        $clause2 = '';
    }
    if($league == 'sba' AND $type == 'season') 
    {
        $table = 'FROM SBA_Player p INNER JOIN SBA_SeasonStats s on p.ID = s.ID INNER JOIN year y on s.Season = y.sba_season';
        $table2 = 'FROM SBAR_Player pr INNER JOIN SBAR_SeasonStats sr on pr.ID = sr.ID INNER JOIN year yr on sr.Season = yr.sba_season';
    }
    else if($league == 'sba' AND $type == 'playoff')
    {
        $table = 'FROM SBA_Player p INNER JOIN SBA_PlayoffStats s on p.ID = s.ID INNER JOIN year y on s.Season = y.sba_season';
        $table2 = 'FROM SBAR_Player pr INNER JOIN SBAR_PlayoffStats sr on pr.ID = sr.ID INNER JOIN year yr on sr.Season = yr.sba_season';
    }
    else if($league == 'fiba' AND $type == 'season') 
    {
        $table = 'FROM FIBA_Player p INNER JOIN FIBA_SeasonStats s on p.ID = s.ID INNER JOIN year y on s.Season = y.fiba_season';
        $table2 = 'FROM FIBAR_Player pr INNER JOIN FIBAR_SeasonStats sr on pr.ID = sr.ID INNER JOIN year yr on sr.Season = yr.fiba_season';
    }
    else if($league == 'fiba' AND $type == 'playoff')
    {
        $table = 'FROM FIBA_Player p INNER JOIN FIBA_PlayoffStats s on p.ID = s.ID INNER JOIN year y on s.Season = y.fiba_season';
        $table2 = 'FROM FIBAR_Player pr INNER JOIN FIBAR_PlayoffStats sr on pr.ID = sr.ID INNER JOIN year yr on sr.Season = yr.fiba_season';
    }
    else if($league == 'ncaa' AND $type == 'season')
    {
        $table = 'FROM NCAA_Player p INNER JOIN NCAA_SeasonStats s on p.ID = s.ID INNER JOIN year y on s.Season = y.ncaa_season';
        $table2 = 'FROM NCAAR_Player pr INNER JOIN NCAAR_SeasonStats sr on pr.ID = sr.ID INNER JOIN year yr on sr.Season = yr.ncaa_season';
    }
    else 
    {
        $table = 'FROM NCAA_Player p INNER JOIN NCAA_PlayoffStats s on p.ID = s.ID INNER JOIN year y on s.Season = y.ncaa_season';
        $table2 = 'FROM NCAAR_Player pr INNER JOIN NCAAR_PlayoffStats sr on pr.ID = sr.ID INNER JOIN year yr on sr.Season = yr.ncaa_season';
    }
    $statarr = array('Games','Minutes','FGA','FGM','FG%','3PA','3PM','3P%','FTA','FTM','FT%','OffensiveRebounds','Rebounds','Assists','Steals','Blocks','Turnovers','Points','Fouls','PlusMinus','DQ','DoubleDoubles','TripleDoubles');
    $secho = '';
    $secho2 = '';
    $secho3 = '';
    $ssize = count($statarr);
    for ($p=0;$p<$ssize;$p++)
    {
        if ($statarr[$p] == 'FG%' || $statarr[$p] == '3P%' || $statarr[$p] == 'FT%')
        {
            $str = $statarr[$p];
            $strA = substr_replace($str,'A',2);
            $strM = substr_replace($str,'M',2);
            $secho .= ", TRUNCATE(`$strM`/`$strA`,3) as `$str`";
        }
        else if ($statarr[$p] == 'Points' || $statarr[$p] == 'Assists' || $statarr[$p] == 'Rebounds' || $statarr[$p] == 'Steals' || $statarr[$p] == 'Blocks')
        {
            $str = $statarr[$p];
            $strPG = substr_replace($str,'PG',1);
            $secho .= ", `".$statarr[$p]."` as ".$statarr[$p]."";
            $secho .= ", TRUNCATE(`$str`/`Games`,2) as `$strPG`";
        }
        else
        {
            $secho .= ", `".$statarr[$p]."` as ".$statarr[$p]."";
        }
    }
    $query = "SELECT y.year,s.Team$secho
    $table
    WHERE p.Name='$name' $clause
    UNION
    SELECT yr.year,sr.Team$secho
    $table2
    WHERE pr.Name='$name' $clause2
    order by year DESC";
    $result = mysqli_query($con,$query);
    $secho2 = "<th>GP</th><th>Min</th><th>FGA</th><th>FGM</th><th>FG%</th><th>3PA</th><th>3PM</th><th>3P%</th><th>FTA</th><th>FTM</th><th>FT%</th><th>ORB</th><th>Reb</th><th>RPG</th><th>Ast</th><th>APG</th><th>Stl</th><th>SPG</th><th>Blk</th><th>BPG</th><th>Trn</th><th>Pts</th><th>PPG</th><th>Fls</th><th>+/-</th><th>DQ</th><th>DD</th><th>TD</th>";
    $echo = "<div class='table-responsive'><table class='table compact'><thead><tr><th>Season</th><th>Team</th>$secho2</tr></thead><tbody>";
    while ($r = mysqli_fetch_array($result)) {
        $echo .= "<tr><td>".$r['year']."</td><td>".$r['Team']."</td><td>".$r['Games']."</td><td>".$r['Minutes']."</td><td>".$r['FGA']."</td><td>".$r['FGM']."</td><td>".$r['FG%']."</td><td>".$r['3PA']."</td><td>".$r['3PM']."</td><td>".$r['3P%']."</td><td>".$r['FTA']."</td><td>".$r['FTM']."</td><td>".$r['FT%']."</td><td>".$r['OffensiveRebounds']."</td><td>".$r['Rebounds']."</td><td>".$r['RPG']."</td><td>".$r['Assists']."</td><td>".$r['APG']."</td><td>".$r['Steals']."</td><td>".$r['SPG']."</td><td>".$r['Blocks']."</td><td>".$r['BPG']."</td><td>".$r['Turnovers']."</td><td>".$r['Points']."</td><td>".$r['PPG']."</td><td>".$r['Fouls']."</td><td>".$r['PlusMinus']."</td><td>".$r['DQ']."</td><td>".$r['DoubleDoubles']."</td><td>".$r['TripleDoubles']."</td></tr>";
    }
    $secho = '';
    $secho2 = '';
    $secho3 = '';
    $ssize = count($statarr);
    for ($p=0;$p<$ssize;$p++)
    {
        if ($statarr[$p] == 'FG%' || $statarr[$p] == '3P%' || $statarr[$p] == 'FT%')
        {
            $str = $statarr[$p];
            $strA = substr_replace($str,'A',2);
            $strM = substr_replace($str,'M',2);
            $secho .= ", TRUNCATE((SUM(`$strM`)/SUM(`$strA`)),3) as `$str`";
        }
        else if ($statarr[$p] == 'Points' || $statarr[$p] == 'Assists' || $statarr[$p] == 'Rebounds' || $statarr[$p] == 'Steals' || $statarr[$p] == 'Blocks')
        {
            $str = $statarr[$p];
            $strPG = substr_replace($str,'PG',1);
            $secho .= ", SUM(`".$statarr[$p]."`) as ".$statarr[$p]."";
            $secho .= ", TRUNCATE((SUM(`$str`)/SUM(`Games`)),2) as `$strPG`";
        }
        else
        {
            $secho .= ", SUM(`".$statarr[$p]."`) as ".$statarr[$p]."";
        }
    }
    $query = "SELECT p.Name$secho
    $table
    WHERE p.Name='$name'
    UNION
    SELECT pr.Name$secho
    $table2
    WHERE pr.Name='$name'
    order by 2 DESC";
    $result = mysqli_query($con,$query);
    while ($r = mysqli_fetch_array($result)) {
        $echo .= "<tr><td colspan='2'></td><td>".$r['Games']."</td><td>".$r['Minutes']."</td><td>".$r['FGA']."</td><td>".$r['FGM']."</td><td>".$r['FG%']."</td><td>".$r['3PA']."</td><td>".$r['3PM']."</td><td>".$r['3P%']."</td><td>".$r['FTA']."</td><td>".$r['FTM']."</td><td>".$r['FT%']."</td><td>".$r['OffensiveRebounds']."</td><td>".$r['Rebounds']."</td><td>".$r['RPG']."</td><td>".$r['Assists']."</td><td>".$r['APG']."</td><td>".$r['Steals']."</td><td>".$r['SPG']."</td><td>".$r['Blocks']."</td><td>".$r['BPG']."</td><td>".$r['Turnovers']."</td><td>".$r['Points']."</td><td>".$r['PPG']."</td><td>".$r['Fouls']."</td><td>".$r['PlusMinus']."</td><td>".$r['DQ']."</td><td>".$r['DoubleDoubles']."</td><td>".$r['TripleDoubles']."</td></tr>";
    }
    $echo .= "</tbody></table></div>";
    return $echo;
}
function printplayersingleseason($con,$name,$league,$type,$season,$export) 
{
    if($export == '1')
    {
        ob_clean();
        $fp = fopen('php://output', 'w');
        $heads = array('Player','GP','Min','FGA','FGM','FG%','3PA','3PM','3P%','FTA','FTM','FT%','ORB','Reb','RPG','Ast','APG','Stl','SPG','Blk','BPG','Trn','Pts','PPG','Fls','+/-','DQ','DD','TD');
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename="season_'.$season.'_stats.csv"');
        header('Pragma: no-cache');
        header('Expires: 0');
        fputcsv($fp,$heads);
    }
    if($season != '')
    {
        $clause = "AND y.year='".$season."'";
        $clause2 = "AND yr.year='".$season."'";
    }
    else
    {
        $clause = '';
        $clause2 = '';
    }
    if($league == 'sba' AND $type == 'season') 
    {
        $table = 'FROM SBA_Player p INNER JOIN SBA_SeasonStats s on p.ID = s.ID INNER JOIN year y on s.Season = y.sba_season';
        $table2 = 'FROM SBAR_Player pr INNER JOIN SBAR_SeasonStats sr on pr.ID = sr.ID INNER JOIN year yr on sr.Season = yr.sba_season';
    }
    else if($league == 'sba' AND $type == 'playoff')
    {
        $table = 'FROM SBA_Player p INNER JOIN SBA_PlayoffStats s on p.ID = s.ID INNER JOIN year y on s.Season = y.sba_season';
        $table2 = 'FROM SBAR_Player pr INNER JOIN SBAR_PlayoffStats sr on pr.ID = sr.ID INNER JOIN year yr on sr.Season = yr.sba_season';
    }
    else if($league == 'fiba' AND $type == 'season') 
    {
        $table = 'FROM FIBA_Player p INNER JOIN FIBA_SeasonStats s on p.ID = s.ID INNER JOIN year y on s.Season = y.fiba_season';
        $table2 = 'FROM FIBAR_Player pr INNER JOIN FIBAR_SeasonStats sr on pr.ID = sr.ID INNER JOIN year yr on sr.Season = yr.fiba_season';
    }
    else if($league == 'fiba' AND $type == 'playoff')
    {
        $table = 'FROM FIBA_Player p INNER JOIN FIBA_PlayoffStats s on p.ID = s.ID INNER JOIN year y on s.Season = y.fiba_season';
        $table2 = 'FROM FIBAR_Player pr INNER JOIN FIBAR_PlayoffStats sr on pr.ID = sr.ID INNER JOIN year yr on sr.Season = yr.fiba_season';
    }
    else if($league == 'ncaa' AND $type == 'season')
    {
        $table = 'FROM NCAA_Player p INNER JOIN NCAA_SeasonStats s on p.ID = s.ID INNER JOIN year y on s.Season = y.ncaa_season';
        $table2 = 'FROM NCAAR_Player pr INNER JOIN NCAAR_SeasonStats sr on pr.ID = sr.ID INNER JOIN year yr on sr.Season = yr.ncaa_season';
    }
    else
    {
        $table = 'FROM NCAA_Player p INNER JOIN NCAA_PlayoffStats s on p.ID = s.ID INNER JOIN year y on s.Season = y.ncaa_season';
        $table2 = 'FROM NCAAR_Player pr INNER JOIN NCAAR_PlayoffStats sr on pr.ID = sr.ID INNER JOIN year yr on sr.Season = yr.ncaa_season';
    }
    $statarr = array('Games','Minutes','FGA','FGM','FG%','3PA','3PM','3P%','FTA','FTM','FT%','OffensiveRebounds','Rebounds','Assists','Steals','Blocks','Turnovers','Points','Fouls','PlusMinus','DQ','DoubleDoubles','TripleDoubles');
    $secho = '';
    $secho2 = '';
    $secho3 = '';
    $ssize = count($statarr);
    for ($p=0;$p<$ssize;$p++)
    {
        if ($statarr[$p] == 'FG%' || $statarr[$p] == '3P%' || $statarr[$p] == 'FT%')
        {
            $str = $statarr[$p];
            $strA = substr_replace($str,'A',2);
            $strM = substr_replace($str,'M',2);
            $secho .= ", TRUNCATE(`$strM`/`$strA`,3) as `$str`";
        }
        else if ($statarr[$p] == 'Points' || $statarr[$p] == 'Assists' || $statarr[$p] == 'Rebounds' || $statarr[$p] == 'Steals' || $statarr[$p] == 'Blocks')
        {
            $str = $statarr[$p];
            $strPG = substr_replace($str,'PG',1);
            $secho .= ", `".$statarr[$p]."` as ".$statarr[$p]."";
            $secho .= ", TRUNCATE(`$str`/`Games`,2) as `$strPG`";
        }
        else
        {
            $secho .= ", `".$statarr[$p]."` as ".$statarr[$p]."";
        }
    }
    $query = "SELECT y.year,s.Team,CONCAT(FirstName,' ',LastName) as name$secho
    $table
    WHERE p.Name LIKE '$name' $clause
    UNION
    SELECT yr.year,sr.Team,CONCAT(FirstName,' ',LastName) as name$secho
    $table2
    WHERE pr.Name LIKE '$name' $clause2
    order by year DESC";
    $result = mysqli_query($con,$query);
    $secho2 = "<th>GP</th><th>Min</th><th>FGA</th><th>FGM</th><th>FG%</th><th>3PA</th><th>3PM</th><th>3P%</th><th>FTA</th><th>FTM</th><th>FT%</th><th>ORB</th><th>Reb</th><th>RPG</th><th>Ast</th><th>APG</th><th>Stl</th><th>SPG</th><th>Blk</th><th>BPG</th><th>Trn</th><th>Pts</th><th>PPG</th><th>Fls</th><th>+/-</th><th>DQ</th><th>DD</th><th>TD</th>";
    if($name == '%')
    {
        $header = "<th>Player</th>";
    }
    else
    {
        $header = "<th>Season</th><th>Team</th>";
    }
    $echo = "<div class='table-responsive'><table id='seasonstats' class='table table-striped compact' style='width:100%;'><thead><tr>".$header."".$secho2."</tr></thead><tbody>";
    while ($r = mysqli_fetch_array($result)) 
    {
        if($name == '%')
        {
            $dt = "<td>".$r['name']."</td>";
        }
        else
        {
            $dt = "<td>".$r['year']."</td><td>".$r['Team']."</td>";
        }
        $echo .= "<tr>".$dt."<td>".$r['Games']."</td><td>".$r['Minutes']."</td><td>".$r['FGA']."</td><td>".$r['FGM']."</td><td>".$r['FG%']."</td><td>".$r['3PA']."</td><td>".$r['3PM']."</td><td>".$r['3P%']."</td><td>".$r['FTA']."</td><td>".$r['FTM']."</td><td>".$r['FT%']."</td><td>".$r['OffensiveRebounds']."</td><td>".$r['Rebounds']."</td><td>".$r['RPG']."</td><td>".$r['Assists']."</td><td>".$r['APG']."</td><td>".$r['Steals']."</td><td>".$r['SPG']."</td><td>".$r['Blocks']."</td><td>".$r['BPG']."</td><td>".$r['Turnovers']."</td><td>".$r['Points']."</td><td>".$r['PPG']."</td><td>".$r['Fouls']."</td><td>".$r['PlusMinus']."</td><td>".$r['DQ']."</td><td>".$r['DoubleDoubles']."</td><td>".$r['TripleDoubles']."</td></tr>";
        if($export == 1)
        {
            $row = array(
            $r['name'],
            $r['Games'],
            $r['Minutes'],
            $r['FGA'],
            $r['FGM'],
            $r['FG%'],
            $r['3PA'],
            $r['3PM'],
            $r['3P%'],
            $r['FTA'],
            $r['FTM'],
            $r['FT%'],
            $r['OffensiveRebounds'],
            $r['Rebounds'],
            $r['RPG'],
            $r['Assists'],
            $r['APG'],
            $r['Steals'],
            $r['SPG'],
            $r['Blocks'],
            $r['BPG'],
            $r['Turnovers'],
            $r['Points'],
            $r['PPG'],
            $r['Fouls'],
            $r['PlusMinus'],
            $r['DQ'],
            $r['DoubleDoubles'],
            $r['TripleDoubles']
        );
        fputcsv($fp, $row);
        }
    }
    $echo .= "</tbody></table></div>";
    return $echo;
}
function printcareer($con,$stat,$league,$type) 
{
    if($league == 'sba' AND $type == 'season') 
    {
        $table = 'FROM SBA_Player p INNER JOIN SBA_SeasonStats s on p.ID = s.ID GROUP BY name';
        $table2 = 'FROM SBAR_Player pr INNER JOIN SBAR_SeasonStats sr on pr.ID = sr.ID GROUP BY name';
    }
    else if($league == 'sba' AND $type == 'playoff')
    {
        $table = 'FROM SBA_Player p INNER JOIN SBA_PlayoffStats s on p.ID = s.ID GROUP BY name';
        $table2 = 'FROM SBAR_Player pr INNER JOIN SBAR_PlayoffStats sr on pr.ID = sr.ID GROUP BY name';
    }
    else if($league == 'ncaa' AND $type == 'season')
    {
        $table = 'FROM NCAA_Player p INNER JOIN NCAA_SeasonStats s on p.ID = s.ID GROUP BY name';
        $table2 = 'FROM NCAAR_Player pr INNER JOIN NCAAR_SeasonStats sr on pr.ID = sr.ID GROUP BY name';
    }
    else 
    {
        $table = 'FROM NCAA_Player p INNER JOIN NCAA_PlayoffStats s on p.ID = s.ID GROUP BY name';
        $table2 = 'FROM NCAAR_Player pr INNER JOIN NCAAR_PlayoffStats sr on pr.ID = sr.ID GROUP BY name';
    }
    if($stat == 'TS%')
    {
        $average = "ROUND(SUM(`Points`) / (2*(SUM(`FGA`)+(.44*SUM(`FTA`)))),3) as category";
        $query = "SELECT CONCAT(FirstName,' ',LastName) as name, $average, '---' as average
        $table
        HAVING SUM(`FGA`) >= '820'
        UNION
        SELECT CONCAT(FirstName,' ',LastName) as name, $average, '---' as average
        $table2
        HAVING SUM(`FGA`) >= '820'
        order by 2 DESC";
    }
    else if($stat == 'PPS')
    {
        $average = "(SUM(`Points`))/(SUM(`FGA`)) as category";
        $query = "SELECT CONCAT(FirstName,' ',LastName) as name, $average, '---' as average
        $table
        HAVING SUM(`FGA`) >= '820'
        UNION
        SELECT CONCAT(FirstName,' ',LastName) as name, $average, '---' as average
        $table2
        HAVING SUM(`FGA`) >= '820'
        order by 2 DESC";
    }
    else if($stat == 'PPS(Real)')
    {
        $average = "(SUM(`Points`) - SUM(`FTM`))/(SUM(`FGA`)) as category";
        $query = "SELECT CONCAT(FirstName,' ',LastName) as name, $average, '---' as average
        $table
        HAVING SUM(`FGA`) >= '820'
        UNION
        SELECT CONCAT(FirstName,' ',LastName) as name, $average, '---' as average
        $table2
        HAVING SUM(`FGA`) >= '820'
        order by 2 DESC";
    }
    else
    {
        $query = "SELECT CONCAT(FirstName,' ',LastName) as name, SUM(`$stat`) as category, (SUM(`$stat`)/SUM(Games)) as average
        $table
        UNION
        SELECT CONCAT(FirstName,' ',LastName) as name, SUM(`$stat`) as category, (SUM(`$stat`)/SUM(Games)) as average
        $table2
        order by 2 DESC";
    }
    $result = mysqli_query($con,$query);
    $count = 1;
    $echo = "<div class='table-responsive'><table class='table table-condensed compact'><thead><tr><th></th><th>Player</th><th>$stat</th><th>Average</th></tr></thead><tbody>";
    while ($r = mysqli_fetch_array($result)) {
        $echo .= "<tr><td>$count</td><td>".$r['name']."</td><td>".$r['category']."</td><td>".$r['average']."</td></tr>";
        $count++;
    }
    $echo .= "</tbody></table></div>";
    return $echo;
}
function printalltime($con,$league,$type,$export) 
{
    if($export == '1')
    {
        ob_clean();
        $fp = fopen('php://output', 'w');
        $heads = array('Player','GP','Min','FGA','FGM','FG%','3PA','3PM','3P%','FTA','FTM','FT%','ORB','Reb','RPG','Ast','APG','Stl','SPG','Blk','BPG','Trn','Pts','PPG','Fls','+/-','DQ','DD','TD');
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename="alltime_stats.csv"');
        header('Pragma: no-cache');
        header('Expires: 0');
        fputcsv($fp,$heads);
    }
    if($league == 'sba' AND $type == 'season') 
    {
        $table = 'FROM SBA_Player p INNER JOIN SBA_SeasonStats s on p.ID = s.ID GROUP BY name';
        $table2 = 'FROM SBAR_Player pr INNER JOIN SBAR_SeasonStats sr on pr.ID = sr.ID GROUP BY name';
    }
    else if($league == 'sba' AND $type == 'playoff')
    {
        $table = 'FROM SBA_Player p INNER JOIN SBA_PlayoffStats s on p.ID = s.ID GROUP BY name';
        $table2 = 'FROM SBAR_Player pr INNER JOIN SBAR_PlayoffStats sr on pr.ID = sr.ID GROUP BY name';
    }
    else if($league == 'fiba' AND $type == 'season') 
    {
        $table = 'FROM FIBA_Player p INNER JOIN FIBA_SeasonStats s on p.ID = s.ID GROUP BY name';
        $table2 = 'FROM FIBAR_Player pr INNER JOIN FIBAR_SeasonStats sr on pr.ID = sr.ID GROUP BY name';
    }
    else if($league == 'fiba' AND $type == 'playoff')
    {
        $table = 'FROM FIBA_Player p INNER JOIN FIBA_PlayoffStats s on p.ID = s.ID GROUP BY name';
        $table2 = 'FROM FIBAR_Player pr INNER JOIN FIBAR_PlayoffStats sr on pr.ID = sr.ID GROUP BY name';
    }
    else if($league == 'ncaa' AND $type == 'season')
    {
        $table = 'FROM NCAA_Player p INNER JOIN NCAA_SeasonStats s on p.ID = s.ID GROUP BY name';
        $table2 = 'FROM NCAAR_Player pr INNER JOIN NCAAR_SeasonStats sr on pr.ID = sr.ID GROUP BY name';
    }
    else 
    {
        $table = 'FROM NCAA_Player p INNER JOIN NCAA_PlayoffStats s on p.ID = s.ID GROUP BY name';
        $table2 = 'FROM NCAAR_Player pr INNER JOIN NCAAR_PlayoffStats sr on pr.ID = sr.ID GROUP BY name';
    }
    $statarr = array('Games','Minutes','FGA','FGM','FG%','3PA','3PM','3P%','FTA','FTM','FT%','OffensiveRebounds','Rebounds','Assists','Steals','Blocks','Turnovers','Points','Fouls','PlusMinus','DQ','DoubleDoubles','TripleDoubles');
    $secho = '';
    $secho2 = '';
    $secho3 = '';
    $ssize = count($statarr);
    for ($p=0;$p<$ssize;$p++)
    {
        if ($statarr[$p] == 'FG%' || $statarr[$p] == '3P%' || $statarr[$p] == 'FT%')
        {
            $str = $statarr[$p];
            $strA = substr_replace($str,'A',2);
            $strM = substr_replace($str,'M',2);
            $secho .= ", TRUNCATE((SUM(`$strM`)/SUM(`$strA`)),3) as `$str`";
        }
        else if ($statarr[$p] == 'Points' || $statarr[$p] == 'Assists' || $statarr[$p] == 'Rebounds' || $statarr[$p] == 'Steals' || $statarr[$p] == 'Blocks')
        {
            $str = $statarr[$p];
            $strPG = substr_replace($str,'PG',1);
            $secho .= ", SUM(`".$statarr[$p]."`) as ".$statarr[$p]."";
            $secho .= ", TRUNCATE((SUM(`$str`)/SUM(`Games`)),2) as `$strPG`";
        }
        else
        {
            $secho .= ", SUM(`".$statarr[$p]."`) as ".$statarr[$p]."";
        }
    }
    $query = "SELECT CONCAT(FirstName,' ',LastName) as name$secho
    $table
    UNION
    SELECT CONCAT(FirstName,' ',LastName) as name$secho
    $table2
    order by 2 DESC";
    $result = mysqli_query($con,$query);
    $secho2 = "<th>GP</th><th>Min</th><th>FGA</th><th>FGM</th><th>FG%</th><th>3PA</th><th>3PM</th><th>3P%</th><th>FTA</th><th>FTM</th><th>FT%</th><th>ORB</th><th>Reb</th><th>RPG</th><th>Ast</th><th>APG</th><th>Stl</th><th>SPG</th><th>Blk</th><th>BPG</th><th>Trn</th><th>Pts</th><th>PPG</th><th>Fls</th><th>+/-</th><th>DQ</th><th>DD</th><th>TD</th>";
    $echo = "<br><br><div class='table-responsive'><table id='alltimestats' class='table table-striped compact' style='width:100%;'><thead><tr><th>Player</th>$secho2</tr></thead><tbody>";
    while ($r = mysqli_fetch_array($result)) {
        $echo .= "<tr><td>".$r['name']."</td><td>".$r['Games']."</td><td>".$r['Minutes']."</td><td>".$r['FGA']."</td><td>".$r['FGM']."</td><td>".$r['FG%']."</td><td>".$r['3PA']."</td><td>".$r['3PM']."</td><td>".$r['3P%']."</td><td>".$r['FTA']."</td><td>".$r['FTM']."</td><td>".$r['FT%']."</td><td>".$r['OffensiveRebounds']."</td><td>".$r['Rebounds']."</td><td>".$r['RPG']."</td><td>".$r['Assists']."</td><td>".$r['APG']."</td><td>".$r['Steals']."</td><td>".$r['SPG']."</td><td>".$r['Blocks']."</td><td>".$r['BPG']."</td><td>".$r['Turnovers']."</td><td>".$r['Points']."</td><td>".$r['PPG']."</td><td>".$r['Fouls']."</td><td>".$r['PlusMinus']."</td><td>".$r['DQ']."</td><td>".$r['DoubleDoubles']."</td><td>".$r['TripleDoubles']."</td></tr>";
        if($export == 1)
        {
            $row = array(
            $r['name'],
            $r['Games'],
            $r['Minutes'],
            $r['FGA'],
            $r['FGM'],
            $r['FG%'],
            $r['3PA'],
            $r['3PM'],
            $r['3P%'],
            $r['FTA'],
            $r['FTM'],
            $r['FT%'],
            $r['OffensiveRebounds'],
            $r['Rebounds'],
            $r['RPG'],
            $r['Assists'],
            $r['APG'],
            $r['Steals'],
            $r['SPG'],
            $r['Blocks'],
            $r['BPG'],
            $r['Turnovers'],
            $r['Points'],
            $r['PPG'],
            $r['Fouls'],
            $r['PlusMinus'],
            $r['DQ'],
            $r['DoubleDoubles'],
            $r['TripleDoubles']
        );
        fputcsv($fp, $row);
        }
    }
    $echo .= "</tbody></table></div>";
    return $echo;
}
function printfranchise($con,$team,$league) 
{
    if ($league == 'sba')
    {
        $from = "SBA_Team t";
        $record = "SBA_RecordByOpponent r";
        $past = "SBA_PastStandings p";
        $ij = "INNER JOIN year y on p.Season = y.sba_season";
    }
    else if ($league == 'fiba')
    {
        $from = "FIBA_Team t";
        $record = "FIBA_RecordByOpponent r";
        $past = "FIBA_PastStandings p";
        $ij = "INNER JOIN year y on p.Season = y.fiba_season";
    }
    else
    {
        $from = "NCAA_Team t";
        $record = "NCAA_RecordByOpponent r";
        $past = "NCAA_PastStandings p";
        $ij = "INNER JOIN year y on p.Season = y.ncaa_season";
    }

    $sql = "SELECT *
    FROM $from
    WHERE ID='$team'";
    $res = mysqli_query($con,$sql);
    $echo = "<br><br><div class='table-responsive'><table class='table compact'><thead><tr><th>Franchise History</th></tr></thead><tbody>";
    while ($r = mysqli_fetch_array($res)) {
        $echo .= "<tr><td>Championships</td><td>".$r['Championships']."</td></tr>";
        $echo .= "<tr><td>Finals Appearances</td><td>".$r['FinalsAppearances']."</td></tr>";
        $echo .= "<tr><td>Playoff Appearances</td><td>".$r['Playoffs']."</td></tr>";
        $echo .= "<tr><td>Overall Record</td><td>".$r['TotalWins']."-".$r['TotalLosses']."</td></tr>";
        $echo .= "<tr><td>Playoff Record</td><td>".$r['PlayoffWins']."-".$r['PlayoffLosses']."</td></tr>";

    }
    $echo .= "</tbody></table></div>";
    $sql2 = "SELECT CONCAT(CityName,' ',Name) as team,r.TotalWins,r.TotalLosses,r.PlayoffWins,r.PlayoffLosses,PlayoffSeriesWins,PlayoffSeriesLosses
    FROM $from
    INNER JOIN $record ON t.ID=r.OppID
    WHERE TeamID = '$team'";
    $res2 = mysqli_query($con,$sql2);
    $echo .= "<br><br><div class='table-responsive'><table class='table compact'><thead><tr><th>Record By Opponent</th><th>Wins</th><th>Losses</th><th>Playoff Wins</th><th>Playoff Losses</th><th>Playoff Series Wins</th><th>Playoff Series Losses</th></tr></thead><tbody>";
    while ($r = mysqli_fetch_array($res2)) {
        $echo .= "<tr><td>".$r['team']."</td><td>".$r['TotalWins']."</td><td>".$r['TotalLosses']."</td><td>".$r['PlayoffWins']."</td><td>".$r['PlayoffLosses']."</td><td>".$r['PlayoffSeriesWins']."</td><td>".$r['PlayoffSeriesLosses']."</td></tr>";
    }
    $echo .= "</tbody></table></div>";
    $query = "SELECT ID, CONCAT(CityName,' ',Name) as team,y.year as Season,Wins,Losses,(Wins/(Wins+Losses)) as percent
    FROM $from
    INNER JOIN $past ON t.ID = p.TeamID
    $ij
    WHERE ID='$team' ORDER BY Season ASC";
    $result = mysqli_query($con,$query);
    $echo .= "<br><br><div class='table-responsive'><table class='table compact'><thead><tr><th>Record by Season</th><th>Wins</th><th>Losses</th><th>Win Percentage</th></tr></thead><tbody>";
    while ($r = mysqli_fetch_array($result)) {
        $echo .= "<tr><td>S".$r['Season']."</td><td>".$r['Wins']."</td><td>".$r['Losses']."</td><td>".$r['percent']."</td></tr>";
    }
    $echo .= "</tbody></table></div>";
    return $echo;
}
function printfranchiseplayerrecords($con,$team,$category,$league) 
{
    if ($league == 'sba')
    {
        $from = "SBA_SingleTeamPlayerRecords s INNER JOIN year y on s.Season = y.sba_season";
    }
    else if ($league == 'fiba')
    {
        $from = "FIBA_SingleTeamPlayerRecords s INNER JOIN year y on s.Season = y.fiba_season";
    }
    else
    {
        $from = "NCAA_SingleTeamPlayerRecords s INNER JOIN year y on s.Season = y.ncaa_season";
    }
    $echo = '';
    $sql = "
    SELECT Rank, Player, Record, y.year as Season, OpponentName
    FROM $from
    WHERE RecordTitle = '$category' AND TeamID = '$team'
    ORDER BY Rank ASC
    ";
    $result = mysqli_query($con,$sql);
    $echo .= "<div class='table-responsive'><table class='table table-condensed compact'><thead><tr><th></th><th>Player</th><th>Rec.</th><th>Year</th><th>Opp.</th></tr></thead><tbody>";
    while ($r = mysqli_fetch_array($result)) {
        $echo .= "<tr><td>".$r['Rank']."</td><td>".$r['Player']."</td><td>".$r['Record']."</td><td>S".$r['Season']."</td><td>".$r['OpponentName']."</td></tr>";
    }
    $echo .= "</tbody></table></div>";
    return $echo;
}
function printfranchiseteamrecords($con,$team,$category,$league) 
{
    if ($league == 'sba')
    {
        $from = "SBA_SingleTeamRecords s INNER JOIN year y on s.Season = y.sba_season";
    }
    else if ($league == 'fiba')
    {
        $from = "FIBA_SingleTeamRecords s INNER JOIN year y on s.Season = y.fiba_season";
    }
    else
    {
        $from = "NCAA_SingleTeamRecords s INNER JOIN year y on s.Season = y.ncaa_season";
    }
    $echo = '';
    $sql = "
    SELECT Rank, Record, y.year as Season, OpponentName
    FROM $from
    WHERE RecordTitle = '$category' AND TeamID = '$team'
    ORDER BY Rank ASC
    ";
    $result = mysqli_query($con,$sql);
    $echo .= "<div class='table-responsive'><table class='table table-condensed compact'><thead><tr><th></th><th>Rec.</th><th>Year</th><th>Opp.</th></tr></thead><tbody>";
    while ($r = mysqli_fetch_array($result)) {
        $echo .= "<tr><td>".$r['Rank']."</td><td>".$r['Record']."</td><td>S".$r['Season']."</td><td>".$r['OpponentName']."</td></tr>";
    }
    $echo .= "</tbody></table></div>";
    return $echo;
}
function printchamps($con,$league) 
{
    if ($league == 'sba')
    {
        $from = "SBA_Champs c INNER JOIN year y on c.Season = y.sba_season ORDER BY year ASC";
    }
    else if ($league == 'fiba')
    {
        $from = "FIBA_Champs c INNER JOIN year y on c.Season = y.fiba_season ORDER BY year ASC";
    }
    else
    {
        $from = "NCAA_Champs c INNER JOIN year y on c.Season = y.ncaa_season ORDER BY year ASC";
    }
    $echo = '';
    $sql = "
    SELECT y.year as Season,Champ,Loser,CONCAT(ChampWins,'-',LoserWins) as Series
    FROM $from";
    $result = mysqli_query($con,$sql);
    $echo .= "<br><br><table class='table table-condensed'><thead><tr><th>Championship History</th><th>Champions</th><th>Runner-Ups</th><th>Series Result</th></tr></thead><tbody>";
    while ($r = mysqli_fetch_array($result)) {
        $echo .= "<tr><td>S".$r['Season']."</td><td>".$r['Champ']."</td><td>".$r['Loser']."</td><td>".$r['Series']."</td></tr>";
    }
    $echo .= "</tbody></table>";
    return $echo;
}
function printexam($con,$id)
{
    $res = "SELECT * from exam where ExamNum=$id ORDER BY ExamNum DESC";
    $result=mysqli_query($con,$res);
    $echo = "<br><br><table class='table'><tr><th>User</th><th>Points</th></tr>";
    while ($r = mysqli_fetch_array($result)) 
    {
        $echo .="<tr><td>".$r['username']."</td><td>".$r['numcorrect']."</td></tr>";
    }
    $echo .= "</table>";
    return $echo;
}
function printexamanswers($con,$id)
{
    $res = "SELECT * from exam_answers where ExamNum=$id";
    $result=mysqli_query($con,$res);
    $echo = "<br><br><table class='table'><tr><th>Exam #</th><th>Question 1</th><th>Question 2</th><th>Question 3</th></tr>";
    while ($r = mysqli_fetch_array($result)) 
    {
        $echo .="<tr><td>".$r['ExamNum']."</td><td>".$r['examq1']."</td><td>".$r['examq2']."</td><td>".$r['examq3']."</td></tr>";
    }
    $echo .= "</table>";
    return $echo;
}
function postToForum($con,$aid,$author,$data,$tid,$title,$fid)
{
    $query="INSERT INTO forums_posts (author_id,author_name,post,topic_id,new_topic,post_date) VALUES ('$aid','$author','$data','$tid','0',UNIX_TIMESTAMP())";
    mysqli_query($con,$query) or die("insert 1 failed");
    $query2="UPDATE forums_topics SET last_post=UNIX_TIMESTAMP(),last_real_post=UNIX_TIMESTAMP(),last_poster_id='$aid',last_poster_name='$author' WHERE tid='$tid'";
    mysqli_query($con,$query2) or die("insert 2 failed");
    $query3="UPDATE forums_forums SET last_post=UNIX_TIMESTAMP(),last_poster_id='$aid',last_poster_name='$author',last_title='$title',last_id='$tid' WHERE id='$fid'";
    mysqli_query($con,$query3) or die("insert 3 failed");
}
function createPlayerPage($con,$pid,$header)
{
    global $lgatty;
    $attyquerystring = '';
    for($x=0;$x<count($lgatty);$x++)
    {
        if($x == 0)
        {
            $attyquerystring .= "`".$lgatty[$x]."`";
        }
        else
        {
            $attyquerystring .= ",`".$lgatty[$x]."`";
        }
    }
    $query = "SELECT username,CONCAT(plast,', ',pfirst) AS pname,CONCAT(pfirst,' ',plast) AS tname,position,position2,build,t.name as team,t.id as tid,num,height,weight,country,p.league,p.active,p.id,p.user_fk,$attyquerystring,c90,c801,c802,c803,c804,c70,bank,tpe,p_bank
        FROM auth_user u 
        INNER JOIN players p ON u.ID = p.user_fk 
        INNER JOIN teams t ON p.team=t.id
        WHERE p.id='$pid'";
    $result = mysqli_query($con,$query);
    while($row = mysqli_fetch_array($result))
    {
        $pos2 = $row['position2'];
        if($pos2 != '')
        {
            $pos2 = "/".$pos2;
        }
        $listinfo = Array('Username','Player Name','Position(s)','Build','Number','Height','Weight','Country');
        $listinfo_short = Array('username','tname','position','build','num','height','weight','country');
        $b = 0;
        $path = $_SERVER['DOCUMENT_ROOT'];
        $playerimage = $path."/images/player-images/".$pid.".png";
        if(file_exists($playerimage))
        {
            $playerimage = "/images/player-images/".$pid.".png";
        }
        else
        {
            $playerimage = "/images/player-images/default.png";
        }
        //
        $echo = "
        <div class='row'>
            <div class='col-xs-12'>
                ".(($header == 1) ? "<center><img src='$playerimage' class='imgphoto'></center><br>" : "")."
                ".(($row['tid'] == 0) ? "<center><img src='/images/team-images/".$row['tid'].".png' style='height:50px;width:50px;'></center><br>" : "")."
                <a href='view_updates.php?pid=".$row['id']."' class='btn btn-primary col-xs-12. col-sm-4'>Update History</a>
                <a href='player_earn_history.php?pid=".$pid."' class='btn btn-primary col-xs-12 col-sm-4'>Earnings History</a>
                <a href='player_purchase_history.php?pid=".$pid."' class='btn btn-primary col-xs-12 col-sm-4'>Purchase History</a>
            </div>
        </div>
        <hr>
        <div class='panel panel-primary hidden-xs'>
            <div class='panel-heading'>Player Information</div>
            <div class='panel-body'>
                <form action='' class='form-inline'>
                    <div class='form-group'>
                        ";
                        for ($a=0;$a<count($listinfo);$a++)
                        {
                            if($listinfo[$a] == 'Position(s)'){$addtl = $pos2;}else{$addtl = '';}
                            $echo .= "<div class='input-group col-sm-4'><span class='input-group-addon' style='width:110px;text-align:left;'><b>".$listinfo[$a]."</b></span><input type='text' class='form-control' readonly='readonly' value=\"".$row[$listinfo_short[$a]].$addtl."\"></div>";
                            if($b >= count($lgatty))
                            {
                                $echo .= "<div class='input-group col-sm-4'><span class='input-group-addon' style='width:110px;text-align:left;'><b></b></span><input type='text' class='form-control' readonly='readonly' value=''>";
                            }
                            else if($b < count($lgatty))
                            {
                                $echo .= "<div class='input-group col-sm-4'><span class='input-group-addon' style='width:110px;text-align:left;'><b>".strtoupper($lgatty[$b])."</b></span><input type='text' class='form-control' readonly='readonly' value='".$row[$lgatty[$b]]."";
                                if($lgatty[$b] == '')
                                {
                                    $echo .= "'>";
                                }
                                else if($lgatty[$b] == $row['c90'])
                                {
                                    $echo .= "/90'>";
                                }
                                else if($lgatty[$b] == $row['c70'])
                                {
                                    $echo .= "/70'>";
                                }
                                else if($lgatty[$b] == $row['c801'] || $lgatty[$b] == $row['c802'] || $lgatty[$b] == $row['c803'] || $lgatty[$b] == $row['c804'])
                                {
                                    $echo .= "/80'>";
                                }
                                else
                                {
                                    $echo .= "/99'>";
                                }
                            }
                            $echo .= "</div>";
                            $b++;
                            if($b >= count($lgatty))
                            {
                                $echo .= "<div class='input-group col-sm-4'><span class='input-group-addon' style='width:110px;text-align:left;'><b></b></span><input type='text' class='form-control' readonly='readonly' value=''>";
                            }
                            else if($b < count($lgatty))
                            {
                                $echo .= "<div class='input-group col-sm-4'><span class='input-group-addon' style='width:110px;text-align:left;'><b>".strtoupper($lgatty[$b])."</b></span><input type='text' class='form-control' readonly='readonly' value='".$row[$lgatty[$b]]."";
                                if($lgatty[$b] == '')
                                {
                                    $echo .= "'>";
                                }
                                else if($lgatty[$b] == $row['c90'])
                                {
                                    $echo .= "/90'>";
                                }
                                else if($lgatty[$b] == $row['c70'])
                                {
                                    $echo .= "/70'>";
                                }
                                else if($lgatty[$b] == $row['c801'] || $lgatty[$b] == $row['c802'] || $lgatty[$b] == $row['c803'] || $lgatty[$b] == $row['c804'])
                                {
                                    $echo .= "/80'>";
                                }
                                else
                                {
                                    $echo .= "/99'>";
                                }
                            }
                            $echo .= "</div>";
                            $b++;
                        }
                    $echo .= "
                        <div class='input-group col-xs-12 col-sm-4'><span class='input-group-addon' style='width:110px;text-align:left;'><b>Cash</b></span><input type='text' class='form-control' readonly='readonly' value='$".number_format($row['p_bank'],0)."'></div><div class='input-group col-xs-12 col-sm-4'><span class='input-group-addon' style='width:110px;text-align:left;'><strong>TPE</strong></span><input type='text' class='form-control' readonly='readonly' value='".$row['tpe']."'></div><div class='input-group col-xs-12 col-sm-4'><span class='input-group-addon' style='width:110px;text-align:left;'><strong>Banked</strong></span><input type='text' class='form-control' readonly='readonly' value='".$row['bank']."'></div>
                    </div>
                </form>
            </div>
        </div>
        <div class='panel panel-primary hidden-sm hidden-md hidden-lg'>
            <div class='panel-heading'>Player Information</div>
            <div class='panel-body'>";
                for ($a=0;$a<count($listinfo);$a++)
                {
                    if($listinfo[$a] == 'Position(s)'){$addtl = $pos2;}else{$addtl = '';}
                    $echo .= "<div class='input-group col-xs-12 col-sm-4'><span class='input-group-addon' style='min-width:110px;text-align:left;'><b>".$listinfo[$a]."</b></span><input type='text' class='form-control' readonly='readonly' value=\"".$row[$listinfo_short[$a]].$addtl."\"></div>";
                }
                for ($a=0;$a<count($lgatty);$a++)
                {
                    $echo .= "<div class='input-group col-xs-12 col-sm-4'><span class='input-group-addon' style='min-width:110px;text-align:left;'><b>".strtoupper($lgatty[$a])."</b></span><input type='text' class='form-control' readonly='readonly' value='".$row[$lgatty[$a]]."";
                    if($lgatty[$a] == $row['c90'])
                    {
                        $echo .= "/90'>";
                    }
                    else if($lgatty[$a] == $row['c70'])
                    {
                        $echo .= "/70'>";
                    }
                    else if($lgatty[$a] == $row['c801'] || $lgatty[$a] == $row['c802'] || $lgatty[$a] == $row['c803'] || $lgatty[$a] == $row['c804'])
                    {
                        $echo .= "/80'>";
                    }
                    else
                    {
                        $echo .= "/99'>";
                    }
                    $echo .= "</div>";
                }
            $echo .="
                <div class='input-group col-xs-12 col-sm-4'><span class='input-group-addon' style='min-width:110px;text-align:left;'><b>Cash</b></span><input type='text' class='form-control' readonly='readonly' value='$".number_format($row['p_bank'],0)."'></div>
                <div class='input-group col-xs-12 col-sm-4'><span class='input-group-addon' style='min-width:110px;text-align:left;'><strong>TPE</strong></span><input type='text' class='form-control' readonly='readonly' value='".$row['tpe']."'></div>
                <div class='input-group col-xs-12 col-sm-4'><span class='input-group-addon' style='min-width:110px;text-align:left;'><strong>Banked</strong></span><input type='text' class='form-control' readonly='readonly' value='".$row['bank']."'></div>
            </div>
        </div>";
    }
return $echo;
}
function createPlayerSnapshot($con,$pid)
{
    $query = "SELECT s.ins,s.jps,s.ft,s.3ps,s.han,s.pas,s.orb,s.drb,s.psd,s.prd,s.stl,s.blk,s.fl,s.qkn,s.str,s.jmp,c90,c801,c802,c803,c804,c70,s.tpe
        FROM auth_user u 
        INNER JOIN players p ON u.ID = p.user_fk 
        INNER JOIN player_snapshot s ON p.id=s.p_fk
        INNER JOIN teams t ON p.team=t.id
        WHERE p.id='$pid'";
    $result = mysqli_query($con,$query);
    while($row = mysqli_fetch_array($result))
    {
        $listarr = Array('INS','JPS','FT','3PS','HAN','PAS','ORB','DRB','PSD','PRD','STL','BLK','FL','QKN','STR','JMP');
        $listarr_short = Array('ins','jps','ft','3ps','han','pas','orb','drb','psd','prd','stl','blk','fl','qkn','str','jmp');
        $echo = "
        <div class='panel panel-primary'>
            <div class=\"panel-heading\"><h4 class='panel-title'><a data-toggle='collapse' href='#collapsesnap'>Player Snapshot</a></h4></div>
            <div class='panel-collapse collapse' id='collapsesnap'>
                <div class='panel-body'>";
                    for ($a=0;$a<count($listarr);$a++)
                    {
                        $echo .= "<div class='input-group col-xs-12 col-sm-4'><span class='input-group-addon' style='min-width:110px;text-align:left;'><b>".$listarr[$a]."</b></span><input type='text' class='form-control' readonly='readonly' value='".$row[$listarr_short[$a]]."";
                        if($listarr_short[$a] == $row['c90'])
                        {
                            $echo .= "/90'>";
                        }
                        else if($listarr_short[$a] == $row['c70'])
                        {
                            $echo .= "/70'>";
                        }
                        else if($listarr_short[$a] == $row['c801'] || $listarr_short[$a] == $row['c802'] || $listarr_short[$a] == $row['c803'] || $listarr_short[$a] == $row['c804'])
                        {
                            $echo .= "/80'>";
                        }
                        else
                        {
                            $echo .= "/99'>";
                        }
                        $echo .= "</div>";
                    }
                $echo .="
                    <div class='input-group col-xs-12 col-sm-4'><span class='input-group-addon' style='min-width:110px;text-align:left;'><strong>TPE</strong></span><input type='text' class='form-control' readonly='readonly' value='".$row['tpe']."'></div>
                </div>
            </div>
        </div>";
    }
return $echo;
}
function createTopic($curl_post_data)
{
    // Rest API
    global $hostlg;
    if($hostlg == 'SBA')
    {
        $communityUrl = 'https://sba.today/forums/';
        $apiKey = '394860db6eaeeae7e5a09941f3134348';
    }
    else if($hostlg == 'EFL')
    {
        $communityUrl = 'https://efl.network/forums/';
        $apiKey = 'e34f899016524f15933281a922c7346e';
    }
    $endpoint = '/index.php?/forums/topics';
    $postData = http_build_query($curl_post_data,'','&');
    $curl = curl_init( $communityUrl . 'api' . $endpoint . '&key=' . $apiKey );
    curl_setopt_array($curl,array(
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_POSTFIELDS => $postData,
    ));
    $response = curl_exec($curl);
    // End Rest API
}
function getTopic($tid)
{
    // Rest API
    global $hostlg;
    if($hostlg == 'SBA')
    {
        $communityUrl = 'https://sba.today/forums/';
        $apiKey = '394860db6eaeeae7e5a09941f3134348';
    }
    else if($hostlg == 'EFL')
    {
        $communityUrl = 'https://efl.network/forums/';
        $apiKey = 'e34f899016524f15933281a922c7346e';
    }
    $endpoint = '/index.php?/forums/topics/'.$tid;
    $curl = curl_init( $communityUrl . 'api' . $endpoint . '&key=' . $apiKey );
    curl_setopt_array( $curl, array(
        CURLOPT_RETURNTRANSFER  => TRUE,
    ) );
    $response = curl_exec( $curl );
    //$resp = array();
    //parse_str($response,$resp);
    $resp = json_decode($response,true);
    //$rep = $resp[1]['url'];
    return $resp;
    // End Rest API
}
function getTopics($league,$mid)
{
    // Rest API
    if($league == 'SBA')
    {
        $communityUrl = 'https://sba.today/forums/';
        $apiKey = '394860db6eaeeae7e5a09941f3134348';
        $forums = "20,32,34,36,78,456,724";
    }
    else if($league == 'EFL')
    {
        $communityUrl = 'https://efl.network/forums/';
        $apiKey = '429324692847bf73df30e79a255b1c05';
        $forums = "24,36,38";
    }
    $endpoint = '/index.php?/forums/topics';
    $curl = curl_init($communityUrl.'api'.$endpoint.'&authors='.$mid.'&forums='.$forums.'&sortDir=desc&perPage=5&key='.$apiKey);
    curl_setopt_array( $curl, array(
        CURLOPT_RETURNTRANSFER  => TRUE,
    ) );
    $response = curl_exec( $curl );
    return $response;
    // End Rest API
}
function createPost($curl_post_data)
{
    // Rest API
    global $hostlg;
    if($hostlg == 'SBA')
    {
        $communityUrl = 'https://sba.today/forums/';
        $apiKey = '394860db6eaeeae7e5a09941f3134348';
    }
    else if($hostlg == 'EFL')
    {
        $communityUrl = 'https://efl.network/forums/';
        $apiKey = 'e34f899016524f15933281a922c7346e';
    }
    $endpoint = '/index.php?/forums/posts';
    $curl = curl_init( $communityUrl . 'api' . $endpoint . '&key=' . $apiKey );
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
    $response = curl_exec($curl);
    // End Rest API
}
function getPost($pid)
{
    // Rest API
    global $hostlg;
    if($hostlg == 'SBA')
    {
        $communityUrl = 'https://sba.today/forums/';
        $apiKey = '394860db6eaeeae7e5a09941f3134348';
    }
    else if($hostlg == 'EFL')
    {
        $communityUrl = 'https://efl.network/forums/';
        $apiKey = 'e34f899016524f15933281a922c7346e';
    }
    $endpoint = '/index.php?/forums/posts/'.$pid;
    $curl = curl_init( $communityUrl . 'api' . $endpoint . '&key=' . $apiKey );
    curl_setopt_array( $curl, array(
        CURLOPT_RETURNTRANSFER  => TRUE,
    ) );
    $response = curl_exec( $curl );
    $resp = array();
    parse_str($response,$resp);
    $resp = json_decode($response,true);
    $rep = $resp['content'];
    return $rep;
    // End Rest API
}
function createMessage($curl_post_data)
{
    // Rest API
    global $hostlg;
    if($hostlg == 'SBA')
    {
        $communityUrl = 'https://sba.today/forums/';
        $apiKey = '394860db6eaeeae7e5a09941f3134348';
    }
    else if($hostlg == 'EFL')
    {
        $communityUrl = 'https://efl.network/forums/';
        $apiKey = '429324692847bf73df30e79a255b1c05';
    }
    $endpoint = '/index.php?/core/messages';
    $postData = http_build_query($curl_post_data,'','&');
    $curl = curl_init( $communityUrl . 'api' . $endpoint . '&key=' . $apiKey );
    curl_setopt_array($curl,array(
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_POSTFIELDS => $postData,
    ));
    $response = curl_exec($curl);
    // End Rest API
}
function returnTopicURL($tid)
{
    if($tid == '')
    {
        return '#';
    }
    // Rest API
    $communityUrl = 'http://sba.today/forums/';
    $apiKey = '394860db6eaeeae7e5a09941f3134348';
    $endpoint = '/index.php?/forums/topics/'.$tid.'';
    $curl = curl_init( $communityUrl . 'api' . $endpoint . '&key=' . $apiKey );
    curl_setopt_array( $curl, array(
        CURLOPT_RETURNTRANSFER  => TRUE,
    ) );
    $response = curl_exec( $curl );
    $resp = array();
    parse_str($response,$resp);
    $resp = json_decode($response,true);
    $rep = $resp['url'];
    return $rep;
}
function calculateSalary($con,$pid,$tpe,$exp,$active,$type)
{
    if(($active == 1) || ($active == 3))
    {
        if($tpe < '251')
        {
            $salary = '0';
        }
        else if($tpe < '401')
        {
            $salary = '1000000';
        }
        else if ($tpe < '551')
        {
            $salary = '1500000';
        }
        else if ($tpe < '701')
        {
            $salary = '2000000';
        }
        else if ($tpe < '851')
        {
            $salary = '2500000';
        }
        else if ($tpe < '1001')
        {
            $salary = '3000000';
        }
        else if ($tpe < '1151')
        {
            $salary = '3500000';
        }
        else if ($tpe < '1301')
        {
            $salary = '4000000';
        }
        else if ($tpe < '1451')
        {
            $salary = '4500000';
        }
        else if ($tpe < '1601')
        {
            $salary = '5000000';
        }
        else if ($tpe < '1751')
        {
            $salary = '5500000';
        }
        else if ($tpe < '1901')
        {
            $salary = '6000000';
        }
        else
        {
            $salary = '6500000';
        }
        if(($exp > '3' && $exp < '9') && ($salary > 0))
        {
            $salary = $salary + '500000';
        }
    }
    else
    {
        $salary = '0';
    }
    if($active == 3 && $type == 'expected')
    {
        $salary = '0';
    }
    if($type == 'min')
    {
        $sql = "UPDATE players SET min_salary = '$salary' WHERE id='$pid'";
        mysqli_query($con,$sql);
    }
    else if($type == 'main')
    {
        $sql = "UPDATE players SET salary = '$salary' WHERE id='$pid'";
        mysqli_query($con,$sql);
    }
    else if($type == 'expected')
    {
        $sql = "UPDATE players SET exp_salary = '$salary' WHERE id='$pid'";
        mysqli_query($con,$sql);
    }
}
function round_up_to_nearest_n($int, $n) 
{
    return round($int / $n) * $n;
}
function audit_updates($con,$user,$task) 
{
    $sql = "INSERT INTO audit_updates (username,task,ipaddress,audittime) VALUES (\"$user\",'$task','$_SERVER[REMOTE_ADDR]',now())";
    mysqli_query($con,$sql);
}
function create_update_history($con,$pid,$atts)
{
    global $lgatty;
    $oldlgatty = Array();
    $attyquerystring = '';
    $oldattyquerystring = '';
    for($x=0;$x<count($lgatty);$x++)
    {
        if($x == 0)
        {
            $attyquerystring .= "`".$lgatty[$x]."`";
            $oldattyquerystring .= "`".$lgatty[$x]."_old`";
            array_push($oldlgatty,$lgatty[$x]."_old");
        }
        else
        {
            $attyquerystring .= ",`".$lgatty[$x]."`";
            $oldattyquerystring .= ",`".$lgatty[$x]."_old`";
            array_push($oldlgatty,$lgatty[$x]."_old");
        }
    }
    $query = "SELECT pu.id as puid,p.id,CONCAT(pfirst,' ',plast) AS pname,p.league,active,status,build,c90,c801,c802,c803,c804,c70,t.name as tname,p.tpe,p.bank as old_bank,pu.earn_tpe,pu.bank as new_bank,pu.last_submitted,pu.approved,pu.approver,notes FROM players p 
            INNER JOIN player_updates pu ON p.ID=pu.pid_fk
            INNER JOIN player_updates_update puu ON pu.id = puu.puid_fk
            INNER JOIN teams t ON p.team = t.id
            WHERE p.id = '$pid'
            ORDER BY puid DESC";
    $result = mysqli_query($con,$query);
    $i = 50000;
    $echo = '';
    while ($r = mysqli_fetch_array($result))
    {
        $pname = $r['pname'];
        if($r['status'] > '1')
        {$timestamp = " | Approved ".$r['approved']." by ".$r['approver'];}
        else
        {$timestamp = '';}
        $concat = "
        <div class='panel panel-primary'>
            <div class='panel-heading'><h4 class='panel-title'><a data-toggle='collapse' href='#collapse$pid$i'>Update #".$r['puid']." | Submitted ".$r['last_submitted']."".$timestamp."</a></h4></div>
            <div class='panel-collapse collapse' id='collapse$pid$i'>
                <div class='panel-body'>";
                    $puid = $r['puid'];
        $concat .= "
        <table class=\"table table-bordered table-hover\">
            <thead>
                <tr >
                    <th>Task</th>
                    <th>Week Ending</th>
                    <th class='hidden-xs'>Points Earned</th>
                    <th class='text-center hidden-sm hidden-md hidden-lg'>P.E.</th>
                </tr>
            </thead>
            <tbody>";
                $query = "SELECT task,week,link,pe FROM player_updates_tasks put WHERE puid_fk = '$puid'";
                $result2 = mysqli_query($con,$query);
                while($s = mysqli_fetch_array($result2))
                {
                    $concat .= "
                            <tr>
                                <td><a href='".$s['link']."' style='text-decoration:underline;' target='_blank'>".$s['task']."</a></td>
                                <td>".$s['week']."</td>
                                <td>".$s['pe']."</td>
                            </tr>
                    ";
                }
                $concat .= "
            </tbody>
        </table>";
        $concat .= "
        <div class='input-group'>
            <span class='input-group-addon'>Update Notes</span>
            <textarea class='form-control' name='notes' rows='2'>".$r['notes']."</textarea>
        </div>";
        if($atts == '1')
        {
            $concat .= "
            <table class='table'>
                <thead>
                    <tr>
                        <th>Attribute Name</th>
                        <th>Old</th>
                        <th>New</th>
                    </tr>
                </thead>
                <tbody>";
                    $sql = "SELECT $attyquerystring,$oldattyquerystring FROM player_updates_update WHERE puid_fk='$puid'";
                    $result3 = mysqli_query($con,$sql);
                    $attconcat = '';
                    while($w = mysqli_fetch_array($result3))
                    {
                        for($f=0;$f<count($lgatty);$f++)
                        {
                            if($r['build'] == 'Standard')
                            {
                                if($r['c90'] == $lgatty[$f])
                                {
                                    $max = " (MAX:90)";
                                }
                                else if($r['c801'] == $lgatty[$f] || $r['c802'] == $lgatty[$f] || $r['c803'] == $lgatty[$f] || $r['c804'] == $lgatty[$f])
                                {
                                    $max = " (MAX:80)";
                                }
                                else if($r['c70'] == $lgatty[$f])
                                {
                                    $max = " (MAX:70)";
                                }
                                else
                                {
                                    $max = "";
                                }
                            }
                            else
                            {
                                $max = "";
                            }
                            $concat .= "
                            <tr>
                                <td>".$lgatty[$f]."".$max."</td>
                                <td>".$w[$oldlgatty[$f]]."</td>
                                <td>".$w[$lgatty[$f]]."</td>
                            </tr>";
                        }
                    }
                    $concat .= "<tr><td>TPE Earned</td><td>".$r['earn_tpe']."</td><td></td></tr>";
                    $concat .= "
                </tbody>
            </table>";
        }
        else
        {

        }
        $concat .= "</div></div></div>";
        $i++;
        $echo .= $concat;
    }
    return $echo;
}
function create_update_history_week($con,$pid,$limit)
{
    if($limit != '')
    {
        $clause = "LIMIT ".$limit;
    }  
    else
    {
        $clause = '';
    }     
    $sql = "SELECT DISTINCT week
    FROM players p
    INNER JOIN player_updates pu ON p.id=pu.pid_fk
    INNER JOIN player_updates_tasks put ON pu.id=put.puid_fk
    WHERE p.id = '$pid' AND week != '' ORDER BY STR_TO_DATE(WEEK,'%m/%d/%Y') DESC $clause";
    $result = mysqli_query($con,$sql);
    $i = 500000;
    $echo = '';
    while ($r = mysqli_fetch_array($result))
    {
        $concat = "
        <div class='panel panel-primary'>
            <div class='panel-heading'><h4 class='panel-title'><a data-toggle='collapse' href='#collapse$pid$i'>Week Ending ".$r['week']."</a></h4></div>
            <div class='panel-collapse collapse' id='collapse$pid$i'>
                <div class='panel-body'>";
        $concat .= "<table class=\"table table-bordered table-hover\">
                        <thead>
                            <tr >
                                <th>Task</th>
                                <th>Week Ending</th>
                                <th class='hidden-xs'>Points Earned</th>
                                <th class='text-center hidden-sm hidden-md hidden-lg'>P.E.</th>
                            </tr>
                        </thead>
                        <tbody>";
                            $query = "SELECT task,week,link,pe
                            FROM players p
                            INNER JOIN player_updates pu ON p.id=pu.pid_fk
                            INNER JOIN player_updates_tasks put ON pu.id=put.puid_fk
                            WHERE p.id = '$pid' AND (pu.status > 1) AND week = '".$r['week']."'";
                            $result2 = mysqli_query($con,$query);
                            $t = 1;
                            while($s = mysqli_fetch_array($result2))
                            {
                                $concat .= "
                                        <tr>
                                            <td><a href='".$s['link']."' style='text-decoration:underline;' target='_blank'>".$s['task']."</a></td>
                                            <td>".$s['week']."</td>
                                            <td>".$s['pe']."</td>
                                        </tr>
                                ";
                                $t++;
                            }
                            $concat .= "
                        </tbody>
                    </table>";
        $concat .= "</div>
            </div>
        </div>";
        $i++;
        $echo .= $concat;
    }
    return $echo;
}
function process_predictions($con,$type,$preyear)
{
    if($type == 'season') 
    {
        $table = 'predictions';
    }
    else if($type == 'playoff')
    {
        $table = 'playoff_predictions';
    }
    else if($type == 'sbdl_season')
    {
        $table = 'sbdl_predictions';
    }
    else 
    {
        $table = 'ncaa_playoff_predictions';
    }
    $sqn="SELECT a_fk,numcorrect FROM $table WHERE year= '$preyear' ORDER BY numcorrect DESC";
    $result=mysqli_query($con,$sqn);
    while ($row = mysqli_fetch_array($result))
    {
        $user = $row['a_fk'];
        //$user = str_replace("'","''",$user);
        $pe = $row['numcorrect'];
        $sql="SELECT p.id
        FROM players p
        WHERE p.user_fk='$user' AND (p.active='1' || p.active='3')";
        $result1 = mysqli_query($con,$sql);
        while ($r = mysqli_fetch_array($result1))
        {
            $pid = $r['id'];
            $sql = "SELECT id FROM player_prediction WHERE p_fk = '$pid' AND season = '$preyear' AND type = '$type'";
            $nppresult = mysqli_query($con,$sql);
            $count = mysqli_num_rows($nppresult);
            if($count == 0)
            {
                $sql = "INSERT INTO player_prediction (p_fk,season,type,pe) VALUES ('$pid','$preyear','$type','$pe')";
                mysqli_query($con,$sql);
            }
            else
            {
                
            }
        }
    }
}
function printplayerratings($con,$pid)
{
    $query = "SELECT CONCAT(pfirst,' ',plast) AS tname,league,tpe,ape,bank,id,ins,jps,ft,3ps,han,pas,orb,drb,psd,prd,stl,blk,fl,qkn,str,jmp FROM players WHERE id = '$pid'";
    $result = mysqli_query($con,$query);
    $secho2 = "<th>Name</th><th>INS</th><th>JPS</th><th>FT</th><th>3PS</th><th>HAN</th><th>PAS</th><th>ORB</th><th>DRB</th><th>PSD</th><th>PRD</th><th>STL</th><th>BLK</th><th>FL</th><th>QKN</th><th>STR</th><th>JMP</th>";
    $echo = "<div class='table-responsive'><table id='season' class='table table-striped compact' style='width:100%;'><thead><tr>".$secho2."</tr></thead><tbody>";
    while ($r = mysqli_fetch_array($result)) 
    {
        $echo .= "<tr><td>".$r['tname']."</td><td>".$r['ins']."</td><td>".$r['jps']."</td><td>".$r['ft']."</td><td>".$r['3ps']."</td><td>".$r['han']."</td><td>".$r['pas']."</td><td>".$r['orb']."</td><td>".$r['drb']."</td><td>".$r['psd']."</td><td>".$r['prd']."</td><td>".$r['stl']."</td><td>".$r['blk']."</td><td>".$r['fl']."</td><td>".$r['qkn']."</td><td>".$r['str']."</td><td>".$r['jmp']."</td></tr>";
    }
    $echo .= "</tbody></table></div>";
    return $echo;
}
function reset_tpe($con,$pid)
{
    global $conn;
    global $hostlg;
    $sql = "SELECT build,tpe,ape FROM players WHERE id='$pid'";
    $rql=mysqli_fetch_array(mysqli_query($con,$sql));
    $build=$rql['build'];
    $tpe=$rql['tpe'];
    $ape=$rql['ape'];
    $bank=$ape;
    if($hostlg == 'SBA')
    {
        if($build == 'Standard')
        {
            $ins = $jps = $ft = $tps = $han = $pas = $orb = $drb = $psd = $prd = $stl = $blk = $qkn = $str = $jmp = '35';
            $fl = '50';
        }
        else
        {
            $ins = $jps = $ft = $tps = $han = $pas = $orb = $drb = $psd = $prd = $stl = $blk = '35';
            $fl = $qkn = $str = $jmp = '20';
        }
        $sql = "UPDATE players
                SET ins='$ins',
                    jps='$jps',
                    ft='$ft',
                    3ps='$tps',
                    han='$han',
                    pas='$pas',
                    orb='$orb',
                    drb='$drb',
                    psd='$psd',
                    prd='$prd',
                    stl='$stl',
                    blk='$blk',
                    fl='$fl',
                    qkn='$qkn',
                    str='$str',
                    jmp='$jmp',
                    tpe='$tpe',
                    ape='$ape',
                    bank='$bank'
                WHERE id='$pid'";
        mysqli_query($con,$sql);
    }
    else if($hostlg == 'EFL')
    {
        $att = 35;
        $stmt = $conn->prepare("UPDATE players SET str=:att,agi=:att,arm=:att,`int`=:att,acc=:att,tkl=:att,spd=:att,han=:att,rbk=:att,pbk=:att,kdi=:att,kac=:att,tpe=:tpe,ape=:ape,bank=:bank WHERE id=:pid");
        $stmt->execute([':pid' => $pid,':att' => $att,':tpe' => $tpe,':ape' => $ape,':bank' => $bank]);
    }
}
function delete_current_update($con,$pid)
{
    $sql = "SELECT id FROM player_updates WHERE status < '2' AND pid_fk = '$pid'";
    $rql=mysqli_fetch_array(mysqli_query($con,$sql));
    $puid = $rql['id'];
    $sql = "DELETE FROM player_updates_tasks WHERE puid_fk = '$puid'";
    mysqli_query($con,$sql);
    $sql = "DELETE FROM player_updates_update WHERE puid_fk = '$puid'";
    mysqli_query($con,$sql);
    $sql = "DELETE FROM player_updates WHERE id = '$puid'";
    mysqli_query($con,$sql);
}
function get_messages($con,$user)
{ 
    $sql = "SELECT u.id,topic,message,created_date,opened,locked FROM user_messages u INNER JOIN auth_user a ON u.a_fk=a.ID WHERE username=\"$user\" ORDER BY created_date DESC";
    $result = mysqli_query($con,$sql);
    $i = 500000000;
    $echo = '';
    while ($r = mysqli_fetch_array($result))
    {
        $concat = "
        <div class='panel panel-primary'>
            <div class='panel-heading'><h4 class='panel-title'><a data-toggle='collapse' href='#collapse$i' onclick=\"readMessage('".$r['id']."')\">".$r['topic']." ".(($r['opened'] == 0) ? "<span class='badge'>NEW</span>" : "")."</a> <span class='pull-right' style='background-color:transparent;top:0;padding:0;margin:0;'>".(($r['locked'] == 0) ? "<a href='#' onclick='lockMessage(".$r['id'].",\"$user\")'><i class='fas fa-unlock'></i></a> " : "<a href='#' onclick='lockMessage(".$r['id'].",\"$user\")'><i class='fas fa-lock'></i></a> ")."<a href='#' class='' onclick='deleteMessage(".$r['id'].",\"$user\")'><i class='fas fa-trash'></i></a></span></h4></div>
            <div class='panel-collapse collapse' id='collapse$i'>
                <div class='panel-body'>";
        $concat .= "<p>Sent ".$r['created_date']."</p>";
        $concat .= "<p>".$r['message']."</[p>";
        $concat .= "</div>
            </div>
        </div>";
        $i++;
        $echo .= $concat;
    }
    $echo .= 
    "<script>
        function readMessage(id)
        {
            $.ajax({url: '../ajax/read_messages.php?id='+id, success: function(result){
            }});
        }
        function lockMessage(id,user)
        {
            $.ajax({url: '../ajax/lock_messages.php?id='+id+'&user='+user, success: function(result){
                $('#ajaxmessages').html(result);
                $('#messages').modal();
            }});
        }
        function deleteMessage(id,user)
        {
            $.ajax({url: '../ajax/delete_messages.php?id='+id+'&user='+user, success: function(result){
                $('#ajaxmessages').html(result);
                $('#messages').modal();
            }});
        }
    </script>";
    return $echo;
}
function audit_messages($con,$user,$topic,$message)
{
    $sql = "INSERT INTO audit_messages (username,topic,task,ipaddress) VALUES (\"$user\",'$topic','$message','$_SERVER[REMOTE_ADDR]')";
    mysqli_query($con,$sql);
}
function lock_messages($con,$id)
{
    $sql = "SELECT locked FROM user_messages WHERE id='$id'";
    $r = mysqli_fetch_array(mysqli_query($con,$sql));
    $locked = $r['locked'];
    if($locked == 0)
    {
        $sql = "UPDATE user_messages SET locked = '1' WHERE id='$id'";
        mysqli_query($con,$sql);
    }
    else
    {
        $sql = "UPDATE user_messages SET locked = '0' WHERE id='$id'";
        mysqli_query($con,$sql);
    }
    
}
function lock_all_messages($con,$user,$lock)
{
    $sql ="SELECT ID FROM auth_user WHERE username=\"$user\"";
    $r = mysqli_fetch_array(mysqli_query($con,$sql));
    $aid = $r['ID'];
    $sql = "UPDATE user_messages SET locked='$lock' WHERE a_fk='$aid'";
    mysqli_query($con,$sql);
}
function read_messages($con,$id)
{
    $sql = "UPDATE user_messages SET opened = '1',opened_date=now() WHERE id='$id'";
    mysqli_query($con,$sql);
}
function delete_messages($con,$id,$user)
{
    $sql = "SELECT topic,message,locked FROM user_messages WHERE id='$id'";
    $r = mysqli_fetch_array(mysqli_query($con,$sql));
    $topic = $r['topic'];
    $message = $r['message'];
    $locked = $r['locked'];
    if($locked == 0)
    {
        audit_messages($con,$user,$topic,$message);
        $sql = "DELETE FROM user_messages WHERE id = '$id'";
        mysqli_query($con,$sql);
    }
}
function delete_all_messages($con,$user)
{
    $sql ="SELECT ID FROM auth_user WHERE username=\"$user\"";
    $r = mysqli_fetch_array(mysqli_query($con,$sql));
    $aid = $r['ID'];
    $sql = "SELECT id FROM user_messages WHERE a_fk ='$aid'";
    $result = mysqli_query($con,$sql);
    while($r = mysqli_fetch_array($result))
    {
        $id = $r['id'];
        delete_messages($con,$id,$user);
    }
}
function player_hover_data($con,$pid)
{
    $query = "SELECT username,CONCAT(plast,', ',pfirst) AS pname,CONCAT(pfirst,' ',plast) AS tname,position,position2,build,t.name as team,t.id as tid,num,height,weight,country,p.league,p.active,p.id,p.user_fk,ins,jps,ft,3ps,han,pas,orb,drb,psd,prd,stl,blk,fl,qkn,str,jmp,c90,c801,c802,c803,c804,c70,bank,tpe,p_bank
        FROM auth_user u 
        INNER JOIN players p ON u.ID = p.user_fk 
        INNER JOIN teams t ON p.team=t.id
        WHERE p.id='$pid'";
    $result = mysqli_query($con,$query);
    while($row = mysqli_fetch_array($result))
    {
        $pos2 = $row['position2'];
        if($pos2 != '')
        {
            $pos2 = "/".$pos2;
        }
        //$listarr = Array('Inside Scoring','Jump Shot','Free Throw','Three Point','Handling','Passing','Offensive Rebounding','Defensive Rebounding','Post Defense','Perimeter Defense','Stealing','Blocking','Fouling','Quickness','Strength','Jumping');
        $listarr = Array('INS','JPS','FT','3PS','HAN','PAS','ORB','DRB','PSD','PRD','STL','BLK','FL','QKN','STR','JMP');
        $listarr_short = Array('ins','jps','ft','3ps','han','pas','orb','drb','psd','prd','stl','blk','fl','qkn','str','jmp');
        $listinfo = Array('User','Name','Pos.','Bld','Num','Hgt','Wgt','Ctry');
        $listinfo_short = Array('username','tname','position','build','num','height','weight','country');
        $b = 0;
        $echo = "
        <form action='' class='form-inline'>
            <div class='form-group'>
                ";
                for ($a=0;$a<count($listinfo);$a++)
                {
                    if($listinfo[$a] == 'Position(s)'){$addtl = $pos2;}else{$addtl = '';}
                    $echo .= "<div class='input-group col-sm-4'><span class='input-group-addon' style='width:110px;text-align:left;'><b>".$listinfo[$a]."</b></span><input type='text' class='form-control' readonly='readonly' value=\"".$row[$listinfo_short[$a]].$addtl."\"></div>";
                    $echo .= "<div class='input-group col-sm-4'><span class='input-group-addon' style='width:110px;text-align:left;'><b>".$listarr[$b]."</b></span><input type='text' class='form-control' readonly='readonly' value='".$row[$listarr_short[$b]]."";
                    if($listarr_short[$b] == $row['c90'])
                    {
                        $echo .= "/90'>";
                    }
                    else if($listarr_short[$b] == $row['c70'])
                    {
                        $echo .= "/70'>";
                    }
                    else if($listarr_short[$b] == $row['c801'] || $listarr_short[$b] == $row['c802'] || $listarr_short[$b] == $row['c803'] || $listarr_short[$b] == $row['c804'])
                    {
                        $echo .= "/80'>";
                    }
                    else
                    {
                        $echo .= "/99'>";
                    }
                    $echo .= "</div>";
                    $b++;
                    $echo .= "<div class='input-group col-xs-12 col-sm-4'><span class='input-group-addon' style='width:110px;text-align:left;'><b>".$listarr[$b]."</b></span><input type='text' class='form-control' readonly='readonly' value='".$row[$listarr_short[$b]]."";
                    if($listarr_short[$b] == $row['c90'])
                    {
                        $echo .= "/90'>";
                    }
                    else if($listarr_short[$b] == $row['c70'])
                    {
                        $echo .= "/70'>";
                    }
                    else if($listarr_short[$b] == $row['c801'] || $listarr_short[$b] == $row['c802'] || $listarr_short[$b] == $row['c803'] || $listarr_short[$b] == $row['c804'])
                    {
                        $echo .= "/80'>";
                    }
                    else
                    {
                        $echo .= "/99'>";
                    }
                    $echo .= "</div>";
                    $b++;
                }
                $echo .= "
                <div class='input-group col-xs-12 col-sm-4'><span class='input-group-addon' style='width:110px;text-align:left;'><b>Cash</b></span><input type='text' class='form-control' readonly='readonly' value='$".number_format($row['p_bank'],0)."'></div><div class='input-group col-xs-12 col-sm-4'><span class='input-group-addon' style='width:110px;text-align:left;'><strong>TPE</strong></span><input type='text' class='form-control' readonly='readonly' value='".$row['tpe']."'></div><div class='input-group col-xs-12 col-sm-4'><span class='input-group-addon' style='width:110px;text-align:left;'><strong>Banked</strong></span><input type='text' class='form-control' readonly='readonly' value='".$row['bank']."'></div>
            </div>
        </form>";
    }
    return $echo;
}
function cap_notification($con,$pid)
{
    global $sublg;
    $sql = "SELECT plast,pfirst FROM players WHERE id='$pid'";
    $result = mysqli_query($con,$sql);
    while($s = mysqli_fetch_array($result))
    {
        $plast = $s['plast'];
        $pfirst = $s['pfirst'];
    }
    $sql = "SELECT u.ID,u.username FROM auth_user u WHERE cap_sub = '1'";
    $result = mysqli_query($con,$sql);
    while($r = mysqli_fetch_array($result))
    {
        $aid = $r['ID'];
        $name = $r['name'];
        $topic = "".$pfirst." ".$plast." has entered the league as a new player!";
        $message = "<p>Hey ".$r['name'].", ".$pfirst." ".$plast." is a newly created ".$sublg." player and is available to be recruited. Hurry now, or lose a chance to land the next big star!</p><p>Check out their player page <a href=\"/forms/player_page.php?pid=".$pid."\">here</a>!</p>";
        $sql = "INSERT INTO user_messages (a_fk,topic,message) VALUES ('$aid','$topic','$message')";
        mysqli_query($con,$sql);
    }
    $sql = "SELECT u.ID,u.username FROM auth_user u WHERE cap_sub_pm = '1'";
    $result = mysqli_query($con,$sql);
    while($r = mysqli_fetch_array($result))
    {
        $aid = $r['ID'];
        $to = array();
        $tosql = "
        SELECT memberid 
        FROM auth_user a
        WHERE a.ID = '$aid' ";
        $toresult = mysqli_query($con,$tosql);
        while($torow = mysqli_fetch_array($toresult))
        {
            array_push($to,$torow['memberid']);
        }
        $curl_post_data = array(
            'from' => '2574',
            'to' => $to,
            'title' => "".$pfirst." ".$plast." has entered the league as a new player!",
            'body' => "<p>Hey ".$r['username'].", ".$pfirst." ".$plast." is a newly created ".$sublg." player and is available to be recruited. Hurry now, or lose a chance to land the next big star!</p><p>Check out their player page <a href=\"/forms/player_page.php?pid=".$pid."\">here</a>!</p>"
        );
        createMessage($curl_post_data);
    }
}
function prediction_submission_notification($con,$user,$type)
{
    $yr = "SELECT year from year WHERE isactive='1'";
    $yrq=mysqli_fetch_array(mysqli_query($con,$yr));
    $year=$yrq['year'];
    $sql = "SELECT u.ID FROM auth_user u WHERE username=\"$user\"";
    $result = mysqli_query($con,$sql);
    while($r = mysqli_fetch_array($result))
    {
        $aid = $r['ID'];
        $topic = $type." Predictions Submitted!";
        $message = "<p>Hey ".$user.",<br><br>Your S".$year." ".$type." Predictions have been submitted.</p>";
        $sql = "INSERT INTO user_messages (a_fk,topic,message) VALUES ('$aid','$topic','$message')";
        mysqli_query($con,$sql);
    }
}
function game_submission_notification($con,$user,$type)
{
    $yr = "SELECT year from year WHERE isactive='1'";
    $yrq=mysqli_fetch_array(mysqli_query($con,$yr));
    $year=$yrq['year'];
    $sql = "SELECT u.ID FROM auth_user u WHERE username=\"$user\"";
    $result = mysqli_query($con,$sql);
    while($r = mysqli_fetch_array($result))
    {
        $aid = $r['ID'];
        $topic = $type." Submitted!";
        $message = "<p>Hey ".$user.",<br><br>Your S".$year." ".$type." team has been submitted.</p>";
        $sql = "INSERT INTO user_messages (a_fk,topic,message) VALUES ('$aid','$topic','$message')";
        mysqli_query($con,$sql);
    }
}
function get_team_dropdown($con,$league)
{
    $league = strtolower($league);
    $table = '';
    if($league == 'sba')
    {
        $table = "SBA_Team";
    }
    else if($league == 'ncaa')
    {
        $table = "NCAA_Team";
    }
    else if($league == 'fiba')
    {
        $table = "FIBA_Team";
    }
    else
    {
        $table = "SBA_Team";
    }
    $sql = "SELECT ID, CONCAT(CityName,' ',Name) as team from $table";
    $result = mysqli_query($con,$sql);
    $echo = '';
    while ($r = mysqli_fetch_array($result)) 
    {
        $echo .= "<option value = '".$r['ID']."'>".$r['team']."</option>";
    }
    return $echo;
}
function Subtract($current,$regress)
{
    $a = $current;
    $b = $regress;
    $loss = 0;
    while($a > $b)
    {
        if($a>95)
        {
            $loss += 12;
        }
        else if($a>90)
        {
            $loss += 7;
        }
        else if($a>85)
        {
            $loss += 5;
        }
        else if($a>80)
        {
            $loss += 3;
        }
        else if($a>70)
        {
            $loss += 2;
        }
        else 
        {
            $loss += 1;
        }
        $a--;
    }
    return $loss;
}
function get_player_spend($con,$pid)
{
    global $lgatty;
    global $hostlg;
    // get num
    $sql = "SELECT * FROM player_updates pu WHERE pu.pid_fk='$pid' AND (status < 2)";
    $numsult = mysqli_query($con,$sql);
    $num = mysqli_num_rows($numsult);

    //Spend Points
    if($num == 0)
    {
        $query = "SELECT * FROM players p WHERE p.id = '$pid'";
        $r = mysqli_fetch_array(mysqli_query($con,$query));
        $s = $r;
        if($hostlg == 'SBA'){$tpstoggle = $r['3ps_toggle'];}
        $ptpe = $r['tpe'];
        $petspend = $r['bank'];
        $totalpe = $ptpe;
    }
    else
    {
        $puid = mysqli_fetch_array($numsult);
        $puid = $puid['id'];
        if($hostlg == 'SBA')
        {
            $query = "SELECT p.c90,p.c801,p.c802,p.c803,p.c804,p.c70,puu.ins,puu.jps,puu.ft,puu.3ps,puu.han,puu.pas,puu.orb,puu.drb,puu.psd,puu.prd,puu.stl,puu.blk,puu.fl,puu.qkn,puu.str,puu.jmp,p.bank,puu.3ps_toggle,pu.earn_tpe,p.tpe,p_bank,p.bank FROM players p 
            INNER JOIN player_updates pu ON p.ID=pu.pid_fk 
            INNER JOIN player_updates_update puu ON pu.id = puu.puid_fk
            WHERE puu.puid_fk = '$puid'";
        }
        else if($hostlg == 'EFL')
        {
            $query = "SELECT p.c90,p.c801,p.c802,p.c803,p.c804,p.c70,puu.str,puu.agi,puu.arm,puu.`int`,puu.acc,puu.tkl,puu.spd,puu.han,puu.rbk,puu.pbk,puu.kdi,puu.kac,p.bank,pu.earn_tpe,p.tpe,p_bank,p.bank FROM players p 
            INNER JOIN player_updates pu ON p.ID=pu.pid_fk 
            INNER JOIN player_updates_update puu ON pu.id = puu.puid_fk
            WHERE puu.puid_fk = '$puid'";
        }
        $r = mysqli_fetch_array(mysqli_query($con,$query));
        if($hostlg == 'SBA'){$tpstoggle = $r['3ps_toggle'];}
        $ptpe = $r['tpe'];
        $petpe = $r['earn_tpe'];
        $petbank = $r['bank'];
        $petspend = $petpe + $petbank;
        $totalpe = $ptpe + $petpe;
        if($hostlg == 'SBA')
        {
            $sql = "SELECT p.ins,p.jps,p.ft,p.3ps,p.han,p.pas,p.orb,p.drb,p.psd,p.prd,p.stl,p.blk,p.fl,p.qkn,p.str,p.jmp FROM players p INNER JOIN player_updates pu ON p.ID=pu.pid_fk INNER JOIN player_updates_update uu ON pu.id=uu.puid_fk WHERE puid_fk='$puid'";
        }
        else if($hostlg == 'EFL')
        {
            $sql = "SELECT p.str,p.agi,p.arm,p.`int`,p.acc,p.tkl,p.spd,p.han,p.rbk,p.pbk,p.kdi,p.kac FROM players p INNER JOIN player_updates pu ON p.ID=pu.pid_fk INNER JOIN player_updates_update uu ON pu.id=uu.puid_fk WHERE puid_fk='$puid'";
        }
        $s = mysqli_fetch_array(mysqli_query($con,$sql));
    }

    $spend = '';
    for($v=0;$v<count($lgatty);$v++)
    {
        if($r['c90'] == $lgatty[$v])
        {
            $max = " max='90'";
            $max2 = "90";
        }
        else if($r['c801'] == $lgatty[$v] || $r['c802'] == $lgatty[$v] || $r['c803'] == $lgatty[$v] || $r['c804'] == $lgatty[$v])
        {
            $max = " max='80'";
            $max2 = "80";
        }
        else if($r['c70'] == $lgatty[$v])
        {
            $max = " max='70'";
            $max2 = "70";
        }
        else
        {
            $max = " max='99'";
            $max2 = "99";
        }
        $change = $r[$lgatty[$v]] - $s[$lgatty[$v]];
        $spend .= "
            <tr>
                <td><div class='hidden-xs vamid text-left'>".strtoupper($lgatty[$v]).":</div><div class='hidden-sm hidden-md hidden-lg vamid'>".strtoupper($lgatty[$v])."</div></td>
                <td class=\"vamid\"><input type='text' class='cb' id='curr_".$lgatty[$v]."' name='curr_".$lgatty[$v]."' value='".$r[$lgatty[$v]]."' min='".$s[$lgatty[$v]]."'".$max." size='3' maxlength='2' onChange=\"calcManualCost('curr_".$lgatty[$v]."','increase_".$lgatty[$v]."','cost_".$lgatty[$v]."');calc();\"></td>
                <td class=\"vamid\">".$max2."</td>
                <td><button type='button' class='btn btn-primary' name='calculate' onClick=\"subtract('curr_".$lgatty[$v]."','increase_".$lgatty[$v]."','cost_".$lgatty[$v]."');calc();\">-1</button><button type='button' class='btn btn-primary hidden-xs' name='calculate' onClick=\"subtractIncrement('curr_".$lgatty[$v]."','increase_".$lgatty[$v]."','cost_".$lgatty[$v]."','5');calc();\">-5</button><button type='button' class='btn btn-primary hidden-xs' name='calculate' onClick=\"subtractIncrement('curr_".$lgatty[$v]."','increase_".$lgatty[$v]."','cost_".$lgatty[$v]."','10');calc();\">-10</button></td>
                <td><button type='button' class='btn btn-primary' name='calculate' onClick=\"add('curr_".$lgatty[$v]."','increase_".$lgatty[$v]."','cost_".$lgatty[$v]."');calc();\">+1</button><button type='button' class='btn btn-primary hidden-xs' name='calculate' onClick=\"addIncrement('curr_".$lgatty[$v]."','increase_".$lgatty[$v]."','cost_".$lgatty[$v]."','5');calc();\">+5</button><button type='button' class='btn btn-primary hidden-xs' name='calculate' onClick=\"addIncrement('curr_".$lgatty[$v]."','increase_".$lgatty[$v]."','cost_".$lgatty[$v]."','10');calc();\">+10</button></td>
                <td><p class='form-control-static' id='increase_".$lgatty[$v]."'>".$change."</p></td>
                <td><p class='form-control-static' id='cost_".$lgatty[$v]."' name='cost_".$lgatty[$v]."'></p></td>
                <script>
                $(document).ready(function(){
                    calcCost('curr_".$lgatty[$v]."','increase_".$lgatty[$v]."','cost_".$lgatty[$v]."');
                    calc();
                });
                </script>
            </tr>";
    }
    $echo = "<div class=\"table-responsive\">
                <table style=\"margin: 0 auto;\" id=\"tpetable\" class=\"table table-condensed\">
                    <thead>
                        <tr>
                            <th class=\"hidden-xs\">Attribute Name</th>
                            <th class=\"hidden-sm hidden-md hidden-lg\">ATT</th>
                            <th class=\"hidden-xs\">Rating</th>
                            <th class=\"hidden-sm hidden-md hidden-lg\">RTG</th>
                            <th class=\"hidden-xs\">Max</th>
                            <th class=\"hidden-sm hidden-md hidden-lg\">MAX</th>
                            <th></th>
                            <th></th>
                            <th class=\"hidden-xs\">Change</th>
                            <th class=\"hidden-sm hidden-md hidden-lg\">CHG</th>
                            <th class=\"hidden-xs\">TPE Cost</th>
                            <th class=\"hidden-sm hidden-md hidden-lg\">TPE</th>
                        </tr>
                    </thead>
                    <tbody>
                        ".$spend."
                    </tbody>
                </table>
            </div>
            <br>
            ".(($hostlg == 'SBA') ? "<div class=\"input-group col-xs-12 col-sm-4\"><span class='input-group-addon' style='width:210px;text-align:left;'>Use 3PT Rating</span><input type=\"checkbox\" ".(($tpstoggle == '1') ? "checked" : "")." data-toggle=\"toggle\" data-on=\"Yes\" data-off=\"No\" name=\"tpstoggle\" id=\"tpstoggle\"></div>
            <script>
                $('#tpstoggle').bootstrapToggle();
            </script>" : "")."
            <br>
            <input id='ptpe' type='hidden' class='form-control' name='ptpe' readonly='readonly' value=\"".$ptpe."\">
            <div class='input-group col-xs-12 col-sm-4'><span class='input-group-addon' style='width:130px;text-align:left;'>TPE to Spend</span><input id='spend_tpe' type='text' class='form-control' name='spend_tpe' readonly='readonly' value=\"".$petspend."\"></div>
            <div class='input-group col-xs-12 col-sm-4'><span class='input-group-addon' style='width:130px;text-align:left;'>TPE Needed</span><input id='cost_tpe' type='text' class='form-control' name='cost_tpe' readonly='readonly' value='0'></div>
            <br>";
    return $echo;
}
function get_player_update($con,$pid,$user)
{
    global $hostlg;
    // get player name and tpe (replace with function)
    $sql = "SELECT CONCAT(pfirst,' ',plast) as pname,tpe FROM players WHERE id='$pid'";
    $nme=mysqli_fetch_array(mysqli_query($con,$sql));
    $pname=$nme['pname'];
    // check if there is a submitted update
    $sql = "SELECT pu.id as puid,notes FROM player_updates pu INNER JOIN player_updates_tasks put ON pu.id=put.puid_fk WHERE pu.pid_fk='$pid' AND (status < 2)";
    $numsult = mysqli_query($con,$sql);
    $num = mysqli_num_rows($numsult);
    $taskdupewholeconcat = "";
    // generate submitted table
    if($num > 0)
    {
        $r = mysqli_fetch_array($numsult);
        $puid = $r['puid'];
        $notes = $r['notes'];
        $concat = "
        <div class='table-responsive'>
        <table class='table table-bordered'>
            <thead>
                <tr>
                    <th>Task</th>
                    <th>Week Ending</th>
                    <th class='hidden-xs'>Points Earned</th>
                    <th class='text-center hidden-sm hidden-md hidden-lg'>P.E.</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>";
        $query = "SELECT task,week,link,pe,puid_fk,id FROM player_updates_tasks put WHERE puid_fk = '$puid'";
        $result2 = mysqli_query($con,$query);
        $x = 0;
        while($s = mysqli_fetch_array($result2))
        {
            $sql = "SELECT p.id,CONCAT(pfirst,' ',plast) as pname FROM auth_user a INNER JOIN players p ON a.ID=p.user_fk WHERE a.username = \"$user\" AND (p.active='1' || p.active='3') AND p.id != '$pid'";
            $resultdupe = mysqli_query($con,$sql);
            $taskdupeconcat = "";
            $urllink = str_replace("#","%23",$s['link']);
            $urllink = str_replace("&","%26",$urllink);
            while($t = mysqli_fetch_array($resultdupe))
            {
                $taskdupeconcat .= "<a href='task_dupe.php?pid=".$t['id']."&task=".$s['task']."&week=".$s['week']."&link=".$urllink."&pe=".$s['pe']."&pid2=".$pid."'><button type='button' class='btn btn-primary'>Duplicate to ".$t['pname']."</button></a>";
            }
            $concat .= "
                <tr>
                    <td class=\"vamid\"><a href='".$s['link']."' style='text-decoration:underline;' target='_blank'>".$s['task']."</a></td>
                    <td class=\"vamid\">".$s['week']."</td>
                    <td class=\"vamid\">".$s['pe']."</td>
                    <td class='col-xs-1'><a href='#taskdupe$x' class='btn btn-primary' data-toggle='modal'><span title='' data-toggle='popover' data-trigger='hover' data-content='Duplicate Task' data-placement='top'><i class='fas fa-file-import'></i></span></a> <a href='#taskdelete$x' class='btn btn-primary' data-toggle='modal'><span title='' data-toggle='popover' data-trigger='hover' data-content='Delete Task' data-placement='top'><i class='fas fa-trash'></i></span></a></td>
                    <div class='modal fade' id='taskdupe$x' role='dialog'>
                        <div class='modal-dialog modal-lg'>
                            <div class='modal-content'>
                                <form method='POST' action='task_dupe.php'>
                                    <div class='modal-header'>
                                        <button type='button' class='close' data-dismiss='modal'>&times;</button>
                                        <h4 class='modal-title'>Duplicate Task</h4>
                                    </div>
                                    <div class='modal-body'>
                                        
                                    </div>
                                    <div class='modal-footer'>
                                        <a href='task_dupe.php?pid=".$pid."&task=".$s['task']."&week=".$s['week']."&link=".$urllink."&pe=".$s['pe']."&pid2=".$pid."'><button type='button' class='btn btn-primary'>Duplicate to ".$pname."</button></a>
                                        ".$taskdupeconcat."
                                        <button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class='modal fade' id='taskdelete$x' role='dialog'>
                        <div class='modal-dialog modal-lg'>
                            <div class='modal-content'>
                                <form method='POST' action='task_delete.php'>
                                    <div class='modal-header'>
                                        <button type='button' class='close' data-dismiss='modal'>&times;</button>
                                        <h4 class='modal-title'>Delete Task</h4>
                                    </div>
                                    <div class='modal-body'>
                                        
                                    </div>
                                    <div class='modal-footer'>
                                        <a href='task_delete.php?putid=".$s['id']."&putpe=".$s['pe']."&puid=".$s['puid_fk']."&pid=".$pid."'><button type='button' class='btn btn-primary'>Delete Task</button></a>
                                        <button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </tr>
            ";
            $x++;
        }
        $concat .= "
            </tbody>
        </table></div><br>";
        if($hostlg == 'SBA')
        {
            $sql = "SELECT 3ps_toggle FROM player_updates_update WHERE puid_fk='$puid'";
            $d = mysqli_fetch_array(mysqli_query($con,$sql));
            $tpstoggle = $d['3ps_toggle'];
            if($tpstoggle == '1')
            {
                $tpsins = '';
            }
            else
            {
                $tpsins = " Please ensure this player's 3PT is set to 0.";
            }
        }
        else
        {
            $tpsins = '';
        }
        $concat .= "<div class='input-group col-xs-12'><span class='input-group-addon' style='width:120px;text-align:left;'>Notes</span><input type='text' class='form-control' readonly='readonly' value=\"".$notes." ".$tpsins."\"></div><br>";
        // Duplicate Whole Update
        while($t = mysqli_fetch_array($resultdupe))
            {
                $taskdupewholeconcat .= "<a href='task_dupe.php?pid=".$t['id']."&puid=".$puid."&type=2'><button type='button' class='btn btn-primary'>Duplicate Update to ".$t['pname']."</button></a>";
            }
        $concat .= "
        <a href='#updatedupe' class='btn btn-primary col-xs-4' data-toggle='modal'><span title='' data-toggle='popover' data-trigger='hover' data-content='Duplicate Update' data-placement='top'></span>Duplicate All</a><br><br>
        <div class='modal fade' id='updatedupe' role='dialog'>
            <div class='modal-dialog modal-lg'>
                <div class='modal-content'>
                    <form method='POST' action='task_dupe.php'>
                        <div class='modal-header'>
                            <button type='button' class='close' data-dismiss='modal'>&times;</button>
                            <h4 class='modal-title'>Duplicate Update</h4>
                        </div>
                        <div class='modal-body'>
                            
                        </div>
                        <div class='modal-footer'>
                            <a href='task_dupe.php?pid=".$pid."&puid=".$puid."&type=2'><button type='button' class='btn btn-primary'>Duplicate Update to ".$pname."</button></a>
                            ".$taskdupewholeconcat."
                            <button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>";
    }
    else
    {
        $concat = "<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>You do not have any outstanding updates waiting to be approved.</strong></div>";
    }
    return $concat;
}
function get_wallet($con,$pid)
{
    // get goodweek
    $goodweek = date("m/d/Y", strtotime('Sunday'));
    // get num
    $sql = "SELECT * FROM player_updates pu INNER JOIN player_updates_tasks put ON pu.id=put.puid_fk WHERE pu.pid_fk='$pid' AND (status < 2)";
    $numsult = mysqli_query($con,$sql);
    $num = mysqli_num_rows($numsult);

    if($num == 0)
    {
        $query = "SELECT * FROM players p WHERE p.id = '$pid'";
        $r = mysqli_fetch_array(mysqli_query($con,$query));
        $s = $r;
        $tpstoggle = $r['3ps_toggle'];
        $ptpe = $r['tpe'];
        $totalpe = $ptpe;
    }
    else
    {
        $puid = mysqli_fetch_array($numsult);
        $puid = $puid['puid_fk'];
        $query = "SELECT p.c90,p.c801,p.c802,p.c803,p.c804,p.c70,puu.ins,puu.jps,puu.ft,puu.3ps,puu.han,puu.pas,puu.orb,puu.drb,puu.psd,puu.prd,puu.stl,puu.blk,puu.fl,puu.qkn,puu.str,puu.jmp,p.bank,puu.3ps_toggle,pu.earn_tpe,p.tpe,p_bank FROM players p 
            INNER JOIN player_updates pu ON p.ID=pu.pid_fk 
            INNER JOIN player_updates_update puu ON pu.id = puu.puid_fk
        WHERE puu.puid_fk = '$puid'";
        $r = mysqli_fetch_array(mysqli_query($con,$query));
        $tpstoggle = $r['3ps_toggle'];
        $ptpe = $r['tpe'];
        $petpe = $r['earn_tpe'];
        $totalpe = $ptpe + $petpe;
        $sql = "SELECT p.ins,p.jps,p.ft,p.3ps,p.han,p.pas,p.orb,p.drb,p.psd,p.prd,p.stl,p.blk,p.fl,p.qkn,p.str,p.jmp FROM players p INNER JOIN player_updates pu ON p.ID=pu.pid_fk INNER JOIN player_updates_update uu ON pu.id=uu.puid_fk WHERE puid_fk='$puid'";
        $s = mysqli_fetch_array(mysqli_query($con,$sql));
    }
    $echo = "
    <div class='input-group'><span class='input-group-addon' style='width:120px;text-align:left;'>Banked TPE</span><input id='bank_tpe' type='text' class='form-control' name='bank_tpe' readonly='readonly' value='".$r['bank']."'></div>
    <div class='input-group'><span class='input-group-addon' style='width:120px;text-align:left;'>Earned TPE</span><input id='earned_tpe' type='text' class='form-control' name='earned_tpe' readonly='readonly' value='0'></div>
    <div class='input-group'><span class='input-group-addon' style='width:120px;text-align:left;'>TPE to Spend</span><input id='spend_tpe' type='text' class='form-control' name='spend_tpe' readonly='readonly' value='0'></div>
    <div class='input-group'><span class='input-group-addon' style='width:120px;text-align:left;'>Total TPE</span><input id='total_tpe' type='text' class='form-control' name='total_tpe' readonly='readonly' min='".$ptpe."' value='0'></div>
    <div><input type='hidden' id='ptpe' value='".$ptpe."'></div>
    <div><input type='hidden' id='weektpe' value='".$goodweek."'></div>";
    return $echo;
}
function get_prediction($con,$pid)
{
    // get year
    $yr = "SELECT year from year WHERE isactive='1'";
    $yrq=mysqli_fetch_array(mysqli_query($con,$yr));
    $year=$yrq['year'];
    $preyear = $year - 1; 
    $gecho = '';
    $sql = "SELECT id,type,pe FROM player_prediction WHERE p_fk='$pid' AND season='$preyear' AND claimed = '0'";
    $result = mysqli_query($con,$sql);
    $num = mysqli_num_rows($result);
    if($num > 0)
    {  
        while($s = mysqli_fetch_array($result))
        {
            $gid = $s['id'];
            if($s['type'] == 'season')
            {
                $type = 'SBA Season';
                $typenum = 0;
            }
            else if($s['type'] == 'sbdl_season')
            {
                $type = 'SBDL Season';
                $typenum = 2;
            }
            $gecho .= "<button class='btn btn-primary' onclick=\"claimPrediction('".$gid."','".$pid."')\">Claim S".$preyear." ".$type." Predictions for ".$s['pe']." TPE</button><br>";
        }
        $gecho .="
        <script>
            function claimPrediction(gid,pid)
            {
                $.ajax({url: '../ajax/claim_prediction.php?gid='+gid+'&pid='+pid, success: function(result){
                    $('#ajaxprediction').html(result);
                    $('#predictionmodal').modal();
                }});
                $.ajax({url: \"../ajax/get_player_spend.php?pid=\"+pid, success: function(result){
                    $(\"#ajaxupdate\").html(result);
                }});
            }
        </script>";
    }
    else
    {
        $gecho .= "<p>There are either no predictions currently, or you have already claimed all eligible predictions. Please check back later.</p>";
    }
    return $gecho;
}
function claim_prediction($con,$gid)
{
    $sql = "SELECT p_fk,pe,type FROM player_prediction WHERE id='$gid'";
    $row = mysqli_fetch_array(mysqli_query($con,$sql));
    $pid = $row['p_fk'];
    $pe = $row['pe'];
    $type = $row['type'];
    $sql = "UPDATE player_prediction SET claimed = '1' WHERE id='$gid'";
    mysqli_query($con,$sql);
    $sql = "UPDATE players SET tpe=tpe+'$pe',ape=ape+'$pe',bank=bank+'$pe' WHERE id='$pid'";
    mysqli_query($con,$sql);
    // Player Update Record
    $sql = "INSERT INTO player_updates (pid_fk,earn_tpe,cost_tpe,status,approver,approved,cleared) VALUES ('$pid','$pe','0','3','Skynet',NOW(),NOW())";
    mysqli_query($con,$sql);
    $puid = mysqli_insert_id($con);
    $week = date("m/d/Y", strtotime('Sunday'));
    $sql = "INSERT INTO player_updates_tasks (puid_fk,task,week,link,pe) VALUES ('$puid','Predictions - ".$type."','$week','','$pe')";
    mysqli_query($con,$sql);
    $sql2 = "SELECT * FROM players WHERE id='$pid'";
    $s = mysqli_fetch_array(mysqli_query($con,$sql2));
    $ins=$s['ins'];
    $jps=$s['jps'];
    $ft=$s['ft'];
    $tps=$s['3ps'];
    $han=$s['han'];
    $pas=$s['pas'];
    $orb=$s['orb'];
    $drb=$s['drb'];
    $psd=$s['psd'];
    $prd=$s['prd'];
    $stl=$s['stl'];
    $blk=$s['blk'];
    $fl=$s['fl'];
    $qkn=$s['qkn'];
    $str=$s['str'];
    $jmp=$s['jmp'];
    $tpstoggle=$s['3ps_toggle'];
    $sql = "INSERT INTO player_updates_update (puid_fk,ins,jps,ft,3ps,han,pas,orb,drb,psd,prd,stl,blk,fl,qkn,str,jmp,ins_old,jps_old,ft_old,3ps_old,han_old,pas_old,orb_old,drb_old,psd_old,prd_old,stl_old,blk_old,fl_old,qkn_old,str_old,jmp_old,3ps_toggle) VALUES ('$puid','$ins','$jps','$ft','$tps','$han','$pas','$orb','$drb','$psd','$prd','$stl','$blk','$fl','$qkn','$str','$jmp','$ins','$jps','$ft','$tps','$han','$pas','$orb','$drb','$psd','$prd','$stl','$blk','$fl','$qkn','$str','$jmp','$tpstoggle')";
    mysqli_query($con,$sql);
}
function get_giveaway($con,$pid)
{
    $gecho = '';
    $today = date("m/d/Y");
    $sql = "SELECT id,task,pe FROM giveaways WHERE '$today' between beg_date AND end_date";
    $numsult = mysqli_query($con,$sql);
    $num = mysqli_num_rows($numsult);
    if($num > 0)
    {
        while($w = mysqli_fetch_array($numsult))
        {
            $gid = $w['id'];
            $gtask = $w['task'];
            $gpe = $w['pe'];
            $sql = "SELECT id FROM player_giveaway WHERE p_fk = '$pid' AND g_fk = '$gid'";
            $numsult2 = mysqli_query($con,$sql);
            $num2 = mysqli_num_rows($numsult2);
            if($num2 == 0)
            {

                $gecho .= "<button class='btn btn-primary' onclick=\"claimGiveaway('".$gid."','".$pid."')\">Claim ".$gtask." for ".$gpe." TPE</button><br>";
            }
        }
        if($gecho != '')
        {
            $gecho .="
            <script>
                function claimGiveaway(gid,pid)
                {
                    $.ajax({url: '../ajax/claim_giveaway.php?gid='+gid+'&pid='+pid, success: function(result){
                        $('#ajaxgiveaway').html(result);
                        $('#giveawaymodal').modal();
                    }});
                    $.ajax({url: \"../ajax/get_player_spend.php?pid=\"+pid, success: function(result){
                        $(\"#ajaxupdate\").html(result);
                    }});
                }
            </script>";
        }
        else
        {
            $gecho .= "<p>There are either no giveaways currently, or you have already claimed all eligible giveaways. Please check back later.</p>";
        }
    }
    return $gecho;
}
function claim_giveaway($con,$gid,$pid)
{
    $sql = "SELECT id FROM player_giveaway WHERE g_fk='$gid' AND p_fk='$pid'";
    $result = mysqli_query($con,$sql);
    $num = mysqli_num_rows($result);
    if($num > 0)
    {

    }
    else
    {
        $sql = "INSERT INTO player_giveaway (g_fk,p_fk) VALUES ('$gid','$pid')";
        mysqli_query($con,$sql);
        $sql = "SELECT task,pe FROM giveaways WHERE id='$gid'";
        $r = mysqli_fetch_array(mysqli_query($con,$sql));
        $pe = $r['pe'];
        $gtask = $r['task'];
        $sql = "UPDATE players SET tpe=tpe+'$pe',ape=ape+'$pe',bank=bank+'$pe' WHERE id='$pid'";
        mysqli_query($con,$sql);
        // Player Update Record
        $sql = "INSERT INTO player_updates (pid_fk,earn_tpe,cost_tpe,status,approver,approved,cleared) VALUES ('$pid','$pe','0','3','Skynet',NOW(),NOW())";
        mysqli_query($con,$sql);
        $puid = mysqli_insert_id($con);
        $week = date("m/d/Y", strtotime('Sunday'));
        $sql = "INSERT INTO player_updates_tasks (puid_fk,task,week,link,pe) VALUES ('$puid','Giveaway - ".$gtask."','$week','','$pe')";
        mysqli_query($con,$sql);
        $sql2 = "SELECT * FROM players WHERE id='$pid'";
        $s = mysqli_fetch_array(mysqli_query($con,$sql2));
        $ins=$s['ins'];
        $jps=$s['jps'];
        $ft=$s['ft'];
        $tps=$s['3ps'];
        $han=$s['han'];
        $pas=$s['pas'];
        $orb=$s['orb'];
        $drb=$s['drb'];
        $psd=$s['psd'];
        $prd=$s['prd'];
        $stl=$s['stl'];
        $blk=$s['blk'];
        $fl=$s['fl'];
        $qkn=$s['qkn'];
        $str=$s['str'];
        $jmp=$s['jmp'];
        $tpstoggle=$s['3ps_toggle'];
        $sql = "INSERT INTO player_updates_update (puid_fk,ins,jps,ft,3ps,han,pas,orb,drb,psd,prd,stl,blk,fl,qkn,str,jmp,ins_old,jps_old,ft_old,3ps_old,han_old,pas_old,orb_old,drb_old,psd_old,prd_old,stl_old,blk_old,fl_old,qkn_old,str_old,jmp_old,3ps_toggle) VALUES ('$puid','$ins','$jps','$ft','$tps','$han','$pas','$orb','$drb','$psd','$prd','$stl','$blk','$fl','$qkn','$str','$jmp','$ins','$jps','$ft','$tps','$han','$pas','$orb','$drb','$psd','$prd','$stl','$blk','$fl','$qkn','$str','$jmp','$tpstoggle')";
        mysqli_query($con,$sql);
    }    
}
function time_Ago($time) 
{ 
    // Calculate difference between current 
    // time and given timestamp in seconds 
    $diff     = time() - $time; 
    // Time difference in seconds 
    $sec     = $diff; 
    // Convert time difference in minutes 
    $min     = round($diff / 60 ); 
    // Convert time difference in hours 
    $hrs     = round($diff / 3600); 
    // Convert time difference in days 
    $days     = round($diff / 86400 ); 
    // Convert time difference in weeks 
    $weeks     = round($diff / 604800); 
    // Convert time difference in months 
    $mnths     = round($diff / 2600640 ); 
    // Convert time difference in years 
    $yrs     = round($diff / 31207680 ); 
    // Check for seconds 
    if($sec <= 60) 
    { 
       $ago = '</h5><h5 class="sim-date"><small>' . $sec . ' seconds ago</small></h5></div>'; 
    } 
    // Check for minutes 
    else if($min <= 60) 
    { 
        if($min==1) 
        { 
           $ago = '</h5><h5 class="sim-date"><small>one minute ago</small></h5></div>'; 
        } 
        else 
        { 
           $ago = '</h5><h5 class="sim-date"><small>' . $min . ' minutes ago</small></h5></div>'; 
        } 
    } 
    // Check for hours 
    else if($hrs <= 24) 
    { 
        if($hrs == 1) 
        {  
           $ago = '</h5><h5 class="sim-date"><small>an hour ago</small></h5></div>'; 
        } 
        else 
        { 
           $ago = '</h5><h5 class="sim-date"><small>' . $hrs . ' hours ago</small></h5></div>'; 
        } 
    } 
    // Check for days 
    else if($days <= 7) 
    { 
        if($days == 1) 
        { 
           $ago = '</h5><h5 class="sim-date"><small>Yesterday</small></h5></div>'; 
        } 
        else 
        { 
           $ago = '</h5><h5 class="sim-date"><small>' . $days . ' days ago</small></h5></div>'; 
        } 
    } 
    // Check for weeks 
    else if($weeks <= 4.3) 
    { 
        if($weeks == 1) 
        { 
           $ago = '</h5><h5 class="sim-date"><small>a week ago</small></h5></div>'; 
        } 
        else 
        { 
            $ago = '</h5><h5 class="sim-date"><small>' . $weeks . ' weeks ago</small></h5></div>'; 
        } 
    } 
    // Check for months 
    else if($mnths <= 12) 
    { 
        if($mnths == 1) 
        { 
            $ago = '</h5><h5 class="sim-date"><small>a month ago</small></h5></div>'; 
        } 
        else 
        { 
            $ago = '</h5><h5 class="sim-date"><small>' . $mnths . ' months ago</small></h5></div>'; 
        } 
    } 
    // Check for years 
    else 
    { 
        if($yrs == 1) 
        { 
            $ago = '</h5><h5 class="sim-date"><small>a year ago</small></h5></div>'; 
        } 
        else 
        { 
            $ago = '</h5><h5 class="sim-date"><small>' . $yrs . ' years ago</small></h5></div>'; 
        } 
    }
    return $ago;
} 
function export_sequencing($league)
{
    global $conn;
    $eid = 2;
    $stmt = $conn->prepare("UPDATE players SET export_id = '0' WHERE active = '0'");
    $stmt->execute();
    $stmt = $conn->prepare("SELECT id FROM players WHERE league = :league AND active > '0' ORDER BY id");
    $stmt->execute([':league' => $league]);
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    foreach ($result as $row)
    {
        $stmt = $conn->prepare("UPDATE players SET export_id = :eid WHERE id=:id");
        $stmt->execute([':id' => $row['id'], ':eid' => $eid]);
        $eid++;
    }
}

?>