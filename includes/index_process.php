<?php
function LogoImagePrint($league,$color) 
{
    if($league == 'EFL')
    {
        $rs="/images/eflfull.png";
        return "<center><img src='$rs' style='height:100px;max-width:100%;object-fit:contain;'></center>";
    }
    else
    {
        $rs = "";
        switch ($color):
            case "dark":
                $rs="/images/sbaofullw.png"; break;
            case "light":                 
                $rs="/images/sbaofullb.png"; break;
            default:
                $rs="/images/sbaofullb.png"; break;
        endswitch;
        return "<center><img src='$rs' style='height:50px;width:200px;max-width:100%;object-fit:contain;'></center>";
    }
}
function HotlinePrint($league) 
{
    if($league == 'EFL')
    {
        $retstr = "<p>&copy; Copyright ".date('Y').".  All Rights Reserved.</p><p><b>Need help? Join our Discord Server <a href='https://discord.gg/f44Wm35' target='_blank'>here</a> or visit our Forums <a href='https://efl.network' target='_blank'>here</a>!</b></p>";
        return "<div class='footer' width='90%'><center>$retstr</center></div>";
    }
    else
    {
        $retstr = "<p>&copy; Copyright ".date('Y').".  All Rights Reserved.</p><p><b>Need help? Join our Discord Server <a href='https://discord.gg/jydHBg2' target='_blank'>here</a> or visit our Forums <a href='https://sba.today/forums/index.php' target='_blank'>here</a>!</b></p>";
        return "<div class='footer' width='90%'><center>$retstr</center></div>";
    }

}
?>