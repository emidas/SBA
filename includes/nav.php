<?php
$mid = $_SESSION['memberid'];
if(empty($_SESSION['style']))
{
    $navimg = "<a class='navbar-brand' href='/home.php'><img src='/images/sbaofullb.png' style='height:20px;width:80px;''></a>";
}
else
{
    $navimg = NavImagePrint($hostlg,$_SESSION['style']);
}
$user = isset($_SESSION['user']) ? $_SESSION['user'] : '';
$isadmin = isset($_SESSION['isadmin']) ? $_SESSION['isadmin'] : '';
$days = 7 - date("N");
$days = ($days > 0) ? "<strong>(".$days." days remaining)</strong>" : "<strong>(LAST DAY)</strong>";

$system = new System();
$sbapreds = $system->is_open('predictions');
$sbdlpreds = $system->is_open('sbdl_predictions');
$bigthrees = $system->is_open('big_3');
$fives = $system->is_open('5x5');
$sba = ($sbapreds > 0) ? "<i class='fas fa-check'></i>" : "<i class='fas fa-times'></i>";
$sbdl = ($sbdlpreds > 0) ? "<i class='fas fa-check'></i>" : "<i class='fas fa-times'></i>";
$bigthree = ($bigthrees > 0) ? "<i class='fas fa-check'></i>" : "<i class='fas fa-times'></i>";
$fivexfive = ($fives > 0) ? "<i class='fas fa-check'></i>" : "<i class='fas fa-times'></i>";

// Host Check
if($hostlg == 'SBA')
{
    $nothostlg = 'EFL';
    $nothostecho = "<a href=\"https://efl.sbao.io/home.php\" class=\"btn btn-primary navbar-btn\">EFLO</a>";
}
else if($hostlg == 'EFL')
{
    $nothostlg = 'SBA';
    $nothostecho = "<a href=\"https://sbao.io/home.php\" class=\"btn btn-primary navbar-btn\">SBAO</a>";
}

// Check if user has any new notifications
$sql = "SELECT u.id FROM user_messages u INNER JOIN auth_user a ON u.a_fk=a.ID WHERE username=\"$user\" AND opened='0'";
$nummess = mysqli_num_rows(mysqli_query($con,$sql));
$messagestyle = '';
// adds unread notification badge if there are unread notifications
if($nummess > 0)
{
    $messagestyle = "
    <style>
        .fa-bell[data-count]:after{
      position:absolute;
      right:35%;
      top:20%;
      content: attr(data-count);
      font-size:30%;
      padding:.6em;
      border-radius:999px;
      line-height:.75em;
      color: white;
      background:rgba(255,0,0,.85);
      text-align:center;
      min-width:2em;
      font-weight:bold;
    }
    </style>";
}
if($hostlg == 'EFL')
{
    $sql = "SELECT id,name,league FROM teams ORDER BY name";  
}
else
{
    $sql = "SELECT id,name,league FROM teams WHERE league != 'NCAA' ORDER BY name";    
}
$result = mysqli_query($con,$sql);
$sbateam = '';
$sbdlteam = '';
$fibateam = '';
$eflteam = '';
$ncaateam = '';
$freeagent = '';
while($r = mysqli_fetch_array($result))
{
    if($r['league'] == 'SBA')
    {
        $sbateam .= "<li><a href='/forms/team_page.php?tid=".$r['id']."'>".$r['name']."</a></li>";
    }
    else if($r['league'] == 'SBDL')
    {
        $sbdlteam .= "<li><a href='/forms/team_page.php?tid=".$r['id']."'>".$r['name']."</a></li>";
    }
    else if($r['league'] == 'FIBA')
    {
        $fibateam .= "<li><a href='/forms/team_page.php?tid=".$r['id']."'>".$r['name']."</a></li>";
    }
    else if($r['league'] == 'EFL')
    {
        $eflteam .= "<li><a href='/forms/team_page.php?tid=".$r['id']."'>".$r['name']."</a></li>";
    }
    else if($r['league'] == 'ECFA')
    {
        $ncaateam .= "<li><a href='/forms/team_page.php?tid=".$r['id']."'>".$r['name']."</a></li>";
    }
    else
    {
        $freeagent .= "<li><a href='/forms/team_page.php?tid=".$r['id']."'>".$r['name']."</a></li>";
    }
}

?>
<style>
.dropdown-submenu {
  position: relative;
}

.dropdown-submenu .dropdown-menu {
  top: 0;
  left: 100%;
  margin-top: -1px;
}

</style>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>                        
            </button>
            <?=$navimg;?>
        </div>
        <div class="navbar-collapse collapse" id="navbar">
            <ul class="nav navbar-nav">
                <li class='dropdown'><a class='dropdown-toggle' data-toggle='dropdown' href='#' role="button">Player<span class='caret'></span></a>
                    <ul class='dropdown-menu'>
                        <li><a href='/forms/player_create.php'>Create-A-Player</a></li>
                        <li class="divider"></li>
                        <li><a href='/forms/view_players.php'>Player Management</a></li>
                        <!--<li><a>TPE Checklist</a></li>-->
                        <li class="divider"></li>
                        <?php if($hostlg == 'SBA') {echo "
                        <li><a href=\"/forms/manage_players.php?league=sba&active=1\">View Active SBA Players</a></li>
                        <li><a href=\"/forms/manage_players.php?league=sbdl&active=1\">View Active SBDL Players</a></li>";}
                        else if($hostlg == 'EFL') {echo "
                        <li><a href=\"/forms/manage_players.php?league=".strtolower($mainlg)."&active=1\">View Active ".$mainlg." Players</a></li>
                        <li><a href=\"/forms/manage_players.php?league=".strtolower($sublg)."&active=1\">View Active ".$sublg." Players</a></li>";}?>
                        <li><a href="/forms/manage_players.php?league=all">View All Players</a></li>  
                        <li class="divider"></li>
                        <!--<li><a href='/forms/build_stats.php'>Player Build Statistics</a></li>
                        <li><a href='/forms/tpe_ladders.php'>TPE Ladders</a></li>
                        <li class="divider"></li>-->
                        <li><a href="/forms/manage_players.php?new=1">New Players</a></li>
                        <li><a href="/forms/manage_players.php?freshman=1">Freshman Players</a></li>
                        <li><a href="/forms/manage_players.php?draft=1">Upcoming Draftees</a></li>
                        <li><a href="/forms/manage_players.php?rookie=1">Rookies</a></li>
                        <?php if($hostlg == 'SBA') {echo "
                        <li><a href=\"/forms/manage_players.php?fa=1\">Pending Free Agents</a></li>
                        <li><a href=\"/forms/manage_players.php?regress=1\">Regressing Players</a></li>
                        <li><a href=\"/forms/manage_players.php?regressed=1\">Regressed Players</a></li>
                        <li><a href=\"/forms/manage_players.php?retiring=1\">Retiring Players</a></li>";}?>
                    </ul>
                </li>
                <li class='dropdown'><a class='dropdown-toggle' data-toggle='dropdown' href='#'>Team<span class='caret'></span></a>
                    <ul class="dropdown-menu">
                        <?php if($hostlg == 'SBA') {echo "
                        <li class=\"dropdown-submenu\"><a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\" class=\"submenu\">SBA</a>
                            <ul class=\"dropdown-menu\">
                                ".$sbateam."
                            </ul>
                        </li>  
                        <li class=\"dropdown-submenu\"><a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\" class=\"submenu\">SBDL</a>
                            <ul class=\"dropdown-menu\">
                                ".$sbdlteam."
                            </ul>
                        </li>
                        <li class=\"dropdown-submenu\"><a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\" class=\"submenu\">FIBA</a>
                            <ul class=\"dropdown-menu\">
                                ".$fibateam."
                            </ul>
                        </li> 
                        <li><a href=\"/forms/team_page.php?tid=0&league=sba\">Free Agents - SBA</a></li>  
                        <li><a href=\"/forms/team_page.php?tid=0&league=sbdl\">Free Agents - SBDL</a></li>";} 
                        else if($hostlg == 'EFL') {echo "  
                        <li class=\"dropdown-submenu\"><a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\" class=\"submenu\">".$mainlg."</a>
                            <ul class=\"dropdown-menu\">
                                ".$eflteam."
                            </ul>
                        </li>  
                        <li class=\"dropdown-submenu\"><a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\" class=\"submenu\">".$sublg."</a>
                            <ul class=\"dropdown-menu\">
                                ".$ncaateam."
                            </ul>
                        </li>
                        <li><a href=\"/forms/team_page.php?tid=0&league=".strtolower($mainlg)."\">Free Agents - ".$mainlg."</a></li>  
                        <li><a href=\"/forms/team_page.php?tid=0&league=".strtolower($sublg)."\">Free Agents - ".$sublg."</a></li>";}?>
                        <li class="divider"></li>
                        <li><a href="/forms/coach_list.php">Coach List</a></li>
                        <?php if($hostlg == 'SBA') {echo "
                        <li><a href=\"/forms/team_salaries.php\">Team Salaries</a></li>
                        <li><a href=\"/forms/upcoming_salaries.php\">Upcoming Salaries</a></li>
                        <li class=\"divider\"></li>
                        <li><a href=\"/forms/pe_index.php\">PE Index</a></li>";}?>
                    </ul>
                </li>
                <li class='dropdown'><a class='dropdown-toggle' data-toggle='dropdown' href='#'>Manage<span class='caret'></span></a>
                    <ul class='dropdown-menu'>
                        <li><a href='/forms/update_dashboard.php'>Update Dashboard</a></li>
                        <li><a href='/forms/simmer_dashboard.php'>Simmer Dashboard</a></li>
                        <?php if($hostlg == 'SBA') {echo "
                        <li class=\"divider\"></li>
                        <li><a href='/forms/coach_update.php?league=".$mainlg."'>GM Office</a></li>
                        <li><a href='/forms/sbdl_strategies.php'>HC Strategies</a></li>
                        <li class=\"divider\"></li>
                        <li><a href='/forms/team_planner.php'>Team Planner</a></li>";}?>
                        <!--<li class=\"divider\"></li>
                        <li><a href='/forms/sba_sim_uploader.php'>Upload SBA Sim <span class='badge'>beta</span></a></li>-->
                    </ul>
                </li>
                <?php if($hostlg == 'SBA') {echo "
                <li class='dropdown'><a class='dropdown-toggle' data-toggle='dropdown' href='#'>Tools<span class='caret'></span></a>
                    <ul class='dropdown-menu'>
                        <li class=\"dropdown-header\">Calculators</li>
                        <li><a href='/calculator/regress.php'>Regression Calculator</a></li>
                        <li><a href='/calculator/tpeplanner.php'>TPE Planner</a></li>
                        <li class=\"divider\"></li>
                        <li class=\"dropdown-header\">Comparison</li>
                        <li><a href=\"/forms/player_comparison.php\">Player Comparison - Statistical</a></li>
                        <li><a href=\"/forms/player_comparison_ratings.php\">Player Comparison - Ratings</a></li>
                    </ul>
                </li>
                <li class=\"dropdown\"><a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\">Data<span class=\"caret\"></span></a>
                    <ul class=\"dropdown-menu multi-level\">
                        <li><a href=\"/forms/player_leaders.php\">Player Leaders</a></li>
                        <li><a href=\"/forms/player_statistics.php\">Player Statistics</a></li>
                        <li><a href=\"/forms/franchise_leaders.php\">Franchise Leaders</a></li>
                        <li><a href=\"/forms/past_champions.php\">Past Champions</a></li>
                        <li class=\"divider\"></li>
                        <li><a href=\"/forms/historical_index.php\">Historical Indexes</a></li>
                        <li><a href=\"/forms/league_leaders.php\">League Leaders by Season</a></li>
                        <li><a href=\"/forms/league_standings.php\">League Standings by Season</a></li>
                        <li><a href=\"/forms/award_winners_by_award.php\">Award Winners by Award</a></li>
                        <li><a href=\"/forms/award_winners.php\">Award Winners by Season</a></li>
                        <li class=\"divider\"></li>
                        <li><a href=\"/forms/prediction_data.php\">Prediction Data</a></li>
                        <li><a href=\"/forms/prediction_results.php\">Prediction Results</a></li>
                    </ul>
                </li>
                <li class='dropdown'><a class='dropdown-toggle' data-toggle='dropdown' href='#'>Games<span class='caret'></span></a>
                    <ul class='dropdown-menu'>
                        <li class=\"dropdown-header\">Fantasy</li>
                        <li><a href=\"/forms/games_big_three.php\">SBAO Big 3 ".$bigthree."</a></li>
                        <li><a href=\"/forms/games_big_three_results.php\">SBAO Big 3 Teams</a></li>
                        <li><a href=\"/forms/games_five_x_five.php\">SBAO 5x5 ".$fivexfive."</a></li>
                        <li><a href=\"/forms/games_five_x_five_results.php\">SBAO 5x5 Teams</a></li>
                        <li class=\"divider\"></li>
                        <li class=\"dropdown-header\">Predictions</li>
                        <li><a href=\"/forms/predictions.php\">".$mainlg." Predictions ".$sba."</a></li>
                        <li><a href=\"/forms/sbdl_predictions.php\">".$sublg." Predictions ".$sbdl."</a>
                    </ul>
                </li>";}?>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <?php if(isset($_SESSION['user']) AND $isadmin == '1') {echo "
                <li class='dropdown'><a class='dropdown-toggle' data-toggle='dropdown' href='#' onclick=\"checkMessages('".mysqli_real_escape_string($con,$user)."')\" style='height:50px;'><span class='fas fa-bell fa-2x ".(($nummess > 0) ? "has-badge" : "")."' data-count='".$nummess."'></span></a></li>
                <li class='dropdown'><a class='dropdown-toggle' data-toggle='dropdown' href='#'><span class=\"badge\">Lvl 1</span> ".$user."<span class='caret'></span></a>
                    <ul class='dropdown-menu'>
                        <li>My Persona</li>
                        <li class='divider'></li>
                        <li><a href='/settings.php'><span class='glyphicon glyphicon-cog'></span>Account Settings</a></li>
                        <li class='divider'></li>
                        <li><a href='/admin.php'><span class='glyphicon glyphicon-wrench'></span>Admin</a></li>
                        <li class='divider'></li>
                        <li><a href='/forms/award_winners_entry.php'><span class='glyphicon glyphicon-wrench'></span>Award Winners Entry</a></li>
                        <li><a href='/admin_settings.php'><span class='glyphicon glyphicon-wrench'></span>Permissions</a></li>
                        <li><a href='/forms/sbdl_predictions_process.php'><span class='glyphicon glyphicon-wrench'></span>Process SBDL Predictions</a></li>
                        <li><a href='/forms/predictions_process.php'><span class='glyphicon glyphicon-wrench'></span>Process Predictions</a></li>
                        <li class='divider'></li>
                        <li><a href='/logout.php'><span class='glyphicon glyphicon-off'></span> Logout</a></li>
                    </ul>
                </li>";} else if(isset($_SESSION['user'])) {echo "
                <li class='dropdown'><a class='dropdown-toggle' data-toggle='dropdown' href='#' onclick=\"checkMessages('".mysqli_real_escape_string($con,$user)."')\" style='height:50px;'><span class='fas fa-bell fa-2x ".(($nummess > 0) ? "has-badge" : "")."' data-count='".$nummess."'></span></a></li>
                <li class='dropdown'><a class='dropdown-toggle' data-toggle='dropdown' href='#'><span class=\"badge\">Lvl 1</span>".$user."<span class='caret'></span></a>
                    <ul class='dropdown-menu'>
                        <li>My Persona</li>
                        <li class='divider'></li>
                        <li><a href='/settings.php'><span class='glyphicon glyphicon-cog'></span>Account Settings</a></li>
                        <li class='divider'></li>
                        <li><a href='/logout.php'><span class='glyphicon glyphicon-off'></span> Logout</a></li>
                    </ul>
                </li>";} else { echo "
                <li><a href='/index.php'><span class='glyphicon glyphicon-log-in'></span> Login</a></li><i class='fas fa-envelope'></i>";} ?>
            </ul>

            <a href="https://<?=$forum_domain?>" class="btn btn-primary navbar-btn">Back to <?=$hostlg?></a>
            <?=$nothostecho?>
        </div>
    </div>
</nav>
<div class='modal fade' id='messages' role='dialog'>
    <div class='modal-dialog modal-lg'>
        <div class='modal-content'>
            <div class='modal-header'>
                <button type='button' class='close' data-dismiss='modal'>&times;</button>
                <h4 class='modal-title'>User Messages</h4>
            </div>
            <div class='modal-body'>
                <div class='panel panel-primary'>
                    <div class='panel-heading'>User Notifications</div>
                    <div class='panel-body'>
                        <div id="ajaxmessages">
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class='modal-footer'>
                <button type='button' class='btn btn-danger' onclick='lockAllMessages("<?=$user?>","1")'>Lock All</button>
                <button type='button' class='btn btn-danger' onclick='lockAllMessages("<?=$user?>","0")'>Unlock All</button>
                <button type='button' class='btn btn-danger' onclick='deleteAllMessages("<?=$user?>")'>Delete All</button>
                <button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button>
            </div>
        </div>
    </div>
</div>
<!--
<ul class="lightrope">
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
</ul>
<style>
body {
  background: #000;
}

.lightrope {
  text-align: center;
  white-space: nowrap;
  overflow: hidden;
  position: absolute;
  z-index: 1;
  margin: -35px 0 0 0;
  padding: 0;
  pointer-events: none;
  width: 100%;
}
.lightrope li {
  position: relative;
  -webkit-animation-fill-mode: both;
          animation-fill-mode: both;
  -webkit-animation-iteration-count: infinite;
          animation-iteration-count: infinite;
  list-style: none;
  margin: 0;
  padding: 0;
  display: block;
  width: 12px;
  height: 28px;
  border-radius: 50%;
  margin: 20px;
  display: inline-block;
  background: #00f7a5;
  box-shadow: 0px 4.6666666667px 24px 3px #00f7a5;
  -webkit-animation-name: flash-1;
          animation-name: flash-1;
  -webkit-animation-duration: 2s;
          animation-duration: 2s;
}
.lightrope li:nth-child(2n+1) {
  background: cyan;
  box-shadow: 0px 4.6666666667px 24px 3px rgba(0, 255, 255, 0.5);
  -webkit-animation-name: flash-2;
          animation-name: flash-2;
  -webkit-animation-duration: 0.4s;
          animation-duration: 0.4s;
}
.lightrope li:nth-child(4n+2) {
  background: #f70094;
  box-shadow: 0px 4.6666666667px 24px 3px #f70094;
  -webkit-animation-name: flash-3;
          animation-name: flash-3;
  -webkit-animation-duration: 1.1s;
          animation-duration: 1.1s;
}
.lightrope li:nth-child(odd) {
  -webkit-animation-duration: 1.8s;
          animation-duration: 1.8s;
}
.lightrope li:nth-child(3n+1) {
  -webkit-animation-duration: 1.4s;
          animation-duration: 1.4s;
}
.lightrope li:before {
  content: "";
  position: absolute;
  background: #222;
  width: 10px;
  height: 9.3333333333px;
  border-radius: 3px;
  top: -4.6666666667px;
  left: 1px;
}
.lightrope li:after {
  content: "";
  top: -14px;
  left: 9px;
  position: absolute;
  width: 52px;
  height: 18.6666666667px;
  border-bottom: solid #222 2px;
  border-radius: 50%;
}
.lightrope li:last-child:after {
  content: none;
}
.lightrope li:first-child {
  margin-left: -40px;
}

@-webkit-keyframes flash-1 {
  0%, 100% {
    background: #00f7a5;
    box-shadow: 0px 4.6666666667px 24px 3px #00f7a5;
  }
  50% {
    background: rgba(0, 247, 165, 0.4);
    box-shadow: 0px 4.6666666667px 24px 3px rgba(0, 247, 165, 0.2);
  }
}

@keyframes flash-1 {
  0%, 100% {
    background: #00f7a5;
    box-shadow: 0px 4.6666666667px 24px 3px #00f7a5;
  }
  50% {
    background: rgba(0, 247, 165, 0.4);
    box-shadow: 0px 4.6666666667px 24px 3px rgba(0, 247, 165, 0.2);
  }
}
@-webkit-keyframes flash-2 {
  0%, 100% {
    background: cyan;
    box-shadow: 0px 4.6666666667px 24px 3px cyan;
  }
  50% {
    background: rgba(0, 255, 255, 0.4);
    box-shadow: 0px 4.6666666667px 24px 3px rgba(0, 255, 255, 0.2);
  }
}
@keyframes flash-2 {
  0%, 100% {
    background: cyan;
    box-shadow: 0px 4.6666666667px 24px 3px cyan;
  }
  50% {
    background: rgba(0, 255, 255, 0.4);
    box-shadow: 0px 4.6666666667px 24px 3px rgba(0, 255, 255, 0.2);
  }
}
@-webkit-keyframes flash-3 {
  0%, 100% {
    background: #f70094;
    box-shadow: 0px 4.6666666667px 24px 3px #f70094;
  }
  50% {
    background: rgba(247, 0, 148, 0.4);
    box-shadow: 0px 4.6666666667px 24px 3px rgba(247, 0, 148, 0.2);
  }
}
@keyframes flash-3 {
  0%, 100% {
    background: #f70094;
    box-shadow: 0px 4.6666666667px 24px 3px #f70094;
  }
  50% {
    background: rgba(247, 0, 148, 0.4);
    box-shadow: 0px 4.6666666667px 24px 3px rgba(247, 0, 148, 0.2);
  }
}

</style>
-->
<?=$messagestyle?>
<script>
$(document).ready(function(){
    $('[data-toggle="popover"]').popover(); 
    $('.dropdown-submenu a.submenu').on("click", function(e){
    $(this).next('ul').toggle();
    e.stopPropagation();
    e.preventDefault();
  });
});
function checkMessages(user)
{
    $.ajax({url: "../ajax/get_messages.php?user="+user, success: function(result){
      $("#ajaxmessages").html(result);
    }});
    $('#messages').modal();
}
function deleteAllMessages(user)
{
    $.ajax({url: '../ajax/delete_all_messages.php?user='+user, success: function(result){
        $('#ajaxmessages').html(result);
        $('#messages').modal();
    }});
}
function lockAllMessages(user,lock)
{
    $.ajax({url: '../ajax/lock_all_messages.php?user='+user+'&lock='+lock, success: function(result){
        $('#ajaxmessages').html(result);
        $('#messages').modal();
    }});
}
</script>
<!--
<script>

    var snowStorm = (function(window, document) {

  // --- common properties ---

  this.autoStart = true;          // Whether the snow should start automatically or not.
  this.excludeMobile = false;      // Snow is likely to be bad news for mobile phones' CPUs (and batteries.) Enable at your own risk.
  this.flakesMax = 128;           // Limit total amount of snow made (falling + sticking)
  this.flakesMaxActive = 64;      // Limit amount of snow falling at once (less = lower CPU use)
  this.animationInterval = 50;    // Theoretical "miliseconds per frame" measurement. 20 = fast + smooth, but high CPU use. 50 = more conservative, but slower
  this.useGPU = true;             // Enable transform-based hardware acceleration, reduce CPU load.
  this.className = null;          // CSS class name for further customization on snow elements
  this.excludeMobile = false;      // Snow is likely to be bad news for mobile phones' CPUs (and batteries.) By default, be nice.
  this.flakeBottom = null;        // Integer for Y axis snow limit, 0 or null for "full-screen" snow effect
  this.followMouse = false;        // Snow movement can respond to the user's mouse
  this.snowColor = '#fff';        // Don't eat (or use?) yellow snow.
  this.snowCharacter = '&bull;';  // &bull; = bullet, &middot; is square on some systems etc.
  this.snowStick = true;          // Whether or not snow should "stick" at the bottom. When off, will never collect.
  this.targetElement = null;      // element which snow will be appended to (null = document.body) - can be an element ID eg. 'myDiv', or a DOM node reference
  this.useMeltEffect = true;      // When recycling fallen snow (or rarely, when falling), have it "melt" and fade out if browser supports it
  this.useTwinkleEffect = false;  // Allow snow to randomly "flicker" in and out of view while falling
  this.usePositionFixed = false;  // true = snow does not shift vertically when scrolling. May increase CPU load, disabled by default - if enabled, used only where supported
  this.usePixelPosition = false;  // Whether to use pixel values for snow top/left vs. percentages. Auto-enabled if body is position:relative or targetElement is specified.

  // --- less-used bits ---

  this.freezeOnBlur = false;       // Only snow when the window is in focus (foreground.) Saves CPU.
  this.flakeLeftOffset = 0;       // Left margin/gutter space on edge of container (eg. browser window.) Bump up these values if seeing horizontal scrollbars.
  this.flakeRightOffset = 0;      // Right margin/gutter space on edge of container
  this.flakeWidth = 8;            // Max pixel width reserved for snow element
  this.flakeHeight = 8;           // Max pixel height reserved for snow element
  this.vMaxX = 5;                 // Maximum X velocity range for snow
  this.vMaxY = 4;                 // Maximum Y velocity range for snow
  this.zIndex = 0;                // CSS stacking order applied to each snowflake

  // --- "No user-serviceable parts inside" past this point, yadda yadda ---

  var storm = this,
  features,
  // UA sniffing and backCompat rendering mode checks for fixed position, etc.
  isIE = navigator.userAgent.match(/msie/i),
  isIE6 = navigator.userAgent.match(/msie 6/i),
  isMobile = navigator.userAgent.match(/mobile|opera m(ob|in)/i),
  isBackCompatIE = (isIE && document.compatMode === 'BackCompat'),
  noFixed = (isBackCompatIE || isIE6),
  screenX = null, screenX2 = null, screenY = null, scrollY = null, docHeight = null, vRndX = null, vRndY = null,
  windOffset = 1,
  windMultiplier = 2,
  flakeTypes = 6,
  fixedForEverything = false,
  targetElementIsRelative = false,
  opacitySupported = (function(){
    try {
      document.createElement('div').style.opacity = '0.5';
    } catch(e) {
      return false;
    }
    return true;
  }()),
  didInit = false,
  docFrag = document.createDocumentFragment();

  features = (function() {

    var getAnimationFrame;

    /**
     * hat tip: paul irish
     * http://paulirish.com/2011/requestanimationframe-for-smart-animating/
     * https://gist.github.com/838785
     */

    function timeoutShim(callback) {
      window.setTimeout(callback, 1000/(storm.animationInterval || 20));
    }

    var _animationFrame = (window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        window.oRequestAnimationFrame ||
        window.msRequestAnimationFrame ||
        timeoutShim);

    // apply to window, avoid "illegal invocation" errors in Chrome
    getAnimationFrame = _animationFrame ? function() {
      return _animationFrame.apply(window, arguments);
    } : null;

    var testDiv;

    testDiv = document.createElement('div');

    function has(prop) {

      // test for feature support
      var result = testDiv.style[prop];
      return (result !== undefined ? prop : null);

    }

    // note local scope.
    var localFeatures = {

      transform: {
        ie:  has('-ms-transform'),
        moz: has('MozTransform'),
        opera: has('OTransform'),
        webkit: has('webkitTransform'),
        w3: has('transform'),
        prop: null // the normalized property value
      },

      getAnimationFrame: getAnimationFrame

    };

    localFeatures.transform.prop = (
      localFeatures.transform.w3 || 
      localFeatures.transform.moz ||
      localFeatures.transform.webkit ||
      localFeatures.transform.ie ||
      localFeatures.transform.opera
    );

    testDiv = null;

    return localFeatures;

  }());

  this.timer = null;
  this.flakes = [];
  this.disabled = false;
  this.active = false;
  this.meltFrameCount = 20;
  this.meltFrames = [];

  this.setXY = function(o, x, y) {

    if (!o) {
      return false;
    }

    if (storm.usePixelPosition || targetElementIsRelative) {

      o.style.left = (x - storm.flakeWidth) + 'px';
      o.style.top = (y - storm.flakeHeight) + 'px';

    } else if (noFixed) {

      o.style.right = (100-(x/screenX*100)) + '%';
      // avoid creating vertical scrollbars
      o.style.top = (Math.min(y, docHeight-storm.flakeHeight)) + 'px';

    } else {

      if (!storm.flakeBottom) {

        // if not using a fixed bottom coordinate...
        o.style.right = (100-(x/screenX*100)) + '%';
        o.style.bottom = (100-(y/screenY*100)) + '%';

      } else {

        // absolute top.
        o.style.right = (100-(x/screenX*100)) + '%';
        o.style.top = (Math.min(y, docHeight-storm.flakeHeight)) + 'px';

      }

    }

  };

  this.events = (function() {

    var old = (!window.addEventListener && window.attachEvent), slice = Array.prototype.slice,
    evt = {
      add: (old?'attachEvent':'addEventListener'),
      remove: (old?'detachEvent':'removeEventListener')
    };

    function getArgs(oArgs) {
      var args = slice.call(oArgs), len = args.length;
      if (old) {
        args[1] = 'on' + args[1]; // prefix
        if (len > 3) {
          args.pop(); // no capture
        }
      } else if (len === 3) {
        args.push(false);
      }
      return args;
    }

    function apply(args, sType) {
      var element = args.shift(),
          method = [evt[sType]];
      if (old) {
        element[method](args[0], args[1]);
      } else {
        element[method].apply(element, args);
      }
    }

    function addEvent() {
      apply(getArgs(arguments), 'add');
    }

    function removeEvent() {
      apply(getArgs(arguments), 'remove');
    }

    return {
      add: addEvent,
      remove: removeEvent
    };

  }());

  function rnd(n,min) {
    if (isNaN(min)) {
      min = 0;
    }
    return (Math.random()*n)+min;
  }

  function plusMinus(n) {
    return (parseInt(rnd(2),10)===1?n*-1:n);
  }

  this.randomizeWind = function() {
    var i;
    vRndX = plusMinus(rnd(storm.vMaxX,0.2));
    vRndY = rnd(storm.vMaxY,0.2);
    if (this.flakes) {
      for (i=0; i<this.flakes.length; i++) {
        if (this.flakes[i].active) {
          this.flakes[i].setVelocities();
        }
      }
    }
  };

  this.scrollHandler = function() {
    var i;
    // "attach" snowflakes to bottom of window if no absolute bottom value was given
    scrollY = (storm.flakeBottom ? 0 : parseInt(window.scrollY || document.documentElement.scrollTop || (noFixed ? document.body.scrollTop : 0), 10));
    if (isNaN(scrollY)) {
      scrollY = 0; // Netscape 6 scroll fix
    }
    if (!fixedForEverything && !storm.flakeBottom && storm.flakes) {
      for (i=0; i<storm.flakes.length; i++) {
        if (storm.flakes[i].active === 0) {
          storm.flakes[i].stick();
        }
      }
    }
  };

  this.resizeHandler = function() {
    if (window.innerWidth || window.innerHeight) {
      screenX = window.innerWidth - 16 - storm.flakeRightOffset;
      screenY = (storm.flakeBottom || window.innerHeight);
    } else {
      screenX = (document.documentElement.clientWidth || document.body.clientWidth || document.body.scrollWidth) - (!isIE ? 8 : 0) - storm.flakeRightOffset;
      screenY = storm.flakeBottom || document.documentElement.clientHeight || document.body.clientHeight || document.body.scrollHeight;
    }
    docHeight = document.body.offsetHeight;
    screenX2 = parseInt(screenX/2,10);
  };

  this.resizeHandlerAlt = function() {
    screenX = storm.targetElement.offsetWidth - storm.flakeRightOffset;
    screenY = storm.flakeBottom || storm.targetElement.offsetHeight;
    screenX2 = parseInt(screenX/2,10);
    docHeight = document.body.offsetHeight;
  };

  this.freeze = function() {
    // pause animation
    if (!storm.disabled) {
      storm.disabled = 1;
    } else {
      return false;
    }
    storm.timer = null;
  };

  this.resume = function() {
    if (storm.disabled) {
       storm.disabled = 0;
    } else {
      return false;
    }
    storm.timerInit();
  };

  this.toggleSnow = function() {
    if (!storm.flakes.length) {
      // first run
      storm.start();
    } else {
      storm.active = !storm.active;
      if (storm.active) {
        storm.show();
        storm.resume();
      } else {
        storm.stop();
        storm.freeze();
      }
    }
  };

  this.stop = function() {
    var i;
    this.freeze();
    for (i=0; i<this.flakes.length; i++) {
      this.flakes[i].o.style.display = 'none';
    }
    storm.events.remove(window,'scroll',storm.scrollHandler);
    storm.events.remove(window,'resize',storm.resizeHandler);
    if (storm.freezeOnBlur) {
      if (isIE) {
        storm.events.remove(document,'focusout',storm.freeze);
        storm.events.remove(document,'focusin',storm.resume);
      } else {
        storm.events.remove(window,'blur',storm.freeze);
        storm.events.remove(window,'focus',storm.resume);
      }
    }
  };

  this.show = function() {
    var i;
    for (i=0; i<this.flakes.length; i++) {
      this.flakes[i].o.style.display = 'block';
    }
  };

  this.SnowFlake = function(type,x,y) {
    var s = this;
    this.type = type;
    this.x = x||parseInt(rnd(screenX-20),10);
    this.y = (!isNaN(y)?y:-rnd(screenY)-12);
    this.vX = null;
    this.vY = null;
    this.vAmpTypes = [1,1.2,1.4,1.6,1.8]; // "amplification" for vX/vY (based on flake size/type)
    this.vAmp = this.vAmpTypes[this.type] || 1;
    this.melting = false;
    this.meltFrameCount = storm.meltFrameCount;
    this.meltFrames = storm.meltFrames;
    this.meltFrame = 0;
    this.twinkleFrame = 0;
    this.active = 1;
    this.fontSize = (10+(this.type/5)*10);
    this.o = document.createElement('div');
    this.o.innerHTML = storm.snowCharacter;
    if (storm.className) {
      this.o.setAttribute('class', storm.className);
    }
    this.o.style.color = storm.snowColor;
    this.o.style.position = (fixedForEverything?'fixed':'absolute');
    if (storm.useGPU && features.transform.prop) {
      // GPU-accelerated snow.
      this.o.style[features.transform.prop] = 'translate3d(0px, 0px, 0px)';
    }
    this.o.style.width = storm.flakeWidth+'px';
    this.o.style.height = storm.flakeHeight+'px';
    this.o.style.fontFamily = 'arial,verdana';
    this.o.style.cursor = 'default';
    this.o.style.overflow = 'hidden';
    this.o.style.fontWeight = 'normal';
    this.o.style.zIndex = storm.zIndex;
    docFrag.appendChild(this.o);

    this.refresh = function() {
      if (isNaN(s.x) || isNaN(s.y)) {
        // safety check
        return false;
      }
      storm.setXY(s.o, s.x, s.y);
    };

    this.stick = function() {
      if (noFixed || (storm.targetElement !== document.documentElement && storm.targetElement !== document.body)) {
        s.o.style.top = (screenY+scrollY-storm.flakeHeight)+'px';
      } else if (storm.flakeBottom) {
        s.o.style.top = storm.flakeBottom+'px';
      } else {
        s.o.style.display = 'none';
        s.o.style.bottom = '0%';
        s.o.style.position = 'fixed';
        s.o.style.display = 'block';
      }
    };

    this.vCheck = function() {
      if (s.vX>=0 && s.vX<0.2) {
        s.vX = 0.2;
      } else if (s.vX<0 && s.vX>-0.2) {
        s.vX = -0.2;
      }
      if (s.vY>=0 && s.vY<0.2) {
        s.vY = 0.2;
      }
    };

    this.move = function() {
      var vX = s.vX*windOffset, yDiff;
      s.x += vX;
      s.y += (s.vY*s.vAmp);
      if (s.x >= screenX || screenX-s.x < storm.flakeWidth) { // X-axis scroll check
        s.x = 0;
      } else if (vX < 0 && s.x-storm.flakeLeftOffset < -storm.flakeWidth) {
        s.x = screenX-storm.flakeWidth-1; // flakeWidth;
      }
      s.refresh();
      yDiff = screenY+scrollY-s.y+storm.flakeHeight;
      if (yDiff<storm.flakeHeight) {
        s.active = 0;
        if (storm.snowStick) {
          s.stick();
        } else {
          s.recycle();
        }
      } else {
        if (storm.useMeltEffect && s.active && s.type < 3 && !s.melting && Math.random()>0.998) {
          // ~1/1000 chance of melting mid-air, with each frame
          s.melting = true;
          s.melt();
          // only incrementally melt one frame
          // s.melting = false;
        }
        if (storm.useTwinkleEffect) {
          if (s.twinkleFrame < 0) {
            if (Math.random() > 0.97) {
              s.twinkleFrame = parseInt(Math.random() * 8, 10);
            }
          } else {
            s.twinkleFrame--;
            if (!opacitySupported) {
              s.o.style.visibility = (s.twinkleFrame && s.twinkleFrame % 2 === 0 ? 'hidden' : 'visible');
            } else {
              s.o.style.opacity = (s.twinkleFrame && s.twinkleFrame % 2 === 0 ? 0 : 1);
            }
          }
        }
      }
    };

    this.animate = function() {
      // main animation loop
      // move, check status, die etc.
      s.move();
    };

    this.setVelocities = function() {
      s.vX = vRndX+rnd(storm.vMaxX*0.12,0.1);
      s.vY = vRndY+rnd(storm.vMaxY*0.12,0.1);
    };

    this.setOpacity = function(o,opacity) {
      if (!opacitySupported) {
        return false;
      }
      o.style.opacity = opacity;
    };

    this.melt = function() {
      if (!storm.useMeltEffect || !s.melting) {
        s.recycle();
      } else {
        if (s.meltFrame < s.meltFrameCount) {
          s.setOpacity(s.o,s.meltFrames[s.meltFrame]);
          s.o.style.fontSize = s.fontSize-(s.fontSize*(s.meltFrame/s.meltFrameCount))+'px';
          s.o.style.lineHeight = storm.flakeHeight+2+(storm.flakeHeight*0.75*(s.meltFrame/s.meltFrameCount))+'px';
          s.meltFrame++;
        } else {
          s.recycle();
        }
      }
    };

    this.recycle = function() {
      s.o.style.display = 'none';
      s.o.style.position = (fixedForEverything?'fixed':'absolute');
      s.o.style.bottom = 'auto';
      s.setVelocities();
      s.vCheck();
      s.meltFrame = 0;
      s.melting = false;
      s.setOpacity(s.o,1);
      s.o.style.padding = '0px';
      s.o.style.margin = '0px';
      s.o.style.fontSize = s.fontSize+'px';
      s.o.style.lineHeight = (storm.flakeHeight+2)+'px';
      s.o.style.textAlign = 'center';
      s.o.style.verticalAlign = 'baseline';
      s.x = parseInt(rnd(screenX-storm.flakeWidth-20),10);
      s.y = parseInt(rnd(screenY)*-1,10)-storm.flakeHeight;
      s.refresh();
      s.o.style.display = 'block';
      s.active = 1;
    };

    this.recycle(); // set up x/y coords etc.
    this.refresh();

  };

  this.snow = function() {
    var active = 0, flake = null, i, j;
    for (i=0, j=storm.flakes.length; i<j; i++) {
      if (storm.flakes[i].active === 1) {
        storm.flakes[i].move();
        active++;
      }
      if (storm.flakes[i].melting) {
        storm.flakes[i].melt();
      }
    }
    if (active<storm.flakesMaxActive) {
      flake = storm.flakes[parseInt(rnd(storm.flakes.length),10)];
      if (flake.active === 0) {
        flake.melting = true;
      }
    }
    if (storm.timer) {
      features.getAnimationFrame(storm.snow);
    }
  };

  this.mouseMove = function(e) {
    if (!storm.followMouse) {
      return true;
    }
    var x = parseInt(e.clientX,10);
    if (x<screenX2) {
      windOffset = -windMultiplier+(x/screenX2*windMultiplier);
    } else {
      x -= screenX2;
      windOffset = (x/screenX2)*windMultiplier;
    }
  };

  this.createSnow = function(limit,allowInactive) {
    var i;
    for (i=0; i<limit; i++) {
      storm.flakes[storm.flakes.length] = new storm.SnowFlake(parseInt(rnd(flakeTypes),10));
      if (allowInactive || i>storm.flakesMaxActive) {
        storm.flakes[storm.flakes.length-1].active = -1;
      }
    }
    storm.targetElement.appendChild(docFrag);
  };

  this.timerInit = function() {
    storm.timer = true;
    storm.snow();
  };

  this.init = function() {
    var i;
    for (i=0; i<storm.meltFrameCount; i++) {
      storm.meltFrames.push(1-(i/storm.meltFrameCount));
    }
    storm.randomizeWind();
    storm.createSnow(storm.flakesMax); // create initial batch
    storm.events.add(window,'resize',storm.resizeHandler);
    storm.events.add(window,'scroll',storm.scrollHandler);
    if (storm.freezeOnBlur) {
      if (isIE) {
        storm.events.add(document,'focusout',storm.freeze);
        storm.events.add(document,'focusin',storm.resume);
      } else {
        storm.events.add(window,'blur',storm.freeze);
        storm.events.add(window,'focus',storm.resume);
      }
    }
    storm.resizeHandler();
    storm.scrollHandler();
    if (storm.followMouse) {
      storm.events.add(isIE?document:window,'mousemove',storm.mouseMove);
    }
    storm.animationInterval = Math.max(20,storm.animationInterval);
    storm.timerInit();
  };

  this.start = function(bFromOnLoad) {
    if (!didInit) {
      didInit = true;
    } else if (bFromOnLoad) {
      // already loaded and running
      return true;
    }
    if (typeof storm.targetElement === 'string') {
      var targetID = storm.targetElement;
      storm.targetElement = document.getElementById(targetID);
      if (!storm.targetElement) {
        throw new Error('Snowstorm: Unable to get targetElement "'+targetID+'"');
      }
    }
    if (!storm.targetElement) {
      storm.targetElement = (document.body || document.documentElement);
    }
    if (storm.targetElement !== document.documentElement && storm.targetElement !== document.body) {
      // re-map handler to get element instead of screen dimensions
      storm.resizeHandler = storm.resizeHandlerAlt;
      //and force-enable pixel positioning
      storm.usePixelPosition = true;
    }
    storm.resizeHandler(); // get bounding box elements
    storm.usePositionFixed = (storm.usePositionFixed && !noFixed && !storm.flakeBottom); // whether or not position:fixed is to be used
    if (window.getComputedStyle) {
      // attempt to determine if body or user-specified snow parent element is relatlively-positioned.
      try {
        targetElementIsRelative = (window.getComputedStyle(storm.targetElement, null).getPropertyValue('position') === 'relative');
      } catch(e) {
        // oh well
        targetElementIsRelative = false;
      }
    }
    fixedForEverything = storm.usePositionFixed;
    if (screenX && screenY && !storm.disabled) {
      storm.init();
      storm.active = true;
    }
  };

  function doDelayedStart() {
    window.setTimeout(function() {
      storm.start(true);
    }, 20);
    // event cleanup
    storm.events.remove(isIE?document:window,'mousemove',doDelayedStart);
  }

  function doStart() {
    if (!storm.excludeMobile || !isMobile) {
      doDelayedStart();
    }
    // event cleanup
    storm.events.remove(window, 'load', doStart);
  }

  // hooks for starting the snow
  if (storm.autoStart) {
    storm.events.add(window, 'load', doStart, false);
  }

  return this;

}(window, document));</script>
-->