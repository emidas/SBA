<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/sba_process.php';
$offseason = new Offseason();
//retirement
$sql = "SELECT id,league FROM players WHERE active = '3'";
$result = mysqli_query($con,$sql);
while($r = mysqli_fetch_array($result))
{
    $player = new Player($r['id']);
    $player->retire();
    if($r['league'] == 'SBA')
    {
        $player->get_carryover();
    }
}
//retire inactive freshman
$sql = "SELECT id FROM players WHERE active = '1' AND league = 'SBDL' AND tpe <= '30'";
$result = mysqli_query($con,$sql);
while($s = mysqli_fetch_array($result))
{
    $player = new Player($s['id']);
    $player->retire();
}
//sbdl bump
$sql = "SELECT id,tpe FROM players WHERE active = '1' AND league ='SBDL' AND (tpe < '199') AND (tpe >= '165')";
$result = mysqli_query($con,$sql);
while($t = mysqli_fetch_array($result))
{
    $id = $t['id'];
    $tpe = $t['tpe'];
    $bump = 199 - $tpe;
    $sql = "UPDATE players
    SET tpe=tpe+$bump,ape=ape+$bump,bank=bank+$bump
    WHERE id = '$id'";
    mysqli_query($con,$sql);
}
//draftees
$offseason->reset_SBDL_bank();
$offseason->graduation();

//year
$sql = "SELECT * FROM year WHERE isactive='1'";
$result = mysqli_query($con,$sql);
while ($row = mysqli_fetch_array($result))
{
    $year = $row['year'];
    $year_old = $year;
    $year = $year + 1;
    $sbay = $row['sba_season'];
    $sbay = $sbay + 1;
    $sbdly = $row['sbdl_season'];
    $sbdly = $sbdly + 1;
    $fibay = $row['fiba_season'];
    if($fibay == 0)
    {
        $sql = "SELECT fiba_season FROM year ORDER BY fiba_season DESC LIMIT 1";
        $res = mysqli_fetch_array(mysqli_query($con,$sql));
        $fibay = $res['fiba_season'];
        $fibay = $fibay + 1;
    }
    else
    {
        $fibay = 0;
    }
    $sql = "UPDATE year SET isactive = '0' WHERE year = '$year_old'";
    mysqli_query($con,$sql);
    $sql = "INSERT INTO year (year,isactive,sba_season,sbdl_season,ncaa_season,fiba_season) VALUES ('$year','1','$sbay','$sbdly','0','$fibay')";
    mysqli_query($con,$sql);
}
//experience
$offseason->increment_experience();

//set new year
$system = new System();
$year = $system->get_year();

//retire inactive rookies
$sql = "SELECT id FROM players WHERE (active = '1' || active = '2') AND league ='SBA' AND tpe <= '125'";
$result = mysqli_query($con,$sql);
while($r = mysqli_fetch_array($result))
{
    $id = $r['id'];
    $sql = "UPDATE players
    SET active = '0',team='0'
    WHERE id = '$id'";
    mysqli_query($con,$sql);
}

//retire fillers
$sql = "SELECT id,league,active FROM players WHERE experience > '8' AND active = '2'";
$result = mysqli_query($con,$sql);
while($r = mysqli_fetch_array($result))
{
    $id = $r['id'];
    $active = $r['active'];
    $sql = "UPDATE players
    SET active = '0',team='0',yrs_remain='0'
    WHERE id = '$id'";
    mysqli_query($con,$sql);
}

// regression
$sql = "SELECT id FROM players WHERE (experience > 8) AND (active > 0) AND (build = 'Standard' || build = 'Freak')";
$result = mysqli_query($con,$sql);
while($r = mysqli_fetch_array($result))
{
    $player = new Player($r['id']);
    $player->regression('offseason');
}

header("location:/home.php");
?>