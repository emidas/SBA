<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
$path = $_SERVER['DOCUMENT_ROOT'];
include __DIR__.'/../connection.php';
include __DIR__.'/../includes/sba_process.php';
/*
$sql = "SELECT id,team FROM players WHERE active > 0 AND league='SBDL' AND team != '0'";
$result = mysqli_query($con,$sql);
while($r = mysqli_fetch_array($result))
{
    $pid = $r['id'];
    $tid = $r['team'];
    $salary = 5000000;
    $years = 3;
    $system = new System();
    $year = $system->get_year();
    $sql = "INSERT INTO contracts (t_fk,p_fk,start,type,years,yrs_remain,salary,salary2,salary3,status) VALUES ('$tid','$pid','$year','Scholarship','$years','$years','$salary','$salary','$salary','3')";
    mysqli_query($con,$sql);
}*/

/*$sql = "INSERT INTO players (id,user_fk,plast,pfirst,position,num,height,weight,country,league)
SELECT unique_ID,user_ID,lastname,firstname,position,jersey,height,weight,hometown,'EFL'
FROM eflmanager.Players
WHERE user_ID != 0";*/

$stmt = $conn->prepare("SELECT unique_ID,user_ID,lastname,firstname,p.position,jersey,height,weight,experience,hometown,a.name as build,inEFL,team,peakTPE,TPE,(TPE-APE) as bank,att_str,att_agl,att_arm,att_spd,att_hnd,att_acc,att_rblk,att_pblk,att_int,att_tak,att_kd,att_ka
FROM eflmanager.Players p INNER JOIN eflmanager.Archetypes a ON p.archetype = a.archetype_ID
WHERE user_ID = 0 AND player_ID != 0 AND isAI ='0' AND hasretired='0' AND inEFL='1'");
$stmt->execute();
$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
foreach ($result as $row)
{
    if($row['inEFL'] == 1)
    {
        $league = "EFL";
    }
    else
    {
        $league = "ECFA";
    }
    $id = '1064';
    $stmt = $conn->prepare("SELECT unique_ID FROM eflmanager.Teams WHERE team_ID = :id AND isEFL = :is");
    $stmt->execute([':id' => $row['team'],':is' => $row['inEFL']]);
    $s = $stmt->fetch(PDO::FETCH_ASSOC);
    $team = $s['unique_ID'];
    if($team == '17' || $team == '18'){$team = '0';}
    $exp = $row['experience'];
    $exp = $exp + 1;
    $foot = 12;
    $fheight = intdiv($row['height'],$foot);
    $iheight = fmod($row['height'],$foot);
    $height = $fheight."'".$iheight;
    $build = $row['build'];
    if($build == 'Dual-Threat')
    {
        $c90 = 'acc';
        $c901 = 'spd';
        $c801 = 'int';
        $c802 = 'han';
    }
    else
    {
        $c802 = '';
        $c901 = '';
        if($build == 'Pocket Passer')
        {
            $c90 = 'acc';
            $c801 = 'spd';
        }
        else if($build == 'Field General')
        {
            $c90 = 'arm';
            $c801 = 'spd';
        }
        else if($build == 'Gunslinger')
        {
            $c90 = 'int';
            $c801 = 'spd';
        }
        else if($build == 'One-Cut' || $build == 'Deep Threat' || $build == 'Vertical Threat')
        {
            $c90 = 'han';
            $c801 = 'str';
        }
        else if($build == 'Workhorse' || $build == 'Running' || $build == 'Balanced' || $build == 'Run Support')
        {
            $c90 = 'spd';
            $c801 = 'han';
        }
        else if($build == 'Scat Back' || $build == 'Coverage')
        {
            $c90 = 'agi';
            $c801 = 'str';
        }
        else if($build == 'Blocking')
        {
            $c90 = 'han';
            $c801 = 'spd';
        }
        else if($build == 'HBack' || $build == 'Possession')
        {
            $c90 = 'spd';
            $c801 = 'str';
        }
        else if($build == 'Red Zone')
        {
            $c90 = 'spd';
            $c801 = 'agi';
        }
        else if($build == '3/4 Tackling' || $build == 'Zone Cover' || $build == 'Pass Rushing')
        {
            $c90 = 'spd';
            $c801 = 'int';
        }
        else if($build == 'Man Cover')
        {
            $c90 = 'han';
            $c801 = 'tkl';
        }
        else if($build == 'Kicker')
        {
            $c90 = '';
            $c801 = '';
        }
        else if($build == '')
        {
            $c90 = '';
            $c801 = '';
        }
    }



    $stmt = $conn->prepare("INSERT INTO efl_online.players (id,user_fk,plast,pfirst,position,build,num,height,weight,country,league,experience,team,tpe,ape,bank,str,agi,arm,`int`,acc,tkl,spd,han,rbk,pbk,kdi,kac,c90,c901,c801,c802,active) VALUES (:id,:user,:last,:first,:pos,:build,:num,:height,:weight,:country,:league,:experience,:team,:tpe,:ape,:bank,:str,:agi,:arm,:inti,:acc,:tkl,:spd,:han,:rbk,:pbk,:kdi,:kac,:c90,:c901,:c801,:c802,'2')");
    $stmt->execute([':id' => $row['unique_ID'],':user' => $id,':last' => $row['lastname'],':first' => $row['firstname'],':pos' => $row['position'],':build' => $row['build'],':num' => $row['jersey'],':height' => $height,':weight' => $row['weight'],':country' => $row['hometown'],':league' => $league,':experience' => $exp,':team' => $team,':tpe' => $row['peakTPE'],':ape' => $row['TPE'],':bank' => $row['bank'],':str' => $row['att_str'],':agi' => $row['att_agl'],':arm' => $row['att_arm'],':inti' => $row['att_int'],':acc' => $row['att_acc'],':tkl' => $row['att_tak'],':spd' => $row['att_spd'],':han' => $row['att_hnd'],':rbk' => $row['att_rblk'],':pbk' => $row['att_pblk'],':kdi' => $row['att_kd'],':kac' => $row['att_ka'],':c90' => $c90,':c901' => $c901,':c801' => $c801,':c802' => $c802]);
}

/*$stmt = $conn->prepare("SELECT unique_ID,name,isEFL FROM eflmanager.Teams");
$stmt->execute();
$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
foreach ($result as $row)
{
    if($row['isEFL'] == 1)
    {
        $league = "EFL";
    }
    else
    {
        $league = "ECFA";
    }
    $stmt = $conn->prepare("INSERT INTO efl_online.teams (id,t_fk,name,league) VALUES (:id,:id,:name,:league)");
    $stmt->execute([':id' => $row['unique_ID'], ':name' => $row['name'],':league' => $league]);
}*/

?>