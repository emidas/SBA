<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include __DIR__.'/../connection.php';
include __DIR__.'/../includes/sba_process.php';
if($hostlg == 'EFL')
{$forum = "efl_forums";}
else if($hostlg == 'SBA')
{
    $forum = 'molholt_sba';
}
$sql = "SELECT u.username
FROM auth_user u 
INNER JOIN $forum.core_members cm ON u.username = cm.name
WHERE (FROM_UNIXTIME(cm.last_activity) < date_sub(now(),interval 2 week)) || (FROM_UNIXTIME(cm.last_visit) < date_sub(now(),interval 3 week))";
$result = mysqli_query($con,$sql);
while($r = mysqli_fetch_array($result))
{
    $user = new User($r['username']);
    $user->set_active('0');
}
$sql = "SELECT u.username
FROM auth_user u 
INNER JOIN $forum.core_members cm ON u.username = cm.name
WHERE (FROM_UNIXTIME(cm.last_activity) >= date_sub(now(),interval 2 week)) && (FROM_UNIXTIME(cm.last_visit) >= date_sub(now(),interval 3 week))";
$result = mysqli_query($con,$sql);
while($r = mysqli_fetch_array($result))
{
    $user = new User($r['username']);
    $user->set_active('1');
}
?>