<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include __DIR__.'/../connection.php';
include __DIR__.'/../includes/sba_process.php';

$sql = "SELECT id,ape,experience,active,league FROM players WHERE active = '1' && ((league='SBA') || (league='SBDL' && tpe > 199))";
$result = mysqli_query($con,$sql);
while ($row = mysqli_fetch_array($result))
{
    $player = new Player($row['id']);
    if($row['league'] == 'SBDL')
    {
        $exp = 1;
    }
    else
    {
        $exp = $row['experience'] + 1;
    }
    if($exp > 8)
    {
        $ape = $player->regression('expected');
    }
    else
    {
        $ape = $row['ape'];
    }
    calculateSalary($con,$row['id'],$ape,$exp,$row['active'],'expected');
}
?>