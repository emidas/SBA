<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include __DIR__.'/../connection.php';
include __DIR__.'/../includes/sba_process.php';
include __DIR__.'/../includes/libraries/simple_html_dom.php';

mysqli_query($con, 'TRUNCATE TABLE standings;');


function sba_ingester ($array, $con) {

	if(is_array($array)) {
		foreach ($array as $row => $value) {
			$t_name = sanitize($con, $value['Team']);
			$t_wins = sanitize($con, $value['W']);
			$t_losses = sanitize($con, $value['L']);
			$t_gb = sanitize ($con, $value['GB']);
			$t_lten = sanitize($con, $value['L10']);
			$league = sanitize($con, $value['League']);

			$sql_ins = "INSERT INTO standings (t_name,t_w,t_l,t_gb,t_lten,league) VALUES ('$t_name', '$t_wins', '$t_losses', '$t_gb', '$t_lten', '$league')";
			$result = mysqli_query($con, $sql_ins);
		}
	}
}

function efl_ingester ($array, $con) {

	if(is_array($array)) {
		foreach ($array as $row => $value) {
			$t_name = sanitize($con, $value['Team']);
			$t_wins = sanitize($con, $value['W']);
			$t_losses = sanitize($con, $value['L']);
			$league = sanitize($con, $value['League']);
			$conf = sanitize($con, $value['Conf']);

			$sql_ins = "INSERT INTO eflo_standings (t_name,t_w,t_l,league,conf) VALUES ('$t_name', '$t_wins', '$t_losses', '$league', '$conf')";
			$result = mysqli_query($con, $sql_ins);
		}
	}
}

if($hostlg == 'SBA') 
{

	// Go to SBA playoff standings
	$html = file_get_html('https://sbahistory.com/sba/playoffstandings.htm');

	// Get every table row
	foreach ($html->find('tr') as $element) {

		// Find the column with the team names
		$td = $element->find('td, .main, a', 1);

		// Ignore empty team columns and get all the teams info
		if ($td != '') {
			$team['Team'] = $td->plaintext;
			$team['W'] = $element->find('td, .main', 1)->plaintext;
			$team['L'] = $element->find('td, .main', 2)->plaintext;
			$team['GB'] = $element->find('td, .main', 4)->plaintext;
			$team['L10'] = $element->find('td, .main', 13)->plaintext;
			$team['League'] = 'sba';
			$sba_standings[] = $team;
		}
	};

	// Remove the headers row
	\array_splice($sba_standings, 0, 1);


	// Do the same but for the SBDL playoffs now
	$html = file_get_html('https://sbahistory.com/sbdl/playoffstandings.htm');

	// Get every table row
	foreach ($html->find('tr') as $element) {

		// Find the column with the team names
		$td = $element->find('td, .main, a', 1);

		// Ignore empty team columns and get all the teams info
		if ($td != '') {
			$team['Team'] = $td->plaintext;
			$team['W'] = $element->find('td, .main', 1)->plaintext;
			$team['L'] = $element->find('td, .main', 2)->plaintext;
			$team['GB'] = $element->find('td, .main', 4)->plaintext;
			$team['L10'] = $element->find('td, .main', 13)->plaintext;
			$team['League'] = 'sbdl';
			$sbdl_standings[] = $team;
		}
	};

	// Remove the headers row
	\array_splice($sbdl_standings, 0, 1);

	sba_ingester($sba_standings, $con);
	sba_ingester($sbdl_standings, $con);
}

else if($hostlg == 'EFL')
{

	// Go to EFL standings
	$html = file_get_html('https://efl.network/index/efl/Standings.html');

	$i = 1;

	// Get every table row
	foreach ($html->find('.hilite') as $element) {

		// Find the column with the team names
		$td = $element->find('td, a', 1);

		// Get team's stats
		if ($td != 'League') {
			$team['Team'] = $td->plaintext;
			$team['W'] = $element->find('td', 1)->plaintext;
			$team['L'] = $element->find('td', 2)->plaintext;
			$team['League'] = 'efl';
			if ($i < 5) {
				$team['Conf'] = 'East';
			} 
			else {
				$team['Conf'] = 'West';
			}

			$efl_standings[] = $team;
			$i++;
		}
	};


	$x = 1;

	// Do the same but for the ECFA
	$html = file_get_html('https://efl.network/index/ECFA/Standings.html');

	// Get every table row
	foreach ($html->find('.hilite') as $element) {

		// Find the column with the team names
		$td = $element->find('td, a', 1);

		// Ignore empty team columns and get all the teams info
		if ($td != 'League') {
			$team['Team'] = $td->plaintext;
			$team['W'] = $element->find('td', 1)->plaintext;
			$team['L'] = $element->find('td', 2)->plaintext;
			$team['League'] = 'ecfa';
			if ($x < 7) {
				$team['Conf'] = 'Heroes';
			} 
			else {
				$team['Conf'] = 'Legends';
			}
			$ecfa_standings[] = $team;
			$x++;
		}
	};


	efl_ingester($efl_standings, $con);
	efl_ingester($ecfa_standings, $con);
}

?>
