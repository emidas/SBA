<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/sba_process.php';
$system = new System();
$year = $system->get_year();
$offseason = new Offseason();

//retire old inactives and regression casualties
$sql = "SELECT id,league,active FROM players WHERE experience > '8' AND ape < '250' AND active != '0'";
$result = mysqli_query($con,$sql);
while($r = mysqli_fetch_array($result))
{
    $player = new Player($r['id']);
    $player->retire();
    if($r['league'] == 'SBA')
    {
        $player->get_carryover();
    }
}

// Generate Player Minimum Salaries
$sql = "SELECT id,ape,experience,active FROM players WHERE league='SBA'";
$result = mysqli_query($con,$sql);
while ($row = mysqli_fetch_array($result))
{
    calculateSalary($con,$row['id'],$row['ape'],$row['experience'],$row['active'],'min');
    calculateSalary($con,$row['id'],$row['ape'],$row['experience'],$row['active'],'expected');
}

// contracts
$offseason->handle_retired_contracts();
$offseason->reset_salary();
$offseason->decrement_contracts();
$offseason->handle_outstanding_offers();
$offseason->handle_expired_contracts();
$offseason->update_contract_salaries();
//$offseason->update_salaries();

// salary needs years logic to determine sal/sal2/sal3
// if yrs_remain = 1 and opt != none, do option logic

// if yrs_remain > 0 and they have a salary, pay them
// pay options when signed, otherwise get paid when signing free agent deal

// options

$sql = "SELECT p_fk,salary,yrs_remain,opt FROM contracts WHERE status = '3' AND (type='Free Agent' || type='Extension' || type='Rookie')";
$result = mysqli_query($con,$sql);
while ($row = mysqli_fetch_array($result))
{
    if(($row['yrs_remain'] == 1) && ($row['opt'] != 'none'))
    {
        // Option
    }
    else
    {
        $pid = $row['p_fk'];
        $salary = $row['salary'];
        if($salary == 0)
        {
            $salary = 1000000;
        }
        $player = new Player($pid);
        $player->set_cash($salary);
        $player->add_earn_history($salary,'S'.$year.' Salary');
    }
}
// Salary Cap
$sql = "SELECT sum(min_salary) as total FROM players WHERE league = 'SBA' AND (active ='1')";
$t_sal = mysqli_fetch_array(mysqli_query($con,$sql));
$t_salary = $t_sal['total'];
$expected_cap = (round_up_to_nearest_n(($t_salary/20), 500000)) + 1500000;
$sql = "UPDATE salary_cap SET cap='$expected_cap'";
mysqli_query($con,$sql);

// Rollover Notification
$system->season_rollover_notification();
header("location:/home.php");
?>