<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
$path = $_SERVER['DOCUMENT_ROOT'];
include __DIR__.'/../connection.php';
include __DIR__.'/../includes/sba_process.php';

$system = new System();
$year = $system->get_year();
$y = $year + 1;

$stmt = $conn->prepare("SELECT id,name FROM teams t WHERE league = 'SBA'");
$stmt->execute();
$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
foreach ($result as $row)
{
    $x=1;
    $stmt = $conn->prepare("INSERT INTO team_draft_picks (t_fk,name,season,round,`order`,init_owner) VALUES (:tid,:name,:season,:round,'',:tid)");
    $stmt->execute([':tid' => $row['id'], ':name' => "S".$y." ".$row['name']." Round ".$x." Draft Pick", ':season' => $year, ':round' => $x]);
    $x++;
    $stmt = $conn->prepare("INSERT INTO team_draft_picks (t_fk,name,season,round,`order`,init_owner) VALUES (:tid,:name,:season,:round,'',:tid)");
    $stmt->execute([':tid' => $row['id'], ':name' => "S".$y." ".$row['name']." Round ".$x." Draft Pick", ':season' => $year, ':round' => $x]);
    $y++;
}
?>