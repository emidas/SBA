<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
$path = $_SERVER['DOCUMENT_ROOT'];
include __DIR__.'/../connection.php';
include __DIR__.'/../includes/sba_process.php';

$system = new System();
$year = $system->get_year();
$preyear = $year - 1;

process_predictions($con,'season',$preyear);
process_predictions($con,'sbdl_season',$preyear);
?>