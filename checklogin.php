<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/connection.php';
include $path.'/includes/head.php';
include $path.'/includes/sba_process.php';
if(empty($_SESSION['user'])){
header("location:/index.php");
}

// Grab POST user and password
$user=$_POST['user'];
$password=$_POST['password'];

if($user != 'emidas')
{
    $sql = "SELECT id FROM forms WHERE form='maintenance' AND isactive='1'";
    $numsult = mysqli_query($con,$sql);
    $num = mysqli_num_rows($numsult);
    if($num > 0)
    {
        header("location:maintenance.php");
        exit();
    }
}

// To protect MySQL injection
$user = sanitize_login($con,$user);
$password = sanitize_login($con,$password);
$stmt = $conn->prepare("SELECT * FROM auth_user WHERE username=:user");
$stmt->execute([':user' => $user]);
$r = $stmt->fetch(PDO::FETCH_ASSOC);
$count = $stmt->rowCount();

//If PHP >= 5.5
if (phpversion() >= '5.5')
{
    if($count==1)
    {
        $check = password_verify($password,$r['password']);
        if($check)
        {
            // Register SESSION, redirect to index with success alert
            $_SESSION['user'] = $r['username'];
            if(isset($_POST['remember']))
            {
                setcookie('user', $r['username'], time() + (86400 * 60), "/"); // 86400 = 1 day
            }
            $_SESSION['memberid'] = $r['memberid'];
            $_SESSION['isadmin'] = $r['isadmin'];
            $_SESSION['issimmer'] = $r['issimmer'];
            $_SESSION['isupdater'] = $r['isupdater'];
            $_SESSION['isbeta'] = $r['isbeta'];
            $_SESSION['style'] = $r['style'];
            if(isset($_POST['remember']))
            {
                header("location:home.php?alert=cookiesuccess");
            }
            else
            {
                header("location:home.php?alert=success");
            }
        }
        else
        {
            if($password == $r['password'])
            {
                // Register SESSION, redirect to index with success alert
                $_SESSION['user'] = $r['username'];
                $_SESSION['memberid'] = $r['memberid'];
                $_SESSION['isadmin'] = $r['isadmin'];
                $_SESSION['issimmer'] = $r['issimmer'];
                $_SESSION['isupdater'] = $r['isupdater'];
                $_SESSION['isbeta'] = $r['isbeta'];
                $_SESSION['style'] = $r['style'];
                ini_set('session.cookie_lifetime', 1209600);
                ini_set('session.gc_maxlifetime', 1209600);
                header("location:home.php?alert=success");
            }
            else if($password != $r['password'])
            {
                header("location:index.php?alert=wrongpass");
            }
        }
    }
    else {
    header("location:index.php?alert=wronguser");
    }
}
// If PHP < 5.5
else
{
    if($count==1)
    {
        if($password == $r['password'])
        {
            $_SESSION['user'] = $r['username'];
            $_SESSION['memberid'] = $r['memberid'];
            $_SESSION['isadmin'] = $r['isadmin'];
            $_SESSION['issimmer'] = $r['issimmer'];
            $_SESSION['isupdater'] = $r['isupdater'];
            $_SESSION['isbeta'] = $r['isbeta'];
            $_SESSION['style'] = $r['style'];
            ini_set('session.cookie_lifetime', (14400000));
            header("location:home.php?alert=success");
        }
        else if ($password != $r['password'])
        {
            header("location:index.php?alert=wrongpass");
        }
    }
    else {
    header("location:index.php?alert=wronguser");
    }
}
?>